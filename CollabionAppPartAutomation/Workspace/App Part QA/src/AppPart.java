import java.util.concurrent.TimeUnit;
import org.junit.*;

import java.util.Date;
import java.util.Random;
import java.util.Scanner;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.datatransfer.*;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.junit.Test;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.util.regex.Pattern;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import javax.imageio.ImageIO;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import com.lowagie.text.pdf.codec.Base64.InputStream;

import org.apache.log4j.Logger;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.hssf.usermodel.*;

public class AppPart {
	
  private WebDriver driver;
  WebDriver driver1;
  String baseURL2, nodeURL;
  private String baseUrl,usernam, userName, pwd, ListName, ViewName, ConfigFile, ChartTheme, ChartCat, ChartName;
  int NumFields;
  String fileName = "E:\\testdata.xls";
  String sheetName = "Sheet1";
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  private Workbook wb;
  private Sheet ws;
  
	
  
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();	// Launching Firefox
    
    driver.manage().window().maximize(); // Maximizing the browser window
    baseUrl = "https://collabion-cdc8ba69ab4311.sharepoint.com/ChartBuilder/Pages/Default.aspx?SPHostUrl=https%3A%2F%2Fcollabion.sharepoint.com&SPLanguage=en-US&SPClientTag=0&SPProductNumber=16.0.4705.1218&SPAppWebUrl=https%3A%2F%2Fcollabion-cdc8ba69ab4311.sharepoint.com%2FChartBuilder"; // Defining the URL
    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
  }
  
 
  public String getCell(int rowIndex, int columnIndex) {
	  Cell cell = null;
	  try {
	    cell = ws.getRow(rowIndex).getCell(columnIndex);
	  } catch (Exception e) {
	   System.err.println("The cell with row '" + rowIndex + "' and column '"
	     + columnIndex + "' doesn't exist in the sheet");
	  }
	  return new DataFormatter().formatCellValue(cell);
	 }
	
  
  @Test
  public void testApp() throws Exception {
	
		    wb = new HSSFWorkbook(new FileInputStream(new File(fileName)));
		    ws = wb.getSheet(sheetName);
		    int count = ws.getLastRowNum();
		    System.out.println(count);
	  
	int i = 1;
	int totalRows = count+1;
	while (i < totalRows) {
	driver.get(baseUrl);// + "/SitePages/DevHome.aspx");
   	 
	if (fileName.indexOf("xlsx") < 0) { //for .xls format
	    wb = new HSSFWorkbook(new FileInputStream(new File(fileName)));
	    ws = wb.getSheet(sheetName);
	   // int count = ws.getLastRowNum();
	//    System.out.println(count);
	   } else { //for .xlsx format
	    wb = new XSSFWorkbook(fileName);
	    ws = (XSSFSheet) wb.getSheet(sheetName);
	   }
	

	  	 
	   userName = getCell(i, 0);
	   pwd = getCell(i, 1);
	   ListName = getCell(i,2);
	   ViewName = getCell(i,3);
	   NumFields = Integer.parseInt(getCell(i,4));
	   ConfigFile = getCell(i,5);
	   System.out.println(ConfigFile);
	   ChartTheme = getCell(i,6);
	   ChartCat = getCell(i,7);
	   ChartName = getCell(i,8);
	   
	FileInputStream file = new FileInputStream("E:\\testdata.xls");
	//InputStream inp = new FileInputStream("workbook.xlsx");
	  
 Workbook wb = WorkbookFactory.create(file);
 Sheet sheet = wb.getSheetAt(0);
	   //XSSFWorkbook workbook = new XSSFWorkbook();
	   //XSSFSheet sheet = workbook.createSheet(ChartName);
//	  Integer cn = sheet.getFirstRowNum();
//	  Integer cn1 = sheet.getLastRowNum();
//	  System.out.println(cn1-cn);
	  

 
 
    driver.manage().timeouts().implicitlyWait(2500, TimeUnit.MILLISECONDS);
    if (driver.findElements(By.id("cred_userid_inputtext")).size() != 0) {
		
	driver.findElement(By.id("cred_userid_inputtext")).sendKeys(userName);
    driver.manage().timeouts().implicitlyWait(2500, TimeUnit.MILLISECONDS);
    driver.findElement(By.id("cred_password_inputtext")).clear();
    driver.findElement(By.id("cred_password_inputtext")).sendKeys(pwd);
    driver.manage().timeouts().implicitlyWait(2500, TimeUnit.MILLISECONDS);
    driver.findElement(By.id("cred_password_inputtext")).submit();
    }
    try{
   
    
    WebDriverWait waitList = new WebDriverWait(driver,60);
    waitList.until(ExpectedConditions.visibilityOfElementLocated(By.id("lists")));
   	   
    
  //  WebDriverWait waitList = new WebDriverWait(driver,60);
  //  driver.switchTo().frame(driver.findElement(By.xpath("/html/body/form/div[12]/div/div[2]/div[2]/div[3]/div[4]/div/div/table/tbody/tr/td/div/div/div/div[1]/div/div/div[2]/div[1]/iframe")));
  //  waitList.until(ExpectedConditions.visibilityOfElementLocated(By.id("lists")));
  
    
	   new Select(driver.findElement(By.id("lists"))).selectByVisibleText(ListName);
	   driver.manage().timeouts().implicitlyWait(6000, TimeUnit.MILLISECONDS);
	   WebElement lists =  driver.findElement(By.id("lists"));
	   String searchText = driver.findElements(By.cssSelector("option")).get(Integer.parseInt(lists.getAttribute("selectedIndex"))).getText(); 
	  
	   
	driver.manage().timeouts().implicitlyWait(3050, TimeUnit.MILLISECONDS);
    
    WebDriverWait waitViews = new WebDriverWait(driver,60);
    
    waitViews.until(ExpectedConditions.visibilityOfElementLocated(By.id("views")));
    
    new Select(driver.findElement(By.id("views"))).selectByVisibleText(ViewName);
    driver.manage().timeouts().implicitlyWait(6000, TimeUnit.MILLISECONDS);
	     		
         
		   Row row1 = sheet.getRow(i);

		   Cell cell2 = row1.createCell(9);
		   Cell cell1 = row1.createCell(10);
		   Cell cell3 = row1.createCell(12);
		   cell2.setCellValue("Pass");
		   cell1.setCellValue("");
		   DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		   Date dateobj = new Date();
		   cell3.setCellValue(df.format(dateobj));
    
    WebDriverWait waitButton = new WebDriverWait(driver,60);
    waitButton.until(ExpectedConditions.visibilityOfElementLocated(By.id("btnFetchFlds")));
    driver.findElement(By.id("btnFetchFlds")).click();
    WebDriverWait waitFields = new WebDriverWait(driver,60);
    waitFields.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//input[@name='fields'])[2]")));
    driver.findElement(By.name("fields")).click();
    
    int j = 2;
    while (j <= NumFields)
    {
    driver.findElement(By.xpath("(//input[@name='fields'])[" + j+"]")).click();
    j++;
    }
    WebDriverWait waitConfig = new WebDriverWait(driver,60);
    waitConfig.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtConfig")));
    driver.findElement(By.id("txtConfig")).clear();
    StringBuilder sb = new StringBuilder();
    FileReader fr=new FileReader(ConfigFile);
    BufferedReader br= new BufferedReader(fr);
    String x="";
    while ((x=br.readLine()) != null)
    {
    	sb.append(x);
    	sb.append("\n");
    }
    br.close();
    String myString = sb.toString();
    StringSelection stringSelection = new StringSelection(myString);
    Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
    clpbrd.setContents(stringSelection, null);
    driver.findElement(By.id("txtConfig")).sendKeys(Keys.LEFT_CONTROL,"v");
    driver.findElement(By.id("txtChart")).clear();
    driver.findElement(By.id("txtChart")).sendKeys(ChartName);
  //  new Select(driver.findElement(By.id("category"))).selectByVisibleText(ChartCat);
    new Select(driver.findElement(By.id("theme"))).selectByVisibleText(ChartTheme);
    
    driver.findElement(By.id("btnBuildChart")).click();
    WebDriverWait waitChart = new WebDriverWait(driver,30);
    waitChart.until(ExpectedConditions.visibilityOfElementLocated(By.id("chartobject-1")));
    driver.findElement(By.id("chartobject-1")).sendKeys(Keys.PAGE_DOWN);
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    WebDriverWait wait = new WebDriverWait(driver,30);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("chartobject-1")));
    WebElement ele = driver.findElement(By.id("chartobject-1"));
    File screen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);// Takes screenshot of current screen
    BufferedImage fullscreen = ImageIO.read(screen); //Writes full screen image to IO stream
    Point point = ele.getLocation(); // Gets X, Y co-ordinates of chart
    int eleWidth = ele.getSize().getWidth();// Gets chart width
    System.out.println(eleWidth);
    int eleHeight = ele.getSize().getHeight();// Gets chart height
    System.out.println(eleHeight);
    BufferedImage eleScreenshot= fullscreen.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight); // crops entire screenshot to filter out the chart
    ImageIO.write(eleScreenshot, "png", screen); // Writes the cropped image to the IO stream
    // Generating a random number to avoid screenshot name conflict
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(1000);
    // Saving the screenshot
    FileUtils.copyFile(screen, new File("E:\\App Part Screen Shots\\New\\screenshot_"+ randomInt+".png"));
    
  /*  baseURL2 = "http://www.collabion.com";
	nodeURL = "http://192.168.0.213:5555/wd/hub";
	DesiredCapabilities capability = DesiredCapabilities.firefox();
	capability.setBrowserName("firefox");
	capability.setPlatform(Platform.WINDOWS);
	driver1 = new RemoteWebDriver(new URL(nodeURL), capability);*/
    	
    driver.manage().timeouts().implicitlyWait(6000, TimeUnit.MILLISECONDS);
     i++;
	
  }catch(Exception e){
	   Row row = sheet.getRow(i);
	   //Cell iter = row.createCell(0);
	   Cell cell1 = row.createCell(9);
	   Cell cell2 = row.createCell(10);
	   Cell cell2_1 = row.createCell(11);
	   Cell cell3 = row.createCell(12);
	   cell1.setCellValue("Fail");
	   //System.err.println(e);
	  // e.printStackTrace();
	   Writer writer = new StringWriter();
	   PrintWriter printWriter = new PrintWriter(writer);
	   e.printStackTrace(printWriter);
	   String s = writer.toString();
	   String lineNum = "AppPart.java";
	   int index1 = s.indexOf(lineNum); 
	   int lineNumStart = index1 + 13;
	   int lineNumEnd = lineNumStart + 3;
	   String printNum;
	   printNum = s.substring(lineNumStart, lineNumEnd);
	   System.out.println(printNum);
	  // String err = e.getMessage();
	   cell2.setCellValue("Error at Line "+printNum);
	   cell2_1.setCellValue(s);
	   DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	   Date dateobj = new Date();
	   cell3.setCellValue(df.format(dateobj));
	   
	   //iter.setCellValue(i);
	 //  FileOutputStream out = new FileOutputStream(new File("E:\\Test_Result_Iteration.xls"));
	//   workbook.write(out);
	 //  out.close();
	   i++;
  }
       FileOutputStream out = new FileOutputStream(new File("E:\\testdata.xls"));
	  wb.write(out);
	  out.close();
		
  }
	
	
  }	
}
 /* @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }*/