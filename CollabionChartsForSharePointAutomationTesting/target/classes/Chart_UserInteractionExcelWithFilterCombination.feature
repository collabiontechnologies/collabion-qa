Feature: User Interaction Feature With SharePoint Webpart Filter Combination(Data Source Excel table) 


@Import 
@ExcelNew2.4Regression 
Scenario: CCSP Import Feature Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown of Export/Import 
	Then Click on Import from Dropdown 
	Then Select file for import 	
	
	#Then Click on Export Tab
	#Then Click on Export Button
	
@BulkExport 
@ExcelNew2.4Regression 
Scenario: CCSP Export Feature Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown of Export/Import 
	#Then Click on Import from Dropdown
	#Then Select file for import
	Then Click on Export Tab 
	Then Click on Export Button 
	
@DataSourceExcelListWithNetwork 
@ExcelNew2.4Regression 
Scenario: Excel DataSelection Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	And click on Yes button 
	
	Then select Network radio button 
	##Then Give Url of Excel
	Then Give Url of Network Excel 
	
	And Give User Name of Network 
	And Give Password of Network 
	And Give Password of File 
	
	
	Then click on Load Button 
	And click on ok button 
	
	Then choose network sheet name 
	Then click on connect button 
	#	    Then click on OK button
	#	    Then go to Select Fields
	#	    And ticked all Select Field Checkbox
	#	  # And uncheck First field Checkbox
	#	  # And uncheck fourth field Checkbox
	#	  # And uncheck fifth field Checkbox
	#	  # And uncheck Sixth field Checkbox
	#	    And click on Apply button
	#	    Then click on Finish button
	
	
@DataSourceExcelListWithCurrentSite 
@ExcelNew2.4Regression 
Scenario: Excel DataSelection Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	And click on Yes button 
	Then Give Url of Excel 
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name 
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	Then click on Finish button 
	
	
	
	  @WizardFilterDataDrivenTesting
	  Scenario Outline: To verify that wizard filter is working as per expectation
	    
	    Given Click on Sign In
	    Given Click on Chart Wizard Dropdown
	    Then Click on Filter Data 
	    Then  User Checks if any filter is applied already and deletes them
	    Then  User adds a new filter  and provides the <ColumnName1> and <OperatorValue1> and <Value1> and Uses <JoiningOperator> to join with <ColumnName2> and <OperatorValue2> and <Value2>
	    Then  User Clicks on The apply button.
	
	    Examples: 
	    | ColumnName1    | OperatorValue1 | Value1      | JoiningOperator | ColumnName2    | OperatorValue2 | Value2           |
	    | "Country Name" | "Equal"        | "China"     | "And"           | "Country Name" | "Equal"        | "Czech Republic" |
	    | "Country Name" | "Equal"        | "Argentina" | "Or"            | "Country Name" | "Equal"        | "Brazil"         |
	
	
	
@GroupByNone 
@ExcelNew2.4Regression 
Scenario: SeriesFunction Testing(GroupBy with None) 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then click on Group Data 
	Then click on clear grouping button 
	And click on Yes button 
	Then click on first checkbox 
	Then click on series function 'Revenue' dropdown 
	Then click on Series Function 'Sales' dropdown 
	#Then click combo dropdown item 
	Then Click Apply button 
	Then click on Finish button 
	
@SeriesFunction 
@ExcelNew2.4Regression 
Scenario: SeriesFunction Testing 
	Given Click on Sign In 
	#Then go to WepartAutomation testing page
	Given Click on Chart Wizard Dropdown 
	Then click on Group Data 
	Then click on clear grouping button 
	And click on Yes button 
	Then click on first checkbox 
	Then click on first dropdown 
	#Then click on Group By dropdown
	Then click on Series Function Dropdown 
	Then click combo dropdown item 
	#Then choose combo dropdown option
	Then Click Apply button 
	Then click on Finish button 
	
@WizardFirstDrilldownSetWithGroupByFields 
@ExcelNew2.4Regression 
Scenario: Verify 'User Interaction'  WizardFirstDrilldownSetWithGroupByFields 
	Given Click on Sign In 
	#Then Click on the Site Pages HyperLink 
	#Then Click on the DrillDownPage HyperLink 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select First DrillDown option 
	#Then Select Group by Fields
	Then Choose Sort By Dropdown 
	Given Click on Configuration 
	And Select Chart Type from dropdown 
	
	Then Give Chart Title 
	Then Give Chart Sub Title 
	Then Give X-Axis Title 
	Then Give Primary Y-axis Title 
	Then Rotate Y-axis Title 
	Then Filled Y-axis width 
	Then Click on Apply to all 
	
	#And click on Apply Yes button
	Then Click on ok button 
	Then Click on  Drilldown caption ok button 
	Then Enable Multi level DrillDown 
	And Enable to change chart type at view mode 
	
	Then Select First DrillDown Field 
	#Then click on 'Customize' button
	Then click on Wizard Drilldown Apply button 
	Then click on Finish button 
	
	#Then Click on ok button
	Then click on DrillDown Bar 
	Then Click on  Drilldown caption ok button 
	Given Click on Select Chart type icon 
	Then Click on  Drilldown caption cancel 
	
	
@DrillDownChartTypeChoosing 
@ExcelNew2.4Regression 
Scenario: Checking Chart Type in Every Drilldown Level 
	Given Click on Sign In 
	#Then click on DrillDown Bar
	Given Click on Select Chart type icon 
	
@SQLServerDatabaseMultiLevelDrillDownUI 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then Click on any bar in the chart 
	Then Click on a bar on the Second level DrillDown 
	
@MultiLevelDrillDownUI 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then Click on SharePointList First Level bar in the chart 
	Then Click on SharePointList Second Level bar in the chart 
	
@ExcelListMultiLevelDrillDownUI 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then Click on ExcelList First Level bar in the chart 
	Then Click on ExcelList Second Level bar in the chart 
	
@TextFilterDataDrivenTesting 
Scenario Outline: Verify Text Filter search with multiple data 
    Given Click on Sign In
	Then User provides <inputText> to the TextFilter 
	
	Examples: 
		| inputText   |
		| "China"     |
		| "Argentina" |
		| "Brazil"    |
		| "Indonesia" |
		
		@ChoiceFilterDataDrivenTesting 
		Scenario Outline: Verify Choice Filter search with multiple data 
			Then The user Selects the <input> for the Choice Filter 
			
			Examples: 
				| input       |
				| "China"     |
				| "Argentina" |
				| "Brazil"    |
				| "Indonesia" |
				
				
				@WizardDynamicFilterSecond 
				Scenario: Verify 'User Interaction' with Wizard Dynamic Filter Second 
					Given Click on Chart Wizard Dropdown 
					Given Click on Dynamic filter 
					#And click on Yes button
					Then Allow viewers to filter data dynamically 
					Then Allow filtering only on the fields and data shown on chart 
					Then click on Finish button 
				@DynamicFilterDataDrivenTesting 
				Scenario Outline: Dynamic Filter Verification 
				    Given Click on Sign In
					Then User selects the <column1> and <operator1> and  <value1> joins by <joperator> with <column2> and <operator2> and  <value2> 
					Examples: 
						| column1        | operator1 | value1      | joperator | column2        | operator2 | value2           |
						| "Country Name" | "Equal"   | "China"     | "And"     | "Country Name" | "Equal"   | "Czech Republic" |
						| "Country Name" | "Equal"   | "Argentina" | "Or"      | "Country Name" | "Equal"   | "Brazil"         |
						
						
						@CombinationOfText&DynamicFilter-UI 
						Scenario Outline: Verify Combination Of Text & DynamicFilter-UI 
							Then User selects the <column1> and <operator1> and  <value1> joins by <joperator> with <column2> and <operator2> and  <value2> 
							Then User provides <inputText> to the TextFilter 
							
							Examples: 
								| column1        | operator1 | value1      | joperator | column2        | operator2 | value2           | inputText   |
								| "Country Name" | "Equal"   | "China"     | "And"     | "Country Name" | "Equal"   | "Czech Republic" | "China"     |
								| "Country Name" | "Equal"   | "Argentina" | "Or"      | "Country Name" | "Equal"   | "Brazil"         | "Argentina" |
								| "Country Name" | "Equal"   | "Argentina" | "Or"      | "Country Name" | "Equal"   | "Brazil"         | "Brazil"    |
								
								@ChoiceFilter&DynamicFilterCombination 
								Scenario Outline: Verify ChoiceFilter & DynamicFilter Combination 
								#Dynamic Filter Use
									Then User selects the <column1> and <operator1> and  <value1> joins by <joperator> with <column2> and <operator2> and  <value2> 
									Then The user Selects the <input> for the Choice Filter 
									
									Examples: 
										| column1        | operator1 | value1      | joperator | column2        | operator2 | value2           | input       |
										| "Country Name" | "Equal"   | "China"     | "And"     | "Country Name" | "Equal"   | "Czech Republic" | "China"     |
										| "Country Name" | "Equal"   | "Argentina" | "Or"      | "Country Name" | "Equal"   | "Brazil"         | "Argentina" |
										| "Country Name" | "Equal"   | "Argentina" | "Or"      | "Country Name" | "Equal"   | "Brazil"         | "Brazil"    |
										
										@Hotspot 
										Scenario: Verify 'User Interaction' using Hotspot 
											Given Click on Chart Wizard Dropdown 
											Then Click on Drill Down 
											Then Select Second DrillDown option 
											Then click on Finish button 
											Then click on DrillDown Bar 
											
										@DrillDownNavigateToLink 
										Scenario: Verify 'User Interaction' with Link DrillDown 
											Given Click on Chart Wizard Dropdown 
											Then Click on Drill Down 
											Then Select Third DrillDown option 
											Then Give Url 
											Then Choose open url option 
											Then click on Finish button 
											Then click on DrillDown Bar 
											
										@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination 
										Scenario: Verify 'User Interaction' using QueryString Filter 
											Given Click on Chart Wizard Dropdown 
											Then Click on Drill Down 
											Then Select Fourth DrillDown option From Wizard 
											Then Give Link Url 
											Then Choose open url option 
											Then click on Finish button 
											Then click on DrillDown Bar 
											
										@NoDrillDown 
										Scenario: Verify 'User Interaction' with NoDrillDown 
											Given Click on Chart Wizard Dropdown 
											Then Click on Drill Down 
											Then Select Fifth DrillDown option 
											Then click on Finish button 
											
											###########--------prerequisites--------
											###########--------1)Clear tick 'Print Chart'---------------------
											###########--------2)Clear tick 'Exporting chart data as Excel Spreadsheet'----------------
											###########--------3)Clear tick 'Exporting charts as image or PDF'-------------
										@ExportSettings 
										Scenario: Verify 'User Interaction' Export Settings 
											Given Click on Chart Wizard Dropdown 
											Given Click on Export Settings 
											#Then Click on Revert Button.
											#And click on Yes button
											Then tick on Enable printing of chart 
											Then tick on Enable exporting chart data as Excel Spreadsheet 
											Then tick on Enable exporting charts as image or PDF 
											Then give export file name 
											#And click on Apply button
											Then click on Finish button 
											Then click on Export Chart Button 
											
										@Macro/DynamicTitle 
										Scenario: Verify 'User Interaction' using Dynamic Title 
											Given Click on Chart Wizard Dropdown 
											Then Click on Filter Data 
											Then click on first Dropdown 
											Then click on second Dropdown 
											Then send search data 
											Then click on Filter Data Apply button 
											Then Click on Captions 
											# And click on Yes button
											And Enter text in Chart Title Field 
											And Enter text in Chart Sub Title Field 
											Then click on Finish button 
											
											#And click on Yes button
										@DataSourceExcel 
										Scenario: Excel DataSelection Checking 
											Given Click on Chart Wizard Dropdown 
											Then format the CSV to Excel data source 
											Then Give Url of Excel 
											Then click on Load Button 
											And click on ok button 
											Then select Sheet name 
											Then click on connect button 
											Then click on OK button 
											Then go to Select Fields 
											And ticked all Select Field Checkbox 
											# And uncheck First field Checkbox
											# And uncheck fourth field Checkbox
											# And uncheck fifth field Checkbox
											# And uncheck Sixth field Checkbox
											And click on Apply button 
											
										@WizardFirstDrilldownSetWithGroupByDate 
										Scenario:
										Verify 'User Interaction' WizardFirstDrilldownSetWithGroupByDate 
											Given Click on Chart Wizard Dropdown 
											Then Click on Drill Down 
											Then Select First DrillDown option 
											Then Select Group by dates 
											#And Choose subset of Data dropdown
											#And Choose matched field for the subset of data
											#And Choose order of Data Storing
											#And Choose the way of showing chart Data
											Then Choose Sort By Dropdown 
											Given Click on Configuration 
											And Select Chart Type from dropdown 
											
											Then Give Chart Title 
											Then Give Chart Sub Title 
											#Then Give X-Axis Title
											#Then Give Primary Y-axis Title
											#Then Rotate Y-axis Title
											#Then Filled Y-axis width
											Then Click on Apply to all 
											Then Click on ok button 
											Then Enable Multi level DrillDown 
											And Enable to change chart type at view mode 
											
											Then Select First DrillDown Field 
											Then click on Finish button 
											Then click on DrillDown Bar 
											
											###########--------prerequisites--------
											###########--------1)Clear tick 'Allow viewers to filter data dynamically'
											###########--------2)Clear all filter Checkbox
										@WizardDynamicFilterFirst 
										Scenario: Verify 'User Interaction' with Wizard Dynamic Filter First 
											Given Click on Chart Wizard Dropdown 
											Given Click on Dynamic filter 
											#And click on Yes button
											Then Allow viewers to filter data dynamically 
											#Then Select fields on which user can apply filters
											Then Tick on Specific Filter 
											And click on Apply button 
											Then click on Finish button 
											
										@SharepointListFilter 
										Scenario: Verify 'User Interaction' With Sharepoint List Filter 
											Given go to the Sharepoint List Page 
											Then Click on the button to choose from options 
											Then Select the proper choice. 
											Then Click on the 'APPLY' button. 
											
										Scenario: Verify 'User Interaction' using Bar DrillDown 
											Given Click on Chart Wizard Dropdown 
											Then click on Finish button 
											Then click on DrillDown Bar 
