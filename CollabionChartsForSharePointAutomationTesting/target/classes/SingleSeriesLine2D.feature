Feature: Single Series Line2D Collabion Chart Configuration
         These includes test cases
         a)Chart Type b)Captions c)Series Customization 
         d)Series Customization e)Labels,values & tooltips
         f)Cosmetics g)Number Formatting h)Axis i)Legend
         j)Trend lines k)Other Settings
         


Background: Flow till Chart type
    Given Click on Chart Wizard Dropdown
   # Then I click on Select Fields
   # Then Click on Filter Data
   # Then click on Group Data 
   # Then click on Top N Records
    Then click on Chart Type
 
 @caseid43
 Scenario: Single Series LineTwoD Chart Type Selection
   Given Choose Single Series from Chart Category Dropdown
    Then Choose LineTwoD Chart
    
    Then Click on Captions
    And click on Yes button
    And Enter text in Chart Title Field 
    And Enter text in Chart Sub Title Field 
    And enter text in X-Axis Title Field
    And enter text in Primary Y-axis Title Field
    And Untick Rotate Y-Axis Title Checkbox
    
    Then Click Series Customization
    And click on Yes button
    Then Choose XAxis Label Column
    Then Choose XAxis Sort Column
    Then Choose XAxis Sort Order
    Then Click on Series
    Then Choose Series value Column
    
    Then Click on Labels,Values & Tool-tips
    And click on Yes button
    Then Choose Display Type
    Then choose nth Label
    Then choose Number of StaggerLines
    And tick on checkbox Rotate_Values_When_Displayed
    And tick on checkbox Place_Values_Inside_Data_Plot
  
    Then Click on Data Plot
    
    
    And tick on ShowShadow
  
    And Give Data Plot Color
    Then Give spline data series line color
    Then Give spline data series line thickness
    Then Give spline data series line Opaqueness
    Then click on Dashed line checkbox
    Then Give spline data series Dash length
    Then Give spline data series Dash Gap length
    
    
    
    
    Then click on Tool-Tips tab
    Then click Include Shadow
    And Give Tool-Tips Border Color
    And Give Tool-Tips Background Color
    
    Given Click on Anchors Tab page
    Then Click on Show Anchors
    Then Give Visual Configuration Sides
    Then Give Visual Configuration Radius
    Then Give Visual Configuration Opaqueness
    
    Then Give Border color  
   Then Give Border Thickness
    
    Then Give Background color
    Then Give Background Opaqueness

    
    
    
    Given Click on Cosmetics 
    And click on Yes button
    Then Give Border Data
   
    
   
    And Click on Canvas
    
    
    And Give Thickness
    And Give Second Opaqueness
    And Give Border Color
    
    And Click on Margin & Padding
    Then Give Left margin
    Then Give Right Margin
    Then Give Top Margin
    Then Give Bottom Margin
    
    Then Give Title Padding
    Then Give Y Axis Title Padding
    Then Give Label Padding
    Then Give X Axis Title Padding
    Then Give Y Axis Value Padding
    Then Give Value Padding
    
   Given Click on Fonts
   Then Give Customize Font Name
   Then Give Customize Font Size
   Then Give Customize Font Color
  
   Then Give side canvas Font Name
   Then Give side canvas Font Size
   Then Give side canvas Font Color
    
    Given Click on Zero Plane
    Then Tick on Show Zero pLane
    Then Give 2D chart color
    Then Give 2D chart Thickness
    Then Give 2D chart Opaqueness
    
    Given Click on Custom Branding
    Then Specify logo URL
    Then Give Link
    Then Give Position
    Then Give Branding Opaqueness
    Then Give Branding Scale
    
    
    Then Click on Number Formatting
    And click on Yes button
    Then Give Data in Prefix & Suffix Field

   
    Then click on Format numbers
    Then click on European numbers
    
  
    Then Give Scales to be used
    Then Give Round numbers
    Then Give Round axis
    Then Click on Force exact number
    Then Click on Force Y Axis decimal values 




    Given Click on Axis
    And click on Yes button
    Then Set adaptive lower limit for axis
    Then Set Force decimals on primary y-axis values
    Then Give Y-Axis minimum value,maximum value,set lower limit,primary y-axis value
   
  
    
    
   Then Click on Axis Grid Lines
    Then Give Number of grid lines
    Then Give Grid Line Thickness
    Then Choose Grid Line color
    Then Give Grid Line Opaqueness
    Then Show grid lines as dashed
    Then Give Dash length
    Then Give Dash gap
    
    Then Tick Show alternate color bands
    Then give alternate color
    Then give alternate Opaqueness
    
    Given Click on The Vertical Grid Lines
    Then Show Vertical Grid Lines
    Then Give Number of Grid Lines
    Then Give Grid Line thickness
    Then Give Grid Line color
    Then Give Grid Line opaqueness
    
   Then Show Grid Lines As Dashed
   Then Give Grid Lines Dash Length
   Then Give Grid lines Dash Gap
    
    Then Show alternate color band
    Then Give alternate color
    Then Give alternate color Opaqueness
    
   
    Given Click on Trend Lines
    And click on Yes button
    Then Give Start Value 
    Then Give End Value
    Then Give Display Text
    Then give Tool-tip Text
    
    Then Click on as filled color zone
    Then Click on over data plot
    Then Click on value on right side

    
    
    Given Click on Other Settings
    And click on Yes button
    Then Give Chart Loading Message
    Then Give Data Loading Message
    Then Give Data Processing Message
    Then Give No-data to display Message
    Then Give Pre-Rendering Message
    Then Give Error in Loading Message
    Then Give Error in Data Message
    Then click on Custom Attribute
    And click on Add Button
    Then Give Attribute Name
    
    
    
          
     Then Click on Connection Fields
     And Select Fields used in chart
     And Select All fields returned from data source