Feature: User Interaction Feature With SharePoint Webpart Filter Combination(Data Source SharePointList). 

@DataSourceSharepointlist 
@SharePointListRegression 
Scenario: SharepointList DataSelection Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then format the SQL to Sharepoint List data source 
	And click on Yes button
	#Then click on OK button
	
	Then click on connect button 
	Then click on OK button 
	Then choose data in Select List Dropdown 
	Then choose data in Select List View Dropdown 
	Then go to Select Fields 
	And click on Apply button 
	
@SeriesFunction(Sharepointlist) 
@SharePointListRegression 
Scenario: SeriesFunction Testing 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then click on Group Data 
	Then click on clear grouping button 
	And click on Yes button 
	Then click on first checkbox 
	Then click on first dropdown 
	#Then click on Group By dropdown
	Then click on Series Function Dropdown 
	Then click combo dropdown item 
	#Then choose combo dropdown option
	
	Then Click Apply button 
	Then click on Finish button 
	
	
	
	
	
	
	
	
@WizardFirstDrilldownSetWithGroupByFields(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction'  WizardFirstDrilldownSetWithGroupByFields 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select First DrillDown option 
	#Then Select Group by Fields
	
	Then Choose Sort By Dropdown 
	
	
	#Given Click on Configuration 
	#Then Give Chart Title 
	#Then Give Chart Sub Title 
	#Then Give X-Axis Title
	#Then Give Primary Y-axis Title
	#Then Rotate Y-axis Title
	#Then Filled Y-axis width
	#Then Click on Apply to all 
	#Then Click on ok button 
	
	Then Enable Multi level DrillDown 
	Then Select First DrillDown Field 
	Then click on Wizard Drilldown Apply button 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@SQLServerDatabaseMultiLevelDrillDownUI(Sharepointlist) 
Scenario: Verify Multi Level DrillDownUI 
#Then Click on the Site Pages HyperLink 
#Then Click on the DrillDownPage HyperLink 
	Then Click on any bar in the chart 
	Then Click on a bar on the Second level DrillDown 
	
@MultiLevelDrillDownUI(Sharepointlist) 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then Click on SharePointList First Level bar in the chart 
	Then Click on SharePointList Second Level bar in the chart 
	
@ExcelListMultiLevelDrillDownUI(Sharepointlist) 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then Click on ExcelList First Level bar in the chart 
	Then Click on ExcelList Second Level bar in the chart 
	
	
@TextFilterDataDrivenTesting(Sharepointlist)2010 
@SharePointListRegression 
Scenario: Verify Text Filter search with multiple data 
	Given Click on Sign In 
	Then First Give data in Text filter And Search 
	
	
@TextFilterDataDrivenTesting(Sharepointlist)2016 
Scenario: Verify Text Filter search with multiple data 
	Then First Give data in Text filter And Search 
	
	
@ChoiceFilterDataDrivenTesting(Sharepointlist)2010 
@SharePointListRegression 
Scenario: Verify Choice Filter search with multiple data 
	Given Click on Sign In 
	Then First Give data in Choice Filter And Search 
	
	
@ChoiceFilterDataDrivenTesting(Sharepointlist)2016 
Scenario: Verify Choice Filter search with multiple data 
	Then First Give data in Choice Filter And Search 
	
	
@WizardDynamicFilterSecond(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction' with Wizard Dynamic Filter Second 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Given Click on Dynamic filter 
	#And click on Yes button
	Then Allow viewers to filter data dynamically 
	Then Allow filtering only on the fields and data shown on chart 
	Then click on Finish button 
	
@DynamicFilterDataDrivenTesting(Sharepointlist) 
Scenario: Verify Wizard Dynamic Filter search with multiple data 
	Then First Give data in Dynamic filter From Excel 
	
	
	
	
	
@CombinationOfText&DynamicFilter-UI(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify Combination Of Text & DynamicFilter-UI 
	Given Click on Sign In 
	Given provide a dynamic Filter 
	Then Provide a value for the TextFilter. 
	
@ChoiceFilter&DynamicFilterCombination(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify ChoiceFilter & DynamicFilter Combination 
	Given Click on Sign In 
	#Dynamic Filter Use
	Given provide a dynamic Filter 
	Given Choose Choice Filter 
	
	
@Hotspot(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction' using Hotspot 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Second DrillDown option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
	
@DrillDownNavigateToLink(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction' with Link DrillDown 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Third DrillDown option 
	Then Give Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction' using QueryString Filter 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Fourth DrillDown option From Wizard 
	Then Give Link Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
@NoDrillDown(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction' with NoDrillDown 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Fifth DrillDown option 
	Then click on Finish button 
	
	###########--------prerequisites--------
	
	###########--------1)Clear tick 'Print Chart'---------------------
	###########--------2)Clear tick 'Exporting chart data as Excel Spreadsheet'----------------
	###########--------3)Clear tick 'Exporting charts as image or PDF'-------------
	
@ExportSettings(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction' Export Settings 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Given Click on Export Settings 
	#Then Click on Revert Button.
	#And click on Yes button
	Then tick on Enable printing of chart 
	Then tick on Enable exporting chart data as Excel Spreadsheet 
	Then tick on Enable exporting charts as image or PDF 
	Then give export file name 
	#And click on Apply button
	Then click on Finish button 
	Then click on Export Chart Button 
	
@Macro/DynamicTitle(Sharepointlist) 
@SharePointListRegression 
Scenario: Verify 'User Interaction' using Dynamic Title 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send search data 
	Then click on Filter Data Apply button 
	Then Click on Captions 
	# And click on Yes button
	And Enter text in Chart Title Field 
	And Enter text in Chart Sub Title Field 
	Then click on Finish button 
	#And click on Yes button
	
	
	
@DataSourceExcel(Sharepointlist) 
Scenario: Excel DataSelection Checking 

	Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	Then Give Url of Excel 
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name 
	
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	
	And ticked all Select Field Checkbox 
	# And uncheck First field Checkbox
	# And uncheck fourth field Checkbox
	# And uncheck fifth field Checkbox
	# And uncheck Sixth field Checkbox
	
	
	And click on Apply button 
	
	
@WizardFirstDrilldownSetWithGroupByDate(Sharepointlist) 
Scenario: Verify 'User Interaction' WizardFirstDrilldownSetWithGroupByDate 
	Given Click on Chart Wizard Dropdown 
	
	
	Then Click on Drill Down 
	Then Select First DrillDown option 
	Then Select Group by dates 
	
	
	#And Choose subset of Data dropdown
	#And Choose matched field for the subset of data
	#And Choose order of Data Storing
	#And Choose the way of showing chart Data 
	
	
	Then Choose Sort By Dropdown 
	
	
	Given Click on Configuration 
	Then Give Chart Title 
	Then Give Chart Sub Title 
	#Then Give X-Axis Title
	#Then Give Primary Y-axis Title
	#Then Rotate Y-axis Title
	#Then Filled Y-axis width
	Then Click on Apply to all 
	Then Click on ok button 
	
	Then Enable Multi level DrillDown 
	Then Select First DrillDown Field 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
	
	
	
Scenario: Verify Multi Level DrillDownUI 
	Then Click on SharepointListData Multi Level bars Upto last level 
	
	
@SharepointListDataDateFilterDataDrivenTesting 
Scenario: Verify Date Filter search with multiple data 
	Then First Give data in Date Filter And Search 
	
	
@SharepointListTextFilterDataDrivenTesting 
Scenario: Verify Text Filter search with multiple data 
	Then Give data in Text filter And Search 
	
@SQLServerDatabaseMultiLevelDrillDownUI 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on SQLServerDatabase Multi Level bars Upto last level 
	
  