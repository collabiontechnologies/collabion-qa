Feature: Collabion feature 
         Test cases for the 
         'SharePoint List'Datasource 
@caseid1
Scenario: Configure Source Page 
          Given Configuration of Web Part
          
          Given Click on Chart Wizard Dropdown
          Then choose SharePoint List Data Provider
          Then click on connect button
          Then click on OK button
          Then choose data in Select List Dropdown
          Then choose data in Select List View Dropdown
          Then go to Select Fields
          And click on Apply button
          Then click on Finish button
          
          Then save chart
          