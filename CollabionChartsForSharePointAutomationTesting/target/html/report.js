$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("2.4VRegression.feature");
formatter.feature({
  "line": 1,
  "name": "SharePoint 2016 CCSP 2.4 New Feature Testing",
  "description": "",
  "id": "sharepoint-2016-ccsp-2.4-new-feature-testing",
  "keyword": "Feature"
});
formatter.before({
  "duration": 5215192215,
  "status": "passed"
});
formatter.before({
  "duration": 130577,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 3,
      "value": "##@SP16Import"
    }
  ],
  "line": 5,
  "name": "CCSP Import Feature Checking",
  "description": "",
  "id": "sharepoint-2016-ccsp-2.4-new-feature-testing;ccsp-import-feature-checking",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@SP16CCSP2.4Regression"
    }
  ]
});
formatter.step({
  "comments": [
    {
      "line": 6,
      "value": "#Given Click on Sign In"
    }
  ],
  "line": 7,
  "name": "Click on SharePoint Page",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Click on SharePoint 2016 Chart Import/Export",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Click on Import from Dropdown",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Select file for import",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinition.Click_on_Sharepoint_Page()"
});
formatter.result({
  "duration": 3091335188,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2016",
      "offset": 20
    }
  ],
  "location": "StepDefinition.click_on_SharePoint_Chart_Import_Export(int)"
});
formatter.result({
  "duration": 5634854541,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.click_on_Import_from_Dropdown()"
});
formatter.result({
  "duration": 1933338085,
  "status": "passed"
});
formatter.match({
  "location": "StepDefinition.select_file_for_import()"
});
