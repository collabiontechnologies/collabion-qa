Feature: User Interaction Feature With SharePoint Webpart Filter Combination(Data Source SQLServerDatabase). 


@DataSourceOracleDatabase
Scenario: Oracle DataSelection Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown
	Then Select Oracle Database as DataSource
	And click on Yes button
	Then Give Oracle Server Name
	Then Give Oracle User Name
	Then Give Oracle User Password
	Then click on connect button



@DataSourceODBCDatabase
Scenario: ODBC DataSelection Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown
	Then Select ODBC Database as DataSource
	And click on Yes button 
	Then Give Connection String
	Then click on connect button
    And click on ok button
    Then Give Query
    Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	Then click on Finish button 

@DataSourceSQLServer 
@SQLRegression 
Scenario: Microsoft SQl server DataSelection Checking 
	Given Click on Sign In 
	
	#Given Click on Chart Wizard Dropdown 
	#Then format the Excel to Microsoft SQl server data source 
	
	#Then tick on Windows Authentication 
	#Then Give Data in Server Field 
	#Then Give Data in Database field 
	##Then tick on Windows Authentication 
	#Then Choose Windows Authentication from Authentication Dropdown
	#Then Choose SQL Authentication from Authentication Dropdown
	#Then Choose Domain Authentication from Authentication Dropdown
	
	#Then click on connect button 
	#Then click on OK button 
	#Then Select data table 
	#Then go to Select Fields 
	#And ticked all Select Field Checkbox 
	#And click on Apply button 
	Given Click on Chart Wizard Dropdown 
	Then format the Excel to Microsoft SQl server data source 
	Then Choose Domain Authentication from Authentication Dropdown 
	Then Give Data in Server Field 
	Then Give Data in Database field 
	Then Give Domain Name 
	Then Give User name 
	Then Give Password 
	Then click on connect button 
	Then click on OK button 
	Then Select data table 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	
@SeriesFunction(SQLServerDatabase) 
@SQLRegression 
Scenario: SeriesFunction Testing 
	Given Click on Sign In 
	
	Given Click on Chart Wizard Dropdown 
	Then click on Group Data 
	Then click on clear grouping button 
	And click on Yes button 
	Then click on first checkbox 
	Then click on first dropdown 
	Then click on Group By dropdown 
	Then click on Series Function Dropdown 
	#Then click combo dropdown item 
	#Then choose combo dropdown option
	
	Then Click Apply button 
	Then click on Finish button 
	
	
@WizardFirstDrilldownSetWithGroupByFields(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify 'User Interaction'  WizardFirstDrilldownSetWithGroupByFields 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select First DrillDown option 
	#Then Select Group by Fields
	
	Then Choose Sort By Dropdown 
	
	
	#Given Click on Configuration 
	#Then Give Chart Title 
	#Then Give Chart Sub Title 
	#Then Give X-Axis Title
	#Then Give Primary Y-axis Title
	#Then Rotate Y-axis Title
	#Then Filled Y-axis width
	#Then Click on Apply to all 
	#Then Click on ok button 
	
	Then Enable Multi level DrillDown 
	Then Select First DrillDown Field 
	Then click on Wizard Drilldown Apply button 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@SQLServerDatabaseMultiLevelDrillDownUI(SQLServerDatabase) 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then click on DrillDown Bar 
	#Then Click on any bar in the chart 
	Then Click on a bar on the Second level DrillDown 
	
@MultiLevelDrillDownUI(SQLServerDatabase) 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then Click on SharePointList First Level bar in the chart 
	Then Click on SharePointList Second Level bar in the chart 
	
@ExcelListMultiLevelDrillDownUI(SQLServerDatabase) 
Scenario: Verify Multi Level DrillDownUI 
	Then Click on the Site Pages HyperLink 
	Then Click on the DrillDownPage HyperLink 
	Then Click on ExcelList First Level bar in the chart 
	Then Click on ExcelList Second Level bar in the chart 
	
	
@TextFilterDataDrivenTesting(SQLServerDatabase)2010 
@SQLRegression 
Scenario: Verify Text Filter search with multiple data 
	Given Click on Sign In 
	Then First Give data in Text filter And Search 
	
	
@TextFilterDataDrivenTesting(SQLServerDatabase)2016 
Scenario: Verify Text Filter search with multiple data 
	Then First Give data in Text filter And Search 
	
@ChoiceFilterDataDrivenTesting(SQLServerDatabase)2010 
@SQLRegression 
Scenario: Verify Choice Filter search with multiple data 
	Given Click on Sign In 
	Then First Give data in Choice Filter And Search 
	
@ChoiceFilterDataDrivenTesting(SQLServerDatabase)2016 
Scenario: Verify Choice Filter search with multiple data 
	Then First Give data in Choice Filter And Search 
	
@WizardDynamicFilterSecond(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify User Interaction with Wizard Dynamic Filter Second 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Given Click on Dynamic filter 
	#And click on Yes button
	Then Allow viewers to filter data dynamically 
	Then Allow filtering only on the fields and data shown on chart 
	Then click on Finish button 
	
@DynamicFilterDataDrivenTesting(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify Wizard Dynamic Filter search with multiple data 
	Given Click on Sign In 
	Then First Give data in Dynamic filter From Excel 
	
	
	
	
	
@CombinationOfText&DynamicFilter-UI(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify Combination Of Text & DynamicFilter-UI 
	Given Click on Sign In 
	Given provide a dynamic Filter SQL 
	Then Provide a value for the TextFilter. 
	
@ChoiceFilter&DynamicFilterCombination(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify ChoiceFilter & DynamicFilter Combination 
	Given Click on Sign In 
	#Dynamic Filter Use
	Given provide a dynamic Filter SQL 
	Given Choose Choice Filter 
	
	
@Hotspot(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify 'User Interaction' using Hotspot 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Second DrillDown option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
	
@DrillDownNavigateToLink(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify 'User Interaction' with Link DrillDown 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Third DrillDown option 
	Then Give Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify 'User Interaction' using QueryString Filter 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Fourth DrillDown option From Wizard 
	Then Give Link Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
@NoDrillDown(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify 'User Interaction' with NoDrillDown 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Drill Down 
	Then Select Fifth DrillDown option 
	Then click on Finish button 
	
	###########--------prerequisites--------
	
	###########--------1)Clear tick 'Print Chart'---------------------
	###########--------2)Clear tick 'Exporting chart data as Excel Spreadsheet'----------------
	###########--------3)Clear tick 'Exporting charts as image or PDF'-------------
	
@ExportSettings(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify 'User Interaction' Export Settings 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Given Click on Export Settings 
	#Then Click on Revert Button.
	#And click on Yes button
	Then tick on Enable printing of chart 
	Then tick on Enable exporting chart data as Excel Spreadsheet 
	Then tick on Enable exporting charts as image or PDF 
	Then give export file name 
	#And click on Apply button
	Then click on Finish button 
	Then click on Export Chart Button 
	
@Macro/DynamicTitle(SQLServerDatabase) 
@SQLRegression 
Scenario: Verify 'User Interaction' using Dynamic Title 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send search data 
	Then click on Filter Data Apply button 
	Then Click on Captions 
	# And click on Yes button
	And Enter text in Chart Title Field 
	And Enter text in Chart Sub Title Field 
	Then click on Finish button 
	#And click on Yes button
	
	
	
@DataSourceExcel(SQLServerDatabase) 
Scenario: Excel DataSelection Checking 

	Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	Then Give Url of Excel 
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name 
	
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	
	And ticked all Select Field Checkbox 
	# And uncheck First field Checkbox
	# And uncheck fourth field Checkbox
	# And uncheck fifth field Checkbox
	# And uncheck Sixth field Checkbox
	
	
	And click on Apply button 
	
	
@WizardFirstDrilldownSetWithGroupByDate(SQLServerDatabase)c 
Scenario: Verify 'User Interaction' WizardFirstDrilldownSetWithGroupByDate 
	Given Click on Chart Wizard Dropdown 
	
	
	Then Click on Drill Down 
	Then Select First DrillDown option 
	Then Select Group by dates 
	
	
	#And Choose subset of Data dropdown
	#And Choose matched field for the subset of data
	#And Choose order of Data Storing
	#And Choose the way of showing chart Data 
	
	
	Then Choose Sort By Dropdown 
	
	
	Given Click on Configuration 
	Then Give Chart Title 
	Then Give Chart Sub Title 
	#Then Give X-Axis Title
	#Then Give Primary Y-axis Title
	#Then Rotate Y-axis Title
	#Then Filled Y-axis width
	Then Click on Apply to all 
	Then Click on ok button 
	
	Then Enable Multi level DrillDown 
	Then Select First DrillDown Field 
	Then click on Finish button 
	Then click on DrillDown Bar 
