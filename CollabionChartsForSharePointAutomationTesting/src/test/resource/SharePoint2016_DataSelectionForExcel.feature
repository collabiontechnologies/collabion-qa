Feature: Excel DataSelection And Some Negative Test Cases depending on Datasource 

@ExcelDataSelectionRegression(SharePoint2016) 
@SP2016ExcelNT
Scenario: Excel DataSelection Regression Checking 

    #Given Click on SharePoint SignIn
    #Then Click on SharePoint Page
    Then Click on SharePoint 2016 Chart Edit
	#Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	 And click on Yes button
	Then Give New Url of Excel
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name 
	
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	
	And ticked all Select Field Checkbox 
	And uncheck First field Checkbox 
	And uncheck fourth field Checkbox 
	And uncheck fifth field Checkbox 
	And uncheck Sixth field Checkbox 
	
	
	And click on Apply button 
	
	Then click on Top N Records 
	#And click on No button
	Then click on Show top first Dropdown 
	Then click on Show top Second Dropdown 
	Then click Apply Button 
	
	Then Click on Filter Data 
	Then Click on Delete Button 
	Then Click on Add Button 
	#Then includes all dates
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send search data 
	Then click on Filter Data Apply button 
	
	Then click on Group Data 
	#And click on Yes button
	Then click on clear grouping button 
	Then click on first checkbox 
	Then click on first dropdown 
	Then click on radiobutton 'distinct values from field' 
	Then Choose parallel dropdown 
	Then Choose Series Display first dropdown 
	Then Choose Series Display second dropdown 
	Then Click Apply button 
	#Then Compare XML
	########Then click on Source XML
	#######Then Copy the XML
	Then click on Finish button 
	#Then Compare XML 
	
	Then Click on SharePoint 2016 Chart Edit
	Then Click on Filter Data 
	And click on Clear All Filters 
	
	Then click on Group Data 
	And click on clear grouping button 
	
	Then click on Top N Records 
	And click on clear Top N Records 
	Then click on Finish button 
	
	
	
@ExcelDataNegativeTestCase(SharePoint2016) 
@SP2016ExcelNT
Scenario: Excel Data Negative Test Case1 
    #Given Click on SharePoint SignIn
    #Then Click on SharePoint Page
    Then Click on SharePoint 2016 Chart Edit
	#Given Click on Chart Wizard Dropdown 
	Then go to Select Fields 
	Then untick all the checkbox 
	Then Click Select Field Apply button 
	
@ExcelDataNegativeTestCase(SharePoint2016)
@SP2016ExcelNT
Scenario: Excel Data Negative Test Case2
    #Given Click on SharePoint SignIn
    #Then Click on SharePoint Page
    Then Click on SharePoint 2016 Chart Edit 
	#Given Click on Chart Wizard Dropdown 
	Then Give Wrong Sheet Range 
	Then click on connect button 
	Then click on OK button 
	Then Click View Data button 
	And click on ok button 
	Then Verify Sheet range 


@ExcelDataNegativeTestCase(SharePoint2016) 
@SP2016ExcelNT
@Verify
Scenario: Excel Data Negative Test Case3 
    #Given Click on SharePoint SignIn
    #Then Click on SharePoint Page
    Then Click on SharePoint 2016 Chart Edit
	#Given Click on Chart Wizard Dropdown 
	Then Give Wrong URL 
	Then click on Load Button
	Then verify URL 
	