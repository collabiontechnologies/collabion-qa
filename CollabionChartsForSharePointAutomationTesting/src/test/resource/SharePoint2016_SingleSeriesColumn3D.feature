Feature: Single Series Column3D Collabion Chart Configuration
         These includes test cases
         a)Chart Type b)Captions c)Series Customization 
         d)Series Customization e)Labels,values & tooltips
         f)Cosmetics g)Number Formatting h)Axis i)Legend
         j)Trend lines k)Other Settings
         



 
 @SingleSeriesColumn3D(SharePoint2016)
 Scenario: Single Series ColumnThreeD Chart Type Selection
    
    #Given Configuration of Web Part
    #Given Click on Chart Wizard Dropdown
    Then Click on SharePoint 2016 Chart Edit
    Then choose Excel Data Provider
    Then Give New Url of Excel
    Then click on Load Button
    And click on ok button
    Then select Sheet name
     
    Then click on connect button
    Then click on OK button
    Then go to Select Fields  
    
    And ticked all Select Field Checkbox
    And uncheck First field Checkbox
    And uncheck fourth field Checkbox
    And uncheck fifth field Checkbox
    And uncheck Sixth field Checkbox
   
    
    And click on Apply button
    Then click on Chart Type
   Given Choose Single Series from Chart Category Dropdown
   Then Choose ColumnThreeD Chart
    
    Then Click on Chart Type Apply Button
    Then Click on Captions
    #And click on Yes button
    
    And Enter text in Chart Title Field 
    And Enter text in Chart Sub Title Field 
    And enter text in X-Axis Title Field
    And enter text in Primary Y-axis Title Field
    And Untick Rotate Y-Axis Title Checkbox
    
     Then Click Captions Apply Button
    Then Click Series Customization
    Then Choose XAxis Label Column
    Then Choose XAxis Sort Column
    Then Choose XAxis Sort Order
    Then Click on Series
    Then Choose Series value Column
    
    Then Click on Labels,Values & Tool-tips
    And click on Yes button
    Then Choose Display Type
    Then choose nth Label
    Then choose Number of StaggerLines
    And tick on checkbox Rotate_Values_When_Displayed
    And tick on checkbox Place_Values_Inside_Data_Plot
    Then Click on Data Plot
   # Then tick on checkbox Use_Round_Edges
   # Then Select Plot Fill Angle
    Then Select Fill Opaqueness
   # And tick on ShowShadow
   # And Choose Gradient Color
    And Give Data Plot Color
    
   

    And select on Show Plot Border
    #And Give Border Thickness
    And Plot Border Opaqueness
    #Then tick on Show_Dashed_Plot_Border
    #Then Give Dash Length
    #Then Give Dash Gap Length
    
    #Then Show Plot Border
    #And select on Show Plot Border
    
 
   
    Then click on Tool-Tips tab
    Then click Include Shadow
   # And Give Tool-Tips Border Color
   # And Give Tool-Tips Background Color
    
    
    Given Click on Cosmetics 
    And click on Yes button
    #Then Give Border Data
   
    
   
    And Click on Canvas
    
    
    #And Give Thickness
    
    Then Give on 3D Show Canvas
    Then Give on 3D Show Base
    Then Give on 3D Canvas Thickness
    Then Give on 3D Base Depth
    #And Give Second Opaqueness
   # And Give Border Color
    
    And Click on Margin & Padding
    Then Give Left margin
    Then Give Right Margin
    Then Give Top Margin
    Then Give Bottom Margin
    
    Then Give Title Padding
    Then Give Y Axis Title Padding
    Then Give Label Padding
    Then Give X Axis Title Padding
    Then Give Y Axis Value Padding
    Then Give Value Padding
    
    Given Click on Fonts
    Then Give Customize Font Name
    Then Give Customize Font Size
    ## Then Give Customize Font Color
    Then Give side canvas Font Name
    Then Give side canvas Font Size
    ## Then Give side canvas Font Color
    
    Given Click on Zero Plane
    Then Give 3D chart Border
    
    Given Click on Custom Branding
    Then Specify logo URL
    Then Give Link
    Then Give Position
    Then Give Branding Opaqueness
    Then Give Branding Scale
    
    
    Given Click on Number Formatting
    And click on Yes button
    Then click on Format numbers 
    Then Give Scales to be used
    Then Give Round numbers
    Then Give Round axis
    Then Click on Force exact number
    Then Click on Force Y Axis decimal values 
    

   
  
    
    
    Given Click on Axis
    And click on Yes button
    Then Set adaptive lower limit for axis
    Then Set Force decimals on primary y-axis values
    Then Give Y-Axis minimum value,maximum value,set lower limit,primary y-axis value
    
    Then Click on Axis Grid Lines
    Then Give Number of grid lines
    Then Give Grid Line Thickness
    Then Choose Grid Line color
    Then Give Grid Line Opaqueness
    Then Show grid lines as dashed
    Then Give Dash length
    Then Give Dash gap
    Then Tick Show alternate color bands
   
    Given Click on Trend Lines
    And click on Yes button
    Then Give Start Value 
    Then Give End Value
    Then Give Display Text
    Then give Tool-tip Text
    Then Click on as filled color zone

    
    
    Given Click on Other Settings
    And click on Yes button
    Then Give Chart Loading Message
    Then Give Data Loading Message
    Then Give Data Processing Message
    Then Give No-data to display Message
    Then Give Pre-Rendering Message
    Then Give Error in Loading Message
    Then Give Error in Data Message
    #Then click on Custom Attribute
    #And click on Add Button
    ##Then Give Attribute Name
    Then Click on Connection Fields
    #And Select Fields used in chart
    #Then click on Finish button
    ##And Select All fields returned from data source
    ##And click on Revert button

    #Then save chart
    #Then delete chart
