 Feature: Automate Excel with a password
 @DataSourceExcelWithPassword
  Scenario: Excel DataSelection Checking
    Given Click on Chart Wizard Dropdown
    Then format the CSV to Excel data source
    Then Give ExcelPath
    Then give ExcelPassword
    Then click on Load Button
    And click on ok button
    Then select Sheet
    Then click on connect button
    Then click on OK button
    Then go to Select Fields
    And ticked all Select Field Checkbox
    And click on Apply button
    Then Click on Filter Data 
		Then Click on Delete Button 
		Then Click on Add Button
		Then click on first Dropdown 
  	Then click on second Dropdown
  	Then provide data in the input field  
  	Then click on Filter Data Apply button 
    Then click on Group Data 
    Then click on first checkbox
    Then click on first dropdown
    Then select aggregate function field and function
    Then Click Apply button
    Then Click on Drill Down
    Then Select First DrillDown option
    Then Enable Multi level DrillDown
    Then Select First DrillDown Field
    Then click on Wizard Drilldown Apply button
    Then click on Finish button
    Then click on any bar on chart
    
    @ExcelWithPasswordNegativeTesting
    Scenario: To check whether Excel can be accessed with wrong password
    Given Click on Chart Wizard Dropdown
    Then format the CSV to Excel data source
    Then Give ExcelPath
    Then give ExcelPassword
    Then click on Load Button
    And click on ok button
    Then select Sheet
    Then click on connect button
    Then click on OK button
    Then go to Select Fields
    And ticked all Select Field Checkbox
    And click on Apply button
    Then click on Finish button
    Given Click on Chart Wizard Dropdown
    Then format the CSV to Excel data source
    Then Give ExcelPath
    Then give wrong  ExcelPassword
    Then click on Load Button
    Then Check of the dialog box and click OK.
    
    @ExcelWithPasswordNegativeTesting
    Scenario: To check whether protected Excel can be accessed with no password
    Given Click on Chart Wizard Dropdown
    Then format the CSV to Excel data source
    Then Give ExcelPath
    Then give ExcelPassword
    Then click on Load Button
    And click on ok button
    Then select Sheet
    Then click on connect button
    Then click on OK button
    Then go to Select Fields
    And ticked all Select Field Checkbox
    And click on Apply button
    Then click on Finish button
    Given Click on Chart Wizard Dropdown
    Then format the CSV to Excel data source
    Then Give ExcelPath
    Then give empty ExcelPassword
    Then click on Load Button
    Then Check of the Enter password dialog box and click OK.
 