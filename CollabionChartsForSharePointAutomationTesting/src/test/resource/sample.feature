Feature: a)Filter Data b)Group Data c)Top N Records
         Single Series Column2D Collabion Chart Configuration
         These includes test cases
         a)Filter Data b)Group Data c)Top N Records

#Background: Flow till Chart type
 # Given Click on Chart Wizard Dropdown
   
@FormattingRegression   
Scenario: This is just a formatting of Excel Datasource
    Given Click on Chart Wizard Dropdown
    Then format the data source 
 
  
@Regression
Scenario: Single Series Column2D Chart Type 'Data Source' Selection checking
  # Given Configuration of Web Part
    Given Click on Chart Wizard Dropdown
    Then choose Excel Data Provider
    Then Give Url of Excel
    Then click on Load Button
    And click on ok button
    Then select Sheet name
     
    Then click on connect button
    Then click on OK button
    Then go to Select Fields  
    
    And ticked all Select Field Checkbox
    And uncheck First field Checkbox
    And uncheck fourth field Checkbox
    And uncheck fifth field Checkbox
    And uncheck Sixth field Checkbox
   
    
    And click on Apply button
    Then click on Finish button
    Then save chart
  # Then Generate TestScenario Result Report
    
    
 @caseid002
Scenario: Single Series Column2D Chart Type Selection Group 'Top N Records' Checking
  
  Given Click on Chart Wizard Dropdown    
  Then click on Top N Records
  #And click on No button
  Then click on Show top first Dropdown
  Then click on Show top Second Dropdown
  Then click Apply Button
  ########Then click on Source XML
  #######Then Copy the XML
  Then click on Finish button
     
@caseid003
 Scenario: Single Series Column2D Chart Type Selection 'Filter Data' Checking
   
  Given Click on Chart Wizard Dropdown
  Then Click on Filter Data
  Then Click on Delete Button
  Then Click on Add Button
  Then includes all dates
  Then click on first Dropdown
  Then click on second Dropdown
  Then send search data
  Then click Apply button
  ########Then click on Source XML
  #######Then Copy the XML
  Then click on Finish button
  
    
 @caseid004
Scenario: Single Series Column2D Chart Type Selection 'Group Data' Checking
  
   Given Click on Chart Wizard Dropdown
   Then click on Group Data
   #And click on Yes button
   Then click on first checkbox   
   Then click on first dropdown 
   Then click on radiobutton 'distinct values from field' 
   Then Choose parallel dropdown
   Then Choose Series Display first dropdown
   Then Choose Series Display second dropdown
   Then Click Apply button
    ########Then click on Source XML
    #######Then Copy the XML
   Then click on Finish button
   
@caseid005
Scenario: Single Series Column2D Chart Type Selection Data Selection 'Clear' Button Checking
    
    Given Click on Chart Wizard Dropdown
    Then Click on Filter Data
    And click on Clear All Filters
    
    Then click on Group Data
    And click on clear grouping button 
    
    Then click on Top N Records
    And click on clear Top N Records
    Then click on Finish button

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 