Feature: All Chart Type with all chart category configuration
         
         


Background: Flow till Chart type
   Given Click on Sign In 
   Given Click on Chart Wizard Dropdown 
   Then click on Chart Type 
   
# @ChartTypeAutomation
# Scenario: Single Series SplineArea Chart Type Selection
#   Given Click on Sign In 
#   Given Click on Chart Wizard Dropdown 
#   Then click on Chart Type 
#   Given Choose Single Series from Chart Category Dropdown
#   Given Choose Multi Series from Chart Category Dropdown
#   Given Choose Stacked Chart from Chart Category Dropdown
#   Given Choose Combination Charts from Chart Category Dropdown
#   Given Choose X-Y charts from Chart Category Dropdown
#   Given Choose Scroll charts from Chart Category Dropdown
#   
#  # Then Choose Spline Area Chart
#   #Then Choose SingleSeriesPieTwoDChart 
#   #Then Choose SingleSeriesPieThreeDChart
#   #Then Choose SingleSeriesDoughnutTwoDChart
#   #Then Choose SingleSeriesFunnelChart
#
#   #Then Choose SingleSeriesWaterfallTwoDChart
#   #Then Choose SingleSeriesParetoThreeDChart
#   #Then Choose SingleSeriesKagiChart
#   #Then Choose MultiSeriesLineTwoDChart
#   #Then Choose SingleSeriesMSAreaChart
#  # Then Choose MultiSeriesBarTwoDChart
#   #Then Choose MultiSeriesBarThreeDChart
#   #Then Choose MultiSeriesSplineChart
#   #Then Choose MultiSeriesSplineAreaChart
#   #Then Choose MultiSeriesLogarithmlineChart
#   #Then Choose MultiSeriesLogarithmicColumnChart
#   #Then Choose MultiSeriesRadarChart
#   #Then Choose MultiSeriesInverseAreaChart
#   #Then choose MultiSeriesInverseTwoDColumnChart
#   #Then choose MultiSeriesInverseTwoDLineChart
#   #Then choose MultiSeriesMarimekkoChart
#   #Then choose MultiSeriesZoomLineChart
#   #Then choose MultiSeriesStepLineChart
#   #Then choose StackedColumnTwoDChart
#   #Then choose StackedColumnThreeDChart
#   #Then choose StackedAreaTwoDChart
#   #Then choose StackedBarTwoDChart
#   #Then choose StackedBarThreeDChart
#   #Then choose TwoDSingleYCombinationChart
#   #Then choose ThreeDSingleYCombinationChart
#   #Then choose MSColumnLineThreeDChart
#   #Then choose ColumnThreeDLineDYChart
#   #Then choose StackedColumnThreeDLineDualY
#   #Then choose TwoDDualYCombination
#   #Then choose TwoDStackedColumnlineDualY
#   #Then choose TwoDStackedColumnSingleY
#   #Then choose ThreeDStackedColumnLineSingleY
#   
#   #Then choose ScatterXYPlot
#   #Then choose Bubble
#   #Then choose ScrollColumnTwoD
#  # Then choose ScrollLineTwoD
#   #Then choose ScrollAreaTwoD
#   #Then choose ScrollStackedColumnTwoD
#   #Then choose ScrollCombiTwoD
#   Then choose ScrollCombiDYTwoD
#  
 @ChartCategorySingleSeriesPieTwoDChart 
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesPieTwoDChart 
 
 @ChartCategorySingleSeriesSplineAreaDChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose Spline Area Chart 
  
@ChartCategoryMultiSeriesBarTwoDChart
@ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesBarTwoDChart
   
 @ChartCategorySingleSeriesPieThreeDChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesPieThreeDChart
  
  
 @ChartCategorySingleSeriesDoughnutTwoDChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesDoughnutTwoDChart
   
     
 @ChartCategorySingleSeriesFunnelChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesFunnelChart
   
   
 @ChartCategorySingleSeriesWaterfallTwoDChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesWaterfallTwoDChart
   
     
 @ChartCategorySingleSeriesParetoThreeDChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesParetoThreeDChart
   
 @ChartCategorySingleSeriesKagiChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesKagiChart
  
 @ChartCategoryMultiSeriesLineTwoDChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose MultiSeriesLineTwoDChart
   
   
 @ChartCategorySingleSeriesMSAreaChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Single Series from Chart Category Dropdown
   Then Choose SingleSeriesMSAreaChart

 @ChartCategoryMultiSeriesBarThreeDChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesBarThreeDChart

   
 @ChartCategoryMultiSeriesSplineChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesSplineChart

   
 @ChartCategoryMultiSeriesSplineAreaChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesSplineAreaChart
   
   
 @ChartCategoryMultiSeriesLogarithmlineChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesLogarithmlineChart
   
     
 @ChartCategoryMultiSeriesLogarithmicColumnChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesLogarithmicColumnChart
   
 @ChartCategoryMultiSeriesRadarChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesRadarChart
  
 @ChartCategoryMultiSeriesInverseAreaChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then Choose MultiSeriesInverseAreaChart
   
   
 @ChartCategoryMultiSeriesInverseTwoDColumnChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then choose MultiSeriesInverseTwoDColumnChart
   
   
 @ChartCategoryMultiSeriesInverseTwoDLineChart
 @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then choose MultiSeriesInverseTwoDLineChart
   
  @ChartCategoryMultiSeriesMarimekkoChart
  @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then choose MultiSeriesMarimekkoChart
  
     
  @ChartCategoryMultiSeriesZoomLineChart
  @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then choose MultiSeriesZoomLineChart
   
   @ChartCategoryMultiSeriesStepLineChart
   @ChartTypeAutomation
 Scenario:  
   Given Choose Multi Series from Chart Category Dropdown
   Then choose MultiSeriesStepLineChart
    
   @ChartCategoryStackedColumnTwoDChart
   @ChartTypeAutomation
 Scenario:  
   Given Choose Stacked Chart from Chart Category Dropdown
   Then choose StackedColumnTwoDChart
   
     
   @ChartCategoryStackedColumnThreeDChart
 Scenario:  
   Given Choose Stacked Chart from Chart Category Dropdown
   Then choose StackedColumnThreeDChart
   
   @ChartCategoryStackedColumnThreeDChart
   @ChartTypeAutomation
 Scenario:  
   Given Choose Stacked Chart from Chart Category Dropdown
   Then choose StackedColumnThreeDChart  

   
     @ChartCategoryStackedAreaTwoDChart
     @ChartTypeAutomation
 Scenario:  
   Given Choose Stacked Chart from Chart Category Dropdown
   Then choose StackedAreaTwoDChart
   
    @ChartCategoryStackedBarTwoDChart
    @ChartTypeAutomation
 Scenario:  
   Given Choose Stacked Chart from Chart Category Dropdown
   Then choose StackedBarTwoDChart
   
     @ChartCategoryStackedBarThreeDChart
     @ChartTypeAutomation
 Scenario:  
   Given Choose Stacked Chart from Chart Category Dropdown
   Then choose StackedBarThreeDChart
   
  @ChartCategoryTwoDSingleYCombinationChart 
  @ChartTypeAutomation  
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose TwoDSingleYCombinationChart   
   
@ChartCategoryThreeDSingleYCombinationChart
@ChartTypeAutomation  
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose ThreeDSingleYCombinationChart
   
 #@ChartCategoryMSColumnLineThreeDChart
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose MSColumnLineThreeDChart
  
  
  @ChartCategoryColumnThreeDLineDYChart
  @ChartTypeAutomation
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose ColumnThreeDLineDYChart
   
   
    @ChartCategoryStackedColumnThreeDLineDualY
    @ChartTypeAutomation
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
    Then choose StackedColumnThreeDLineDualY
   
  
    @ChartCategoryTwoDDualYCombination
    @ChartTypeAutomation
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose TwoDDualYCombination
   
   
  @ChartCategoryTwoDStackedColumnlineDualY
  @ChartTypeAutomation
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose TwoDStackedColumnlineDualY
  
  @ChartCategoryTwoDStackedColumnSingleY
  @ChartTypeAutomation
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose TwoDStackedColumnSingleY
   
   @ChartCategoryThreeDStackedColumnLineSingleY
   @ChartTypeAutomation
 Scenario:  
   Given Choose Combination Charts from Chart Category Dropdown
   Then choose ThreeDStackedColumnLineSingleY
   
   @ChartCategoryScatterXYPlot
   @ChartTypeAutomation
 Scenario:  
   Given Choose X-Y charts from Chart Category Dropdown
   Then choose ScatterXYPlot
   
   
   @ChartCategoryBubble
   @ChartTypeAutomation
 Scenario:  
   Given Choose X-Y charts from Chart Category Dropdown
   Then choose Bubble
   
   
  @ChartCategoryScrollColumnTwoD
  @ChartTypeAutomation
 Scenario:  
   Given Choose Scroll charts from Chart Category Dropdown
   Then choose ScrollColumnTwoD
   
   @ChartCategoryScrollLineTwoD]
   @ChartTypeAutomation
 Scenario:  
   Given Choose Scroll charts from Chart Category Dropdown
   Then choose ScrollLineTwoD
   
   @ChartCategoryScrollAreaTwoD
   @ChartTypeAutomation
 Scenario:  
   Given Choose Scroll charts from Chart Category Dropdown
   Then choose ScrollAreaTwoD
    
 @ChartCategoryScrollStackedColumnTwoD
 @ChartTypeAutomation
 Scenario:  
   Given Choose Scroll charts from Chart Category Dropdown
   Then choose ScrollStackedColumnTwoD
   
   
  @ChartCategoryScrollCombiTwoD
  @ChartTypeAutomation
 Scenario:  
   Given Choose Scroll charts from Chart Category Dropdown
   Then choose ScrollCombiTwoD
   
 @ChartCategoryScrollCombiDYTwoD
 @ChartTypeAutomation
 Scenario:  
   Given Choose Scroll charts from Chart Category Dropdown
   Then choose ScrollCombiDYTwoD
     
   
