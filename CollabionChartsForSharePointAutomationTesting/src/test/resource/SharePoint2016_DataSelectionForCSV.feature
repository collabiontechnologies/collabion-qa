Feature: CSV DataSelection And Some Negative Test Cases depending on Datasource 

@CSVDataSelectionRegression(SharePoint2016) 
@SP2016CSVListRegression
Scenario: CSV DataSelection Checking 

    #Given Click on SharePoint SignIn
    #Then Click on SharePoint Page
     Then Click on SharePoint 2016 Chart Edit
	#Given Click on Chart Wizard Dropdown 
	
	Then format the Sharepoint list to CSV data source 
    And click on Yes button
	Then Select Radio Button of static CSV data 
	Then Give data in CSV Data Field 
	Then Click on Parse Data Button 
	Then click on OK button 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	Then click on Top N Records 
	#And click on No button
	Then click on Show top first Dropdown 
	Then click on Show top Second Dropdown 
	Then click Apply Button 
	
	Then Click on Filter Data 
	Then Click on Delete Button 
	Then Click on Add Button 
	#Then includes all dates
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send CSV search data 
	Then click on Filter Data Apply button 
	
	Then click on Group Data 
	#And click on Yes button
	Then click on first checkbox 
	Then click on first dropdown 
	Then click on radiobutton 'distinct values from field' 
	Then Choose parallel dropdown 
	Then Choose Series Display first dropdown 
	Then Choose Series Display second dropdown 
	Then Click Apply button 
	#Then click on Source XML
	#Then Copy the XML
    Then click on Finish button
#	 
	#Then Compare CSVData XML 
##	
##	
	##Given Click on Chart Wizard Dropdown 
	Then Click on SharePoint 2016 Chart Edit
	Then Click on Filter Data 
	And click on Clear All Filters 
	
	Then click on Group Data 
	And click on clear grouping button 
	
	Then click on Top N Records 
	And click on clear Top N Records 
	Then click on Finish button 
	
	
@CSVDataNegativeTestCase(SharePoint2016) 
@SP2016CSVListRegression
Scenario: CSV Data Negative Test Case1 
	#Given Click on SharePoint SignIn
    #Then Click on SharePoint Page
    Then Click on SharePoint 2016 Chart Edit
	#Given Click on Chart Wizard Dropdown 
	Then go to Select Fields 
	Then Untick all the Checkbox 
	Then Click Select Field Apply button 
	
@CSVDataNegativeTestCase(SharePoint2016) 
@SP2016CSVListRegression
Scenario: CSV Data Negative Test Case2 
    #Given Click on SharePoint SignIn
    #Then Click on SharePoint Page
    Then Click on SharePoint 2016 Chart Edit
	#Given Click on Chart Wizard Dropdown 
	Then Clear CSVData Field 
	Then Click on Parse Data Button
	#Then Click on Parse Data Negative Button 
	#Then Verify CSV Data field 
	
	
