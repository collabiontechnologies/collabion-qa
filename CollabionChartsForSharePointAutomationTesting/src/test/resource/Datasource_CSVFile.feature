Feature: Collabion feature 
         Test cases for the 
         'CSV File'Datasource 
          Includes Configure Source,
          Data Selection,User Interaction


@caseid37
Scenario: Configure'Data Source'  
    Given Configuration of Web Part

    Given Click on Chart Wizard Dropdown
    Then Choose CSV File
    #Then Select Radio Button of static CSV data
    Then Give data in CSV Data Field
    Then Click on Parse Data Button
    Then click on OK button
    Then go to Select Fields  
    And ticked all Select Field Checkbox
    And click on Apply button
    Then click on Finish button
    
    Then save chart