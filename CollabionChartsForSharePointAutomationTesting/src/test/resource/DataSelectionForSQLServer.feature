Feature: SQl server DataSelection And Some Negative Test Cases depending on Datasource 


@MicrosoftSQlServerDataSelectionRegression 
Scenario: Microsoft SQl server DataSelection Checking 

	Given Click on Chart Wizard Dropdown 
	Then format the Excel to Microsoft SQl server data source 
	
	#Then tick on Windows Authentication 
	
	
	#Then tick on Windows Authentication 
	Then Choose Windows Authentication from Authentication Dropdown
	#Then Choose SQL Authentication from Authentication Dropdown
	#Then Choose Domain Authentication from Authentication Dropdown
	Then Give Data in Server Field 
	Then Give Data in Database field 
	
	Then click on connect button 
	Then click on OK button 
	Then Select data table 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	
	
	
	And click on Apply button 
	Then click on Top N Records 
	#And click on No button
	Then click on Show top first Dropdown 
	Then click on Show top Second Dropdown 
	Then click Apply Button 
	
	Then Click on Filter Data 
	#And click on Yes button
	Then Click on Delete Button 
	Then Click on Add Button 
	#Then includes all dates
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send SQL server search data 
	Then click on Filter Data Apply button 
	
	Then click on Group Data 
	#And click on Yes button
	Then click on first checkbox 
	Then click on first dropdown 
	Then click on radiobutton 'distinct values from field' 
	Then Choose parallel dropdown 
	Then Choose Series Display first dropdown 
	Then Choose Series Display second dropdown 
	Then Click Apply button 
	########Then click on Source XML
	#######Then Copy the XML
	Then click on Finish button 
	Then Compare SQLServerData XML 
	
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	And click on Clear All Filters 
	
	Then click on Group Data 
	And click on clear grouping button 
	
	Then click on Top N Records 
	And click on clear Top N Records 
	Then click on Finish button 
	
@SQLServerNegativeDataSourceTesting
Scenario: SQL server data selection negative Test cases with no Data.
 Given Click on Chart Wizard Dropdown 
 Then format the Excel to Microsoft SQl server data source
 Given Click on the Authetication DropDown and Select Domain Autheticaion
 Then  leave all the input fields empty.
 Then  Click on Connects
 Then  Check for the red border color.
 

@SQLServerNegativeDataSourceTesting
Scenario: SQL server data selection negative Test cases with Wrong Data. 
Given Click on Chart Wizard Dropdown
Then format the Excel to Microsoft SQl server data source 
Given Click on the Authetication DropDown and Select Domain Autheticaion
Then  Provide Incorrect Domain Name
Then  Provide incorrect User Name
Then  Provide incorrect password 
Then  Provide incorrect DB Name
Then  Click on Connects
Then  Check for the Error Dialogue.

	
	
Scenario: SQL Server Data Negative Test Case1 
	Given Click on Chart Wizard Dropdown 
	Then format the Excel to Microsoft SQl server data source 
	
	#Then tick on Windows Authentication 
	
	
	#Then tick on Windows Authentication 
	Then Choose Windows Authentication from Authentication Dropdown
	#Then Choose SQL Authentication from Authentication Dropdown
	#Then Choose Domain Authentication from Authentication Dropdown
	Then Give Data in Server Field 
	Then Give Data in Database field 
	
	Then click on connect button 
	Then click on OK button 
	Then Select data table 
	Then go to Select Fields 
	Then untick all the checkbox 
	Then Click Select Field Apply button 
	Then click on OK button 
	Then go to Configuration Page 
	
#@SQLServerDataNegativeTestCase 
Scenario: SQL Server Data Negative Test Case2 
	Given Click on Chart Wizard Dropdown 
	Then Give Wrong Server Name 
	Then click on connect button 
	Then click on OK button 
	
	
	
#@SQLServerDataNegativeTestCase 
Scenario: SQL Server Data Negative Test Case3 
#Then go to Configuration Page
	Given Click on Chart Wizard Dropdown 
	Then Give Data in Server Field
	Then click on OK button 
	Then go to Select Fields 
	Then Verify QueryString Error 
	
#@SQLServerDataNegativeTestCase 
Scenario: SQL Server Data Negative Test Case4 
	Given Click on Chart Wizard Dropdown 
	Then Give Data in Server Field 
	Then Give Wrong Database Name 
	Then click on connect button 
	
	
	#Then Close Wizard
	#Then Give Wrong UserName
	#And Give Wrong Password
	
	
