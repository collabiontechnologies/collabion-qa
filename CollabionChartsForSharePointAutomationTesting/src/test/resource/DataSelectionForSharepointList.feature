Feature: SharepointList DataSelection And Some Negative Test Cases depending on Datasource

  @SharepointListDataSelectionRegression
  Scenario: SharepointList DataSelection Checking
    Given Click on Chart Wizard Dropdown
    Then format the SQL to Sharepoint List data source
    Then click on connect button
    Then click on OK button
    Then choose data in Select List Dropdown
    Then choose data in Select List View Dropdown
    Then go to Select Fields
    And click on Apply button
    Then click on Top N Records
    #And click on No button
    Then click on Show top first Dropdown
    Then click on Show top Second Dropdown
    Then click Apply Button
    Then Click on Filter Data
    Then Click on Delete Button
    Then Click on Add Button
    #Then includes all dates
    Then click on first Dropdown
    Then click on second Dropdown
    Then send search data
    Then click on Filter Data Apply button
    Then click on Group Data
    #And click on Yes button
    Then click on first checkbox
    Then click on first dropdown
    Then click on radiobutton 'distinct values from field'
    Then Choose parallel dropdown
    Then Choose Series Display first dropdown
    Then Choose Series Display second dropdown
    Then Click Apply button
    ########Then click on Source XML
    #######Then Copy the XML
    Then click on Finish button
    Then Compare SharePointListData XML
    Given Click on Chart Wizard Dropdown
    Then Click on Filter Data
    And click on Clear All Filters
    Then click on Group Data
    And click on clear grouping button
    Then click on Top N Records
    And click on clear Top N Records
    Then click on Finish button

  @SharepointListDataNegativeTestCase
  Scenario: SharepointList Data Negative Test Case1
    Given Click on Chart Wizard Dropdown
    Then format the SQL to Sharepoint List data source
    Then click on connect button
    Then click on OK button
    Then choose data in Select List Dropdown
    Then choose data in Select List View Dropdown
    Then go to Select Fields
    Then untick all the checkbox
    Then Click Select Field Apply button
