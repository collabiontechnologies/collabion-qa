Feature: SharePoint 2016 CCSP 2.4 New Feature Testing

##@SP16Import
@SP16CCSP2.4Regression 
Scenario: CCSP Import Feature Checking 
	#Given Click on Sign In 
	Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Import/Export
    Then Click on Import from Dropdown 
    Then Select file for import 	



##@BulkExport 
@SP16CCSP2.4Regression 
Scenario: CCSP Export Feature Checking 
	Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Import/Export
	Then Click on Export Tab 
	Then Click on Export Button 
	
##@DataSourceExcelListWithNetwork 
@SP16CCSP2.4Regression	
	Scenario: Excel DataSelection Checking 
	#Given Click on Sign In 
	Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Then format the CSV to Excel data source 
	And click on Yes button 
	
	Then select Network radio button 
	##Then Give Url of Excel
	Then Give Url of Network Excel 
	
	And Give User Name of Network 
	And Give Password of Network 
	And Give Password of File 
	
	
	Then click on Load Button 
	And click on ok button 
	
	Then choose network sheet name 
	Then click on connect button 
	
##@DataSourceExcelListWithCurrentSite 
@SP16CCSP2.4Regression	
Scenario: Excel DataSelection Checking 
    Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Then format the CSV to Excel data source 
	And click on Yes button 
	Then Give New Url of Excel
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name 
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	Then click on Finish button 
	
	
		
##@GroupByNone 
@SP16CCSP2.4Regression
Scenario: SeriesFunction Testing(GroupBy with None) 
    Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Then click on Group Data 
	Then click on clear grouping button 
	And click on Yes button 
	Then click on first checkbox 
	Then click on series function 'Revenue' dropdown 
	Then click on Series Function 'Sales' dropdown 
	#Then click combo dropdown item 
	Then Click Apply button 
	Then click on Finish button 
	
##@SeriesFunction 
@SP16CCSP2.4Regression 
Scenario: SeriesFunction Testing 
   Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Then click on Group Data 
	Then click on clear grouping button 
	And click on Yes button 
	Then click on first checkbox 
	Then click on first dropdown 
	#Then click on Group By dropdown
	Then click on Series Function Dropdown 
	Then click combo dropdown item 
	#Then choose combo dropdown option
	Then Click Apply button 
	Then click on Finish button 
	
	
##@WizardFirstDrilldownSetWithGroupByFields 
@SP16CCSP2.4Regression 
Scenario: Verify 'User Interaction'  WizardFirstDrilldownSetWithGroupByFields 
	Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select First DrillDown option 
	#Then Select Group by Fields
	Then Choose Sort By Dropdown 
	Given Click on Configuration 
	And Select Chart Type from dropdown 
	
	Then Give Chart Title 
	Then Give Chart Sub Title 
	Then Give X-Axis Title 
	Then Give Primary Y-axis Title 
	Then Rotate Y-axis Title 
	Then Filled Y-axis width 
	Then Click on Apply to all 
	
	#And click on Apply Yes button
	Then Click on ok button 
	Then Click on  Drilldown caption ok button 
	Then Enable Multi level DrillDown 
	And Enable to change chart type at view mode 
	
	Then Select First DrillDown Field 
	#Then click on 'Customize' button
	Then click on Wizard Drilldown Apply button 
	Then click on Finish button 
	
	#Then Click on ok button
	Then click on DrillDown Bar 
	Then Click on  Drilldown caption ok button 
	Given Click on Select Chart type icon 
	Then Click on  Drilldown caption cancel 
	
##@DrillDownChartTypeChoosing 
@SP16CCSP2.4Regression 
Scenario: Checking Chart Type in Every Drilldown Level 
	Then Click on SharePoint Page
	Given Click on Select Chart type icon 
	
	
	
##@DataSourceOracleDatabase
@SP16CCSP2.4Regression 
Scenario: Oracle DataSelection Checking 
	Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit
	Then Select Oracle Database as DataSource
	And click on Yes button
	Then Give Oracle Server Name
	Then Give Oracle User Name
	Then Give Oracle User Password
	Then click on connect button