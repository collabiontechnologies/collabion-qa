Feature: Publishing site test 


@ChartCreateRegression 
@JiraRegression
Scenario: Excel DataSelection Checking 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	And click on Yes button 
	Then Give Url of Excel 
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name 
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	Then click on Finish button 
	
@FilterDataRegression 
@JiraRegression
Scenario: Filter Data Regression First
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send search data 
	Then click on Filter Data Apply button 
	
@Specific
@JiraRegression
Scenario Outline: Filter Data Regression Second
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then filter with <Column>
	Then select the <FieldValue>
	Then give <SpecificSearch>
	Then search <Particulars>
	#And click on Apply button
    Then click on Finish button 
	Examples: 
    | Column              | FieldValue                 | SpecificSearch    | Particulars       | 
	| "Date"              | "Is Previous"              |  "9"              |"Month(s)"         |
    | "Date"              | "In Past (incl. today)"    |  "11"             |"Quarter(s)"       |
	
		
		
@Data 
@JiraRegression
Scenario Outline: Filter Data Regression Third
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then filter with <Column>
	Then select the <FieldValue>
	#Then give <SpecificSearch>
	Then search with <Searchvalue>
	#And click on Apply button
    Then click on Finish button 
	Examples: 
		| Column              | FieldValue       | Searchvalue           | 
		| "Date"              | "Is"             |  "Today"              |
		| "Country Name"      | "Equal"          |  "Argentina"          |
		| "Company Name"      | "Equal"          |  "Dabtype"            |
		| "Email"             | "Equal"          |  "bgeorge6@noaa.gov"  |
		| "Phone"             | "Not Equal"      |  "0-(562)555-2699"    |
	
	
@DateRange
@JiraRegression	
Scenario Outline: Filter Data Regression Forth
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then filter with <Column>
	Then select the <FieldValue>
	Then give first date<FirstDate>
	Then give second date<SecondDate>
	#And click on Apply button
    Then click on Finish button 
	Examples: 
    | Column              | FieldValue                 | FirstDate             |   SecondDate             | 
	| "Date"              | "Is Between"               |  "11/09/2014"         |   "11/09/2016"         |
	
@DateLessEqualGreater	
@JiraRegression
Scenario Outline: Filter Data Regression Fifth 
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then filter with <Column>
	Then select the <FieldValue>
	Then search with date<Date>
	#And click on Apply button
    Then click on Finish button 
	Examples: 
    | Column              | FieldValue                 | Date                | 
	| "Date"              | "Less or Equal"            |  "11/09/2014"       |
	| "Date"              | "Less"                     |  "14/09/2014"       |
	| "Date"              | "Greater or Equal"         |  "11/09/2014"       |
	| "Date"              | "Greater"                  |  "11/09/2014"       |
	| "Date"              | "Not Equal"                |  "11/09/2014"       |
	| "Date"              | "Equal"                    |  "11/09/2014"       |
	


@DateEmptyOrNotEmpty	
@JiraRegression
Scenario Outline: Filter Data Regression sixth
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then filter with <Column>
	Then select the <FieldValue>
	#And click on Apply button
    Then click on Finish button 
	Examples: 
    | Column              | FieldValue                 | 
	| "Date"              | "Is Not Empty"             |  
	| "Date"              | "Is Empty"             |  
	

@DateContainsBeginsWith	
@JiraRegression
Scenario Outline: Filter Data Regression seventh
	Given Click on Sign In 
	Given Click on Chart Wizard Dropdown 
	Then Click on Filter Data 
	Then filter with <Column>
	Then select the <FieldValue>
	Then Contains or Begins <With>
	#And click on Apply button
    Then click on Finish button 
	Examples: 
    | Column              | FieldValue         | With    |
	| "Country Name"      | "Contains"         | "S"      |
	| "Company Name"      | "Begins With"      | "A"      |

	
@WebpartFilterIssueTest	
@JiraRegression	
Scenario Outline: Filter Data Regression eighth 
	Given Click on Sign In 
	#Then go to another site
	Given Click on Chart Wizard Dropdown 
	Then format the SQL to Sharepoint List data source 
	And click on Yes button
	#Then click on OK button
	
	Then click on connect button 
	Then click on OK button 
	Then choose options from Select List Dropdown 
	Then choose options from Select List View Dropdown 
	Then go to Select Fields 
	And click on Apply button
	
	Then click on Finish button
	
	Then User provides <inputText> to the TextFilter 
    Examples: 
		| inputText   |
		| "Google"    |
		| "Microsoft" |
		
@PublishingSite		
@JiraRegression
Scenario Outline: Filter Data Regression ninth
	Then go to publishing site
	Then capture Image 
    Then select Dynamic filter with <DynamicFilterFirst>
	Then select Dynamic filter the <DynamicFilterSecond>
	Then search with Dynamic Filter <DynamicFilterSearch>
	And click on Apply button
	Examples:
			   |   DynamicFilterFirst   | DynamicFilterSecond | DynamicFilterSearch   |
			   | "Company Name"         | "Equal"             | "Dell"                |
				
						
		
@PSAdminSignIn	
@JiraRegression
Scenario: Publishing site admin Sign In
	Then go to publishing site and Sign In 


@PSArkaSignIn	
@JiraRegression
Scenario: Publishing site user Sign In 
	Then go to publishing site
	Given Click on Arka Sign In
	Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	And click on Yes button 
	Then Give Url of Excel of Publishing Site
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name of Publishing Site
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	Then click on Finish button 