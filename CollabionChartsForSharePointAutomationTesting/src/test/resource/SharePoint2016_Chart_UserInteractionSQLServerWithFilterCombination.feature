Feature: User Interaction Feature With SharePoint Webpart Filter Combination(Data Source SQLServerDatabase). 

@DataSourceSQLServer(SharePoint2016) 
Scenario: Microsoft SQl server DataSelection Checking 

#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then format the Excel to Microsoft SQl server data source 
	
	Then tick on Windows Authentication 
	
	
	Then Give Data in Server Field 
	Then Give Data in Database field 
	#Then tick on Windows Authentication 
	Then click on connect button 
	Then click on OK button 
	Then Select data table 
	Then go to Select Fields 
	And ticked all Select Field Checkbox 
	And click on Apply button 
	
@SeriesFunction(SQLServerDatabase)(SharePoint2016) 
Scenario: SeriesFunction Testing 

#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then click on Group Data 
	Then click on clear grouping button 
	And click on Yes button 
	Then click on first checkbox 
	Then click on first dropdown 
	#Then click on Group By dropdown
	Then click on Series Function Dropdown 
	Then click combo dropdown item 
	#Then choose combo dropdown option
	
	Then Click Apply button 
	Then click on Finish button 
	
	
@WizardFirstDrilldownSetWithGroupByFields(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction'  WizardFirstDrilldownSetWithGroupByFields 
#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select First DrillDown option 
	#Then Select Group by Fields
	
	#Then Choose Sort By Dropdown
	
	
	Given Click on Configuration 
	Then Give Chart Title 
	Then Give Chart Sub Title 
	#Then Give X-Axis Title
	#Then Give Primary Y-axis Title
	#Then Rotate Y-axis Title
	#Then Filled Y-axis width
	Then Click on Apply to all 
	Then Click on ok button 
	
	Then Enable Multi level DrillDown 
	Then Select First DrillDown Field 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@SQLServerDatabaseMultiLevelDrillDownUI(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify Multi Level DrillDownUI 
	#Then Click on the Site Pages HyperLink 
	#Then Click on the DrillDownPage HyperLink 
	Then Click on any bar in the chart 
	Then Click on a bar on the Second level DrillDown 
	
@MultiLevelDrillDownUI(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify Multi Level DrillDownUI 
	#Then Click on the Site Pages HyperLink 
	#Then Click on the DrillDownPage HyperLink 
	Then Click on SharePointList First Level bar in the chart 
	Then Click on SharePointList Second Level bar in the chart 
	
@ExcelListMultiLevelDrillDownUI(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify Multi Level DrillDownUI 
	#Then Click on the Site Pages HyperLink 
	#Then Click on the DrillDownPage HyperLink 
	Then Click on ExcelList First Level bar in the chart 
	Then Click on ExcelList Second Level bar in the chart 
	
	
@TextFilterDataDrivenTesting(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify Text Filter search with multiple data 
	Then First Give data in Text filter And Search 
	
	
@ChoiceFilterDataDrivenTesting(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify Choice Filter search with multiple data 
	Then First Give data in Choice Filter And Search 
	
@DynamicFilterDataDrivenTesting(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify Wizard Dynamic Filter search with multiple data 
	Then First Give data in Dynamic filter From Excel 
	
	
@WizardDynamicFilterSecond(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' with Wizard Dynamic Filter Second 
#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Given Click on Dynamic filter 
	#And click on Yes button
	Then Allow viewers to filter data dynamically 
	Then Allow filtering only on the fields and data shown on chart 
	Then click on Finish button 
	
	
@CombinationOfText&DynamicFilter-UI(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify Combination Of Text & DynamicFilter-UI 
	Given provide a dynamic Filter 
	Then Provide a value for the TextFilter. 
	
@ChoiceFilter&DynamicFilterCombination(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify ChoiceFilter & DynamicFilter Combination 
#Dynamic Filter Use
	Given provide a dynamic Filter 
	Given Choose Choice Filter 
	
	
@Hotspot(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' using Hotspot 
#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Second DrillDown option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
	
@DrillDownNavigateToLink(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' with Link DrillDown 
#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Third DrillDown option 
	Then Give Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' using QueryString Filter 
#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Fourth DrillDown option From Wizard 
	Then Give Link Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
@NoDrillDown(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' with NoDrillDown 
#Given Click on Chart Wizard Dropdown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Fifth DrillDown option 
	Then click on Finish button 
	
	###########--------prerequisites--------
	
	###########--------1)Clear tick 'Print Chart'---------------------
	###########--------2)Clear tick 'Exporting chart data as Excel Spreadsheet'----------------
	###########--------3)Clear tick 'Exporting charts as image or PDF'-------------
	
@ExportSettings(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' Export Settings 
#Given Click on Chart Wizard Dropdown 
	Given Click on SharePoint SignIn 
	Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Given Click on Export Settings 
	#Then Click on Revert Button.
	#And click on Yes button
	Then tick on Enable printing of chart 
	Then tick on Enable exporting chart data as Excel Spreadsheet 
	Then tick on Enable exporting charts as image or PDF 
	Then give export file name 
	#And click on Apply button
	Then click on Finish button 
	Then click on Export Chart Button 
	
@Macro/DynamicTitle(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' using Dynamic Title 
#Given Click on Chart Wizard Dropdown 
	Given Click on SharePoint SignIn 
	Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Filter Data 
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send search data 
	Then click on Filter Data Apply button 
	Then Click on Captions 
	# And click on Yes button
	And Enter text in Chart Title Field 
	And Enter text in Chart Sub Title Field 
	Then click on Finish button 
	#And click on Yes button
	
	
	
@DataSourceExcel(SQLServerDatabase)(SharePoint2016) 
Scenario: Excel DataSelection Checking 

#Given Click on Chart Wizard Dropdown 
	Given Click on SharePoint SignIn 
	Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then format the CSV to Excel data source 
	Then Give Url of Excel 
	Then click on Load Button 
	And click on ok button 
	Then select Sheet name 
	
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	
	And ticked all Select Field Checkbox 
	# And uncheck First field Checkbox
	# And uncheck fourth field Checkbox
	# And uncheck fifth field Checkbox
	# And uncheck Sixth field Checkbox
	
	
	And click on Apply button 
	
	
@WizardFirstDrilldownSetWithGroupByDate(SQLServerDatabase)(SharePoint2016) 
Scenario: Verify 'User Interaction' WizardFirstDrilldownSetWithGroupByDate 
#Given Click on Chart Wizard Dropdown 
	Given Click on SharePoint SignIn 
	Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select First DrillDown option 
	Then Select Group by dates 
	
	
	#And Choose subset of Data dropdown
	#And Choose matched field for the subset of data
	#And Choose order of Data Storing
	#And Choose the way of showing chart Data 
	
	
	Then Choose Sort By Dropdown 
	
	
	Given Click on Configuration 
	Then Give Chart Title 
	Then Give Chart Sub Title 
	#Then Give X-Axis Title
	#Then Give Primary Y-axis Title
	#Then Rotate Y-axis Title
	#Then Filled Y-axis width
	Then Click on Apply to all 
	Then Click on ok button 
	
	Then Enable Multi level DrillDown 
	Then Select First DrillDown Field 
	Then click on Finish button 
	Then click on DrillDown Bar 
