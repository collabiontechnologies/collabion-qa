Feature: a)Filter Data b)Group Data c)Top N Records
         Single Series Column2D Collabion Chart Configuration
         These includes test cases
         a)Filter Data 
         b)Group Data(Checking depending on distinct values & Checking depending on series function) 
         c)Top N Records

#Background: Flow till Chart type
 # Given Click on Chart Wizard Dropdown
   
#@Series
Scenario: Excel DataSelection Checking
 Given Click on Chart Wizard Dropdown 
  Then click on Group Data 
  Then click on first checkbox   
  Then click on first dropdown
  Then click on Series Function Dropdown
  #Then click combo dropdown item
  Then choose combo dropdown option
  Then Click Apply button
  Then click on Finish button
    
# @ExcelDataSelectionRegression
Scenario: Excel DataSelection Checking
  
  Given Click on Chart Wizard Dropdown 
  Then choose Excel Data Provider
  Then Give Url of Excel
  Then click on Load Button
  And click on ok button
  Then select Sheet name
     
  Then click on connect button
  Then click on OK button
  Then go to Select Fields  
    
  And ticked all Select Field Checkbox
  And uncheck First field Checkbox
  And uncheck fourth field Checkbox
  And uncheck fifth field Checkbox
  And uncheck Sixth field Checkbox
   
    
  And click on Apply button
     
  Then click on Top N Records
  #And click on No button
  Then click on Show top first Dropdown
  Then click on Show top Second Dropdown
  Then click Apply Button
  
  Then Click on Filter Data
  Then Click on Delete Button
  Then Click on Add Button
  Then includes all dates
  Then click on first Dropdown
  Then click on second Dropdown
  Then send search data
  Then click on Filter Data Apply button
   
  Then click on Group Data
  #And click on Yes button
  Then click on first checkbox   
  Then click on first dropdown 
  Then click on radiobutton 'distinct values from field' 
  Then Choose parallel dropdown
  Then Choose Series Display first dropdown
  Then Choose Series Display second dropdown
  Then Click Apply button
  #  Then Compare XML
  ########Then click on Source XML
  #######Then Copy the XML
  Then click on Finish button
  Then Compare XML

  Given Click on Chart Wizard Dropdown
  Then Click on Filter Data
  And click on Clear All Filters
    
  Then click on Group Data
  And click on clear grouping button 
    
  Then click on Top N Records
  And click on clear Top N Records
  Then click on Finish button
  
  
  
   
   


  
# @SharepointListDataSelectionRegression
Scenario: SharepointList DataSelection Checking
  
  Given Click on Chart Wizard Dropdown
  Then format the Excel to Sharepoint List data source 
  
  Then click on connect button
  Then click on OK button
  Then choose data in Select List Dropdown
  Then choose data in Select List View Dropdown
  Then go to Select Fields
  And click on Apply button
      
  Then click on Top N Records
  #And click on No button
  Then click on Show top first Dropdown
  Then click on Show top Second Dropdown
  Then click Apply Button
  
  Then Click on Filter Data
  Then Click on Delete Button
  Then Click on Add Button
  Then includes all dates
  Then click on first Dropdown
  Then click on second Dropdown
  Then send search data
  Then click on Filter Data Apply button
  
 Then click on Group Data
  #And click on Yes button
  Then click on first checkbox   
  Then click on first dropdown 
  Then click on radiobutton 'distinct values from field' 
  Then Choose parallel dropdown
  Then Choose Series Display first dropdown
  Then Choose Series Display second dropdown
  Then Click Apply button
  ########Then click on Source XML
  #######Then Copy the XML
  Then click on Finish button
  Then Compare SharePointListData XML  
  
  Given Click on Chart Wizard Dropdown
  Then Click on Filter Data
  And click on Clear All Filters
    
  Then click on Group Data
  And click on clear grouping button 
    
  Then click on Top N Records
  And click on clear Top N Records
  Then click on Finish button
   #Then Send Html Report in mail
   
   
   
   
   
   
   
   
   

#  @CSVDataSelectionRegression
Scenario: CSV DataSelection Checking
  
  Given Click on Chart Wizard Dropdown
 
  Then format the Sharepoint list to CSV data source
  
  Then Select Radio Button of static CSV data
  Then Give data in CSV Data Field
  Then Click on Parse Data Button
  Then click on OK button
  Then go to Select Fields
  And ticked all Select Field Checkbox
  And click on Apply button
  Then click on Top N Records
  #And click on No button
  Then click on Show top first Dropdown
  Then click on Show top Second Dropdown
  Then click Apply Button
  
  Then Click on Filter Data
  Then Click on Delete Button
  Then Click on Add Button
  Then includes all dates
  Then click on first Dropdown
  Then click on second Dropdown
  Then send CSV search data
  Then click on Filter Data Apply button
  
 Then click on Group Data
  #And click on Yes button
  Then click on first checkbox   
  Then click on first dropdown 
  Then click on radiobutton 'distinct values from field' 
  Then Choose parallel dropdown
  Then Choose Series Display first dropdown
  Then Choose Series Display second dropdown
  Then Click Apply button
  ########Then click on Source XML
  #######Then Copy the XML
  Then click on Finish button
  Then Compare CSVData XML  
  
  
  Given Click on Chart Wizard Dropdown
  Then Click on Filter Data
  And click on Clear All Filters
    
  Then click on Group Data
  And click on clear grouping button 
    
  Then click on Top N Records
  And click on clear Top N Records
  Then click on Finish button
  #Then Send Html Report in mail
  
  
  
  
  
  
  
  
    
# @MicrosoftSQlServerDataSelectionRegression
Scenario: Microsoft SQl server DataSelection Checking
  
  Given Click on Chart Wizard Dropdown
  Then format the CSV to Microsoft SQl server data source  
  
  Then tick on Windows Authentication 
  Then Give Data in Server Field
  Then Give Data in Database field
  #Then tick on Windows Authentication 
  Then click on connect button
  Then click on OK button
  Then Select data table
  Then go to Select Fields  
  And ticked all Select Field Checkbox
         
   
    
  And click on Apply button   
  Then click on Top N Records
  #And click on No button
  Then click on Show top first Dropdown
  Then click on Show top Second Dropdown
  Then click Apply Button
  
  Then Click on Filter Data
  #And click on Yes button
  Then Click on Delete Button
  Then Click on Add Button
  Then includes all dates
  Then click on first Dropdown
  Then click on second Dropdown
  Then send SQL server search data
  Then click on Filter Data Apply button
  
  Then click on Group Data
  #And click on Yes button
  Then click on first checkbox   
  Then click on first dropdown 
  Then click on radiobutton 'distinct values from field' 
  Then Choose parallel dropdown
  Then Choose Series Display first dropdown
  Then Choose Series Display second dropdown
  Then Click Apply button
  ########Then click on Source XML
  #######Then Copy the XML
  Then click on Finish button
  Then Compare SQLServerData XML   
  
  Given Click on Chart Wizard Dropdown
  Then Click on Filter Data
  And click on Clear All Filters
    
  Then click on Group Data
  And click on clear grouping button 
    
  Then click on Top N Records
  And click on clear Top N Records
  Then click on Finish button
  #Then Send Html Report in mail
  
  
  
  
  
  
  
  
  
    
#  @WebPartsinCurrentpageDataSelectionRegression
Scenario: WebParts in Currentpage DataSelection Checking
  
  Given Click on Chart Wizard Dropdown
 
  Then format the Microsoft SQl server to WebParts data source     
  
  Then Click on Get Provider Parts
  Then click on OK button
  Then Select Current Web part
  Then go to Select Fields  
  And ticked all Select Field Checkbox
   
    
  And click on Apply button
  Then click on Top N Records
  #And click on No button
  Then click on Show top first Dropdown
  Then click on Show top Second Dropdown
  Then click Apply Button
  Then Click on Filter Data
  Then Click on Delete Button
  Then Click on Add Button
  Then includes all dates
  Then click on first Dropdown
  Then click on second Dropdown
  Then send search data
  Then click Apply button
  Then click on Group Data
  #And click on Yes button
  Then click on first checkbox   
  Then click on first dropdown 
  Then click on radiobutton 'distinct values from field' 
  Then Choose parallel dropdown
  Then Choose Series Display first dropdown
  Then Choose Series Display second dropdown
  Then Click Apply button
  ########Then click on Source XML
  #######Then Copy the XML
  Then click on Finish button
  Then Compare XML   
  
  
  Given Click on Chart Wizard Dropdown
  Then Click on Filter Data
  And click on Clear All Filters
    
  Then click on Group Data
  And click on clear grouping button 
    
  Then click on Top N Records
  And click on clear Top N Records
  Then click on Finish button
  #Then Send Html Report in mail
  
  
  
  
  
  
  
  
  
  
  
  
 
# @FormattingRegression   
Scenario: This is just a formatting of Excel Datasource
    Given Click on Chart Wizard Dropdown
    Then format the data source 
 
  
#@DataSelectionRegression
Scenario: Single Series Column2D Chart Type 'Data Source' Selection checking
  # Given Configuration of Web Part
    Given Click on Chart Wizard Dropdown
    Then choose Excel Data Provider
    Then Give Url of Excel
    Then click on Load Button
    And click on ok button
    Then select Sheet name
     
    Then click on connect button
    Then click on OK button
    Then go to Select Fields  
    
    And ticked all Select Field Checkbox
    And uncheck First field Checkbox
    And uncheck fourth field Checkbox
    And uncheck fifth field Checkbox
    And uncheck Sixth field Checkbox
   
    
    And click on Apply button
    Then click on Finish button
    Then save chart
  # Then Generate TestScenario Result Report
 
  
 # @DataSelectionClearRegression
Scenario: Single Series Column2D Chart Type Selection Data Selection 'Clear' Button Checking
    
    Given Click on Chart Wizard Dropdown
    Then Click on Filter Data
    And click on Clear All Filters
    
    Then click on Group Data
    And click on clear grouping button 
    
    Then click on Top N Records
    And click on clear Top N Records
    Then click on Finish button
    #Then Send Html Report in mail
  
  
  
  
 