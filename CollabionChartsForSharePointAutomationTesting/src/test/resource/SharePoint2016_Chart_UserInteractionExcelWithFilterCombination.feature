Feature: SharePoint 2016 User Interaction Feature With SharePoint Webpart Filter Combination(Data Source Excel table) 




@DataSourceExcelList(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Excel DataSelection Checking 
#Given Click on SharePoint SignIn
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	#Given Click on Chart Wizard Dropdown 
	Then format the CSV to Excel data source 
	And click on Yes button
	Then Give New Url of Excel 
	Then click on Load Button 
	#And click on Yes button
	And click on ok button 
	Then select Sheet name 
	
	Then click on connect button 
	Then click on OK button 
	Then go to Select Fields 
	
	And ticked all Select Field Checkbox 
	# And uncheck First field Checkbox
	# And uncheck fourth field Checkbox
	# And uncheck fifth field Checkbox
	# And uncheck Sixth field Checkbox
	
	
	And click on Apply button 
	
	
	
	
@SeriesFunction(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: SeriesFunction Testing 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then click on Group Data 
	Then click on clear grouping button 
	Then click on first checkbox 
	Then click on first dropdown 
	#Then click on Group By dropdown
	Then click on Series Function Dropdown 
	Then click combo dropdown item 
	#Then choose combo dropdown option
	
	Then Click Apply button 
	Then click on Finish button 
	
	
@WizardFirstDrilldownSetWithGroupByFields(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify 'User Interaction'  WizardFirstDrilldownSetWithGroupByFields 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select First DrillDown option 
	#Then Select Group by Fields
	
	Then Choose Sort By Dropdown 
	
	
	#   Given Click on Configuration
	#   Then Give Chart Title
	#   Then Give Chart Sub Title
	#  #Then Give X-Axis Title
	#  #Then Give Primary Y-axis Title
	#  #Then Rotate Y-axis Title
	#  #Then Filled Y-axis width
	#   Then Click on Apply to all  
	#   Then Click on ok button 
	
	Then Enable Multi level DrillDown 
	Then Select First DrillDown Field 
	Then click on Wizard Drilldown Apply button 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@SQLServerDatabaseMultiLevelDrillDownUI(SharePoint2016) 
Scenario: Verify Multi Level DrillDownUI 
	#Then Click on the Site Pages HyperLink 
	#Then Click on the DrillDownPage HyperLink 
	Then Click on any bar in the chart 
	Then Click on a bar on the Second level DrillDown 
	
@MultiLevelDrillDownUI(SharePoint2016) 
Scenario: Verify Multi Level DrillDownUI 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	#Then Click on the Site Pages HyperLink 
	#Then Click on the DrillDownPage HyperLink 
	Then Click on SharePointList First Level bar in the chart 
	Then Click on SharePointList Second Level bar in the chart 
	
@ExcelListMultiLevelDrillDownUI(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify Multi Level DrillDownUI 
	#Then Click on the Site Pages HyperLink 
	#Then Click on the DrillDownPage HyperLink 
	Then Click on ExcelList First Level bar in the chart 
	Then Click on ExcelList Second Level bar in the chart 
	
	
@TextFilterDataDrivenTesting(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify Text Filter search with multiple data 
	Then First Give data in Text filter And Search 
	
	
@ChoiceFilterDataDrivenTesting(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify Choice Filter search with multiple data 
	Then First Give data in Choice Filter And Search 
	
	
	
	
@WizardDynamicFilterSecond(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify 'User Interaction' with Wizard Dynamic Filter Second 
#Given Click on SharePoint SignIn
#Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Given Click on Dynamic filter 
	#And click on Yes button
	Then Allow viewers to filter data dynamically 
	Then Allow filtering only on the fields and data shown on chart 
	Then click on Finish button 
	
	
@CombinationOfText&DynamicFilter-UI(SharePoint2016) 
Scenario: Verify Combination Of Text & DynamicFilter-UI 
	#Given Click on SharePoint SignIn 
	Given provide a dynamic Filter 
	Then Provide a value for the TextFilter. 
	
	
@DynamicFilterDataDrivenTesting(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify Wizard Dynamic Filter search with multiple data 
	#Given Click on SharePoint SignIn 
	Then First Give data in Dynamic filter From Excel 
	
@ChoiceFilter&DynamicFilterCombination(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify ChoiceFilter & DynamicFilter Combination 
#Dynamic Filter Use
	#Given Click on SharePoint SignIn 
	Given provide a dynamic Filter 
	Given Choose Choice Filter 
	
	
@Hotspot(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify 'User Interaction' using Hotspot 
#Given Click on SharePoint SignIn
#Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Second DrillDown option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
	
@DrillDownNavigateToLink(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify 'User Interaction' with Link DrillDown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Third DrillDown option 
	Then Give Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
	
@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(SharePoint2016)
@SP2016ExcelListRegression 
Scenario: Verify 'User Interaction' using QueryString Filter 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Fourth DrillDown option From Wizard 
	Then Give Link Url 
	Then Choose open url option 
	Then click on Finish button 
	Then click on DrillDown Bar 
	
@NoDrillDown(SharePoint2016) 
@SP2016ExcelListRegression
Scenario: Verify 'User Interaction' with NoDrillDown 
	#Given Click on SharePoint SignIn 
	#Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Drill Down 
	Then Select Fifth DrillDown option 
	Then click on Finish button 
	
	###########--------prerequisites--------
	
	###########--------1)Clear tick 'Print Chart'---------------------
	###########--------2)Clear tick 'Exporting chart data as Excel Spreadsheet'----------------
	###########--------3)Clear tick 'Exporting charts as image or PDF'-------------
	
@ExportSettings(SharePoint2016) 
Scenario: Verify 'User Interaction' Export Settings 
#Given Click on SharePoint SignIn
#Then Click on SharePoint Page
	Then Click on SharePoint 2016 Chart Edit 
	Given Click on Export Settings 
	#Then Click on Revert Button.
	#And click on Yes button
	Then tick on Enable printing of chart 
	Then tick on Enable exporting chart data as Excel Spreadsheet 
	Then tick on Enable exporting charts as image or PDF 
	Then give export file name 
	#And click on Apply button
	Then click on Finish button 
	Then click on Export Chart Button 
	
@Macro/DynamicTitle(SharePoint2016) 
Scenario: Verify 'User Interaction' using Dynamic Title 
	Given Click on SharePoint SignIn 
	Then Click on SharePoint Page 
	Then Click on SharePoint 2016 Chart Edit 
	Then Click on Filter Data 
	Then click on first Dropdown 
	Then click on second Dropdown 
	Then send search data 
	Then click on Filter Data Apply button 
	Then Click on Captions 
	# And click on Yes button
	And Enter text in Chart Title Field 
	And Enter text in Chart Sub Title Field 
	Then click on Finish button 
	#And click on Yes button
