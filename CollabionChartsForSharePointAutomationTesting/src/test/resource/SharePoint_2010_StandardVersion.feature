Feature: CCSP Standard version And Licensing test cases for SharePoint Server 2010

  Background: Edit button clicking
     Given Click on Chart Wizard Dropdown
 @DataSourceSelection2010
  Scenario: Verify Source Selection is performed properly
    Then Selects Dataprovider as ExcelSheet
    Then Provides the path of 2010 ExcelFile.
    Then Click on the load button.
    Then Click on the OK button.
    Then Select the Sheet Name.
    Then Click on the Connect button.
    Then Click on the OK button.
    Then Click on SelectFields

  #@WizardFilter
  #Scenario: Test Filter can be applied succesfully.

  @Grouping2010
  Scenario: Test Grouping Function Properly
    Given Click On Group Data and verify the statutory Message.
    Then  Enable Group Data.
    Then  Select the Group By Field.
    Then  Verify The 'Distinct Values' Option is not clickable. 
    Then  Selects the 'one or more fields' Option.
    Then  Verify that aggregate by 'Average' is not allowed.
    Then  Verify that aggregate by 'MIN' is not allowed.
    Then  Verify that aggregate by 'MAX' is not allowed.
    Then  Verify that aggregate by 'Sum' is allowed and performed succesfully.
    Then  Click Apply button
    Then  click on Finish button

  @TOP-N-RECORDS2010
  Scenario: Checking the Top-n-records for Standard Version.
    Then click on Top N Records
    Then Check The Option Background Color.
    Then Check whether the TOP-N-RECORDS Funtionalities are displaed.
    Then Verify Feature Not Available Message

  @chartConfiguration2010
  Scenario: Check whether Chart configuration is working properly
    Then click on Chart Type
    ##And click on Yes button
    Given Choose Single Series from Chart Category Dropdown
    Then Choose Column2D Chart
    Then Click on Captions
    And click on Yes button
    And Enter text in Chart Title Field
    And Enter text in Chart Sub Title Field
    And enter text in X-Axis Title Field
    And enter text in Primary Y-axis Title Field
    And Untick Rotate Y-Axis Title Checkbox
    #Then Click Series Customization
    Then Click Captions Apply Button
    Then Click Series Customization
    #And click on Yes button
    Then Choose XAxis Label Column
    Then Choose XAxis Sort Column
    Then Choose XAxis Sort Order
    Then Click on Series
    Then Choose Series value Column
    Then Click on Labels,Values & Tool-tips
    And click on Yes button
    Then Choose Display Type
    Then choose nth Label
    Then choose Number of StaggerLines
    And tick on checkbox Rotate_Values_When_Displayed
    And tick on checkbox Place_Values_Inside_Data_Plot
    Then Click on Data Plot
    Then tick on checkbox Use_Round_Edges
    Then Select Plot Fill Angle
    Then Select Fill Opaqueness
    And tick on ShowShadow
    And Choose Gradient Color
    And Give Data Plot Color
    And Give Border Thickness
    And Plot Border Opaqueness
    Then tick on Show_Dashed_Plot_Border
    Then Give Dash Length
    Then Give Dash Gap Length
    Then Show Plot Border
    #And select on Show Plot Border
    Then click on Tool-Tips tab
    Then click Include Shadow
    #And Give Tool-Tips Border Color
    #And Give Tool-Tips Background Color
    Given Click on Cosmetics
    And click on Yes button
    Then Give Chart Background
    # Then Give Border Cosmetics Color
    Then Give Border Data
    And Click on Canvas
    And Give First Opaqueness
    #And Give Background Color
    And Give Thickness
    And Give Second Opaqueness
    # And Give Border Color
    And Click on Margin & Padding
    Then Give Left margin
    Then Give Right Margin
    Then Give Top Margin
    Then Give Bottom Margin
    Then Give Title Padding
    Then Give Y Axis Title Padding
    Then Give Label Padding
    Then Give X Axis Title Padding
    Then Give Y Axis Value Padding
    Then Give Value Padding
    Given Click on Fonts
    Then Give Customize Font Name
    Then Give Customize Font Size
    ## Then Give Customize Font Color
    Then Give side canvas Font Name
    Then Give side canvas Font Size
    ## Then Give side canvas Font Color
    Given Click on Zero Plane
    Then Tick on Show Zero pLane
    # Then Give 2D chart color
    Then Give 2D chart Thickness
    Then Give 2D chart Opaqueness
    Given Click on Custom Branding
    Then Specify logo URL
    Then Give Link
    Then Give Position
    Then Give Branding Opaqueness
    Then Give Branding Scale
    Then Click on Number Formatting
    And click on Yes button
    Then Give Data in Prefix & Suffix Field
    Then click on Format numbers
    Then click on European numbers
    Then Give Scales to be used
    Then Give Round numbers
    Then Give Round axis
    Then Click on Force exact number
    Then Click on Force Y Axis decimal values
    Given Click on Axis
    And click on Yes button
    Then Set adaptive lower limit for axis
    Then Set Force decimals on primary y-axis values
    Then Give Y-Axis minimum value,maximum value,set lower limit,primary y-axis value
    Then Click on Axis Grid Lines
    Then Give Number of grid lines
    Then Give Grid Line Thickness
    # Then Choose Grid Line color
    Then Give Grid Line Opaqueness
    Then Show grid lines as dashed
    Then Give Dash length
    Then Give Dash gap
    # Then give alternate color
    Then give alternate Opaqueness
    Then Tick Show alternate color bands
    Given Click on Trend Lines
    And click on Yes button
    Then Give Start Value
    Then Give End Value
    Then Give Display Text
    Then give Tool-tip Text
    Then Click on as filled color zone
    Given Click on Other Settings
    And click on Yes button
    Then Give Chart Loading Message
    Then Give Data Loading Message
    Then Give Data Processing Message
    Then Give No-data to display Message
    Then Give Pre-Rendering Message
    Then Give Error in Loading Message
    Then Give Error in Data Message

  @VerifyConnectionString2010
  Scenario: verify whether The connection Fields page is disabled and Proper Error Message is displayed.
    Given Click on Other Settings
    Then Click on Connection Fields
    Then Verify Feature Not Available Message

  @DrilldownNonClickableFieldCheck2010
  Scenario: Verify 'User Interaction' using Hotspot
  
  
    Then Click on Drill Down
    Then Check Drill Down Non-Clickable Field
    Then click on Finish button
    Then click on DrillDown Bar

  @WizardDynamicFilterNonClickableFieldCheck2010
  Scenario: Verify 'User Interaction' with Wizard Dynamic Filter Second
    Given Click on Dynamic filter
    Then Check Dynamic Filter Non-Clickable Field
    Then click on Finish button

  @ExportSettingsNonClickableFieldCheck2010
  Scenario: Verify 'User Interaction' Export Settings
    Given Click on Export Settings
    Then Check Export Settings Non-Clickable Field
    Then click on Finish button

  @DrillDownFeatureNotAvailableVerification2010
  Scenario: Verify DrillDown_FeatureNotAvailable
    Then Click on Drill Down
    Then Verify Feature Not Available Message

  @DynamicFilterFeatureNotAvailableVerification2010
  Scenario: Verify DynamicFilter_FeatureNotAvailable
    Given Click on Dynamic filter
    Then Verify Feature Not Available Message

  @ExportSettingsFeatureNotAvailableVerification2010
  Scenario: Verify ExportSettings_FeatureNotAvailable
    Given Click on Export Settings
    Then Verify Feature Not Available Message

  @DrillDownSelectionBarColorVerification2010
  Scenario: Verify DrillDown SelectionBar_ColorVerification
    Then Click on Drill Down
    Then Verify Selection Bar color

  @DynamicFilterSelectionBarColorVerification2010
  Scenario: Verify DynamicFilter SelectionBar_ColorVerification
    Given Click on Dynamic filter
    Then Verify Selection Bar color

  @ExportSettingsSelectionBarColorVerification2010
  Scenario: Verify ExportSettings SelectionBar_ColorVerification
    Given Click on Export Settings
    Then Verify Selection Bar color

