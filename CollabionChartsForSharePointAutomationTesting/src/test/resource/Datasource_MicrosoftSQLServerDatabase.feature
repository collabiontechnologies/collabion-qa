Feature: Collabion feature 
         Test cases for the 
         'MicrosoftSQLServer'Datasource 
          Includes Configure Source,
          Data Selection,User Interaction


@caseid34
Scenario: Configure 'Data Source'  
    Given Configuration of Web Part

    Given Click on Chart Wizard Dropdown
    Then choose Microsoft SQL Server
    Then tick on Windows Authentication 
    Then Give Data in Server Field
    Then Give Data in Database field
    Then click on connect button
    Then click on OK button
    Then Select data table
    Then go to Select Fields  
    And ticked all Select Field Checkbox
    And click on Apply button
    Then click on Finish button
    
    Then save chart
    
    