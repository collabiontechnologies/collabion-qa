package com.cucumber.CollabionChartsForSharePointAutomationTesting;

import java.io.*;
import org.apache.poi.xssf.usermodel.*;

public class ExcelUtil {

	private XSSFSheet ExcelWSheet;
	private XSSFWorkbook ExcelWBook;

	// Constructor to connect to the Excel with sheetname and Path
	public ExcelUtil(String Path, String SheetName) throws Exception {

		try {
			// Open the Excel file
			FileInputStream ExcelFile = new FileInputStream(Path);

			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			// ExcelWSheet.getRow(1).createCell(5).setCellValue("Pass");
			// ExcelWSheet.getRow(2).createCell(5).setCellValue("Fail");

		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
	}

	// This method is to set the rowcount of the excel.
	public int excel_get_rows() throws Exception {

		try {
			return ExcelWSheet.getPhysicalNumberOfRows();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}

	public int excel_get_columns() throws Exception {
		try {
			return ExcelWSheet.getDefaultColumnWidth();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return 0;

	}

	// This method to get the data and get the value as strings.
	public String getCellDataasstring(int RowNum, int ColNum) throws Exception {

		try {
			String CellData = ExcelWSheet.getRow(RowNum).getCell(ColNum).getStringCellValue();

			return CellData;

		} catch (Exception e) {

			System.out.println("return 'Errors in Getting Cell Data'");
		}
		return null;
	}

	///// This method to get the data and get the value as number.
	public double getCellDataasnumber(int RowNum, int ColNum) throws Exception {

		try {
			double CellData = ExcelWSheet.getRow(RowNum).getCell(ColNum).getNumericCellValue();

			System.out.println("The value of CellData " + CellData);
			return CellData;
		} catch (Exception e) {
			return 000.00;
		}
	}
}
	