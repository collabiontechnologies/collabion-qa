package com.cucumber.CollabionChartsForSharePointAutomationTesting;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.WebDriverWait;


public class DriverFactoryPage {

	  public static WebDriver driver = null;
	  public static WebDriverWait waitVar = null;

	 /////////SharePoint2010////////////
	  
  //     public static String baseURL = "http://Administrator:P@ssw0rd@pc-nibir2010/SitePages/NewChartForAutomation.aspx";
       
   //////SharePoint2010-Pro Version/////////// public static String baseURL = "http://Administrator:P@ssw0rd@pc-nibir2010/SitePages/NewTestAutomation.aspx";

	  
	 // public static String baseURL = "http://Administrator:P@ssw0rd@pc-nibir2010/SitePages/ChartNewCreation.aspx";
      /////////SharePoint2016-Standard Version//////////////public static String baseURL = "http://spadmin:P@ssw0rd@ccsptestsvr/SitePages/CollabionStandardAutomation.aspx";

	  /////////SharePoint2016-Pro Version////////////  public static String baseURL = "http://spadmin:P@ssw0rd@ccsptestsvr/SitePages/CollabionStandardAutomation.aspx";
      //
	  public static String baseURL = "http://Administrator:P@ssw0rd@ccsptestsvr/SitePages/CollabionStandardAutomation.aspx";

	  /////////SharePoint2010-License Page//////////////    public static String baseURLofLicense = "http://Administrator:P@ssw0rd@ccsptestsvr:2016/_admin/CollabionCharts/CollabionChartActivationPage.aspx";

       ///////////////////Publishing Page/////////////////////////  
	  //public static String baseURL = "http://Administrator:P@ssw0rd@pc-nibir2010:8080/Pages/Test666.aspx";
	 // public static String baseURL = "http://Arkaprovo:P@ssw0rd@pc-nibir2010:8080/Pages/Test666.aspx";
	
	
	
		  //NewSqlServerChart.aspx
		  ///NewSharepointlistchart.aspx
	  /**
	   *  This function is to invoke Selenium Webdriver
	   * 
	   * @throws InterruptedException
	 * @throws IOException 
	   */
	 
	  public void createDriver() throws InterruptedException, IOException {
		  
		 
//		  ProfilesIni profile = new ProfilesIni();
//		  FirefoxProfile ffprofile = profile.getProfile("default");
//	
//		  try {
//			ffprofile.addExtension(new File("C:\\Users\\jayati.chowdhury\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\fd5tecug.default\\extensions\\firebug@software.joehewitt.com.xpi"));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		  
//		 driver = new FirefoxDriver(ffprofile);

		  //Original FF Driver call
		  ProfilesIni allProfiles = new ProfilesIni();
		  FirefoxProfile profile = allProfiles.getProfile("SharePoint.QA");
		  driver = new FirefoxDriver(profile);
		  
		// driver = new FirefoxDriver();
		  
	

	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

        driver.get(baseURL);
        //Thread.sleep(3000);
//        driver.findElement(By.xpath(".//*[@id='ctl00_IdWelcome_ExplicitLogin']")).click();
        
        
    
//       Alert alert=driver.switchTo().alert();
//       String alertString=alert.getText();
//       System.out.println(alertString);
//       alert.authenticateUsing(new  UserAndPassword("Administrator","P@ssw0rd"));
//       alert.accept();
       
//	    WebDriverWait wait = new WebDriverWait(driver, 10);      
//	    Alert alert = wait.until(ExpectedConditions.alertIsPresent());
////	    driver.switchTo().alert();
//	    alert.authenticateUsing(new UserAndPassword("Administrator","P@ssw0rd"));

	    waitVar = new WebDriverWait(driver, 15);
	  }

	  
	  /**
	   * This function is to close driver instance
	   */
	  public void teardown() {
	    driver.quit();
	  }
	}