package com.cucumber.CollabionChartsForSharePointAutomationTesting;

import java.io.IOException;
import java.net.MalformedURLException;

import cucumber.api.java.After;
//import com.github.mkolisnyk.cucumber.reporting.CucumberResultsOverview;

import com.cucumber.CollabionChartsForSharePointAutomationTesting.DriverFactoryPage;

import cucumber.api.java.Before;

public class HooksPage {
	StringBuffer verificationErrors;
	DriverFactoryPage df = new DriverFactoryPage();

	@Before
	public void beforeFeature() throws InterruptedException, IOException {

		verificationErrors = new StringBuffer();

		df.createDriver();

	}

@After
	public void afterScenario() throws Exception {

		Thread.sleep(10000);
		if (!(verificationErrors.length() == 0)) {

			verificationErrors.toString();
		}
		df.teardown();
	}

}
