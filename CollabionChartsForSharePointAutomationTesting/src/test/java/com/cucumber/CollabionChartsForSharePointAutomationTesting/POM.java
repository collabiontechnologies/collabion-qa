package com.cucumber.CollabionChartsForSharePointAutomationTesting;

import java.util.List;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

//import javax.mail.BodyPart;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.Multipart;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.security.Credentials;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
//import org.xml.sax.InputSource;
//import org.xml.sax.SAXException;
import org.xml.sax.SAXException;

import org.im4java.core.CompareCmd;
import org.im4java.process.StandardStream;
import org.im4java.core.IMOperation;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WriteException;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.cs.Ale;
import cucumber.api.java.en.*;
import cucumber.api.java.it.E;
import gherkin.formatter.model.DataTableRow;

import java.util.*;
//import javax.mail.*;
//import javax.mail.internet.*;
import javax.activation.*;
import com.cucumber.CollabionChartsForSharePointAutomationTesting.DriverFactoryPage;
import com.cucumber.CollabionChartsForSharePointAutomationTesting.ExcelUtil;

//import com.github.mkolisnyk.cucumber.reporting.*;
public class POM extends DriverFactoryPage {
	StringBuffer verificationErrors;
	String ExecResult = "PASS", ErrorDesc = " ";


	// private static Document doc;

	public void GoToUrl() throws IOException {
		// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

		try {
			driver.findElement(By.id("//*[@id='ctl00_PlaceHolderPageTitleInTitleArea_wikiPageNameDisplay']")).getText();
			Assert.assertTrue(driver.getPageSource().contains("Configure Source"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("GoToUrl() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 1, 2);
			ExcelWriter(ExecResult, ErrorDesc, 1, 3);
			// System.out.print(" GoToUrl() finally passed ");

		}
	}
	
	
	
	public void SharePoint2010SignIn() throws IOException{
	try{
		Thread.sleep(5000);
		driver.findElement(By.xpath(".//*[@id='ctl00_IdWelcome_ExplicitLogin']")).click();
		Runtime.getRuntime().exec("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\New AutoIt v3 Script.exe");
		
	/*	//Thread.sleep(10000);
		// before opening the new window
		String parentWindow = driver.getWindowHandle();

		// after the new window was closed
		driver.switchTo().window(parentWindow);*/
		
		
		
		
		
		//Runtime.getRuntime().exec("AuthenticationHandle.exe");
		//New AutoIt v3 Script.exe
		//driver.navigate().to("http://Administrator:P@ssw0rd@pc-nibir2010/SitePa Runtime.getRuntime().exec("C:\\AutoITScript\\AuthenticationHandle.exe");ges/CCSPAutomationTestingDontTouch.aspx");
		Thread.sleep(2000);
		/*Alert alert = driver.switchTo().alert();
		alert.authenticateUsing((Credentials) new UsernamePasswordCredentials("Administrator", "P@ssw0rd"));
		*/
/*		
		Robot rb =new Robot();
	    rb.keyPress(KeyEvent.VK_ENTER);
	    rb.keyRelease(KeyEvent.VK_ENTER);
	    
	    rb.keyPress(KeyEvent.VK_SHIFT);
	    rb.keyPress(KeyEvent.VK_A);
	    
	    rb.keyRelease(KeyEvent.VK_A);
	    rb.keyRelease(KeyEvent.VK_SHIFT);*/
	    
	} catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("Sharepoint2010SignIn()fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("Sharepoint2016SignIn() fail in catch:" +
		// ErrorDesc);
		//ExcelWriter(ExecResult, ErrorDesc, 470, 2);
		//ExcelWriter(ExecResult, ErrorDesc, 470, 3);
		// System.out.print(" Sharepoint2016SignIn() finally passed ");

	}
	}

	///////////////////////// SharePoint2016//////////////////////////
	public void Sharepoint2016SignIn() throws IOException {
		try {
			driver.findElement(By.id("O365_MainLink_SignIn")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Sharepoint2016SignIn()fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 383, 2);
			ExcelWriter(ExecResult, ErrorDesc, 383, 3);
			// System.out.print(" Sharepoint2016SignIn() finally passed ");

		}
	}

	public void Sharepoint2016Page() throws IOException {
		try {
			driver.findElement(By.xpath("//span[text()='Page']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Sharepoint2016Page() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016Page() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 384, 2);
			ExcelWriter(ExecResult, ErrorDesc, 384, 3);
			// System.out.print(" Sharepoint2016Page() finally passed ");

		}
	}

	public void Sharepoint2016Edit() throws IOException {
		try {
			driver.findElement(By.xpath("//span[text()='Edit']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Sharepoint2016Edit() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016Edit() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 385, 2);
			ExcelWriter(ExecResult, ErrorDesc, 385, 3);
			// System.out.print(" Sharepoint2016Edit() finally passed ");

		}
	}

	public void Sharepoint2016ChartEdit() throws IOException {
		try {
		Thread.sleep(3000);
			// driver.findElement(By.className("js-webpart-menuCell")).click();
			driver.findElement(By.xpath("//span[text()='Edit']")).click();
			// driver.findElement(By.xpath("//*[@id='WebPartWPQ2_MenuControlBox']/div")).click();

			// driver.findElement(By.xpath(".//*[@id='WebPartWPQ14_MenuLink']")).click();

			driver.findElement(By.xpath("//*[@id='WebPartWPQ4_MenuControlBox']/div")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//span[text()='Edit This Chart']")).click();
			// driver.findElement(By.xpath("//span[text()='Edit This
			// Chart']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Sharepoint2016ChartEdit() fail" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 386, 2);
			ExcelWriter(ExecResult, ErrorDesc, 386, 3);
			// System.out.print(" Sharepoint2016ChartEdit() finally passed ");

		}
	}

	public void ChartEdit() throws IOException {
		try {
			driver.findElement(By.xpath(".//*[@id='WebPartWPQ14_MenuLink']")).click();
			driver.findElement(By.xpath("//span[text()='Edit This Chart']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartEdit() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("ChartEdit() fail in catch" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 388, 2);
			ExcelWriter(ExecResult, ErrorDesc, 388, 3);
			// System.out.print(" ChartEdit() finally passed ");

		}
	}

	public void NewUrlOfExcel() throws IOException {
		try {
		Thread.sleep(4500);
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://ccsptestsvr/Shared Documents/SalesDataSheet.xlsx");
			/// Urlfield.sendKeys(ExcelName);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("NewUrlOfExcel() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("NewUrlOfExcel() fail in catch" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 387, 2);
			ExcelWriter(ExecResult, ErrorDesc, 387, 3);
			// System.out.print(" NewUrlOfExcel() finally passed ");

		}
	}

	public void NewUrlOfExcel2010() throws IOException {
		try {
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://pc-nibir2010/Shared Documents/SalesDataSheet.xlsx");
			/// Urlfield.sendKeys(ExcelName);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("NewUrlOfExcel() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("NewUrlOfExcel() fail in catch" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 387, 2);
			ExcelWriter(ExecResult, ErrorDesc, 387, 3);
			// System.out.print(" NewUrlOfExcel() finally passed ");

		}
	}

	public void SharePointSelectList2016() throws InterruptedException {

		WebElement SelectList = driver.findElement(By.id("dsEntity"));
		SelectList.click();
		// SelectList.sendKeys(Keys.ENTER);
		Thread.sleep(10000);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		// String jQuerySelector =
		// "CCSP.jQuery(\".x-combo-list-item:eq(14)\").click();";
		// String jQuerySelector =
		// "CCSP.jQuery(\".x-combo-list-item:eq(11)\").click()";
		String jQuerySelector = "CCSP.jQuery(\".x-combo-list-item:eq(23)\").click()";

		String findDropdown = "return " + jQuerySelector;
		System.out.println(findDropdown);

		js.executeScript(findDropdown);

		/*
		 * //Thread.sleep(1000); WebElement DropDownValue =
		 * driver.findElement(By.xpath("//body/div[17]/div/div[7]"));
		 * DropDownValue.click();
		 */
		/*
		 * Thread.sleep(1000); WebElement DropDownValue =
		 * driver.findElement(By.xpath(".//*[@class='x-combo-list-item']/div[7]"
		 * )); DropDownValue.click();
		 */

		// .//*[@class='x-combo-list-item']/div[7]

		// x-combo-list-item
		// SelectList.sendKeys(Keys.ARROW_DOWN);
		// SelectList.sendKeys(Keys.ENTER);
		// Thread.sleep(1000);
		// List<WebElement>dropdownList=new WebDriverWait(driver,10).until(Ex)

		// WebElement DropDownValue =new
		// WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(By.xpath("//body/div[17]/div/div[7]")));
		/*
		 * WebElement DropDownValue =
		 * driver.findElement(By.xpath("//body/div[17]/div/div[7]"));
		 * DropDownValue.click();
		 */

		// WebElement SelectList = driver.findElement(By.id("dsEntity"));

		// SelectList.sendKeys(Keys.ARROW_DOWN);
		// SelectList.sendKeys(Keys.ENTER);
	}
	/*
	 * Thread.sleep(6000); WebElement DropDownValue =
	 * driver.findElement(By.xpath("//div[7]")); DropDownValue.click();
	 */

	/*
	 * SelectList.click(); Thread.sleep(5400); WebElement DropDownValue =
	 * driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[7]"));
	 * DropDownValue.click();
	 */

	/*
	 *
	 *
	 *
	 *
	 *
	 *
	 * //WebElement DropDownValue =
	 * driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[9]"));
	 *
	 *
	 *
	 *
	 * //Thread.sleep(4500);
	 *
	 *
	 * /*Thread.sleep(5000); try { Assert.assertNotNull("SelectList");
	 *
	 * // Assert.assertTrue(SelectList.isSelected());
	 *
	 * } catch (AssertionError e) {
	 *
	 * // String message = e.getMessage(); System.out.println(
	 * "SharePoint Select List Dropdown Element Not Found: " + e.getMessage());
	 * } // SelectList.sendKeys(Keys.ENTER); // Thread.sleep(11000);
	 */

	///////////////////////// End of SharePoint2016//////////////////////////
	public void WebPart_Configuration() throws InterruptedException, IOException {

		try {
			driver.findElement(By.xpath(".//*[@id='Ribbon.WikiPageTab-title']/a/span[1]")).click();
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='Ribbon.WikiPageTab.EditAndCheckout.SaveEdit-SelectedItem']/span[2]"))
					.click();
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='Ribbon.EditingTools.CPInsert-title']/a/span[1]")).click();
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='Ribbon.EditingTools.CPInsert-title']/a/span[1]")).click();
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='Ribbon.EditingTools.CPInsert.WebParts.WebPart-Large']/span[2]"))
					.click();
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='ctl00_WebPartAdder_tbl']/tbody/tr[2]/td[2]/div[1]/div[3]")).click();
			Thread.sleep(1500);
			driver.findElement(
					By.xpath(".//*[@id='ctl00_WebPartAdder_tbl']/tbody/tr[2]/td[4]/table/tbody/tr[1]/td[1]/div/div[3]"))
					.click();
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='ctl00_WebPartAdder_tbl']/tbody/tr[3]/td/div/button[1]")).click();
			Thread.sleep(1500);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WebPart_Configuration() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("WebPart_Configuration() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 2, 2);
			ExcelWriter(ExecResult, ErrorDesc, 2, 3);
			// System.out.print(" WebPart_Configuration() finally passed ");

		}
	}

	public void SQLtoSharepointList() throws InterruptedException, IOException {
		try {
		   Thread.sleep(4900);
			//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			Thread.sleep(5100);
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[1]"));

			DropDownValue.click();

			Thread.sleep(6100);

			DataProvider.sendKeys(Keys.ENTER);

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SQLtoSharepointList() fail in catch:" + ErrorDesc);

		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 3, 2);
			ExcelWriter(ExecResult, ErrorDesc, 3, 3);

		}
	}

	public void SharepointlisttoCSV() throws InterruptedException, IOException {
		try {
			Thread.sleep(4500);
			// driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			Thread.sleep(5000);
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[4]"));
			DropDownValue.click();

			/*
			 * DataProvider.sendKeys(Keys.ARROW_DOWN);
			 *
			 * DataProvider.sendKeys(Keys.ARROW_DOWN);
			 *
			 * DataProvider.sendKeys(Keys.ARROW_DOWN);
			 *
			 * DataProvider.sendKeys(Keys.ENTER);
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SharepointlisttoCSV() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("SharepointlisttoCSV() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 4, 2);
			ExcelWriter(ExecResult, ErrorDesc, 4, 3);
			// System.out.print(" SharepointlisttoCSV() finally passed ");

		}

	}

	public void CSVtoExcel() throws InterruptedException, IOException {
		try {
		Thread.sleep(4000);
		//	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[6]"));
			DropDownValue.click();

			/*
			 * Thread.sleep(1100); DataProvider.sendKeys(Keys.ARROW_DOWN);
			 * Thread.sleep(1300); DataProvider.sendKeys(Keys.ARROW_DOWN);
			 * Thread.sleep(1500); DataProvider.sendKeys(Keys.ENTER);
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CSVtoExcel() in catch:" + ErrorDesc);

		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 5, 2);
			ExcelWriter(ExecResult, ErrorDesc, 5, 3);
			// System.out.print(" CSVtoExcel() finally passed ");

		}
	}

	public void ExceltoSQL() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			Thread.sleep(1000);
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[2]"));
			DropDownValue.click();

			/*
			 * Thread.sleep(1100); DataProvider.sendKeys(Keys.ARROW_UP);
			 * Thread.sleep(1300); DataProvider.sendKeys(Keys.ARROW_UP);
			 * Thread.sleep(1500); DataProvider.sendKeys(Keys.ARROW_UP);
			 * Thread.sleep(1700); DataProvider.sendKeys(Keys.ARROW_UP);
			 * Thread.sleep(1900); // DataProvider.sendKeys(Keys.ARROW_DOWN); //
			 * Thread.sleep(2100); DataProvider.sendKeys(Keys.ENTER);
			 */
			
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExceltoSQL() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("ExceltoSQL() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 6, 2);
			ExcelWriter(ExecResult, ErrorDesc, 6, 3);
			// System.out.print(" ExceltoSQL() finally passed ");

		}
	}

	public void save_chart() throws InterruptedException, IOException {

		try {
			Thread.sleep(6000);

			driver.findElement(By.xpath(".//*[@id='ctl00_PageStateActionButton']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("save_chart() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("save_chart() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 7, 2);
			ExcelWriter(ExecResult, ErrorDesc, 7, 3);
			// System.out.print(" save_chart() finally passed ");

		}
	}

	public void delete_chart() throws InterruptedException, IOException {
		try {

			driver.findElement(By.xpath(".//*[@id='Ribbon.WikiPageTab-title']/a/span[1]")).click();
			Thread.sleep(1500);
			driver.findElement(By.xpath(".//*[@id='Ribbon.WikiPageTab.EditAndCheckout.SaveEdit-SelectedItem']/span[2]"))
					.click();
			Thread.sleep(1500);
			Thread.sleep(15000);
			WebElement ChartStartingDropdown = driver.findElement(By.className("ms-WPHeaderTdMenu"));

			ChartStartingDropdown.click();
			ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1100);
			ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1200);
			ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1300);
			ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1400);
			ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1500);
			ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1600);
			ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1700);
			ChartStartingDropdown.sendKeys(Keys.ENTER);
			// WebElement ChartDelete =
			// driver.findElement(By.className("ms-MenuUIULLink"));
			// Assert.assertNotNull(ChartDelete);
			// ChartDelete.click(); }
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("delete_chart() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("delete_chart() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 8, 2);
			ExcelWriter(ExecResult, ErrorDesc, 8, 3);
			// System.out.print(" delete_chart() finally passed ");

		}
	}

	public void DropdownClick() throws InterruptedException, IOException {
		try {
			//Thread.sleep(15000);
			
			//ms-WPHeaderTdMenu
			 Actions a1 = new Actions(driver);
		     a1.moveToElement(driver.findElement
		     (By.id("chartPanelContainer"))).build().perform();
		     Thread.sleep(3000L);
			WebElement ChartStartingDropdown = driver.findElement(By.className("ms-WPHeaderMenuImg"));
			// .//*[@id='WebPartWPQ14_MenuLink']/img

			// WebElement ChartStartingDropdown =
			// driver.findElement(By.xpath(".//*[@id='WebPartWPQ14_MenuLink']/img"));
			ChartStartingDropdown.click();
			ChartStartingDropdown.click();
			// Thread.sleep(16000);
			WebElement ChartStartingDropdownOption = driver.findElement(By.xpath(".//*[@id='CCSP_EditChart']/span[1]"));
			ChartStartingDropdownOption.click();

			// Thread.sleep(15000);
			/*
			 * WebElement ChartStartingDropdown =
			 * driver.findElement(By.id("WebPartWPQ3_MenuLink"));
			 * Assert.assertNotNull("ChartStartingDropdown");
			 * ChartStartingDropdown.sendKeys(Keys.ARROW_DOWN);
			 * ChartStartingDropdown.sendKeys(Keys.ENTER);
			 * driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			 * Thread.sleep(5000);
			 */
			// Assert.assertTrue(driver.getPageSource().contains("Configure
			// Source"));

			Thread.sleep(15500);
			try {
				Assert.assertTrue(driver.getPageSource().contains("Configure Source"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(
						"Chart Wizard Starting Dropdown Element Not Found/DropdownClick() fail: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DropdownClick() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("DropdownClick() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 9, 2);
			ExcelWriter(ExecResult, ErrorDesc, 9, 3);
			// System.out.print(" DropdownClick() finally passed ");

		}

	}
	
/////////////////////////////CCSP 2.4 -Modification Import/Bulk Export feature/////////////////////////////	

public void SP16Import_ExportChartDropdown() throws IOException
	{
try{
			 Actions a1 = new Actions(driver);
		     a1.moveToElement(driver.findElement
		     (By.id("chartPanelContainer"))).build().perform();
		     Thread.sleep(3000L);
		
		driver.findElement(By.xpath("//span[text()='Edit']")).click();	
        driver.findElement(By.xpath("//*[@id='WebPartWPQ4_MenuControlBox']/div")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath(".//*[@id='CCSP_ImportChart']/span[1]")).click();
		
		
		
		
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Import_ExportChartDropdown() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectExcelDataProvider() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 422, 2);
			ExcelWriter(ExecResult, ErrorDesc, 422, 3);
			// System.out.print(" SelectExcelDataProvider() finally passed ");

		}
}
	public void Import_ExportChartDropdown() throws IOException
	{
		
		try{
			 Actions a1 = new Actions(driver);
		     a1.moveToElement(driver.findElement
		     (By.id("chartPanelContainer"))).build().perform();
		     Thread.sleep(3000L);
		
		WebElement ChartStartingDropdown = driver.findElement(By.className("ms-WPHeaderMenuImg"));
		ChartStartingDropdown.click();
        WebElement ChartStartingDropdownOption = driver.findElement(By.xpath(".//*[@id='CCSP_ImportChart']/span[1]"));
		ChartStartingDropdownOption.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Import_ExportChartDropdown() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectExcelDataProvider() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 422, 2);
			ExcelWriter(ExecResult, ErrorDesc, 422, 3);
			// System.out.print(" SelectExcelDataProvider() finally passed ");

		}

	
	}	
	
	public void ImportFromDropdown() throws IOException
	{
		try{
		Thread.sleep(1500);
		WebElement ImportFromDropdown = driver.findElement(By.xpath(".//*[@id='importmode']"));
		ImportFromDropdown.click();
		ImportFromDropdown.sendKeys(Keys.ARROW_DOWN);
		ImportFromDropdown.sendKeys(Keys.ENTER);
		}
		 catch (Exception e) {
				ExecResult = "FAIL";
				ErrorDesc = e.getMessage();
				System.out.println("ImportFromDropdown() fail in catch:" + ErrorDesc);
			} finally {
				// System.out.print("SelectExcelDataProvider() fail in catch:" +
				// ErrorDesc);
				ExcelWriter(ExecResult, ErrorDesc, 423, 2);
				ExcelWriter(ExecResult, ErrorDesc, 423, 3);
				// System.out.print(" SelectExcelDataProvider() finally passed ");

			}
		
	}
	
	public void SelectfileforImport() throws Exception
	{
		try{
		WebElement ImportFromDropdown = driver.findElement(By.xpath(".//*[@id='fbutton-file']"));
		
		ImportFromDropdown.sendKeys("C:\\ExportedCharts\\ExportedCharts.xml");
		//ImportFromDropdown.sendKeys(Keys.INSERT.);
		}
	   catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("SelectfileforImport() fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("SelectExcelDataProvider() fail in catch:" +
		// ErrorDesc);
		ExcelWriter(ExecResult, ErrorDesc, 424, 2);
		ExcelWriter(ExecResult, ErrorDesc, 424, 3);
		// System.out.print(" SelectExcelDataProvider() finally passed ");

	}

}
	
	
	public void ExportChartTabClick() throws Exception
	{
		try{
		driver.findElement(By.xpath("//span[text()='Export']")).click();
		}
	catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("ExportChartTabClick() fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("SelectExcelDataProvider() fail in catch:" +
		// ErrorDesc);
		ExcelWriter(ExecResult, ErrorDesc, 425, 2);
		ExcelWriter(ExecResult, ErrorDesc, 425, 3);
		// System.out.print(" SelectExcelDataProvider() finally passed ");

	}


		
	}
	
	public void ImportButtonClick() throws InterruptedException, IOException
	{
		try{
		Thread.sleep(3000);
		WebElement ImportFromDropdown = driver.findElement(By.xpath("//button[text()='Import']"));
		ImportFromDropdown.click();
		//ImportFromDropdown.click();
	
		driver.findElement(By.xpath("//button[text()='Yes']")).click();
		Thread.sleep(200);
		driver.findElement(By.xpath("//button[text()='OK']")).click();
		}
	  catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("ImportButtonClick() fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("SelectExcelDataProvider() fail in catch:" +
		// ErrorDesc);
		ExcelWriter(ExecResult, ErrorDesc, 426, 2);
		ExcelWriter(ExecResult, ErrorDesc, 426, 3);
		// System.out.print(" SelectExcelDataProvider() finally passed ");

	}

}
	/*WebElement ExportButtonClick=driver.findElement(By.xpath(".//*[@id='btnImport']/tbody/tr[2]/td[2]"));
	
	ExportButtonClick.click();*/
	//ExportButtonClick.sendKeys(Keys.ENTER);
		
	
	
	public void ExportButtonClick() throws AWTException, IOException
	{
		try{
			//Thread.sleep(3000);
		WebElement ExportButtonClick=driver.findElement(By.xpath(".//*[@id='btnImport']/tbody/tr[2]/td[2]"));
		
		ExportButtonClick.click();
	    //Thread.sleep(4000);
		//ExportButtonClick.sendKeys(Keys.ENTER);
      

		//ExportButtonClick.sendKeys(Keys.TAB);
	
		Thread.sleep(1000);
		 Robot rb =new Robot();
		 //rb.keyPress(KeyEvent.KEY_PRESSED);
		 rb.keyPress(KeyEvent.VK_TAB);
		 rb.keyRelease(KeyEvent.VK_TAB);
		 
		 rb.keyPress(KeyEvent.VK_ENTER);
		 rb.keyRelease(KeyEvent.VK_ENTER);
		 //rb.keyPress(KeyEvent.VK_ACCEPT);

		}
	catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("ExportButtonClick() fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("SelectExcelDataProvider() fail in catch:" +
		// ErrorDesc);
		ExcelWriter(ExecResult, ErrorDesc, 427, 2);
		ExcelWriter(ExecResult, ErrorDesc, 427, 3);
		// System.out.print(" SelectExcelDataProvider() finally passed ");

	}

}
		/*
		 Robot rb =new Robot();
		 rb.keyPress(KeyEvent.VK_ACCEPT);*/
		 //rb.keyPress(KeyEvent.VK_ACCEPT);
		
	
/////////////////////////////CCSP 2.4 -Modification Import/Bulk Export feature/////////////////////////////	END/////////////////////////
	public void SelectExcelDataProvider() throws InterruptedException, IOException {
		try {
		Thread.sleep(8000);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			// DataProvider.sendKeys("Microsoft Office Excel File");
			DataProvider.click();
		driver.findElement(By.xpath("//div[6]")).click();
		
		
	/*	
			// Select DataProviderSource = new
			// Select(driver.findElement(By.id("dsProvider")));
			// DataProviderSource.selectByVisibleText("Microsoft Office Excel
			// File");
			// String expectedText="Microsoft Office Excel File";
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			// DataProvider.sendKeys("Microsoft Office Excel File");
			DataProvider.click();
			Thread.sleep(1100);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1200);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1300);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1400);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1500);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1600);
			// String actualText= DataProvider.getText();
			DataProvider.sendKeys(Keys.ENTER);
*/
			Thread.sleep(5000);

			try {
				Assert.assertTrue(driver.getPageSource().contains("URL of Excel file"));
			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Excel DataProvider Not Found/SelectExcelDataProvider() fail: " + e.getMessage());
			}

			// Assert.assertTrue("Login not
			// successful",expectedText.equals(actualText));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SelectExcelDataProvider() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectExcelDataProvider() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 10, 2);
			ExcelWriter(ExecResult, ErrorDesc, 10, 3);
			// System.out.print(" SelectExcelDataProvider() finally passed ");

		}

	}
////////////////////////////*****************'CCSP 2.4 Modification'**************** Select Chart Type-to chang the drilldown chart at any phase*************//////////////////
	public void SelectChartTypeAfterDrilldown() throws Exception {
		try{
		WebElement SelectChartTypeAfterDrilldown = driver.findElement(By.xpath("//div[@id='chartPanelContainer']/div/div/img[1]"));
		
		SelectChartTypeAfterDrilldown.click();
		
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[contains(text(),'Multi Series Line 2D')]")).click();
		
		
		Thread.sleep(1000);
		SelectChartTypeAfterDrilldown.click();
		driver.findElement(By.xpath("//*[contains(text(),'Step Line')]")).click();
		}
	 catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("SelectChartTypeAfterDrilldown() fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("SelectExcelDataProvider() fail in catch:" +
		// ErrorDesc);
		ExcelWriter(ExecResult, ErrorDesc, 428, 2);
		ExcelWriter(ExecResult, ErrorDesc, 428, 3);
		// System.out.print(" SelectExcelDataProvider() finally passed ");

	}

}

		/*JavascriptExecutor js = (JavascriptExecutor) driver;
        String jQuerySelector = "CCSP.jQuery(\".x-menu-item:eq(9)\")";
        String findDropdown = "return " + jQuerySelector;
	    System.out.println(findDropdown);
	   
		
		*/
		
		
		
		
		
		/*
		Thread.sleep(2000);
		List<WebElement> DropdownSelection = driver.findElements(By.tagName("li"));
	    DropdownSelection.get(26).click();
		
		
        WebElement DropdownSelection = driver.findElement(By.xpath(".//*[@id='ctl00_m_g_138015fc_a86d_403d_9fdc_c6cffb0dec96_ctl00_imgDrillChartType']"));
		
        DropdownSelection.sendKeys(Keys.ARROW_DOWN);
        DropdownSelection.sendKeys(Keys.ARROW_DOWN);
        DropdownSelection.sendKeys(Keys.ARROW_DOWN);
        DropdownSelection.sendKeys(Keys.E);
		
		
		
		SelectChartTypeAfterDrilldown.sendKeys(Keys.ARROW_DOWN);
		SelectChartTypeAfterDrilldown.sendKeys(Keys.ARROW_DOWN);
		SelectChartTypeAfterDrilldown.sendKeys(Keys.ENTER);
		
		*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
/////////////////////////////////**************'CCSP 2.4 Modification'**************'New Features of Excel Datasource'***//////////////////////////////////////
	public void NetworkRadioButton() throws IOException {

	try{
	
    List<WebElement> RadioButton = driver.findElements(By.name("xlssel"));
    RadioButton.get(1).click();


	} catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("NetworkRadioButton() fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("SelectExcelDataProvider() fail in catch:" +
		// ErrorDesc);
		ExcelWriter(ExecResult, ErrorDesc, 429, 2);
		ExcelWriter(ExecResult, ErrorDesc, 429, 3);
		// System.out.print(" SelectExcelDataProvider() finally passed ");

	}

}
	

	public void NetworkUserName() throws IOException {
		try{
		WebElement UserName = driver.findElement(By.id("xlsLogin"));
		UserName.click();
		UserName.clear();
		UserName.sendKeys("poweruser1");
		}
		 catch (Exception e) {
				ExecResult = "FAIL";
				ErrorDesc = e.getMessage();
				System.out.println("NetworkUserName() fail in catch:" + ErrorDesc);
			} finally {
				// System.out.print("SelectExcelDataProvider() fail in catch:" +
				// ErrorDesc);
				ExcelWriter(ExecResult, ErrorDesc, 430, 2);
				ExcelWriter(ExecResult, ErrorDesc, 430, 3);
				// System.out.print(" SelectExcelDataProvider() finally passed ");

			}

		
	}

	public void NetworkPassword() throws IOException {
		try{
		WebElement Password = driver.findElement(By.id("xlsPass"));
		Password.click();
		Password.clear();
		Password.sendKeys("P@ssw0rd");
		}
		catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("NetworkPassword() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectExcelDataProvider() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 431, 2);
			ExcelWriter(ExecResult, ErrorDesc, 431, 3);
			// System.out.print(" SelectExcelDataProvider() finally passed ");

		}
		
	}
	public void FilePassword() throws IOException
	{
		try{
		WebElement UserName = driver.findElement(By.id("xlPass"));
		UserName.click();
		UserName.sendKeys("");
		}
		catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FilePassword() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectExcelDataProvider() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 432, 2);
			ExcelWriter(ExecResult, ErrorDesc, 432, 3);
			// System.out.print(" SelectExcelDataProvider() finally passed ");

		}
	}
	/*public void LoadButton()
	{
		WebElement LoadButton = driver.findElement(By.id("loadBtn"));
		LoadButton.click();
		
      //  driver.findElement(By.xpath("//button[text()='Load']")).click();
	
	}
	*/
	public void UrlOfNetworkExcel() throws IOException {
		try{
		WebElement NetworkExcel = driver.findElement(By.id("xlsUrl"));
		NetworkExcel.click();
		NetworkExcel.clear();
		//NetworkExcel.sendKeys("http://pc-nibir2010/Shared Documents/SalesDataSheet.xlsx");
		NetworkExcel.sendKeys("\\\\"+"192.168.0.228\\PublicShare\\Best Selling Music Artist.xlsx");
		}
		catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("UrlOfNetworkExcel() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectExcelDataProvider() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 433, 2);
			ExcelWriter(ExecResult, ErrorDesc, 433, 3);
			// System.out.print(" SelectExcelDataProvider() finally passed ");

		}
	}
	
	public void NetworkSheetNameChoose() throws IOException {
	
		try {

			WebElement SheetChoose = driver.findElement(By.xpath("//input[contains(@id,'xlsSheetName')]"));
			Assert.assertNotNull("SheetChoose");
			SheetChoose.click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//div[text()='Raw']")).click();
			// SheetChoose.sendKeys(Keys.ARROW_DOWN);
		}
		catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("NetworkSheetNameChoose() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectExcelDataProvider() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 434, 2);
			ExcelWriter(ExecResult, ErrorDesc, 434, 3);
			// System.out.print(" SelectExcelDataProvider() finally passed ");

		}
		
	}
	
/////////////////////////////////**************'CCSP2.4 Modification'*************END****//////////////////////////////////////
	
	
	public void UrlOfExcel() throws IOException {
		try {
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://pc-nibir2010/Shared Documents/SalesDataSheet.xlsx");
			/// Urlfield.sendKeys(ExcelName);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("UrlOfExcel() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("UrlOfExcel() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 11, 2);
			ExcelWriter(ExecResult, ErrorDesc, 11, 3);
			// System.out.print(" UrlOfExcel() finally passed ");

		}
	}

	public void WrongUrlOfExcel() throws IOException {
		try {
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://ccsptestsvr/Shared Documents/SalesDataSheet.xls");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WrongUrlOfExcel() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("WrongUrlOfExcel() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 12, 2);
			ExcelWriter(ExecResult, ErrorDesc, 12, 3);
			// System.out.print(" WrongUrlOfExcel() finally passed ");

		}
	}

	public void WrongUrlOfExcel2010() throws IOException {
		try {
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://pc-nibir2010/Shared Documents/SalesDataSheet.xls");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WrongUrlOfExcel() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("WrongUrlOfExcel() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 472, 2);
			ExcelWriter(ExecResult, ErrorDesc, 472, 3);
			// System.out.print(" WrongUrlOfExcel() finally passed ");

		}
	}

	public void Verify_URL() throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);
		
  //Invalid file format.
  //Please provide Microsoft Excel (XLSX) file.
			Assert.assertTrue(driver.getPageSource().contains("Please provide Microsoft Excel (XLSX) file."));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Verify_URL() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Verify_URL() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 13, 2);
			ExcelWriter(ExecResult, ErrorDesc, 13, 3);
			// System.out.print(" Verify_URL() finally passed ");

		}
	}

	public void LoadButtonClick() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			Thread.sleep(1000);
			driver.findElement(By.xpath("//button[text()='Load']")).click();
			Thread.sleep(1000);
			// JavascriptExecutor js = (JavascriptExecutor) driver;
			// String jQuerySelector ="var
			// b=document.getElementsByClassName('x-btn-text');for(var
			// i=0;i<b.length;i++){ if(b[i].innerHTML==='Load')b[i].click();}";
			// js.executeScript(jQuerySelector);
			// //Thread.sleep(1000);
			// Assert.assertTrue(driver.getPageSource().contains("Sheet Name"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("LoadButtonClick() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("LoadButtonClick() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 14, 2);
			ExcelWriter(ExecResult, ErrorDesc, 14, 3);
			// System.out.print(" LoadButtonClick() finally passed ");

		}
	}

	public void ViewData() throws InterruptedException, IOException {
		try {

			Thread.sleep(19000);
			driver.findElement(By.xpath("//button[text()='View Data']")).click();
			// Thread.sleep(2000);
			// Assert.assertTrue(driver.getPageSource().contains("No data to
			// display"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ViewData() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ViewData() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 15, 2);
			ExcelWriter(ExecResult, ErrorDesc, 15, 3);
			// System.out.print(" ViewData() finally passed ");

		}
	}

	public void VerifySheetRange() throws InterruptedException, IOException {
		try {
			Thread.sleep(2400);
			Assert.assertTrue(driver.getPageSource().contains("No data to display"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("VerifySheetRange() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("VerifySheetRange() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 16, 2);
			ExcelWriter(ExecResult, ErrorDesc, 16, 3);
			// System.out.print(" VerifySheetRange() finally passed ");

		}
	}

	public void OKClick() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Assert.assertNotNull(driver.findElement(By.xpath("//button[text()='OK']")));
			driver.findElement(By.xpath("//button[text()='OK']")).click();

			// Assert.assertNotNull("OkButton");
			// Assert.assertTrue(driver.getPageSource().contains("Configure
			// Source"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("OKClick() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("OKClick() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 391, 2);
			ExcelWriter(ExecResult, ErrorDesc, 391, 3);
			// System.out.print(" OKClick() finally passed ");

		}

	}

	public void SheetChoose() throws IOException {
		try {

			WebElement SheetChoose = driver.findElement(By.xpath("//input[contains(@id,'xlsSheetName')]"));
			Assert.assertNotNull("SheetChoose");
			SheetChoose.click();
			Thread.sleep(500);
			driver.findElement(By.xpath("//div[text()='MOCK_DATA']")).click();
			// SheetChoose.sendKeys(Keys.ARROW_DOWN);

			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// SheetChoose.sendKeys(Keys.ENTER);
			// Assert.assertNotNull(driver.findElement(By.className("x-panel-header")));
			// driver.findElement(By.className("x-panel-header")).click();

			// SheetChoose.sendKeys(Keys.ENTER);")) x-panel-header
			// SheetChoose.sendKeys(Keys.ENTER);
			// SheetChoose.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SheetChoose() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SheetChoose() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 17, 2);
			ExcelWriter(ExecResult, ErrorDesc, 17, 3);
			// System.out.print(" SheetChoose() finally passed ");

		}

	}

	public void SheetRange() throws InterruptedException, IOException {
		try {

			WebElement SheetRange = driver.findElement(By.id("xlsRange"));
			Assert.assertNotNull("SheetRange");
			// SheetRange.click();
			SheetRange.clear();
			SheetRange.sendKeys("test");
			Thread.sleep(2000);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SheetRange() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SheetRange() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 18, 2);
			ExcelWriter(ExecResult, ErrorDesc, 18, 3);
			// System.out.print(" SheetRange() finally passed ");

		}

	}

	public void Connect() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Assert.assertNotNull(driver.findElement(By.xpath("//button[text()='Connect']")));
			// driver.findElements(By.className("x-btn-text")).contains("Connect");
			// Thread.sleep(3000);
			driver.findElement(By.xpath("//button[text()='Connect']")).click();
			// driver.findElement(By.xpath("//button[text()='Connect']")).click();
			// driver.findElement(By.xpath("//button[text()='Connect']")).click();
			// Button.listIterator();
			// Assert.assertTrue(driver.getPageSource().contains("Connection
			// succeeded"));
			// driver.findElement(By.xpath("//button[text()='OK']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Connect() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Connect() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 19, 2);
			ExcelWriter(ExecResult, ErrorDesc, 19, 3);
			// System.out.print(" Connect() finally passed ");

		}
	}

	public void SelectSharePointDataProvider() throws InterruptedException, IOException {
		try {
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));

			Assert.assertNotNull("DataProvider");
			DataProvider.click();
			// DataProvider.sendKeys(Keys.ARROW_DOWN);
			DataProvider.sendKeys(Keys.ENTER);
			Thread.sleep(2500);

			try {
				Assert.assertTrue(driver.getPageSource().contains("Select list"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(
						"SharePoint DataProvider Not Connected/SelectSharePointDataProvider() fail: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SelectSharePointDataProvider() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SelectSharePointDataProvider() fail in catch:"
			// + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 20, 2);
			ExcelWriter(ExecResult, ErrorDesc, 20, 3);
			// System.out.print(" SelectSharePointDataProvider() finally passed
			// ");

		}
	}

	public void SharePointSelectList() throws InterruptedException, IOException {

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement DataSelectionDropdown = driver.findElement(By.xpath("//input[contains(@id,'dsEntity')]"));
		// Assert.assertNotNull(DataSelectionDropdown);

		DataSelectionDropdown.click();

		Thread.sleep(4500);
		driver.findElement(By.xpath("html/body/div/div/div[text()='SalesDataExcel']")).click();
		// DataSelectionDropdown.sendKeys(Keys.ARROW_DOWN);
		// DataSelectionDropdown.sendKeys(Keys.ARROW_DOWN);
		// DataSelectionDropdown.sendKeys(Keys.ARROW_DOWN);
		// DataSelectionDropdown.sendKeys(Keys.ENTER);

	}

	/*
	 * DataSelection2.sendKeys(Keys.ARROW_DOWN);
	 * DataSelection2.sendKeys(Keys.ENTER);
	 */

	/*
	 * Thread.sleep(2000); List<WebElement> DataSelection1 =
	 * DataSelectionDropdown.findElements(By.xpath(
	 * ".//*[@class='x-combo-list-inner']")); List<WebElement> DataSelection2 =
	 * DataSelection1.get(1).findElements(By.xpath(
	 * ".//*[@class='x-combo-list-item']")); DataSelection2.get(3).click();
	 */

	/*
	 * WebElement DataSelection =
	 * driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[3]"));
	 * DataSelection.click();
	 */

	public void SharePointSelectListView() throws InterruptedException, IOException {
		try {
			Thread.sleep(7900);
			WebElement SelectListView = driver.findElement(By.id("dsView"));
			// Assert.assertNotNull("SelectListView");
			SelectListView.click();

			Thread.sleep(5500);
			// SelectListView.sendKeys(Keys.ARROW_DOWN);
			// //SelectListView.sendKeys(Keys.ARROW_DOWN);
			// SelectListView.sendKeys(Keys.ENTER);
			driver.findElement(By.xpath("html/body/div/div/div[text()='All Items']")).click();

			try {
				Assert.assertNotNull("SelectListView");
			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(
						"SharePoint SelectListView Dropdown option Element Not Found/SharePointSelectListView() fail: "
								+ e.getMessage());
			}
			// Thread.sleep(1000);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SharePointSelectListView() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("SharePointSelectListView() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 21, 2);
			ExcelWriter(ExecResult, ErrorDesc, 21, 3);
			// System.out.print(" SharePointSelectListView() finally passed ");

		}
	}

	public void ClickSelecfields() throws InterruptedException, IOException {
		try {

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Assert.assertNotNull(driver.findElement(By.xpath("//div/li[1]/ul/li[2]/div/a/span")));
			driver.findElement(By.xpath("//div/li[1]/ul/li[2]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickSelecfields() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ClickSelecfields() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 22, 2);
			ExcelWriter(ExecResult, ErrorDesc, 22, 3);
			// System.out.print(" ClickSelecfields() finally passed ");

		}

	}

	public void ClickGroupdata() throws InterruptedException, IOException {
		try {
			Thread.sleep(8000);

			try {
				Assert.assertNotNull(driver.findElement(By.xpath("//div/li[2]/ul/li[2]/div/a/span")));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Not Found Group Data Menu/ClickGroupdata() fail: " + e.getMessage());
			}

			driver.findElement(By.xpath("//div/li[2]/ul/li[2]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickGroupdata() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ClickGroupdata() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 23, 2);
			ExcelWriter(ExecResult, ErrorDesc, 23, 3);
			// System.out.print(" ClickGroupdata() finally passed ");

		}

	}

	public void GroupdataFirstCheckBox() throws InterruptedException, IOException {
		try {
			WebElement FirstCheckBox = driver.findElement(By.name("groupBy-checkbox"));

			FirstCheckBox.click();

			Thread.sleep(2000);

			try {
				Assert.assertTrue(FirstCheckBox.isSelected());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out
						.println("Groupdata First CheckBox Not Found/GroupdataFirstCheckBox() fail: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("GroupdataFirstCheckBox() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GroupdataFirstCheckBox() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 24, 2);
			ExcelWriter(ExecResult, ErrorDesc, 24, 3);
			// System.out.print(" GroupdataFirstCheckBox() finally passed ");
			// Thread.sleep(2000);
		}
	}

	
	public void functionDropdown() throws InterruptedException
	{
		
		WebElement Grid= driver.findElement(By.id("columnGridGroup"));
		Thread.sleep(2000);
		
		//WebElement table= Grid.findElement(By.id("x-grid3-row-table"));
		//WebElement functionDropdown = table.findElement(By.xpath("/div[7]/table/tbody/tr/td[2]/div"));
		
		

		//functionDropdown.click();
	}
	
	
	public void GroupdataFirstDropdown() throws IOException {
		try {
			// driver.findElement(By.cssSelector("div.x-combo-list-item")).sendKeys("NumberCol");

			WebElement FirstDropdown = driver.findElement(By.name("gd-combogroup"));

			try {
				Assert.assertTrue(FirstDropdown.isEnabled());
				Assert.assertNotNull(FirstDropdown);
			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out
						.println("Groupdata First Dropdown Not Found/GroupdataFirstDropdown() fail: " + e.getMessage());
			}
			FirstDropdown.click();
			FirstDropdown.sendKeys(Keys.ARROW_DOWN);
			FirstDropdown.sendKeys(Keys.ENTER);
			/*
			 * Thread.sleep(2500);
			 * driver.findElement(By.xpath("//div/div[text()='(None)']")).click(
			 * );
			 */
			/*
			 * WebElement DropDownValue=driver.findElement(By.xpath(
			 * ".//*[@class='x-combo-list-inner']/div[2]"));
			 * DropDownValue.click();
			 */
			// FirstDropdown.sendKeys(Keys.ARROW_DOWN);
			/*
			 * FirstDropdown.sendKeys(Keys.ARROW_DOWN);
			 * FirstDropdown.sendKeys(Keys.ARROW_DOWN);
			 * FirstDropdown.sendKeys(Keys.ARROW_DOWN);
			 */
			/*
			 * FirstDropdown.click(); FirstDropdown.sendKeys(Keys.ARROW_DOWN);
			 * FirstDropdown.sendKeys(Keys.ENTER);
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("GroupdataFirstDropdown() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GroupdataFirstDropdown() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 25, 2);
			ExcelWriter(ExecResult, ErrorDesc, 25, 3);
			// System.out.print(" GroupdataFirstDropdown() finally passed ");

		}
	}

	public void Group_By_Dropdown() throws IOException {
		try {

			WebElement Group_By_Dropdown = driver.findElement(By.xpath(".//*[@id='gd-datesgroup']"));
			Group_By_Dropdown.click();
			try {

				Assert.assertNotNull(Group_By_Dropdown);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Group By Dropdown Element Not Found/Group_By_Dropdown() fail: " + e.getMessage());
			}
			Group_By_Dropdown.sendKeys(Keys.ARROW_DOWN);
			Group_By_Dropdown.sendKeys(Keys.ARROW_DOWN);
			Group_By_Dropdown.sendKeys(Keys.ARROW_DOWN);
			Group_By_Dropdown.sendKeys(Keys.ARROW_DOWN);
			Group_By_Dropdown.sendKeys(Keys.ARROW_DOWN);
			Group_By_Dropdown.sendKeys(Keys.ARROW_DOWN);
			Group_By_Dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Group_By_Dropdown() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Group_By_Dropdown() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 26, 2);
			ExcelWriter(ExecResult, ErrorDesc, 26, 3);
			// System.out.print(" Group_By_Dropdown() finally passed ");

		}
	}

	public void Distinct_values_from_field() throws IOException {
		try {
			WebElement Distinct_values_from_field = driver.findElement(By.id("radioSplit"));
			Assert.assertNotNull(Distinct_values_from_field);
			Distinct_values_from_field.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Distinct_values_from_field() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Distinct_values_from_field() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 27, 2);
			ExcelWriter(ExecResult, ErrorDesc, 27, 3);
			// System.out.print(" Distinct_values_from_field() finally passed
			// ");

		}
	}

	public void Choose_parallel_dropdown() throws IOException {
		try {
			WebElement Choose_parallel_dropdown = driver.findElement(By.id("gd-splitgroup"));
			Assert.assertNotNull(Choose_parallel_dropdown);
			Choose_parallel_dropdown.click();
			Choose_parallel_dropdown.sendKeys(Keys.ARROW_DOWN);
			Choose_parallel_dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Choose_parallel_dropdown() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Choose_parallel_dropdown() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 28, 2);
			ExcelWriter(ExecResult, ErrorDesc, 28, 3);
			// System.out.print(" Choose_parallel_dropdown() finally passed ");

		}
	}

	public void Series_Display_first_dropdown() throws IOException {
		try {
			WebElement dropdown = driver.findElement(By.id("splitFunc3"));
			Assert.assertNotNull(dropdown);
			dropdown.click();
			dropdown.sendKeys(Keys.ARROW_DOWN);
			dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Series_Display_first_dropdown() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 29, 2);
			ExcelWriter(ExecResult, ErrorDesc, 29, 3);

		}

	}

	public void Series_Display_second_dropdown() throws IOException {
		try {
			WebElement dropdown = driver.findElement(By.id("splitColumn3"));
			Assert.assertNotNull(dropdown);
			dropdown.click();
			dropdown.sendKeys(Keys.ARROW_DOWN);
			dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Series_Display_second_dropdown() fail in catch " + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 30, 2);
			ExcelWriter(ExecResult, ErrorDesc, 30, 3);

		}
	}

	public void ClearGroupingButton() throws InterruptedException, IOException {
		try {
			WebElement button = driver.findElement(By.xpath("//button[text()='Clear Grouping']"));

			try {
				Assert.assertTrue(button.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Clear Grouping Button Not Found Enabled: " + e.getMessage());
			}

			button.click();
			// WebElement YesButton =
			// driver.findElement(By.xpath("//button[text()='Yes']"));
			// YesButton.click();
			// Thread.sleep(5000);

			// WebElement Yesbutton =
			// driver.findElement(By.xpath("//button[text()='Yes']"));
			// Yesbutton.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClearGroupingButton() fail in catch " + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 31, 2);
			ExcelWriter(ExecResult, ErrorDesc, 31, 3);

		}

	}

	public void NoButton() throws InterruptedException, IOException {
		try {
			// Thread.sleep(2000);
			WebElement button = driver.findElement(By.xpath("//button[text()='No']"));

			try {
				Assert.assertNotNull(button);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Clear Grouping Button Not Found Enabled: " + e.getMessage());
			}
			button.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("NoButton() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 32, 2);
			ExcelWriter(ExecResult, ErrorDesc, 32, 3);

		}
	}

	protected void sleep(int i) throws IOException {
		
			driver.manage().timeouts().implicitlyWait(i, TimeUnit.SECONDS);
		
	}

	public void ClearGroupingYesButton() throws InterruptedException, IOException {
		try {
			// Thread.sleep(5000);
			// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			WebDriverWait waitList = new WebDriverWait(driver, 40);
			waitList.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[text()='Yes']")));

			WebElement button = driver.findElement(By.xpath("//button[text()='Yes']"));
			button.click();
			sleep(20);
			// Thread.sleep(10000);

			try {
				Assert.assertNotNull("button");
				Assert.assertTrue(button.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Yes Button Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClearGroupingYesButton()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 33, 2);
			ExcelWriter(ExecResult, ErrorDesc, 33, 3);

		}
	}

	public void Top_N_Records() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			Assert.assertNotNull(driver.findElement(By.xpath("//div/li[2]/ul/li[3]/div/a/span")));
			driver.findElement(By.xpath("//div/li[2]/ul/li[3]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" Top_N_Records()fail in catch" + ErrorDesc);
		} finally {

			// ExcelWriter(ExecResult, ErrorDesc, 34, 2);
			// ExcelWriter(ExecResult, ErrorDesc, 34, 3);

		}

	}

	public void ShowTopFirstField() throws IOException

	{
		try {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement Topvalue = driver.findElement(By.id("txtTopValue"));
			Topvalue.clear();
			Topvalue.sendKeys("4");
			try {
				Assert.assertNotNull("Topvalue");

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Show top Field Element Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show top Field Element Not Found: " + e.getMessage());
		} finally {
			// System.out.print("ShowTopFirstField() fail in catch"+ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 35, 2);
			ExcelWriter(ExecResult, ErrorDesc, 35, 3);

		}

	}

	public void ShowTopDropdown() throws IOException {
		try {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement TopDropdown = driver.findElement(By.id("cmbTopMode"));
			TopDropdown.click();
			TopDropdown.sendKeys(Keys.ARROW_DOWN);
			TopDropdown.sendKeys(Keys.ENTER);
			// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			// TopDropdown.sendKeys(Keys.TAB);
			// TopDropdown.sendKeys(Keys.ENTER);
			try {
				Assert.assertNotNull(TopDropdown);
				Assert.assertTrue(TopDropdown.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Show top Dropdown Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ShowTopDropdown() error in finally" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 36, 2);
			ExcelWriter(ExecResult, ErrorDesc, 36, 3);

		}
	}

	public void SourceXML() throws IOException {
		try {
			WebElement Panel = driver.findElement(By.id("chartPreviewPanel"));

			WebElement button = Panel.findElement(By.className("x-tool-gear"));
			button.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SourceXML fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("SourceXML() fail in catch"+ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 37, 2);
			ExcelWriter(ExecResult, ErrorDesc, 37, 3);

		}
	}
	///////////////////////////////////////////////////// XML Response(Excel
	///////////////////////////////////////////////////// Datasource)///////////////////////////////////////////////////////////////////////////

	public void XMLResponse() throws AWTException, InterruptedException, IOException, ParserConfigurationException,
			TransformerException, SAXException {
		try {

			String request = "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx?GetChartXml=g_6666e595_1e50_4d68_8bff_1541ace6e5a6&RootChartId=ctl00_m_g_6666e595_1e50_4d68_8bff_1541ace6e5a6_ctl00_chartPanelChart";
			// String request =
			// "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx";
			URL url = new URL(request);

			Authenticator.setDefault(new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("Administrator", "P@ssw0rd".toCharArray());
				}
			});

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			// get a response - maybe "success" or "true", XML or JSON etc.
			InputStream inputStream = connection.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}

			bufferedReader.close();

			System.out.println(response.toString());
			// XMLComparison();

			/////////// save XML response in XMl file///////////

			// Parse the given input
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Node doc = (Node) builder.parse(
					new InputSource(new StringReader(response.toString().trim().replaceFirst("^([\\W]+)<", "<"))));

			// Write the parsed document to an xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("ExpectedText.xml"));
			transformer.transform(source, result);

			Thread.sleep(16000);
			// XMLComparison();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLResponse()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 38, 2);
			ExcelWriter(ExecResult, ErrorDesc, 38, 3);

		}
	}

	public void XMLComparison() throws IOException {
		try {

			File f1 = new File("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualText.XML");
			File f2 = new File(
					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ExpectedText.XML");

			// File f3= new
			// File("C:\\Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualSharepointlist.XML");

			FileReader fr1 = null;
			FileReader fr2 = null;
			// FileReader fr3 = null;

			try {
				fr1 = new FileReader(f1);
				fr2 = new FileReader(f2);
				// fr3 = new FileReader(f3);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			try {
				Diff Exceldiff = new Diff(fr1, fr2);
				// Diff SharepointListdiff = new Diff(fr2, fr3);

				System.out.println("Similar? " + Exceldiff.similar());
				System.out.println("Identical? " + Exceldiff.identical());

				DetailedDiff detDiff = new DetailedDiff(Exceldiff);
				List differences = detDiff.getAllDifferences();
				for (Object object : differences) {
					Difference difference = (Difference) object;
					System.out.println("*******ExcelDataDifference****************");
					System.out.println(difference);
					System.out.println("********ExcelDataDifference***************");
				}

			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLComparison() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 39, 2);
			ExcelWriter(ExecResult, ErrorDesc, 39, 3);

		}
	}

	///////////////////////////////////////////////////// XML
	///////////////////////////////////////////////////// Response(SharePointList
	///////////////////////////////////////////////////// Datasource)///////////////////////////////////////////////////////////////////////////

	public void XMLSharePointListDataResponse() throws AWTException, InterruptedException, IOException,
			ParserConfigurationException, TransformerException, SAXException {
		try {
			String request = "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx?GetChartXml=g_6666e595_1e50_4d68_8bff_1541ace6e5a6&RootChartId=ctl00_m_g_6666e595_1e50_4d68_8bff_1541ace6e5a6_ctl00_chartPanelChart";
			// String request =
			// "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx";
			URL url = new URL(request);

			Authenticator.setDefault(new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("Administrator", "P@ssw0rd".toCharArray());
				}
			});

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			// get a response - maybe "success" or "true", XML or JSON etc.
			InputStream inputStream = connection.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}

			bufferedReader.close();

			System.out.println(response.toString());

			/////////// save XML response in XMl file///////////

			// Parse the given input
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Node doc = (Node) builder.parse(
					new InputSource(new StringReader(response.toString().trim().replaceFirst("^([\\W]+)<", "<"))));

			// Write the parsed document to an xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("ExpectedSharepointlist.XML"));
			transformer.transform(source, result);
			Thread.sleep(17000);
			XMLSharepointListDataComparison();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLSharePointListDataResponse() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc);

			ExcelWriter(ExecResult, ErrorDesc, 40, 2);
			ExcelWriter(ExecResult, ErrorDesc, 40, 3);

		}
	}

	public void XMLSharepointListDataComparison() throws IOException {
		try {

			File f3 = new File(

					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualSharepointlist.XML");
			File f2 = new File(

					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ExpectedSharepointlist.XML");

			// File f3= new
			// File("C:\\Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualSharepointlist.XML");

			FileReader fr3 = null;
			FileReader fr2 = null;
			// FileReader fr3 = null;

			try {
				fr3 = new FileReader(f3);
				fr2 = new FileReader(f2);
				// fr3 = new FileReader(f3);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			try {
				Diff SharepointListdiff = new Diff(fr3, fr2);
				// Diff SharepointListdiff = new Diff(fr2, fr3);

				System.out.println("Similar? " + SharepointListdiff.similar());
				System.out.println("Identical? " + SharepointListdiff.identical());

				DetailedDiff detDiff = new DetailedDiff(SharepointListdiff);
				List differences = detDiff.getAllDifferences();
				for (Object object : differences) {
					Difference difference = (Difference) object;
					System.out.println("*******SharepointListDataDifference****************");
					System.out.println(difference);
					System.out.println("********SharepointListDataDifference***************");
				}

			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLSharepointListDataComparison() fail in catch" + ErrorDesc);
		} finally {

			// System.out.print("XMLSharepointListDataComparison() fail in
			// catch"+ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 41, 2);
			ExcelWriter(ExecResult, ErrorDesc, 41, 3);

		}
	}

	///////////////////////////////////////////////////// XML Response(CSV
	///////////////////////////////////////////////////// Datasource)///////////////////////////////////////////////////////////////////////////

	public void XMLCSVDataResponse() throws AWTException, InterruptedException, IOException,
			ParserConfigurationException, TransformerException, SAXException {
		try {
			String request = "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx?GetChartXml=g_6666e595_1e50_4d68_8bff_1541ace6e5a6&RootChartId=ctl00_m_g_6666e595_1e50_4d68_8bff_1541ace6e5a6_ctl00_chartPanelChart";
			// String request =
			// "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx";
			URL url = new URL(request);

			Authenticator.setDefault(new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("Administrator", "P@ssw0rd".toCharArray());
				}
			});

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			// get a response - maybe "success" or "true", XML or JSON etc.
			InputStream inputStream = connection.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}

			bufferedReader.close();

			System.out.println(response.toString());

			/////////// save XML response in XMl file///////////

			// Parse the given input
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Node doc = (Node) builder.parse(
					new InputSource(new StringReader(response.toString().trim().replaceFirst("^([\\W]+)<", "<"))));

			// Write the parsed document to an xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("ExpectedCSV.XML"));
			transformer.transform(source, result);
			Thread.sleep(18000);
			XMLCSVDataComparison();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLCSVDataResponse() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("XMLCSVDataResponse() fail in catch"+ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 42, 2);
			ExcelWriter(ExecResult, ErrorDesc, 42, 3);

		}
	}

	public void XMLCSVDataComparison() throws IOException {
		try {

			File f4 = new File("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualCSV.XML");
			File f2 = new File(
					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ExpectedCSV.XML");

			// File f3= new
			// File("C:\\Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualSharepointlist.XML");

			FileReader fr4 = null;
			FileReader fr2 = null;
			// FileReader fr3 = null;

			try {
				fr4 = new FileReader(f4);
				fr2 = new FileReader(f2);
				// fr3 = new FileReader(f3);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			try {
				Diff CSVdiff = new Diff(fr4, fr2);
				// Diff SharepointListdiff = new Diff(fr2, fr3);

				System.out.println("Similar? " + CSVdiff.similar());
				System.out.println("Identical? " + CSVdiff.identical());

				DetailedDiff detDiff = new DetailedDiff(CSVdiff);
				List differences = detDiff.getAllDifferences();
				for (Object object : differences) {
					Difference difference = (Difference) object;
					System.out.println("*******CSVDataDifference****************");
					System.out.println(difference);
					System.out.println("********CSVDataDifference***************");
				}

			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLCSVDataComparison() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc);

			ExcelWriter(ExecResult, ErrorDesc, 43, 2);
			ExcelWriter(ExecResult, ErrorDesc, 43, 3);

		}
	}

	public void WizardDrilldownApply() throws InterruptedException, IOException {
		try {
			// Thread.sleep(10000);
			// WebElement ButtonPanel =
			// driver.findElement(By.id("drilldownStep"));
			// List<WebElement> Button =
			// ButtonPanel.findElements(By.className("x-btn"));
			// Assert.assertNotNull(Button.get(5));
			// Button.get(6).click();
			// Button.get(6).click();
			driver.findElement(By.xpath(".//*[@id='DrillDownMainContainer']//button[text()='Apply']")).click();
			// Button.get(5).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();

			System.out.println("WizardDrilldownApply() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("WizardDrilldownApply() fail in
			// finally"+ErrorDesc); ExcelWriter(ExecResult, ErrorDesc, 44, 2);
			ExcelWriter(ExecResult, ErrorDesc, 44, 2);
	        ExcelWriter(ExecResult, ErrorDesc, 44, 3);
		}
	}

	public void Apply1() throws InterruptedException, IOException {
		try {
			Thread.sleep(5000);
			WebElement ButtonPanel = driver.findElement(By.id("groupingStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.className("x-btn"));
			Assert.assertNotNull(Button.get(5));
			Button.get(5).click();
			Button.get(5).click();
			Button.get(5).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Apply1() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("Apply1() fail in finally"+ErrorDesc);
			// ExcelWriter(ExecResult, ErrorDesc, 45, 2);
			ExcelWriter(ExecResult, ErrorDesc, 45, 3);

		}

	}

	public void SelectFieldApply() throws InterruptedException, IOException {
		try {
			WebElement ButtonPanel = driver.findElement(By.id("fieldsStep"));
			WebElement Apply = ButtonPanel.findElement(By.tagName("button"));

			Apply.click();

			Thread.sleep(500);
			WebElement Modal = driver.findElement(By.xpath("//div//span[text()='Please select at least two fields.']"));
			String Text = Modal.getText();
			Assert.assertTrue(Modal.getText().contains("Please select at least two fields."));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SelectFieldApply() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc);

			ExcelWriter(ExecResult, ErrorDesc, 46, 2);
			ExcelWriter(ExecResult, ErrorDesc, 46, 3);

		}

	}

	public void CaptionsApplyClick() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);
			WebElement ButtonPanel = driver.findElement(By.id("axisTitlesStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("button"));
			Assert.assertNotNull(Button.get(4));
			Button.get(4).click();
			Button.get(4).click();
			Button.get(4).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CaptionsApplyClick() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("CaptionsApplyClick()fail in catch"+ErrorDesc);
		    ExcelWriter(ExecResult, ErrorDesc, 47, 2);
			ExcelWriter(ExecResult, ErrorDesc, 47, 3);

		}
	}

	public void ApplyClick() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			WebElement ButtonPanel = driver.findElement(By.id("toprecordsStep"));
			WebElement Button = ButtonPanel.findElement(By.className("x-btn"));

			///////////////////////////////////////////////////// XML
			///////////////////////////////////////////////////// Response(SQL
			///////////////////////////////////////////////////// Server
			///////////////////////////////////////////////////// Datasource)///////////////////////////////////////////////////////////////////////////

			Button.click();

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ApplyClick() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 48, 2);
			ExcelWriter(ExecResult, ErrorDesc, 48, 3);

		}

	}

	public void XMLSQLServerDataResponse() throws AWTException, InterruptedException, IOException,
			ParserConfigurationException, TransformerException, SAXException {
		try {

			String request = "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx?GetChartXml=g_6666e595_1e50_4d68_8bff_1541ace6e5a6&RootChartId=ctl00_m_g_6666e595_1e50_4d68_8bff_1541ace6e5a6_ctl00_chartPanelChart";
			// String request =
			// "http://pc-nibir2010/_layouts/CollabionCharts/WebService.aspx";
			URL url = new URL(request);

			Authenticator.setDefault(new Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("Administrator", "P@ssw0rd".toCharArray());
				}
			});

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			// get a response - maybe "success" or "true", XML or JSON etc.
			InputStream inputStream = connection.getInputStream();
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = bufferedReader.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}

			bufferedReader.close();

			System.out.println(response.toString());
			XMLComparison();

			/////////// save XML response in XMl file///////////

			// Parse the given input
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Node doc = (Node) builder.parse(
					new InputSource(new StringReader(response.toString().trim().replaceFirst("^([\\W]+)<", "<"))));

			// Write the parsed document to an xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("ExpectedSQL.XML"));
			transformer.transform(source, result);
			Thread.sleep(19000);
			XMLSQLServerDataComparison();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLSQLServerDataResponse()fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 49, 2);
			ExcelWriter(ExecResult, ErrorDesc, 49, 3);

		}
	}

	public void XMLSQLServerDataComparison() throws IOException {
		try {

			File f4 = new File("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualSQL.XML");
			File f2 = new File(
					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ExpectedSQL.XML");

			// File f3= new
			// File("C:\\Collabion\\CollabionChartsForSharePointAutomationTesting\\ActualSharepointlist.XML");

			FileReader fr4 = null;
			FileReader fr2 = null;
			// FileReader fr3 = null;

			try {
				fr4 = new FileReader(f4);
				fr2 = new FileReader(f2);
				// fr3 = new FileReader(f3);

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

			try {
				Diff SQLServerDatadiff = new Diff(fr4, fr2);
				// Diff SharepointListdiff = new Diff(fr2, fr3);

				System.out.println("Similar? " + SQLServerDatadiff.similar());
				System.out.println("Identical? " + SQLServerDatadiff.identical());

				DetailedDiff detDiff = new DetailedDiff(SQLServerDatadiff);
				List differences = detDiff.getAllDifferences();
				for (Object object : differences) {
					Difference difference = (Difference) object;
					System.out.println("*******SQLServerDataDifference****************");
					System.out.println(difference);
					System.out.println("********SQLServerDataDifference***************");
				}

			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XMLSQLServerDataComparison() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 50, 2);
			ExcelWriter(ExecResult, ErrorDesc, 50, 3);

		}
	}

	/*
	 * ////////////////////
	 *
	 * ///////////Compare Actual XMl file & Expected file///////////
	 *
	 *
	 * /* DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	 * dbf.setNamespaceAware(true); dbf.setCoalescing(true);
	 * dbf.setIgnoringElementContentWhitespace(true);
	 * dbf.setIgnoringComments(true); DocumentBuilder db =
	 * dbf.newDocumentBuilder();
	 *
	 * Document doc1 = (Document) db.parse(new File("file1.xml"));
	 * doc1.normalizeDocument();
	 *
	 * Document doc2 = (Document) db.parse(new File("file2.xml"));
	 * doc2.normalizeDocument();
	 *
	 * Assert.assertTrue(((Node) doc1).isEqualNode((Node) doc2)); }
	 */

	public void CopyXML() throws AWTException, InterruptedException, IOException {
		try {

			// WebElement Panel=driver.findElement(By.id("chartPreviewPanel"));
			// WebElement
			// button=driver.findElement(By.cssSelector("embed#ZeroClipboardMovie_1"));
			WebElement copy = driver.findElement(By.xpath("//div[24]"));

			/*
			 * WebElement
			 * windowheader=driver.findElement(By.className("x-window-header"));
			 * Thread.sleep(2000); WebElement
			 * copy=windowheader.findElement(By.className("x-tool-save"));
			 */

			// copy.click();

			// button.getText();

			// String S = button.getText();

			// System.out.println(S);

			// NotepadOpening();

			// Robot robot = new Robot();

			// robot.keyPress(KeyEvent.VK_PASTE);
			// robot.delay(110);

			// robot.keyRelease(KeyEvent.VK_PASTE);

			// robot.keyPress(KeyEvent.VK_CONTROL);
			// robot.delay(120);

			// robot.delay(130);
			// robot.keyRelease(KeyEvent.VK_CONTROL);

			// StringSelection stringSelection = new StringSelection(myString);
			// Clipboard clpbrd =
			// Toolkit.getDefaultToolkit().getSystemClipboard();
			// clpbrd.setContents(stringSelection, null);
			// String copy = "button.click()";
			// System.out.println(copy);
			// }
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CopyXML()fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 51, 2);
			ExcelWriter(ExecResult, ErrorDesc, 51, 3);

		}

	}

	public void NotepadOpening() throws IOException {
		try {

			Runtime rs = Runtime.getRuntime();

			try {
				rs.exec("notepad");
			} catch (IOException e) {
				System.out.println(e);
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("NotepadOpening() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("NotepadOpening() fail in catch"+ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 52, 2);
			ExcelWriter(ExecResult, ErrorDesc, 52, 3);

		}
	}

	public static void type(String characters) throws AWTException {

		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		StringSelection stringSelection = new StringSelection(characters);
		clipboard.setContents(stringSelection, stringSelection);
		// control+V is for pasting...

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
	}

	public void PasteText() throws AWTException, IOException {
		try {
			Robot robot = new Robot();

			// For platform independence:
			int ctrlOrCmdKey = -1;
			if (System.getProperty("os.name").toLowerCase().contains("mac")) {
				ctrlOrCmdKey = KeyEvent.VK_META;
			} else {
				ctrlOrCmdKey = KeyEvent.CTRL_MASK;
			}

			robot.keyPress(ctrlOrCmdKey);
			// Thread.sleep(10);
			robot.keyPress(KeyEvent.VK_V);
			// Thread.sleep(10);
			robot.keyRelease(KeyEvent.VK_V);
			// Thread.sleep(10);
			robot.keyRelease(KeyEvent.CTRL_MASK);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("PasteText() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 54, 2);
			ExcelWriter(ExecResult, ErrorDesc, 54, 3);

		}

	}

	/*
	 * public void fileWriter() {
	 *
	 *
	 * FileWriter fileWriter = new FileWriter("ExpectedText.xml");
	 * BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	 * bufferedWriter.write('NotepadOpening()'); bufferedWriter.close();
	 *
	 * }
	 */

	public void ClickField() throws IOException {
		try {
			WebElement field = driver.findElement(By.cssSelector("div.cc-x-grid3-hdchecker"));
			field.click();

			// driver.findElement(By.className("cc-x-grid3-checker-on")).getClass();

			/*
			 * try { Assert.assertTrue(field.isSelected());;
			 *
			 * } catch (AssertionError e) {
			 *
			 * // String message = e.getMessage(); System.out.println(
			 * "Show top Dropdown Not Found: " + e.getMessage());
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickField() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 55, 2);
			ExcelWriter(ExecResult, ErrorDesc, 55, 3);

		}
	}

	/*
	 * if (field) { field.click();
	 * Assert.assertEquals("null",field.getAttribute("checked"));
	 * cc-x-grid3-checker-on } else { System.out.println(
	 * "Fail:field is Disabled"); }
	 */

	public void FirstClickField() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			WebElement field = driver.findElement(By.xpath("//div[1]/table/tbody/tr/td[3]/div/div"));

			// Thread.sleep(1000);
			try {
				Assert.assertNotNull(field);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("First Click Field Not Found: " + e.getMessage());

			}
			field.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FirstClickField() fail in catch" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 56, 2);
			ExcelWriter(ExecResult, ErrorDesc, 56, 3);

		}

	}

	public void FourthClickField() throws InterruptedException, IOException {
		try {

			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			WebElement field1 = driver.findElement(By.xpath("//div[4]/table/tbody/tr/td[3]/div/div"));

			try {
				Assert.assertNotNull(field1);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Fourth Click Field Not Found: " + e.getMessage());
			}

			Thread.sleep(1000);
			field1.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FourthClickField() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc);
			//
			ExcelWriter(ExecResult, ErrorDesc, 57, 2);
			ExcelWriter(ExecResult, ErrorDesc, 57, 3);

		}
	}

	public void FifthClickField() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			WebElement field2 = driver.findElement(By.xpath("//div[5]/table/tbody/tr/td[3]/div/div"));

			try {
				Assert.assertNotNull(field2);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Fifth Click Field Not Found: " + e.getMessage());
			}

			Thread.sleep(1000);
			field2.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FifthClickField() fail in catch:" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 58, 2);
			ExcelWriter(ExecResult, ErrorDesc, 58, 3);

		}
	}

	public void SixthClickField() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			WebElement field3 = driver.findElement(By.xpath("//div[6]/table/tbody/tr/td[3]/div/div"));
			try {
				Assert.assertNotNull(field3);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Sixth Click Field Not Found: " + e.getMessage());
			}

			Thread.sleep(1000);
			field3.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SixthClickField() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 59, 2);
			ExcelWriter(ExecResult, ErrorDesc, 59, 3);

		}
	}

	public void ApplyButtonClick() throws InterruptedException, IOException {
		try {

			WebElement field4 = driver.findElement(By.xpath("//button[text()='Apply']"));

			// Thread.sleep(1000);

			try {
				Assert.assertNotNull(field4);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Apply Button is Not Found: " + e.getMessage());
			}
			Thread.sleep(1600);
			field4.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ApplyButtonClick() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 60, 2);
			ExcelWriter(ExecResult, ErrorDesc, 60, 3);

		}
		//
		// field4.click();
	}

	public void FilterDataApplyButtonClick() throws InterruptedException, IOException {
		try {
			WebElement ButtonPanel = driver.findElement(By.id("groupingStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.className("x-btn"));
			Assert.assertNotNull(Button.get(5));
			Button.get(5).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FourthClickField() fail in catch" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 61, 2);
			ExcelWriter(ExecResult, ErrorDesc, 61, 3);

		}
	}

	public void PanelClick() throws IOException {
		try {

			Assert.assertNotNull(driver.findElement(By.cssSelector(" td.x-toolbar-left")));
			driver.findElement(By.cssSelector(" td.x-toolbar-left")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("PanelClick() fail in catch" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 62, 2);
            ExcelWriter(ExecResult, ErrorDesc, 62, 3);
		}

	}

	public void Chart_Category_Apply_Button() throws IOException {
		try {
			WebElement ButtonPanel = driver.findElement(By.id("chartTypeStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.className("button"));
			Assert.assertNotNull(Button.get(0));
			Button.get(0).click();
			Button.get(0).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Chart_Category_Apply_Button() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 63, 2);
			ExcelWriter(ExecResult, ErrorDesc, 63, 3);

		}
	}

	public void ChartTypeApplyClick() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);
			WebElement ButtonPanel = driver.findElement(By.id("chartTypeStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("button"));
			Assert.assertNotNull(Button.get(0));
			Button.get(0).click();
			Button.get(0).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartTypeApplyClick() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 64, 2);
			ExcelWriter(ExecResult, ErrorDesc, 64, 3);

		}
	}

	public void TopNRecordApplyclick() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			WebElement ButtonPanel = driver.findElement(By.id("toprecordsStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.className("button"));

			Button.get(0).click();
			Button.get(0).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TopNRecordApplyclick()fail in catch " + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 65, 2);
			ExcelWriter(ExecResult, ErrorDesc, 65, 3);

		}

	}

	public void Clear_All_Filters() throws InterruptedException, IOException {
		try {
			WebElement Button = driver.findElement(By.xpath("//button[text()='Clear all filter(s)']"));
			Button.click();

			Thread.sleep(4400);

			WebElement YesButton = driver.findElement(By.xpath("//button[text()='Yes']"));
			YesButton.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Clear_All_Filters() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 66, 2);
			ExcelWriter(ExecResult, ErrorDesc, 66, 3);

		}
	}
	/*
	 * Thread.sleep(5000); WebElement ButtonPanel =
	 * driver.findElement(By.id("groupingStep")); List<WebElement> Button =
	 * ButtonPanel.findElements(By.className("x-btn")); Button.get(6).click();
	 * Button.get(6).click(); //Button.get(5).click();
	 */

	/*
	 *
	 *
	 * Thread.sleep(5000);
	 *
	 * JavascriptExecutor js = (JavascriptExecutor) driver; String
	 * jQuerySelector = "$(\":contains('Apply')\").click()"; String ApplyButton
	 * = "return " + jQuerySelector ;
	 *
	 * WebElement FontName = (WebElement) js.executeScript(ApplyButton);
	 *
	 *
	 * //Thread.sleep(5000);
	 *
	 * // WebElement Button =
	 * driver.findElement(By.xpath("//tbody/tr[2]/td[2]"));
	 *
	 * // WebElement Button =
	 * driver.findElement(By.xpath("//button[text()='Apply']")); //
	 * Button.click(); //Button.click();
	 */

	public void Applyclick() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			WebElement ButtonPanel = driver.findElement(By.id("filteringStep"));
			List<WebElement> all_Buttons = ButtonPanel.findElements(By.className("x-btn-text"));

			// for(int i=0;i<all_Buttons.size();i++)
			// {

			all_Buttons.get(7).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Applyclick() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 67, 2);
			ExcelWriter(ExecResult, ErrorDesc, 67, 3);

		}

		// System.out.println(all_Buttons.get(i).isEnabled());
	}

	public void AllclearRecords() throws InterruptedException, IOException {
		try {

			WebElement Button = driver.findElement(By.xpath("//button[text()='Clear']"));
			Button.click();

			Thread.sleep(1000);
			WebElement YesButton = driver.findElement(By.xpath("//button[text()='Yes']"));
			YesButton.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("AllclearRecords() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print("AllclearRecords() fail in catch"+ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 68, 2);
			ExcelWriter(ExecResult, ErrorDesc, 68, 3);

		}
	}
	/*
	 * Thread.sleep(3500); WebElement ButtonPanel =
	 * driver.findElement(By.id("filteringStep")); List<WebElement> all_Buttons
	 * = ButtonPanel.findElements(By.className("x-btn-text"));
	 *
	 * // for(int i=0;i<all_Buttons.size();i++) // {
	 *
	 * all_Buttons.get(8).click();
	 */

	/*
	 * //WebElement Button =
	 * driver.findElement(By.xpath("//tbody/tr[2]/td[2]")); WebElement Button=
	 * driver.findElement(By.xpath("//button[text()='Apply']"));
	 *
	 *
	 * try { Assert.assertTrue(Button.isEnabled());
	 *
	 * } catch (AssertionError e) {
	 *
	 * // String message = e.getMessage(); System.out.println(
	 * "Apply Button is Not Found as Enabled: " + e.getMessage()); }
	 * Thread.sleep(1000); Button.click();
	 */
	// Button.click();

	// driver.findElement(By.xpath("//button[text()='Apply']")).click();

	// WebElement Apply= driver.findElement(By.id("cmbTopMode"));
	// Apply.click();
	// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	// Button.sendKeys(Keys.TAB);
	// Button.sendKeys(Keys.ENTER);
	/*
	 * try { Assert.assertTrue(Button.isEnabled());
	 *
	 * } catch (AssertionError e) {
	 *
	 * // String message = e.getMessage(); System.out.println(
	 * "Apply Button is Not Found as Enabled: " + e.getMessage()); }
	 */

	public void FinishButtonClick() throws InterruptedException, IOException {
		 try{
		Thread.sleep(2000);
		WebElement Button = driver.findElement(By.xpath("//button[text()='Finish']"));
		Button.click();
		 }
	 catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("FinishClick() fail in catch" + ErrorDesc);
	} finally {
		//
		ExcelWriter(ExecResult, ErrorDesc, 69, 2);
		ExcelWriter(ExecResult, ErrorDesc, 69, 3);

	}
		/*
		 * try { Assert.assertTrue(Button.isEnabled());
		 *
		 * } catch (AssertionError e) {
		 *
		 * // String message = e.getMessage(); System.out.println(
		 * "Finish Button is Not Found as Enabled: " + e.getMessage()); }
		 * Thread.sleep(1000);
		 *
		 * }catch (Exception e) { ExecResult = "FAIL"; ErrorDesc =
		 * e.getMessage(); System.out.println(
		 * "Clear_All_Filters() fail in catch"+ErrorDesc); } finally {
		 * ExcelWriter(ExecResult, ErrorDesc, 69,2); ExcelWriter(ExecResult,
		 * ErrorDesc, 69,3); System.out.print( "into Finally");
		 *
		 * } // Button.click();
		 */ }

	public void FinishClick() throws InterruptedException, IOException {
		try {
			Thread.sleep(2000);

			WebElement Button = driver.findElement(By.xpath("//button[text()='Finish']"));

			try {
				Assert.assertTrue(Button.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Apply Button is Not Found as Enabled: " + e.getMessage());
			}

			Button.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FinishClick() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 70, 2);
			ExcelWriter(ExecResult, ErrorDesc, 70, 3);

		}

	}

	public void ClickfilterData() throws InterruptedException, IOException {
		try {
			Thread.sleep(8000);
			Assert.assertNotNull(driver.findElement(By.xpath("//div/li[2]/ul/li[1]/div/a/span")));
			driver.findElement(By.xpath("//div/li[2]/ul/li[1]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickfilterData() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 71, 2);
			ExcelWriter(ExecResult, ErrorDesc, 71, 3);

		}

	}
	/////////////////////////// Microsoft SQL Server
	/////////////////////////// Database//////////////////////////

	public void ChooseSQLServer() throws InterruptedException, IOException {
		try {
			Thread.sleep(2000);

			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			Thread.sleep(1300);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1500);
			DataProvider.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChooseSQLServer() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 72, 2);
			ExcelWriter(ExecResult, ErrorDesc, 72, 3);

		}

	}

	public void Windows_Authentication() throws InterruptedException, IOException {
		try {
			// Thread.sleep(2200);
			WebElement Authentication = driver.findElement(By.id("winAuthnentication"));
			Assert.assertNotNull(Authentication);
			Authentication.click();
			// Thread.sleep(200);
			// Authentication.click();
			// Thread.sleep(400);
			// Authentication.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Windows_Authentication()  fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 73, 2);
			ExcelWriter(ExecResult, ErrorDesc, 73, 2);

		}
	}

	public void sqlServer() throws IOException {
		try {
			WebElement Server = driver.findElement(By.xpath("//input[contains(@id, 'sqlServer')]"));
			Assert.assertNotNull(Server);
			// Server.click();
			Server.clear();
			// Server.sendKeys("(local)");
			Server.sendKeys(".");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("sqlServer() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 74, 2);
			ExcelWriter(ExecResult, ErrorDesc, 74, 3);

		}

	}

	public void WrongServerName() throws IOException {
		try {
			// TODO Auto-generated method stub
			WebElement Server = driver.findElement(By.xpath("//input[contains(@id, 'sqlServer')]"));
			Assert.assertNotNull(Server);
			// Server.click();
			Server.clear();
			Server.sendKeys("(locals)");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WrongServerName() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 75, 2);
			ExcelWriter(ExecResult, ErrorDesc, 75, 3);

		}
	}

	public void WrongDatabaseName() throws IOException {
		try {
			WebElement Database = driver.findElement(By.xpath("//input[contains(@id, 'sqlDbase')]"));
			Assert.assertNotNull(Database);
			Database.clear();
			Database.sendKeys("fusionchartsdbg");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WrongDatabaseName() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 76, 2);
			ExcelWriter(ExecResult, ErrorDesc, 76, 3);

		}
	}

	public void connectbutton() throws InterruptedException, IOException {
		try {
			driver.findElement(By.xpath("//button[text()='Connect']")).click();
			Thread.sleep(5000);

			// Assert.assertTrue(driver.getPageSource().contains("Connection to
			// server could not be established. Please check your connection
			// settings and try again."));
			// Thread.sleep(3000);
			// driver.findElement(By.xpath("//button[text()='OK']")).click();;
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("connectbutton() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 77, 2);
			ExcelWriter(ExecResult, ErrorDesc, 77, 3);

		}

	}

	public void QueryString_Error() throws IOException {
		try {
			Assert.assertTrue(driver.getPageSource().contains(
					"Connection to server could not be established. Please check your connection settings and try again."));

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("QueryString_Error()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 78, 2);
			ExcelWriter(ExecResult, ErrorDesc, 78, 3);

		}
	}

	public void ExcelUntickSelectFields() throws InterruptedException, IOException {
		try {

			Thread.sleep(2000);
			WebElement Allfield = driver.findElement(By.cssSelector("div.cc-x-grid3-hdchecker"));
			// if(Allfield.isSelected())
			Allfield.click();
		//	Allfield.click();

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelUntickSelectFields()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 408, 2);
			ExcelWriter(ExecResult, ErrorDesc, 408, 3);

		}

	}

	public void CSVUntickSelectFields() throws InterruptedException, IOException {

      try{
		WebElement Allfield = driver.findElement(By.cssSelector("div.cc-x-grid3-hdchecker"));
		Allfield.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CSVUntickSelectFields() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 407, 2);
			ExcelWriter(ExecResult, ErrorDesc, 407, 3);

		}
	}

	public void ConfigureSourcePage() throws IOException {
		try {

			WebElement Allfield = driver.findElement(By.xpath("//div/li[1]/ul/li[1]/div/a/span"));
			Assert.assertNotNull(Allfield);
			Allfield.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CSVUntickSelectFields() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 80, 2);
			ExcelWriter(ExecResult, ErrorDesc, 80, 3);

		}

	}

	public void username() throws InterruptedException, IOException {
		try {
			driver.findElement(By.id("sqlLogin")).clear();
			driver.findElement(By.id("sqlLogin")).sendKeys("test");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("username() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 81, 2);
			ExcelWriter(ExecResult, ErrorDesc, 81, 3);

		}

	}

	public void password() throws InterruptedException, IOException {
		try {
			driver.findElement(By.id("sqlPass")).clear();
			driver.findElement(By.id("sqlPass")).sendKeys("test");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("password() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 82, 2);
			ExcelWriter(ExecResult, ErrorDesc, 82, 3);

		}

	}

	public void sqlDbase() throws IOException {
		try {
			WebElement Dbase = driver.findElement(By.xpath("//input[contains(@id, 'sqlDbase')]"));
			Assert.assertNotNull(Dbase);
			// Dbase.click();
			Dbase.clear();
			Dbase.sendKeys("fusionchartsdb");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("sqlDbase() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 83, 2);
			ExcelWriter(ExecResult, ErrorDesc, 83, 3);

		}
	}

	public void DataTable() throws InterruptedException, IOException {
		try {
			WebElement Dbase = driver.findElement(By.xpath("//input[contains(@id, 'dsEntity')]"));
			Assert.assertNotNull(Dbase);
			Dbase.click();
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[9]"));
			DropDownValue.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DataTable() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 84, 2);
			ExcelWriter(ExecResult, ErrorDesc, 84, 3);

		}

		/*
		 * Thread.sleep(500); Dbase.sendKeys(Keys.ARROW_DOWN);
		 * Thread.sleep(1000); Dbase.sendKeys(Keys.ARROW_DOWN);
		 * Thread.sleep(1100); Dbase.sendKeys(Keys.ARROW_DOWN);
		 * Thread.sleep(1200); Dbase.sendKeys(Keys.ARROW_DOWN);
		 * Thread.sleep(1300); Dbase.sendKeys(Keys.ARROW_DOWN);
		 * Thread.sleep(2000); Dbase.sendKeys(Keys.ENTER)
		 */
	}

	////////////////////// CSV File///////////////////////////

	public void ChooseCSVFile() throws InterruptedException, IOException {
		try {
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull("DataProvider");
			DataProvider.click();
			Thread.sleep(1000);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1100);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1200);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1300);
			DataProvider.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChooseCSVFile() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 85, 2);
			ExcelWriter(ExecResult, ErrorDesc, 85, 3);

		}
	}

	public void CSVDataStoredRadioButton() throws IOException {
		try {
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			WebElement Panel = driver.findElement(By.id("dataSourceStep"));
			// List<WebElement> CSVDataStored =
			// Panel.findElements(By.cssSelector("input[value=’dsc-plain’]"));
			List<WebElement> CSVDataStored = Panel.findElements(By.tagName("input"));

			for (int i = 0; i < CSVDataStored.size(); i++) {
				Assert.assertNotNull(CSVDataStored.get(2));
				// Assert.assertNotNull("CSVDataStored");
				CSVDataStored.get(2).click();
				// CSVDataStored.get(4).click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CSVDataStoredRadioButton() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 86, 2);
			ExcelWriter(ExecResult, ErrorDesc, 86, 3);

		}
	}

	public void CSVScanner() throws IOException {
		try {
			Thread.sleep(3000);
			WebElement CSVData = driver.findElement(By.id("csvData"));
			Assert.assertNotNull("CSVData");

			Scanner scanner = new Scanner(
					new File("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\CSVData.txt"));
			scanner.useDelimiter(",");
			while (scanner.hasNext()) {
				String scan = scanner.next() + ",";
				scanner.useDelimiter("\\s*,\\s*");
				// System.out.print(scanner.next()+",");
				CSVData.click();
				CSVData.sendKeys(scan);
			}

			scanner.close();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CSVScanner()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 87, 2);
			ExcelWriter(ExecResult, ErrorDesc, 87, 3);

		}
	}

	public void ClearCSVDataField() throws IOException {
		try {
			WebElement csvData = driver.findElement(By.id("csvData"));
			csvData.clear();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClearCSVDataField() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 88, 2);
			ExcelWriter(ExecResult, ErrorDesc, 88, 3);

		}

	}

	public void Verify_CSV() throws IOException {
		try {

			Assert.assertTrue(driver.getPageSource().contains("No CSV data provided. Please, enter some data"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Verify_CSV() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 89, 2);
			ExcelWriter(ExecResult, ErrorDesc, 89, 3);

		}
	}
	/*
	 * public void CSVData() throws InterruptedException { WebElement CSVData =
	 * driver.findElement(By.id("csvData")); Assert.assertNotNull("CSVData");
	 * CSVData.click();
	 *
	 * /*CSVData.sendKeys("Company Name,Employee Name,salary " +
	 * "Sintek,Saheena,78900" + "Simaentik,Kajol,78989" +
	 * "Kutitek,Rahul,67890");
	 */
	// Thread.sleep(3000);
	/*
	 * driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
	 *
	 * String csv = ReadCSVFile(); csv =
	 * csv.replace(System.getProperty("line.separator"), "\\n");
	 *
	 * JavascriptExecutor js = (JavascriptExecutor) driver;
	 * js.executeScript("Ext.getCmp(\'csvData\').setValue(\'" + csv + "\')");
	 * System.out.println("Ext.getCmp(\'csvData\').setValue(\'" + csv + "\')");
	 *
	 * //CSVData.sendKeys(ReadCSVFile()); }
	 *
	 * public String ReadCSVFile(){ //String myCurrentDir =
	 * System.getProperty("java.class.path"); //myCurrentDir =
	 * myCurrentDir.substring(0, myCurrentDir.indexOf(";") - 3);
	 *
	 * String myCurrentDir = System.getProperty("sun.java.command");
	 * myCurrentDir =
	 * myCurrentDir.substring(22,myCurrentDir.lastIndexOf("/src")).replace("/",
	 * File.separator); // String strFile =
	 * "C:\\Collabion\\CollabionChartsForSharePointAutomationTesting\\CSVData.txt";
	 * myCurrentDir = myCurrentDir + File.separator + "CSVData.txt";
	 * System.out.println(myCurrentDir); //System.out.println(myCurrentDir);
	 *
	 * File o_File1 = new File(myCurrentDir); try { BufferedReader o_br1=new
	 * BufferedReader(new FileReader(o_File1)); return
	 * ReadAllStringFromFile(o_br1); } catch (IOException e) {
	 * e.printStackTrace(); } return "";
	 *
	 * }
	 *
	 * public String ReadAllStringFromFile(BufferedReader buffIn) throws
	 * IOException { StringBuilder everything = new StringBuilder(); String
	 * line; while( (line = buffIn.readLine()) != null) {
	 * everything.append(line);
	 * everything.append(System.getProperty("line.separator")); } return
	 * everything.toString(); }
	 */

	public void Parse_Data() throws InterruptedException, IOException {
		try {
			
			Thread.sleep(2000);
			WebElement Parse_Data = driver.findElement(By.xpath("//button[text()='Parse Data']"));
			Assert.assertNotNull("Parse_Data");
			Parse_Data.click();
			Thread.sleep(2400);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Parse_Data() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 90, 2);
			ExcelWriter(ExecResult, ErrorDesc, 90, 3);

		}
	}

	//////////////////////////////// Web Parts in current
	//////////////////////////////// page//////////////////////////////////////

	public void Current_Page_Data_Provider() throws IOException {
		try {
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull("DataProvider");
			DataProvider.click();
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			DataProvider.sendKeys(Keys.ARROW_DOWN);
			DataProvider.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Current_Page_Data_Provider() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 91, 2);
			ExcelWriter(ExecResult, ErrorDesc, 91, 3);

		}
	}

	public void Get_Provider_Parts() throws IOException {
		try {
			WebElement Get_Provider_Parts = driver.findElement(By.xpath("//button[text()='Get Provider Parts']"));
			Assert.assertNotNull("Get_Provider_Parts");
			Get_Provider_Parts.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Get_Provider_Parts() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 92, 2);
			ExcelWriter(ExecResult, ErrorDesc, 92, 3);

		}
	}

	public void Current_Web_part() throws InterruptedException, IOException {
		try {

			WebElement Current_Web_part = driver.findElement(By.id("dsConnectedWebpart"));
			Assert.assertNotNull("Current_Web_part");
			Current_Web_part.click();
			Thread.sleep(1000);
			Current_Web_part.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Current_Web_part() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 93, 2);
			ExcelWriter(ExecResult, ErrorDesc, 93, 3);

		}
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Chart Type\\\\\\\\\\

	public void ChartTypeClick() throws InterruptedException, IOException {
		try {
			Thread.sleep(5000);
			driver.findElement(By.xpath("//div/li[3]/ul/li[1]/div/a/span")).click();
			Assert.assertTrue(driver.getPageSource().contains("Chart Type"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartTypeClick() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 94, 2);
			ExcelWriter(ExecResult, ErrorDesc, 94, 3);

		}

	}

	public void ChartcategorySingleSeriesChoose() throws IOException {
		try {

			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#chartTypeStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				WebElement checkbox1 = elementsRoots.get(i).findElement(By.tagName("input"));
				checkbox1.click();
				WebElement SingleSeries = (WebElement) (new WebDriverWait(driver, 10)
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Single Series']"))));
				SingleSeries.click();

				try {
					Assert.assertTrue(checkbox1.isEnabled());
					Assert.assertNotNull(checkbox1);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Chartcategory Choose Element Not Found: " + e.getMessage());
				}
			}
				} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartcategoryChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 531, 2);
			ExcelWriter(ExecResult, ErrorDesc, 531, 3);

		}

	}
			
		
	/////////////////////////	
		
			public void ChartcategoryMultiSeriesChoose() throws IOException {
		try {

			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#chartTypeStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				WebElement checkbox1 = elementsRoots.get(i).findElement(By.tagName("input"));
				checkbox1.click();
				WebElement SingleSeries = (WebElement) (new WebDriverWait(driver, 10)
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Multi Series']"))));
				SingleSeries.click();

				try {
					Assert.assertTrue(checkbox1.isEnabled());
					Assert.assertNotNull(checkbox1);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Chartcategory Choose Element Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartcategoryChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 532, 2);
			ExcelWriter(ExecResult, ErrorDesc, 532, 3);

		}

	}	
			
	/////////////////////////////		
			
				public void ChartcategoryStackedChartChoose() throws IOException {
		try {

			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#chartTypeStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				WebElement checkbox1 = elementsRoots.get(i).findElement(By.tagName("input"));
				checkbox1.click();
				WebElement SingleSeries = (WebElement) (new WebDriverWait(driver, 10)
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Stacked Charts']"))));
				SingleSeries.click();

				try {
					Assert.assertTrue(checkbox1.isEnabled());
					Assert.assertNotNull(checkbox1);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Chartcategory Choose Element Not Found: " + e.getMessage());
				}
			}
			} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartcategoryChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 533, 2);
			ExcelWriter(ExecResult, ErrorDesc, 533, 3);

		}

	}
	////////////////////////		
			public void ChartcategoryCombinationChartChoose() throws IOException {
		try {

			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#chartTypeStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				WebElement checkbox1 = elementsRoots.get(i).findElement(By.tagName("input"));
				checkbox1.click();
				WebElement SingleSeries = (WebElement) (new WebDriverWait(driver, 10)
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Combination Charts']"))));
				SingleSeries.click();

				try {
					Assert.assertTrue(checkbox1.isEnabled());
					Assert.assertNotNull(checkbox1);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Chartcategory Choose Element Not Found: " + e.getMessage());
				}
			}
			} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartcategoryChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 534, 2);
			ExcelWriter(ExecResult, ErrorDesc, 534, 3);

		}

	}
			
/////////////////////////////////			

	public void ChartcategoryXYChartChoose() throws IOException {
		try {

			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#chartTypeStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				WebElement checkbox1 = elementsRoots.get(i).findElement(By.tagName("input"));
				checkbox1.click();
				WebElement SingleSeries = (WebElement) (new WebDriverWait(driver, 10)
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='X-Y Charts']"))));
				SingleSeries.click();

				try {
					Assert.assertTrue(checkbox1.isEnabled());
					Assert.assertNotNull(checkbox1);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Chartcategory Choose Element Not Found: " + e.getMessage());
				}
			}
			
			} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartcategoryChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 535, 2);
			ExcelWriter(ExecResult, ErrorDesc, 535, 3);

		}

	}
///////////////////////////////////

public void ChartcategoryScrollChartChoose() throws IOException {
		try {

			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#chartTypeStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				WebElement checkbox1 = elementsRoots.get(i).findElement(By.tagName("input"));
				checkbox1.click();
				WebElement SingleSeries = (WebElement) (new WebDriverWait(driver, 10)
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Scroll Charts']"))));
				SingleSeries.click();

				try {
					Assert.assertTrue(checkbox1.isEnabled());
					Assert.assertNotNull(checkbox1);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Chartcategory Choose Element Not Found: " + e.getMessage());
				}
			}
          } catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartcategoryChoose() fail in catch" + ErrorDesc);
		 } finally {

			ExcelWriter(ExecResult, ErrorDesc, 536, 2);
			ExcelWriter(ExecResult, ErrorDesc, 536, 3);

		}

	}





			
			/*
			 * WebElement Tag1 = driver.findElement(By.cssSelector(
			 * "div#chartTypeStep.x-panel.x-panel-noborder"));
			 *
			 * driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			 * WebElement Tag2 = Tag1.findElement(By.tagName("input"));
			 *
			 * if (Tag2.isDisplayed()) { for (int i = 0; i <
			 * Tag2.toString().length(); i++) { if (i == 1) { Tag2.click();
			 * Tag2.click(); Tag2.click(); Tag2.sendKeys(Keys.ARROW_UP);
			 * Tag2.sendKeys(Keys.ENTER); break; }
			 *
			 * } }
			 */
		

	public void Column2DChoose() throws IOException {
		try {
			WebElement Chart = driver.findElement(By
					.xpath("//img[contains(@src,'//_layouts/CollabionCharts/resources/images/charts/Column2D.jpg')]"));
			// "//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Column2D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(" Column2D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Column2DChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 96, 2);
			ExcelWriter(ExecResult, ErrorDesc, 96, 3);

		}
	}

	public void ccsptestsvrColumn2DChoose() throws IOException {
    try{
		WebElement Chart = driver.findElement(By.xpath("//img[contains(@src,'Column2D.jpg')]"));
		Chart.click();
       } catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ccsptestsvrColumn2DChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 406, 2);
			ExcelWriter(ExecResult, ErrorDesc, 406, 3);

		}
	}

	public void Apply() throws IOException {
		try {

			WebElement Button = driver.findElement(By.className("x-btn-text"));
			if (Button.isDisplayed()) {

				for (int i = 0; i < Button.toString().length(); i++) {

					Button.click();
					break;

					// WebElement Apply=
					// driver.findElement(By.xpath("//button[text()='Apply']"));
					// Apply.click();
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ccsptestsvrColumn2DChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 97, 2);
			ExcelWriter(ExecResult, ErrorDesc, 97, 3);

		}

	}
	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Captions\\\\\\\\\\

	public void Captions() throws InterruptedException, IOException {
		try {
			Thread.sleep(8000);

			driver.findElement(By.xpath("//div/li[3]/ul/li[2]/div/a/span")).click();

			Assert.assertTrue(driver.getPageSource().contains("Captions"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Captions() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 98, 2);
			ExcelWriter(ExecResult, ErrorDesc, 98, 3);

		}
	}

	public void ChartTitle() throws InterruptedException, IOException {
		try {
			try {
				Thread.sleep(3000);

				List<WebElement> elementsRoot = driver
						.findElements(By.cssSelector("div#axisTitlesStep.x-panel.x-panel-noborder"));

				for (int i = 0; i < elementsRoot.size(); i++) {

					WebElement checkbox = elementsRoot.get(i).findElement(By.tagName("input"));

					checkbox.click();
					Thread.sleep(3200);
					checkbox.clear();
					checkbox.sendKeys(Keys.DELETE);
					checkbox.sendKeys(Keys.BACK_SPACE);
					checkbox.sendKeys(Keys.CLEAR);
					// checkbox.click();

					checkbox.sendKeys("{wizardFilter}");

					// Assert.assertTrue(checkbox.getTagName().matches("input"));

				} // catch (AssertionError e) {

			} catch (Exception e) {

				System.out.println("Chart Title field not found: " + e.getMessage());

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartTitle() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 99, 2);
			ExcelWriter(ExecResult, ErrorDesc, 99, 3);

		}

	}

	public void ChartSubTitle() throws InterruptedException, IOException {
		try {
			try {
				List<WebElement> elementsRoots = driver
						.findElements(By.cssSelector("div#axisTitlesStep.x-panel.x-panel-noborder"));

				for (int i = 0; i < elementsRoots.size(); i++) {
					List<WebElement> checkbox1 = elementsRoots.get(i).findElements(By.tagName("input"));

					checkbox1.get(1).click();
					checkbox1.get(1).clear();
					checkbox1.get(1).sendKeys(Keys.DELETE);
					checkbox1.get(1).sendKeys(Keys.BACK_SPACE);
					checkbox1.get(1).sendKeys(Keys.CLEAR);

					checkbox1.get(1).sendKeys("{userFilter}");
					// try {

					// Assert.assertTrue(checkbox1.get(1).isDisplayed());

				} // catch (AssertionError e) {

			} catch (Exception e) {

				System.out.println("Chart Sub Title field not found: " + e.getMessage());

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChartSubTitle() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 100, 2);
			ExcelWriter(ExecResult, ErrorDesc, 100, 3);

		}

	}

	public void XAxisTitle() throws IOException {
		try {
			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#axisTitlesStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				List<WebElement> checkbox1 = elementsRoots.get(i).findElements(By.tagName("input"));
				checkbox1.get(2).click();
				checkbox1.get(2).clear();
				checkbox1.get(2).sendKeys(Keys.DELETE);
				checkbox1.get(2).sendKeys(Keys.BACK_SPACE);
				checkbox1.get(2).sendKeys(Keys.CLEAR);
				checkbox1.get(2).sendKeys("TestXAxisTitle");

				try {
					Assert.assertTrue(checkbox1.get(2).isDisplayed());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("X Axis Title Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XAxisTitle() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 101, 2);
			ExcelWriter(ExecResult, ErrorDesc, 101, 3);

		}
	}

	public void PrimaryYAxisTitle() throws IOException {
		try {
			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#axisTitlesStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				List<WebElement> checkbox1 = elementsRoots.get(i).findElements(By.tagName("input"));
				checkbox1.get(3).click();
				checkbox1.get(3).clear();
				checkbox1.get(3).sendKeys(Keys.DELETE);
				checkbox1.get(3).sendKeys(Keys.BACK_SPACE);
				checkbox1.get(3).sendKeys(Keys.CLEAR);
				checkbox1.get(3).sendKeys("TestPrimaryYAxisTitle");

				try {
					Assert.assertTrue(checkbox1.get(3).isDisplayed());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Primary Y Axis Title Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("PrimaryYAxisTitle() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 102, 2);
			ExcelWriter(ExecResult, ErrorDesc, 102, 3);

		}
	}

	public void YAxisTitle() throws IOException {
		try {
			List<WebElement> elementsRoots = driver
					.findElements(By.cssSelector("div#axisTitlesStep.x-panel.x-panel-noborder"));

			for (int i = 0; i < elementsRoots.size(); i++) {
				List<WebElement> checkbox1 = elementsRoots.get(i).findElements(By.tagName("input"));
				checkbox1.get(4).click();
				checkbox1.get(4).click();

				try {
					Assert.assertTrue(checkbox1.get(4).isDisplayed());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println(" Y Axis Title Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("YAxisTitle() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 103, 2);
			ExcelWriter(ExecResult, ErrorDesc, 103, 3);

		}
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-SeriesCustomization\\\\\\\\\\

	public void SeriesCustomization() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			driver.findElement(By.xpath("//div/li[3]/ul/li[3]/div/a/span")).click();

			Assert.assertTrue(driver.getPageSource().contains("Series Customization"));
			// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

			Thread.sleep(10000);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SeriesCustomization() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 104, 2);
			ExcelWriter(ExecResult, ErrorDesc, 104, 3);

		}
	}

	public void XAxisLabelColumn() throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);
			WebElement XAxisLabel = driver.findElement(
					By.cssSelector("input#_Categories_SortColumn.x-form-text.x-form-field.x-trigger-noedit"));
			XAxisLabel.click();
			XAxisLabel.sendKeys(Keys.ARROW_DOWN);
			XAxisLabel.sendKeys(Keys.ENTER);

			try {
				Assert.assertTrue(XAxisLabel.isEnabled());
				Assert.assertNotNull(XAxisLabel);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("X Axis Label Column Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XAxisLabelColumn()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 105, 2);
			ExcelWriter(ExecResult, ErrorDesc, 105, 3);

		}
	}

	public void XAxisSortColumn() throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);
			WebElement XAxisSortColumn = driver.findElement(
					By.cssSelector("input#_Categories_ColumnName.x-form-text.x-form-field.x-trigger-noedit"));
			XAxisSortColumn.click();
			XAxisSortColumn.sendKeys(Keys.ARROW_DOWN);
			XAxisSortColumn.sendKeys(Keys.ENTER);

			try {
				Assert.assertTrue(XAxisSortColumn.isEnabled());
				Assert.assertNotNull(XAxisSortColumn);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("X Axis Sort Column Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XAxisSortColumn() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 106, 2);
			ExcelWriter(ExecResult, ErrorDesc, 106, 3);

		}
	}

	public void XAxisSortOrder() throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);
			WebElement XAxisSortOrder = driver
					.findElement(By.cssSelector("input#_Categories_Sort.x-form-text.x-form-field.x-trigger-noedit"));
			XAxisSortOrder.click();
			XAxisSortOrder.sendKeys(Keys.ARROW_DOWN);
			XAxisSortOrder.sendKeys(Keys.ENTER);
			try {
				Assert.assertTrue(XAxisSortOrder.isEnabled());
				Assert.assertNotNull(XAxisSortOrder);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("X Axis Sort Order Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("XAxisSortOrder() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 107, 2);
			ExcelWriter(ExecResult, ErrorDesc, 107, 3);

		}
	}

	public void SeriesCustomizationTabChange() throws IOException {
		try {

			List<WebElement> panel = driver.findElements(By.cssSelector("div#axesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> SeriesCustomizationXAxis = panel.get(i)
						.findElements(By.cssSelector("span.x-tab-strip-text"));
				SeriesCustomizationXAxis.get(1).click();

				try {
					Assert.assertTrue(driver.getPageSource().contains("Data Columns"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Series Customization Tab Change Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SeriesCustomizationTabChange() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 108, 2);
			ExcelWriter(ExecResult, ErrorDesc, 108, 3);

		}
	}

	public void SeriesValueColumn() throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);
			WebElement SeriesValueColumn = driver.findElement(
					By.cssSelector("input#_DataSets_0_ColumnName.x-form-text.x-form-field.x-trigger-noedit"));
			SeriesValueColumn.click();
			SeriesValueColumn.sendKeys(Keys.ARROW_DOWN);
			SeriesValueColumn.sendKeys(Keys.ENTER);

			try {
				Assert.assertTrue(SeriesValueColumn.isEnabled());

				Assert.assertNotNull(SeriesValueColumn);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Series Value Column Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" SeriesValueColumn() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 109, 2);
			ExcelWriter(ExecResult, ErrorDesc, 109, 3);

		}
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Labels,Values & Tooltips\\\\\\\\\\

	public void LabelsValuesTooltips() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			Assert.assertNotNull(driver.findElement(By.xpath("//div/li[3]/ul/li[4]/div/a/span")));
			driver.findElement(By.xpath("//div/li[3]/ul/li[4]/div/a/span")).click();
			// driver.findElement(By.xpath("//button[text()='Next
			// »']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("LabelsValuesTooltips() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 110, 2);
			ExcelWriter(ExecResult, ErrorDesc, 110, 3);

		}
	}

	public void Display() throws IOException {
		try {
			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> Display = panel.get(i).findElements(By.tagName("input"));
				// Assert.assertNotNull(Display.get(1));
				Display.get(1).click();
				Display.get(1).sendKeys(Keys.ARROW_DOWN);
				Display.get(1).sendKeys(Keys.ARROW_DOWN);
				Display.get(1).sendKeys(Keys.ARROW_DOWN);
				Display.get(1).sendKeys(Keys.ARROW_DOWN);
				Display.get(1).sendKeys(Keys.ARROW_DOWN);
				Display.get(1).sendKeys(Keys.ENTER);

				try {
					Assert.assertTrue(Display.get(1).isEnabled());
					Assert.assertNotNull(Display.get(1));
				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Display as Dropdown Element Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Display() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 111, 2);
			ExcelWriter(ExecResult, ErrorDesc, 111, 3);

		}
	}

	public void nthLabel() throws IOException {
		try {
			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> nthLabel = panel.get(i).findElements(By.tagName("input"));
				nthLabel.get(2).click();
				nthLabel.get(2).clear();
				nthLabel.get(2).sendKeys("4");

				try {
					Assert.assertTrue(nthLabel.get(2).isEnabled());
					Assert.assertNotNull(nthLabel.get(2));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("n-thLabel Element Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("nthLabel()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 112, 2);
			ExcelWriter(ExecResult, ErrorDesc, 112, 3);

		}

	}

	public void stagger() throws IOException {
		try {
			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> nthLabel = panel.get(i).findElements(By.tagName("input"));
				nthLabel.get(3).click();
				nthLabel.get(3).clear();
				nthLabel.get(3).sendKeys("5");

				try {
					Assert.assertTrue(nthLabel.get(3).isEnabled());
					Assert.assertNotNull(nthLabel.get(3));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Number of stagger lines: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("stagger() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 113, 2);
			ExcelWriter(ExecResult, ErrorDesc, 113, 3);

		}

	}

	public void RotateValuesWhenDisplayed() throws IOException {
		try {
			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> nthLabel = panel.get(i).findElements(By.tagName("input"));
				nthLabel.get(8).click();

				try {
					Assert.assertTrue(nthLabel.get(8).isSelected());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Rotate Values When Displayed checkbox Not Found: " + e.getMessage());
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("RotateValuesWhenDisplayed() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 114, 2);
			ExcelWriter(ExecResult, ErrorDesc, 114, 3);

		}
	}

	public void PlaceValuesInsideDataPlot() throws IOException {
		try {
			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> nthLabel = panel.get(i).findElements(By.tagName("input"));
				nthLabel.get(9).click();

				try {
					Assert.assertTrue(nthLabel.get(9).isSelected());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Place Values Inside DataPlot checkbox Not Found: " + e.getMessage());
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("PlaceValuesInsideDataPlot() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 115, 2);
			ExcelWriter(ExecResult, ErrorDesc, 115, 3);

		}

	}

	public void LabelValuesTooltipTabChange() throws IOException {
		try {

			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> LabelValuesTooltipTabChange = panel.get(i)
						.findElements(By.cssSelector("span.x-tab-strip-text"));
				LabelValuesTooltipTabChange.get(1).click();

				try {

					Assert.assertTrue(driver.getPageSource().contains("Common properties"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Data Plot Tab page Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("LabelValuesTooltipTabChange() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 116, 2);
			ExcelWriter(ExecResult, ErrorDesc, 116, 3);

		}

	}

	public void UseRoundEdges() throws IOException {
		try {
			List<WebElement> paneldiv = driver.findElements(By.cssSelector("fieldset#dpCommonProp.x-fieldset"));
			for (int i = 0; i < paneldiv.size(); i++) {
				List<WebElement> LabelValuesTooltipTabChange = paneldiv.get(i).findElements(By.tagName("input"));
				LabelValuesTooltipTabChange.get(0).click();

				try {

					Assert.assertTrue(LabelValuesTooltipTabChange.get(0).isSelected());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Use Round Edges checkbox Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("UseRoundEdges() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 117, 2);
			ExcelWriter(ExecResult, ErrorDesc, 117, 3);

		}

	}

	public void PlotFillAngle() throws IOException {
		try {
			List<WebElement> paneldiv = driver.findElements(By.cssSelector("fieldset#dpCommonProp.x-fieldset"));
			for (int i = 0; i < paneldiv.size(); i++) {
				List<WebElement> PlotFillAngle = paneldiv.get(i).findElements(By.tagName("input"));
				PlotFillAngle.get(1).click();
				PlotFillAngle.get(1).clear();
				PlotFillAngle.get(1).sendKeys("250");

				try {

					Assert.assertTrue(PlotFillAngle.get(1).isEnabled());
					Assert.assertNotNull(PlotFillAngle);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Plot Fill Angle Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("PlotFillAngle() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 118, 2);
			ExcelWriter(ExecResult, ErrorDesc, 118, 3);

		}
	}

	public void PlotFillOpaqueness() throws IOException {
		try {
			List<WebElement> paneldiv = driver.findElements(By.cssSelector("fieldset#dpCommonProp.x-fieldset"));
			for (int i = 0; i < paneldiv.size(); i++) {
				List<WebElement> PlotFillOpaqueness = paneldiv.get(i).findElements(By.tagName("input"));
				PlotFillOpaqueness.get(2).click();
				PlotFillOpaqueness.get(2).clear();
				PlotFillOpaqueness.get(2).sendKeys("110");
				try {

					Assert.assertTrue(PlotFillOpaqueness.get(2).isEnabled());
					Assert.assertNotNull(PlotFillOpaqueness.get(2));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Plot Fill Opaqueness Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("PlotFillOpaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 119, 2);
			ExcelWriter(ExecResult, ErrorDesc, 119, 3);

		}
	}

	public void ShowShadow() throws IOException {
		try {
			List<WebElement> paneldiv = driver.findElements(By.cssSelector("fieldset#dpCommonProp.x-fieldset"));
			for (int i = 0; i < paneldiv.size(); i++) {
				List<WebElement> PlotFillOpaqueness = paneldiv.get(i).findElements(By.tagName("input"));
				PlotFillOpaqueness.get(5).click();

				try {
					Assert.assertTrue(PlotFillOpaqueness.get(4).isEnabled());
					Assert.assertNotNull(PlotFillOpaqueness);

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Show Shadow Checkbox Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ShowShadow() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 120, 2);
			ExcelWriter(ExecResult, ErrorDesc, 120, 3);

		}
	}

	/*
	 * public void ShowGradientColor() throws InterruptedException {
	 * List<WebElement> paneldiv =
	 * driver.findElements(By.cssSelector("fieldset#dpCommonProp.x-fieldset"));
	 * for(int i = 0; i < paneldiv.size(); i++) { List<WebElement>
	 * PlotFillOpaqueness= paneldiv.get(i).findElements(By.tagName("input"));
	 * PlotFillOpaqueness.get(5).click(); Thread.sleep(4000);
	 * PlotFillOpaqueness.get(5).click(); } }
	 */
	public void GradientColor() throws InterruptedException, IOException {
		// try{

		List<WebElement> paneldiv = driver.findElements(By.cssSelector("fieldset#dpCommonProp.x-fieldset"));
		for (int i = 0; i < paneldiv.size(); i++) {
			List<WebElement> GradientColor = paneldiv.get(i).findElements(By.tagName("button"));
			GradientColor.get(0).click();
			// Thread.sleep(1500);
			// GradientColor.get(0).click();
			Thread.sleep(4000);
			WebElement colorpalette = driver.findElement(By.className("x-color-palette"));
			// Thread.sleep(11500);
			WebElement color = colorpalette.findElement(By.className("color-800080"));
			Thread.sleep(2500);
			color.click();
			// color.click();
			// GradientColor.get(0).sendKeys("339966");
		}
		/*
		 * }catch (Exception e) { ExecResult = "FAIL"; ErrorDesc =
		 * e.getMessage(); System.out.println(
		 * "Clear_All_Filters() fail in catch"+ErrorDesc); } finally {
		 * ExcelWriter(ExecResult, ErrorDesc,121,2); ExcelWriter(ExecResult,
		 * ErrorDesc,121,3);
		 *
		 *
		 * }
		 */
	}

	public void DataPlotColor() throws InterruptedException, IOException {
		try {
			Thread.sleep(9000);
			List<WebElement> paneldiv = driver.findElements(By.cssSelector("fieldset#dpCommonProp.x-fieldset"));
			for (int i = 0; i < paneldiv.size(); i++) {
				List<WebElement> GradientColor = paneldiv.get(i).findElements(By.tagName("input"));
				GradientColor.get(5).click();
				GradientColor.get(5).clear();
				GradientColor.get(5).sendKeys("339966");

				try {
					Assert.assertTrue(GradientColor.get(5).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Data Plot Color field Not Found: " + e.getMessage());
				}

				// GradientColor.get(5).sendKeys(Keys.TAB);
				// ((WebElement) GradientColor).click();

				/*
				 * Thread.sleep(3000); GradientColor.get(5).sendKeys("\t");
				 *
				 * Thread.sleep(4000); GradientColor.get(5).sendKeys("\t");
				 *
				 * Thread.sleep(5000); GradientColor.get(5)).se(Keys.TAB);
				 */

				// Thread.sleep(4000);
				// GradientColor.get(5).sendKeys(Keys.ENTER);
				// Thread.sleep(5000);
				// GradientColor.get(5).click();
				// Thread.sleep(5000);
				// GradientColor.get(5).sendKeys(Keys.TAB);
				// Thread.sleep(6000);

				// GradientColor.get(5).sendKeys("10");
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DataPlotColor() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 122, 2);
			ExcelWriter(ExecResult, ErrorDesc, 122, 3);

		}
	}

	public void Show_Plot_Border() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			WebElement div = driver.findElement(By.cssSelector("div#dpDataPlotTabEx.x-panel.x-panel-noborder"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Show_Plot_Border = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 2) {

					Show_Plot_Border.get(0).click();
					Assert.assertNotNull(Show_Plot_Border.get(0));
					// Show_Plot_Border.get(0).click();
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_Plot_Border()) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 123, 2);
			ExcelWriter(ExecResult, ErrorDesc, 123, 3);

		}
	}

	public void BorderColor() throws InterruptedException, IOException {
		try {

			Thread.sleep(3000);

			/*
			 * List<WebElement> paneldiv =
			 * driver.findElements(By.className("span.x-tab-strip-text"));
			 * for(int i = 0; i < paneldiv.size(); i++) { List<WebElement>
			 * BorderColor=
			 * paneldiv.get(i).findElements(By.cssSelector("x-btn-arrow"));
			 * BorderColor.get(2).click(); //BorderThickness.clear(); //
			 * BorderColor.get(2).sendKeys(Keys.ARROW_LEFT); //
			 * BorderColor.get(2).sendKeys(Keys.ENTER);
			 */

			WebElement div = driver.findElement(By.cssSelector("div#dpDataPlotTabEx.x-panel.x-panel-noborder"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));
			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> BorderOpaqueness = fieldset.get(i).findElements(By.tagName("button"));

				if (i == 2) {
					BorderOpaqueness.get(0).click();
					// BorderOpaqueness.get(0).click();
				}

				Thread.sleep(4000);
				WebElement colorpalette = driver.findElement(By.className("x-color-palette"));
				WebDriverWait waitList = new WebDriverWait(driver, 60);
				waitList.until(ExpectedConditions.visibilityOfElementLocated(By.id("color-800080")));
				WebElement color = colorpalette.findElement(By.className("color-800080"));
				Thread.sleep(4500);
				color.click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("BorderColor()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 124, 2);
			ExcelWriter(ExecResult, ErrorDesc, 124, 3);

		}
	}

	public void BorderThickness() throws InterruptedException, IOException {
		try {
			Thread.sleep(7000);
			WebElement div = driver.findElement(By.cssSelector("div#dpDataPlotTabEx.x-panel.x-panel-noborder"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));
			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> BorderThickness = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 2) {
					BorderThickness.get(1).click();
					BorderThickness.get(1).clear();
					BorderThickness.get(1).sendKeys("26");

					try {
						Assert.assertTrue(BorderThickness.get(1).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Border Thickness Field Not Found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("BorderThickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 125, 2);
			ExcelWriter(ExecResult, ErrorDesc, 125, 3);

		}

	}

	public void BorderOpaqueness() throws InterruptedException, IOException {
		try {

			WebElement div = driver.findElement(By.cssSelector("div#dpDataPlotTabEx.x-panel.x-panel-noborder"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));
			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> BorderOpaqueness = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 2) {
					BorderOpaqueness.get(2).click();
					BorderOpaqueness.get(2).clear();
					BorderOpaqueness.get(2).sendKeys("65");

					try {
						Assert.assertTrue(BorderOpaqueness.get(2).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Border Thickness Field Not Found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" BorderOpaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 126, 2);
			ExcelWriter(ExecResult, ErrorDesc, 126, 3);

		}
	}

	public void DashedPlotBorder() throws InterruptedException, IOException {
		try {

			WebElement div = driver.findElement(By.cssSelector("div#dpDataPlotTabEx.x-panel.x-panel-noborder"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));
			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> DashedPlotBorder = fieldset.get(i).findElements(By.tagName("input"));
				if (i == 2) {
					DashedPlotBorder.get(3).click();
					try {
						Assert.assertTrue(DashedPlotBorder.get(3).isSelected());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Dashed Plot Border Checkbox Not Found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DashedPlotBorder() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 127, 2);
			ExcelWriter(ExecResult, ErrorDesc, 127, 3);

		}
	}

	public void DashLength() throws InterruptedException, IOException {
		try {
			// Thread.sleep(1000);
			WebElement div = driver.findElement(By.cssSelector("div#dpDataPlotTabEx.x-panel.x-panel-noborder"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));
			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> DashLength = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 2) {
					DashLength.get(4).click();

					DashLength.get(4).clear();
					DashLength.get(4).sendKeys("6");

					try {
						Assert.assertTrue(DashLength.get(4).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Dash Length Field Not Found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DashLength() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 128, 2);
			ExcelWriter(ExecResult, ErrorDesc, 128, 3);

		}

	}

	public void DashGapLength() throws InterruptedException, IOException {
		try {
			WebElement div = driver.findElement(By.cssSelector("div#dpDataPlotTabEx.x-panel.x-panel-noborder"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));
			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> DashGapLength = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 2) {
					DashGapLength.get(5).click();
					DashGapLength.get(5).clear();
					DashGapLength.get(5).sendKeys("9");

					try {
						Assert.assertTrue(DashGapLength.get(5).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Dash Gap Length Field Not Found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DashGapLength() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 129, 2);
			ExcelWriter(ExecResult, ErrorDesc, 129, 3);

		}
	}

	public void TooltipsTabChange() throws IOException {
		try {

			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> LabelValuesTooltipTabChange = panel.get(i)
						.findElements(By.cssSelector("span.x-tab-strip-text"));
				LabelValuesTooltipTabChange.get(2).click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TooltipsTabChange() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 130, 2);
			ExcelWriter(ExecResult, ErrorDesc, 130, 3);

		}
	}

	public void Show_Tool_Tips() throws InterruptedException, IOException {
		try {
			// Thread.sleep(1000);
			WebElement fieldset = driver.findElement(By.tagName("fieldset"));
			WebElement Show_Tool_Tips = fieldset.findElement(By.cssSelector("label.x-fieldset-header"));

			Show_Tool_Tips.click();

			try {
				Assert.assertTrue(driver.getPageSource().contains("Show tool-tip"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Tooltip tab page not found Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_Tool_Tips() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 131, 2);
			ExcelWriter(ExecResult, ErrorDesc, 131, 3);

		}

	}

	public void IncludeShadow() throws InterruptedException, IOException {
		try {

			Thread.sleep(1000);
			WebElement paneldiv = driver.findElement(By.cssSelector("div#dpToolTipsTab.x-panel.x-panel-noborder"));
			List<WebElement> IncludeShadow = paneldiv.findElements(By.tagName("input"));

			// List<WebElement> IncludeShadow=
			// paneldiv.findElements(By.tagName("input"));

			for (int i = 0; i < IncludeShadow.size(); i++) {

				if (IncludeShadow.get(i).isDisplayed()) {
					// IncludeShadow.get(i).getAttribute("type");
					IncludeShadow.get(1).click();

					try {
						Assert.assertTrue(IncludeShadow.get(1).isSelected());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Include Shadow Checkbox Not Found: " + e.getMessage());
					}
					break;

				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("IncludeShadow() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 132, 2);
			ExcelWriter(ExecResult, ErrorDesc, 132, 3);

		}
	}

	public void Tool_Tips_Border_Color() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement paneldiv = driver.findElement(By.cssSelector("div#dpToolTipsTab.x-panel.x-panel-noborder"));
			List<WebElement> IncludeShadow = paneldiv.findElements(By.tagName("button"));

			// List<WebElement> IncludeShadow=
			// paneldiv.findElements(By.tagName("input"));

			for (int i = 0; i < IncludeShadow.size(); i++) {

				if (IncludeShadow.get(i).isDisplayed()) {
					// IncludeShadow.get(i).getAttribute("type");
					IncludeShadow.get(0).click();
					{

						WebElement colorpalette = driver.findElement(By.className("x-color-palette"));
						WebElement color = colorpalette.findElement(By.className("color-800080"));
						color.click();
					}

					/*
					 * WebElement colorpalette1 =
					 * driver.findElement(By.className("x-color-palette"));
					 * WebElement color1 =
					 * colorpalette1.findElement(By.className("color-00CCFF"));
					 * color1.click();/* } //
					 * driver.findElement(By.className("color-800080")).click();
					 *
					 * /* Thread.sleep(1600); JavascriptExecutor js =
					 * (JavascriptExecutor) driver; String jQuerySelector =
					 * "$(\".color-993366\")"; String findDropdown = "return " +
					 * jQuerySelector ; System.out.println(findDropdown);
					 * WebElement FontName = (WebElement)
					 * js.executeScript(findDropdown);
					 */

					// FontName.click();

					// FontName.sendKeys(Keys.ARROW_DOWN);
					// FontName.sendKeys(Keys.ENTER);

					/*
					 * Thread.sleep(1500); // WebElement colorpalette =
					 * driver.findElement(By.className("x-color-palette"));
					 * WebElement colorpalette1 =
					 * driver.findElement(By.className("color-800080"));
					 * WebDriverWait waitList = new WebDriverWait(driver,40);
					 * WebElement color =
					 * driver.findElement(By.className("color-800080"));
					 * //Thread.sleep(2500); color.click();sleep(5);
					 * //color.click();
					 */
					break;
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Tool_Tips_Border_Color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 133, 2);
			ExcelWriter(ExecResult, ErrorDesc, 133, 3);

		}

	}

	public void Tool_Tips_Background_Color() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement paneldiv = driver.findElement(By.cssSelector("div#dpToolTipsTab.x-panel.x-panel-noborder"));
			List<WebElement> IncludeShadow = paneldiv.findElements(By.tagName("button"));

			// List<WebElement> IncludeShadow=
			// paneldiv.findElements(By.tagName("input"));

			for (int i = 0; i < IncludeShadow.size(); i++) {

				IncludeShadow.get(1).click();

				WebElement colorpalette = driver.findElement(By.className("x-color-palette"));
				WebElement color = colorpalette.findElement(By.className("color-800080"));

				color.click();

				break;

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Tool_Tips_Background_Color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 134, 2);
			ExcelWriter(ExecResult, ErrorDesc, 134, 3);

		}
	}

	// }

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Number_Cosmetics\\\\\\\\\\
	public void Click_on_Cosmetics() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			driver.findElement(By.xpath("//div/li[3]/ul/li[5]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Click_on_Cosmetics()fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 135, 2);
			ExcelWriter(ExecResult, ErrorDesc, 135, 3);

		}

	}

	public void Image_SWF_URL() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(35,TimeUnit.SECONDS);
			Thread.sleep(20000);
			WebElement paneldiv = driver.findElement(By.id("chartCosmeticsStep"));
			List<WebElement> fieldset = paneldiv.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> Image_SWF_URL = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 0) {

					// Image_SWF_URL.get(1).click();
					// Image_SWF_URL.get(1).clear();
					// Image_SWF_URL.get(0).click();
					Image_SWF_URL.get(0).clear();
					Image_SWF_URL.get(0).sendKeys("11");
					Image_SWF_URL.get(1).sendKeys(
							"http://www.hdwallpapersimages.com/wp-content/uploads/2014/01/Winter-Tiger-Wild-Cat-Images.jpg");
					Image_SWF_URL.get(2).clear();
					Image_SWF_URL.get(2).sendKeys("13");

					try {
						Assert.assertTrue(Image_SWF_URL.get(2).isEnabled());

					} catch (AssertionError e) {
						verificationErrors.append("Value is incorrect- " + e.getMessage());
						// String message = e.getMessage();
						// System.out.println("Image SWF URL field Not Found: "
						// + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Image_SWF_URL() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 136, 2);
			ExcelWriter(ExecResult, ErrorDesc, 136, 3);

		}
	}

	public void Opaqueness_value() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);
			WebElement paneldiv = driver.findElement(By.id("chartCosmeticsStep"));
			List<WebElement> fieldset = paneldiv.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Opaqueness_value = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {

					Opaqueness_value.get(2).click();
					Opaqueness_value.get(2).clear();
					Opaqueness_value.get(2).sendKeys("71");
					Opaqueness_value.get(1).click();
					Opaqueness_value.get(1).clear();
					Opaqueness_value.get(1).sendKeys("47");
					Opaqueness_value.get(0).click();

					// color.click();
					// Opaqueness_value.get(3).sendKeys("40");
					// Opaqueness_value.get(0).clear();
					// Opaqueness_value.get(0).sendKeys("40");
					/*
					 * try {
					 * Assert.assertTrue(Image_SWF_URL.get(2).isEnabled());
					 *
					 * } catch (AssertionError e) {
					 *
					 * // String message = e.getMessage(); System.out.println(
					 * "Image SWF URL field Not Found: " + e.getMessage()); }
					 */
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Opaqueness_value() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 137, 2);
			ExcelWriter(ExecResult, ErrorDesc, 137, 3);

		}
	}

	public void Border_Cosmetics_Color() throws InterruptedException, IOException {
		try {

			Thread.sleep(3000);
			WebElement paneldiv = driver.findElement(By.id("chartCosmeticsStep"));
			List<WebElement> fieldset = paneldiv.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Opaqueness_color = fieldset.get(i).findElements(By.tagName("button"));

				if (i == 1) {
					Opaqueness_color.get(0).click();
					WebElement colorpalette = driver.findElement(By.className("x-color-palette"));
					WebElement color = colorpalette.findElement(By.className("color-800080"));
					// Thread.sleep(2500);
					color.click();
					Assert.assertNotNull(color);
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Border_Cosmetics_Color()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 138, 2);
			ExcelWriter(ExecResult, ErrorDesc, 138, 3);

		}
	}

	public void Image_SWF_Opaqueness() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);
			WebElement paneldiv = driver.findElement(By.id("chartCosmeticsStep"));
			List<WebElement> fieldset = driver.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Image_SWF_Opaqueness = fieldset.get(i).findElements(By.tagName("input"));

				// Image_SWF_Opaqueness.get(3).click();
				// Image_SWF_Opaqueness.get(3).clear();
				// Image_SWF_Opaqueness.get(3).sendKeys("Test2");

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Image_SWF_Opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 139, 2);
			ExcelWriter(ExecResult, ErrorDesc, 139, 3);

		}

	}

	public void Border_Opaqueness() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);
			WebElement paneldiv = driver.findElement(By.id("chartCosmeticsStep"));
			List<WebElement> fieldset = driver.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Border_Opaqueness = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {

					// Image_SWF_Opaqueness.get(3).click();
					Border_Opaqueness.get(2).click();
					Assert.assertNotNull(Border_Opaqueness.get(2));
					// Image_SWF_Opaqueness.get(3).sendKeys("Test2");
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Border_Opaqueness() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 140, 2);
			ExcelWriter(ExecResult, ErrorDesc, 140, 3);

		}

	}

	public void Canvas() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#chartCosmeticsStep.x-panel.x-panel-noborder"));
			List<WebElement> CanvasTabChange = panel.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < CanvasTabChange.size(); i++) {

				CanvasTabChange.get(1).click();
				try {
					Assert.assertTrue(driver.getPageSource().contains("Canvas Background"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Image SWF URL field Not Found: " + e.getMessage());

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Canvas() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 141, 2);
			ExcelWriter(ExecResult, ErrorDesc, 141, 3);

		}

	}

	public void CanvasThickness() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("fieldset#ccfsCanvasBorder.x-fieldset"));
			List<WebElement> Thickness = panel.findElements(By.tagName("input"));

			for (int i = 0; i < Thickness.size(); i++) {

				Thickness.get(0).click();
				Thickness.get(0).clear();
				Thickness.get(0).sendKeys("56");

				try {
					Assert.assertTrue(Thickness.get(0).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Canvas Thickness field Not Found: " + e.getMessage());

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CanvasThickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 142, 2);
			ExcelWriter(ExecResult, ErrorDesc, 142, 3);

		}

	}

	public void CanvasBorderOpaqueness() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("fieldset#ccfsCanvasBorder.x-fieldset"));
			List<WebElement> Thickness = panel.findElements(By.tagName("input"));

			for (int i = 0; i < Thickness.size(); i++) {

				Thickness.get(1).click();
				Thickness.get(1).clear();
				Thickness.get(1).sendKeys("34");

				try {
					Assert.assertTrue(Thickness.get(1).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Canvas Border Opaqueness field Not Found: " + e.getMessage());

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CanvasBorderOpaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 143, 2);
			ExcelWriter(ExecResult, ErrorDesc, 143, 3);

		}

	}

	public void CanvasBorderColor() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("fieldset#ccfsCanvasBorder.x-fieldset"));
			List<WebElement> BorderColor = panel.findElements(By.tagName("button"));

			for (int i = 0; i < BorderColor.size(); i++) {

				BorderColor.get(0).click();

				/*
				 * WebElement colorpalette =
				 * driver.findElement(By.className("x-color-palette"));
				 *
				 *
				 * WebElement color =
				 * colorpalette.findElement(By.className("color-800080"));
				 *
				 * color.click();sleep(5);
				 */

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CanvasBorderColor() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 144, 2);
			ExcelWriter(ExecResult, ErrorDesc, 144, 3);

		}

	}

	public void CanvasBackgroundOpaqueness() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			Thread.sleep(1200);
			WebElement panel = driver.findElement(By.id("ccCanvasTab"));

			List<WebElement> FirstOpaqueness = panel.findElements(By.className("x-form-num-field"));

			Assert.assertNotNull(FirstOpaqueness.get(0));
			FirstOpaqueness.get(0).click();
			FirstOpaqueness.get(0).clear();
			FirstOpaqueness.get(0).sendKeys("98");

			/*
			 * try { Assert.assertTrue(FirstOpaqueness.get(1).isEnabled());
			 *
			 * } catch (AssertionError e) {
			 *
			 * // String message = e.getMessage(); System.out.println(
			 * "Canvas Background Opaqueness Not Found: " + e.getMessage());
			 *
			 * }
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CanvasBackgroundOpaqueness()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 145, 2);
			ExcelWriter(ExecResult, ErrorDesc, 145, 3);

		}

	}

	public void CanvasBackgroundColor() throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);
			WebElement panel = driver.findElement(By.id("ccCanvasTab"));

			List<WebElement> button = panel.findElements(By.tagName("button"));

			button.get(0).click();
			Assert.assertNotNull(button.get(0));
			// SecondOpaqueness.get(2).clear();
			// SecondOpaqueness.get(2).sendKeys("FFFF00");

			/*
			 * WebElement colorpalette =
			 * driver.findElement(By.className("x-color-palette"));
			 *
			 * WebElement color =
			 * colorpalette.findElement(By.className("color-800080"));
			 *
			 * color.click();
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CanvasBackgroundColor() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 146, 2);
			ExcelWriter(ExecResult, ErrorDesc, 146, 3);

		}

	}

	public void Margin_Padding() throws InterruptedException, IOException {

		try {
			Thread.sleep(1000);
			WebElement panel = driver.findElement(By.cssSelector("div#chartCosmeticsStep.x-panel.x-panel-noborder"));
			List<WebElement> MarginPaddingTabChange = panel.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < MarginPaddingTabChange.size(); i++) {

				MarginPaddingTabChange.get(2).click();

				try {
					Assert.assertTrue(driver.getPageSource().contains("Margins"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Margin_Padding Tab Page Not Found: " + e.getMessage());

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Margin_Padding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 147, 2);
			ExcelWriter(ExecResult, ErrorDesc, 147, 3);

		}
	}

	public void LeftMargin() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 0) {
					input.get(0).click();
					input.get(0).clear();
					input.get(0).sendKeys("14");

					try {
						Assert.assertTrue(input.get(0).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Left Margin Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("LeftMargin()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 148, 2);
			ExcelWriter(ExecResult, ErrorDesc, 148, 3);

		}

	}

	public void RightMargin() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 0) {
					input.get(1).click();
					input.get(1).clear();
					input.get(1).sendKeys("15");

					try {
						Assert.assertTrue(input.get(1).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Right Margin Field Not Found: " + e.getMessage());

					}
				}
			}
		}

		catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("RightMargin() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 149, 2);
			ExcelWriter(ExecResult, ErrorDesc, 149, 3);

		}
	}

	public void TopMargin() throws InterruptedException, IOException {
		try {

			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 0) {
					input.get(2).click();
					input.get(2).clear();
					input.get(2).sendKeys("16");

					try {
						Assert.assertTrue(input.get(2).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Top Margin Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TopMargin() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 150, 2);
			ExcelWriter(ExecResult, ErrorDesc, 150, 3);

		}
	}

	public void BottomMargin() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 0) {
					input.get(3).click();
					input.get(3).clear();
					input.get(3).sendKeys("18");

					try {
						Assert.assertTrue(input.get(3).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Bottom Margin Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("BottomMargin() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 151, 2);
			ExcelWriter(ExecResult, ErrorDesc, 151, 3);

		}
	}

	public void Title_Padding() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(0).click();
					input.get(0).clear();
					input.get(0).sendKeys("19");

					try {
						Assert.assertTrue(input.get(0).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Title Padding Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Title_Padding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 152, 2);
			ExcelWriter(ExecResult, ErrorDesc, 152, 3);

		}
	}

	public void Y_Axis_Title_Padding() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(1).click();
					input.get(1).clear();
					input.get(1).sendKeys("19");

					try {
						Assert.assertTrue(input.get(1).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Y Axis Title Padding Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Y_Axis_Title_Padding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 153, 2);
			ExcelWriter(ExecResult, ErrorDesc, 153, 3);

		}

	}

	public void Label_Padding() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(2).click();
					input.get(2).clear();
					input.get(2).sendKeys("19");

					try {
						Assert.assertTrue(input.get(2).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Label Padding Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Label_Padding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 154, 2);
			ExcelWriter(ExecResult, ErrorDesc, 154, 3);

		}
	}

	public void X_Axis_Title_Padding() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(3).click();
					input.get(3).clear();
					input.get(3).sendKeys("19");

					try {
						Assert.assertTrue(input.get(3).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("X Axis Title Padding Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("X_Axis_Title_Padding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 155, 2);
			ExcelWriter(ExecResult, ErrorDesc, 155, 3);

		}
	}

	public void Y_Axis_Value_Padding() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(5).click();
					input.get(5).clear();
					input.get(5).sendKeys("23");

					try {
						Assert.assertTrue(input.get(5).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();Y_Axis_Value_Padding
						System.out.println("Y Axis Value Padding Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Y_Axis_Value_Padding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 156, 2);
			ExcelWriter(ExecResult, ErrorDesc, 156, 3);

		}
	}

	public void Value_Padding() throws InterruptedException, IOException {
		try {
			WebElement panel = driver
					.findElement(By.cssSelector("div#ccMarginsAndPaddingsTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(6).click();
					input.get(6).clear();
					input.get(6).sendKeys("24");

					try {
						Assert.assertTrue(input.get(6).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();Y_Axis_Value_Padding
						System.out.println("Value Padding Field Not Found: " + e.getMessage());

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Value_Padding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 157, 2);
			ExcelWriter(ExecResult, ErrorDesc, 157, 3);

		}

	}

	public void Fonts() throws InterruptedException, IOException {
		try {

			Thread.sleep(1000);
			WebElement panel = driver.findElement(By.cssSelector("div#chartCosmeticsStep.x-panel.x-panel-noborder"));
			List<WebElement> FontsTabChange = panel.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < FontsTabChange.size(); i++) {

				FontsTabChange.get(3).click();

				try {
					Assert.assertTrue(driver.getPageSource().contains("Customize Font"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Fonts Page Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Fonts() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 158, 2);
			ExcelWriter(ExecResult, ErrorDesc, 158, 3);

		}
	}

	public void Customize_Font_Name() throws InterruptedException, IOException {
		try {

			JavascriptExecutor js = (JavascriptExecutor) driver;
			String jQuerySelector = "CCSP.jQuery(\"label:contains('Font Name'):first\").siblings().first().children().children()[0]";
			String findDropdown = "return " + jQuerySelector;

			WebElement FontName = (WebElement) js.executeScript(findDropdown);

			FontName.click();
			FontName.sendKeys(Keys.ARROW_DOWN);
			FontName.sendKeys(Keys.ENTER);

			// FontName.sendKeys(Keys.TAB);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Customize_Font_Name() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 159, 2);
			ExcelWriter(ExecResult, ErrorDesc, 159, 3);

		}

	}

	public void Customize_Font_Size() throws InterruptedException, IOException {
		try {

			JavascriptExecutor js = (JavascriptExecutor) driver;
			// String jQuerySelector = "$(\"label:contains('Font
			// Name'):eq(1)\").siblings().first().children()[0]";
			// String jQuerySelector = "$(\"label:contains('Font
			// Name'):first\").siblings().first().children()[0]";

			String jQuerySelector = "CCSP.jQuery(\"label:contains('Font Size'):first\").siblings().first().children().filter(':first').children()[0]";

			String findfield = "return " + jQuerySelector;

			// System.out.println(findfield);

			WebElement FontSize = (WebElement) js.executeScript(findfield);

			FontSize.click();
			FontSize.clear();

			FontSize.sendKeys("23");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Customize_Font_Size() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 160, 2);
			ExcelWriter(ExecResult, ErrorDesc, 160, 3);

		}

	}

	public void Customize_font_Color() throws InterruptedException, IOException {
		try {
			// Thread.sleep(5500);

			JavascriptExecutor js = (JavascriptExecutor) driver;
			String jQuerySelector = "$(\"label:contains('Font Color'):first\").siblings().first().children().filter(':first').children()[0]";
			String findfield = "return " + jQuerySelector;

			WebElement FontColor = (WebElement) js.executeScript(findfield);

			FontColor.click();

			/*
			 * // Thread.sleep(10500);
			 *
			 * WebElement menu =
			 * driver.findElement(By.className("x-color-menu"));
			 *
			 * // Thread.sleep(16500); // WebElement colorfield =
			 * menu.findElement(By.className("input")); // colorfield.click();
			 * // colorfield.sendKeys("00FF00");
			 * driver.manage().timeouts().setScriptTimeout(100,TimeUnit.SECONDS)
			 * ; Thread.sleep(10500); WebElement colorpalette =
			 * driver.findElement(By.className("x-color-palette"));
			 * Thread.sleep(11500); WebElement color =
			 * colorpalette.findElement(By.className("color-800080"));
			 * Thread.sleep(12500); color.click(); color.click();
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Customize_font_Color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 161, 2);
			ExcelWriter(ExecResult, ErrorDesc, 161, 3);

		}

	}

	public void canvas_Font_Name() throws InterruptedException, IOException {
		try {

			JavascriptExecutor js = (JavascriptExecutor) driver;
			String jQuerySelector = "CCSP.jQuery(\"label:contains('Font Name'):eq(1)\").siblings().first().children().filter(':first').children()[0]";
			String findDropdown = "return " + jQuerySelector;

			WebElement FontName = (WebElement) js.executeScript(findDropdown);

			FontName.click();
			FontName.sendKeys(Keys.ARROW_DOWN);
			FontName.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("canvas_Font_Name() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 162, 2);
			ExcelWriter(ExecResult, ErrorDesc, 162, 3);

		}

	}

	public void canvas_Font_Size() throws InterruptedException, IOException {
		try {

			JavascriptExecutor js = (JavascriptExecutor) driver;
			String jQuerySelector = "CCSP.jQuery(\"label:contains('Font Size'):eq(1)\").siblings().first().children().filter(':first').children()[0]";
			String findDropdown = "return " + jQuerySelector;

			WebElement FontSize = (WebElement) js.executeScript(findDropdown);

			FontSize.click();
			FontSize.clear();
			FontSize.sendKeys("25");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("canvas_Font_Size()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 163, 2);
			ExcelWriter(ExecResult, ErrorDesc, 163, 3);

		}

	}

	public void canvas_Font_Color() throws InterruptedException, IOException {
		try {

			JavascriptExecutor js = (JavascriptExecutor) driver;
			String jQuerySelector = "$(\"label:contains('Font Color'):eq(1)\").siblings().first().children().filter(':first').children()[0]";
			String findDropdown = "return " + jQuerySelector;

			WebElement FontColor = (WebElement) js.executeScript(findDropdown);

			FontColor.click();
			// FontName.sendKeys(Keys.TAB);
			// FontName.sendKeys(Keys.BACK_SPACE);
			// Thread.sleep(1500);
			Thread.sleep(5000);

			List<WebElement> colorpalette = driver.findElements(By.className("x-color-palette"));

			WebElement color = colorpalette.get(1).findElement(By.className("color-800080"));

			color.click();
			// FontName.sendKeys("800080");
			// FontName.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("canvas_Font_Color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 164, 2);
			ExcelWriter(ExecResult, ErrorDesc, 164, 3);

		}

	}

	public void Show_Zero_pLane() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#ccZeroPlaneTab.x-panel.x-panel-noborder"));
			List<WebElement> Checkbox = panel.findElements(By.cssSelector("label.x-fieldset-header"));

			// List<WebElement> input= panel.findElements(By.tagName("input"));
			// List<WebElement> Checkbox =
			// panel.findElements(By.xpath("//input[@type='checkbox']"));

			for (int i = 0; i < Checkbox.size(); i++) {

				Checkbox.get(0).click();
				Checkbox.get(0).click();
				// input.get(1).clear();
				// input.get(1).sendKeys("24");

				try {
					Assert.assertTrue(Checkbox.get(0).isEnabled());
					Assert.assertNotNull(Checkbox.get(0));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Show Zero pLane Not Found: " + e.getMessage());
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_Zero_pLane() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 165, 2);
			ExcelWriter(ExecResult, ErrorDesc, 165, 3);

		}

	}

	public void TwoD_chart_color() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#ccZeroPlaneTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 0) {
					input.get(1).click();

					WebElement colorpalette = driver.findElement(By.className("x-color-palette"));

					WebElement color = colorpalette.findElement(By.className("color-800080"));
					Thread.sleep(2500);
					color.click();
					// color.click();
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TwoD_chart_color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 166, 2);
			ExcelWriter(ExecResult, ErrorDesc, 166, 3);

		}

	}

	public void TwoD_chart_thickness() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#ccZeroPlaneTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 0) {
					input.get(2).click();
					input.get(2).clear();
					input.get(2).sendKeys("4");

					try {
						Assert.assertTrue(input.get(2).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("TwoD chart thickness Not Found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TwoD_chart_thickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 167, 2);
			ExcelWriter(ExecResult, ErrorDesc, 167, 3);

		}
	}

	public void TwoD_chart_Opaqueness() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#ccZeroPlaneTab.x-panel.x-panel-noborder"));
			List<WebElement> LeftMargin = panel.findElements(By.tagName("fieldset"));
			for (int i = 0; i < LeftMargin.size(); i++) {
				List<WebElement> input = LeftMargin.get(i).findElements(By.tagName("input"));

				if (i == 0) {
					input.get(3).click();

					input.get(1).clear();
					input.get(1).sendKeys("24");

					try {
						Assert.assertTrue(input.get(1).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("TwoD chart Opaqueness Not Found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TwoD_chart_Opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 168, 2);
			ExcelWriter(ExecResult, ErrorDesc, 168, 3);

		}
	}

	public void Zero_Plane() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement panel = driver.findElement(By.cssSelector("div#chartCosmeticsStep.x-panel.x-panel-noborder"));
			List<WebElement> ZeroPlaneTabChange = panel.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < ZeroPlaneTabChange.size(); i++) {

				ZeroPlaneTabChange.get(4).click();

				try {
					Assert.assertTrue(driver.getPageSource().contains("Show Zero Plane"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println(" Zero Plane tab Page Not Connected: " + e.getMessage());
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Zero_Plane()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 169, 2);
			ExcelWriter(ExecResult, ErrorDesc, 169, 3);

		}
	}

	public void Custom_Branding() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement panel = driver.findElement(By.cssSelector("div#chartCosmeticsStep.x-panel.x-panel-noborder"));
			List<WebElement> CustomBrandingTabChange = panel.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < CustomBrandingTabChange.size(); i++) {

				CustomBrandingTabChange.get(6).click();

				try {
					Assert.assertTrue(driver.getPageSource().contains("Specify logo URL"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Custom Branding Page Not Connected: " + e.getMessage());
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Custom_Branding() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 170, 2);
			ExcelWriter(ExecResult, ErrorDesc, 170, 3);

		}
	}
	// LabelValuesTooltipTabChange.get(1).click();
	// driver.findElement(By.xpath("//input[ends-with(@id,'_ccCanvasTab')]")).click();

	public void Specify_logo_URL() throws InterruptedException, IOException {
		try {
			// Thread.sleep(1000);
			WebElement panel = driver.findElement(By.cssSelector("div#ccLogoTab.x-panel.x-panel-noborder"));
			List<WebElement> Specify_logo_URL = panel.findElements(By.cssSelector("input"));

			for (int i = 0; i < Specify_logo_URL.size(); i++) {

				Specify_logo_URL.get(0).click();
				Specify_logo_URL.get(0).clear();
				Specify_logo_URL.get(0).sendKeys("http://photos.prnewswire.com/prn/20140612/690645");

				try {
					Assert.assertTrue(Specify_logo_URL.get(0).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Specify logo URL Field Not Connected: " + e.getMessage());
				}
				break;

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" Specify_logo_URL() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 171, 2);
			ExcelWriter(ExecResult, ErrorDesc, 171, 3);

		}
	}

	public void Give_Link() throws InterruptedException, IOException {
		try {
			// Thread.sleep(2000);
			WebElement panel = driver.findElement(By.cssSelector("div#ccLogoTab.x-panel.x-panel-noborder"));
			List<WebElement> Link = panel.findElements(By.cssSelector("input"));

			for (int i = 0; i < Link.size(); i++) {

				Link.get(1).click();

				Thread.sleep(5000);
				try {
					// Assert.assertTrue(Link.get(1).isEnabled());
					Assert.assertNotNull(Link.get(1));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Link Field Not Connected: " + e.getMessage());
				}
				Link.get(1).clear();
				Link.get(1).sendKeys("http://docs.collabion.com/charts/2.1/");
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_Link() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 172, 2);
			ExcelWriter(ExecResult, ErrorDesc, 172, 3);

		}
	}

	public void Give_Position() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#ccLogoTab.x-panel.x-panel-noborder"));
			List<WebElement> Position = panel.findElements(By.cssSelector("input"));

			for (int i = 0; i < Position.size(); i++) {

				Position.get(2).click();
				Position.get(2).sendKeys(Keys.ARROW_DOWN);
				Position.get(2).sendKeys(Keys.ENTER);
				// Position.get(2).click();
				// Link.get(1).clear();
				// Link.get(1).sendKeys("http://docs.collabion.com/charts/2.1/");

				try {
					Assert.assertTrue(Position.get(2).isSelected());
					Assert.assertNotNull(Position.get(2));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Link Field Not Connected: " + e.getMessage());
				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_Position() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 173, 2);
			ExcelWriter(ExecResult, ErrorDesc, 173, 3);

		}

	}

	public void Branding_Opaqueness() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#ccLogoTab.x-panel.x-panel-noborder"));
			List<WebElement> Opaqueness = panel.findElements(By.cssSelector("input"));

			for (int i = 0; i < Opaqueness.size(); i++) {

				Opaqueness.get(3).click();

				Opaqueness.get(3).clear();
				Opaqueness.get(3).sendKeys("80");
				try {
					Assert.assertTrue(Opaqueness.get(3).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Branding Opaqueness Field Not Connected: " + e.getMessage());
				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Branding_Opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 174, 2);
			ExcelWriter(ExecResult, ErrorDesc, 174, 3);

		}
	}

	public void Branding_Scale() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.cssSelector("div#ccLogoTab.x-panel.x-panel-noborder"));
			List<WebElement> Opaqueness = panel.findElements(By.cssSelector("input"));

			for (int i = 0; i < Opaqueness.size(); i++) {

				Opaqueness.get(4).click();

				Opaqueness.get(4).clear();
				Opaqueness.get(4).sendKeys("70");
				try {
					Assert.assertTrue(Opaqueness.get(4).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Branding Scale Field Not Connected: " + e.getMessage());
				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Branding_Scale() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 175, 2);
			ExcelWriter(ExecResult, ErrorDesc, 175, 3);

		}

	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Number_Formatting\\\\\\\\\\

	public void Click_on_Number_Formatting() throws InterruptedException, IOException {
		try {

			// driver.findElement(By.xpath("//button[text()='Next
			// »']")).click();

			driver.findElement(By.xpath("//div/li[3]/ul/li[6]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Click_on_Number_Formatting() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 176, 2);
			ExcelWriter(ExecResult, ErrorDesc, 176, 3);

		}

	}

	public void field() throws InterruptedException, IOException {
		try {

			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement Trendlines_Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> field = fieldset.get(i).findElements(By.tagName("input"));
				Thread.sleep(3000);
				// if (fieldset.get(1).isDisplayed() )
				// {
				// Prefix_field.get(0).click();
				field.get(0).sendKeys("TestPrefix");
				try {
					Assert.assertTrue(field.get(0).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Prefix to be added before each number field Not Connected: " + e.getMessage());
				}
				// field.get(0).sendKeys(Keys.TAB);
				// Thread.sleep(1000);
				Thread.sleep(4000);
				field.get(1).sendKeys("TestSuffix");
				// field.get(1).sendKeys(Keys.TAB);
				// field.get(2).click();
				try {
					Assert.assertTrue(field.get(1).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Suffix to be added after each number Field Not Connected: " + e.getMessage());
				}

				break;

				// }
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("field() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 177, 2);
			ExcelWriter(ExecResult, ErrorDesc, 177, 3);

		}
	}

	public void Format_numbers() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			// String checkboxXPath = "//input[contains(@type,'checkbox') and
			// contains(@title,'Show Plot Border')]";

			Thread.sleep(4800);

			WebElement Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Format_numbers_checkbox = fieldset.get(i)
						.findElements(By.cssSelector("label.x-fieldset-header"));
				// Format_numbers.get(i).isDisplayed();

				if (i == 1) {
					Format_numbers_checkbox.get(0).click();

					try {
						Assert.assertTrue(Format_numbers_checkbox.get(0).isEnabled());
						Assert.assertNotNull(Format_numbers_checkbox.get(0));

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Format numbers checkbox Not Selected: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Format_numbers() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 178, 2);
			ExcelWriter(ExecResult, ErrorDesc, 178, 3);

		}
	}

	public void European_numbers() throws InterruptedException, IOException {
		try {
			Thread.sleep(5500);
			WebElement Trendlines_Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> rdEuropean = fieldset.get(i).findElements(By.name("NumberFormat"));

				if (i == 1) {

					rdEuropean.get(1).click();

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("European_numbers() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 179, 2);
			ExcelWriter(ExecResult, ErrorDesc, 179, 3);

		}
	}
	/*
	 * try { Assert.assertTrue( rdEuropean.isSelected());
	 *
	 * } catch (AssertionError e) {
	 *
	 * // String message = e.getMessage(); System.out.println("European numbers
	 * Not
	 *
	 * ed: " + e.getMessage()); }
	 *
	 * }
	 */

	public void Default_Scale_Unit() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement Trendlines_Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Scales_to_be_used = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 2) {
					Scales_to_be_used.get(3).click();
					Scales_to_be_used.get(3).clear();
					Scales_to_be_used.get(3).sendKeys("TestUnit");

					try {
						Assert.assertTrue(Scales_to_be_used.get(3).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Default Scale Unit Not Connected: " + e.getMessage());
					}

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Default_Scale_Unit() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 180, 2);
			ExcelWriter(ExecResult, ErrorDesc, 180, 3);

		}
	}

	public void Round_numbers_to_how_many_decimals() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement Trendlines_Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Scales_to_be_used = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 3) {
					Scales_to_be_used.get(0).click();
					Scales_to_be_used.get(0).clear();
					Scales_to_be_used.get(0).sendKeys("10");

					try {
						Assert.assertTrue(Scales_to_be_used.get(0).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Round numbers decimals Not Found: " + e.getMessage());
					}

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" Round_numbers_to_how_many_decimals()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 181, 2);
			ExcelWriter(ExecResult, ErrorDesc, 181, 3);

		}
	}

	public void Round_axis() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement Trendlines_Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Scales_to_be_used = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 3) {
					Scales_to_be_used.get(1).click();
					Scales_to_be_used.get(1).clear();
					Scales_to_be_used.get(1).sendKeys("7");

					try {
						Assert.assertTrue(Scales_to_be_used.get(1).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Round axis values Not Found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Round_axis()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 182, 2);
			ExcelWriter(ExecResult, ErrorDesc, 182, 3);

		}
	}

	public void Force_exact_number_of_decimals() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement Trendlines_Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Scales_to_be_used = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 3) {
					Scales_to_be_used.get(2).click();

					Thread.sleep(1400);

					// Scales_to_be_used.get(2).click();
					// Scales_to_be_used.get(2).clear();
					// Scales_to_be_used.get(2).sendKeys("9");

					try {
						Assert.assertTrue(Scales_to_be_used.get(2).isSelected());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Force exact number of decimals Not Selected: " + e.getMessage());
					}

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Force_exact_number_of_decimals() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 183, 2);
			ExcelWriter(ExecResult, ErrorDesc, 183, 3);

		}
	}

	public void Force_Y_Axis_decimal_values() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			WebElement Trendlines_Span = driver.findElement(By.id("numberFormatStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> Scales_to_be_used = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 3) {
					Scales_to_be_used.get(3).click();

					Thread.sleep(1400);

					// Scales_to_be_used.get(3).click();
					// Scales_to_be_used.get(0).clear();
					// Scales_to_be_used.get(0).sendKeys("9");
					try {
						Assert.assertTrue(Scales_to_be_used.get(3).isSelected());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Force exact number of decimals Not Selected : " + e.getMessage());
					}

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Force_Y_Axis_decimal_values() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 184, 2);
			ExcelWriter(ExecResult, ErrorDesc, 184, 3);

		}
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Axis\\\\\\\\\\

	public void Click_on_Axis() throws InterruptedException, IOException {
		try {
			driver.findElement(By.xpath("//div/li[3]/ul/li[7]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Click_on_Axis() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 185, 2);
			ExcelWriter(ExecResult, ErrorDesc, 185, 3);

		}

	}

	public void Y_Axis_minimum_value() throws InterruptedException, IOException {
		try {

			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			List<WebElement> Axis = driver.findElements(By.id("divisionLinesStep"));

			for (int i = 0; i < Axis.size(); i++) {

				List<WebElement> field = Axis.get(i).findElements(By.tagName("input"));
				field.get(1).click();
				// Thread.sleep(2000);
				field.get(1).clear();
				field.get(1).sendKeys("50");

				try {
					Assert.assertTrue(field.get(1).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println(" Y Axis minimum value not found: " + e.getMessage());
				}
				field.get(2).clear();
				field.get(2).sendKeys("70");

				try {
					Assert.assertTrue(field.get(2).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println(" Y-Axis maximum value not found: " + e.getMessage());
				}
				// field.get(3).clear();
				field.get(3).click();

				try {
					Assert.assertTrue(field.get(3).isSelected());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println(" Adaptive lower limit for axis Checkbox not Selected: " + e.getMessage());
				}

				field.get(4).click();

				try {
					Assert.assertTrue(field.get(4).isEnabled());
					Assert.assertNotNull(field.get(4));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println(
							" Force decimals on primary y-axis values Checkbox not Selected: " + e.getMessage());
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Y_Axis_minimum_value() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 186, 2);
			ExcelWriter(ExecResult, ErrorDesc, 186, 3);

		}
	}

	/*
	 * public void Y_Axis_maximum_value() throws InterruptedException {
	 * //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 * List<WebElement> Axis= driver.findElements(By.id("divisionLinesStep"));
	 *
	 * for(int i = 0; i <Axis.size(); i++) {
	 *
	 * List<WebElement> field = Axis.get(i).findElements(By.tagName("input"));
	 * field.get(2).click(); field.get(2).sendKeys("70");
	 *
	 *
	 * } }
	 */

	public void adaptive_lower_limit_for_axis() throws InterruptedException, IOException {
		try {

			Thread.sleep(10000);
			List<WebElement> Axis = driver.findElements(By.id("divisionLinesStep"));

			for (int i = 0; i < Axis.size(); i++) {

				List<WebElement> field = Axis.get(i).findElements(By.tagName("input"));
				field.get(3).click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("adaptive_lower_limit_for_axis() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 187, 2);
			ExcelWriter(ExecResult, ErrorDesc, 187, 3);

		}
	}

	public void decimals_on_primary_y_axis_values() throws InterruptedException, IOException {
		try {
			List<WebElement> Axis = driver.findElements(By.id("divisionLinesStep"));

			for (int i = 0; i < Axis.size(); i++) {

				List<WebElement> field = Axis.get(i).findElements(By.tagName("input"));
				field.get(4).click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("decimals_on_primary_y_axis_values() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 188, 2);
			ExcelWriter(ExecResult, ErrorDesc, 188, 3);

		}
	}

	public void Axis_Grid_Lines() throws InterruptedException, IOException {
		try {
			WebElement Axis = driver.findElement(By.cssSelector("div#divisionLinesStep.x-panel.x-panel-noborder"));
			List<WebElement> span = Axis.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < span.size(); i++) {

				span.get(1).click();

				try {
					Assert.assertTrue(driver.getPageSource().contains("Show axis grid lines"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Show axis grid lines page Not Found: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Axis_Grid_Lines() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 189, 2);
			ExcelWriter(ExecResult, ErrorDesc, 189, 3);

		}
	}

	public void Show_grid_lines() throws InterruptedException, IOException {
		try {

			WebElement div = driver.findElement(By.cssSelector("div#dlAxisGridLinesTab.x-panel.x-panel-noborder"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> input = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(0).click();
					input.get(0).clear();
					input.get(0).sendKeys("3");

					try {
						Assert.assertTrue(input.get(0).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Number of grid lines field Not Selected: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_grid_lines() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 190, 2);
			ExcelWriter(ExecResult, ErrorDesc, 190, 3);

		}

	}

	public void Grid_Line_Thickness() throws InterruptedException, IOException {
		try {
			WebElement div = driver.findElement(By.cssSelector("div#dlAxisGridLinesTab.x-panel.x-panel-noborder"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> input = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(1).click();
					input.get(1).clear();
					input.get(1).sendKeys("5");
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Grid_Line_Thickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 191, 2);
			ExcelWriter(ExecResult, ErrorDesc, 191, 3);

		}
	}

	public void Grid_Line_color() throws InterruptedException, IOException {
		try {

			WebElement div = driver.findElement(By.cssSelector("div#dlAxisGridLinesTab.x-panel.x-panel-noborder"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> button = fieldset.get(i).findElements(By.tagName("button"));

				if (i == 1) {
					button.get(0).click();

				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Grid_Line_color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 192, 2);
			ExcelWriter(ExecResult, ErrorDesc, 192, 3);

		}

	}

	public void Grid_Line_Opaqueness() throws InterruptedException, IOException {
		try {

			WebElement div = driver.findElement(By.cssSelector("div#dlAxisGridLinesTab.x-panel.x-panel-noborder"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> input = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(2).click();
					input.get(2).clear();
					input.get(2).sendKeys("7");

					try {
						Assert.assertTrue(input.get(2).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Grid Line Opaqueness Field Not Selected: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Grid_Line_Opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 193, 2);
			ExcelWriter(ExecResult, ErrorDesc, 193, 3);

		}
	}

	public void grid_lines_as_dashed1() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			Thread.sleep(1200);
			WebElement div = driver.findElement(By.id("dlYAxisValuesTab"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				List<WebElement> header = fieldset.get(i).findElements(By.cssSelector("input"));

				if (i == 1) {
					header.get(0).click();
					break;

					// Thread.sleep(4500);

					// input.get(3).click();

					// input.get(3).clear();
					// input.get(3).sendKeys("3");

					/*
					 * try { Assert.assertTrue(input.get(3).isSelected());
					 *
					 * } catch (AssertionError e) {
					 *
					 * // String message = e.getMessage(); System.out.println(
					 * "Grid lines as dashed not Selected: " + e.getMessage());
					 * }
					 */
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("grid_lines_as_dashed1()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 194, 2);
			ExcelWriter(ExecResult, ErrorDesc, 194, 3);

		}
	}

	public void Give_Dash_length() throws InterruptedException, IOException {
		try {

			WebElement div = driver.findElement(By.cssSelector("div#dlAxisGridLinesTab.x-panel.x-panel-noborder"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> input = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(4).click();
					input.get(4).clear();
					input.get(4).sendKeys("5");

					try {
						Assert.assertTrue(input.get(4).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Dash length not Found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_Dash_length()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 195, 2);
			ExcelWriter(ExecResult, ErrorDesc, 195, 3);

		}
	}

	public void Give_Dash_gap() throws InterruptedException, IOException {
		try {

			WebElement div = driver.findElement(By.cssSelector("div#dlAxisGridLinesTab.x-panel.x-panel-noborder"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> input = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					input.get(5).click();
					input.get(5).clear();
					input.get(5).sendKeys("2");

					try {
						Assert.assertTrue(input.get(5).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Dash gap not Found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_Dash_gap()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 196, 2);
			ExcelWriter(ExecResult, ErrorDesc, 196, 3);

		}
	}

	public void show_alternate_color_bands() throws InterruptedException, IOException {
		try {

			WebElement panel = driver.findElement(By.id("dlAxisGridLinesTab"));
			List<WebElement> Anchors = panel.findElements(By.cssSelector("label.x-fieldset-header"));

			for (int i = 0; i < Anchors.size(); i++) {

				Thread.sleep(1500);
				Anchors.get(2).click();
				// Anchors.get(2).sendKeys(Keys.TAB);

				// Anchors.get(2).sendKeys(Keys.TAB);

				// Thread.sleep(1000);

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("show_alternate_color_bands() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 197, 2);
			ExcelWriter(ExecResult, ErrorDesc, 197, 3);

		}
	}

	/*
	 * public void alternate_color_bands() throws InterruptedException {
	 * WebElement div= driver.findElement(By.id("dlAxisGridLinesTab"));
	 *
	 * List<WebElement> fieldset= div.findElements(By.tagName("fieldset"));
	 *
	 *
	 *
	 *
	 * for(int i = 0; i <fieldset.size(); i++) {
	 *
	 * List<WebElement> input=
	 * fieldset.get(i).findElements(By.tagName("input"));
	 *
	 * if(i==2) { input.get(0).click(); // input.get(5).clear(); //
	 * input.get(5).sendKeys("2");
	 *
	 * /* try { Assert.assertTrue(input.get(0).isEnabled());
	 *
	 * } catch (AssertionError e) {
	 *
	 * // String message = e.getMessage(); System.out.println(
	 * "Alternate color bands checkbox not found: " + e.getMessage()); } }
	 *
	 * } }
	 */

	public void give_alternate_color() throws IOException {
		try {
			WebElement div = driver.findElement(By.id("dlAxisGridLinesTab"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> button = div.findElements(By.tagName("button"));

				if (i == 2) {
					button.get(1).click();
					// button.get(1).click();

					// List<WebElement> fieldset=
					// div.findElements(By.tagName("fieldset"));
					// if(i==2)
					// {

					// input.get(5).clear();
					// input.get(5).sendKeys("2");

					/*
					 * try { Assert.assertTrue(button.get(0).isEnabled());
					 *
					 * } catch (AssertionError e) {
					 *
					 * // String message = e.getMessage(); System.out.println(
					 * "Alternate color not Found: " + e.getMessage()); } }
					 */

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("give_alternate_color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 198, 2);
			ExcelWriter(ExecResult, ErrorDesc, 198, 3);

		}
	}

	public void give_alternate_Opaqueness() throws IOException {
		try {
			WebElement div = driver.findElement(By.id("dlAxisGridLinesTab"));
			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> input = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 3) {
					input.get(1).click();
					input.get(1).clear();
					input.get(1).sendKeys("95");
					break;
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("give_alternate_Opaqueness() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 199, 2);
			ExcelWriter(ExecResult, ErrorDesc, 199, 3);

		}
	}

	public void grid_lines_as_dashed() throws InterruptedException, IOException {
		try {
			Thread.sleep(1700);
			WebElement div = driver.findElement(By.id("dlAxisGridLinesTab"));

			List<WebElement> fieldset = div.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {

				List<WebElement> tagName = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 2) {
					tagName.get(0).click();
					// tagName.get(0).clear();
					// tagName.get(0).sendKeys("2");

					try {
						Assert.assertTrue(fieldset.get(0).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Alternate Opaqueness not Found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("grid_lines_as_dashed()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 200, 2);
			ExcelWriter(ExecResult, ErrorDesc, 200, 3);

		}

	}
	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Trend Lines\\\\\\\\\\

	public void Click_on_Trend_Lines() throws InterruptedException, IOException { // Thread.sleep(2000);
		try {
			driver.findElement(By.xpath("//div/li[3]/ul/li[9]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Click_on_Trend_Lines()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 201, 2);
			ExcelWriter(ExecResult, ErrorDesc, 201, 3);

		}

	}

	public void Start_Value() throws InterruptedException, IOException {
		try {
			// Thread.sleep(3000);
			List<WebElement> Trendlines_Span = driver.findElements(By.id("trendLinesStep"));

			for (int i = 0; i < Trendlines_Span.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> elementToClick = Trendlines_Span.get(i).findElements(By.tagName("input"));
				elementToClick.get(0).click();
				elementToClick.get(0).clear();
				elementToClick.get(0).sendKeys("100");

				try {
					Assert.assertTrue(elementToClick.get(0).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Trend line Start Value Not found: " + e.getMessage());

				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Start_Value() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 202, 2);
			ExcelWriter(ExecResult, ErrorDesc, 202, 3);

		}
	}

	public void End_Value() throws InterruptedException, IOException {// Thread.sleep(3000);
		try {
			List<WebElement> Trendlines_Span = driver.findElements(By.id("trendLinesStep"));

			for (int i = 0; i < Trendlines_Span.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> elementToClick = Trendlines_Span.get(i).findElements(By.tagName("input"));
				elementToClick.get(1).click();
				elementToClick.get(1).clear();
				elementToClick.get(1).sendKeys("200");

				try {
					Assert.assertTrue(elementToClick.get(1).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Trend line End Value Not found: " + e.getMessage());

				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("End_Value() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 203, 2);
			ExcelWriter(ExecResult, ErrorDesc, 203, 3);

		}
	}

	public void Display_Text() throws InterruptedException, IOException {// Thread.sleep(3000);
		try {
			List<WebElement> Trendlines_Span = driver.findElements(By.id("trendLinesStep"));

			for (int i = 0; i < Trendlines_Span.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> elementToClick = Trendlines_Span.get(i).findElements(By.tagName("input"));
				elementToClick.get(2).click();
				elementToClick.get(2).clear();
				elementToClick.get(2).sendKeys("Test Display Text");

				try {
					Assert.assertTrue(elementToClick.get(2).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Trend line Display Text Value Not found: " + e.getMessage());
				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Display_Text() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 204, 2);
			ExcelWriter(ExecResult, ErrorDesc, 204, 3);

		}
	}

	public void Tool_tip_Text() throws InterruptedException, IOException {// Thread.sleep(3000);
		try {
			List<WebElement> Trendlines_Span = driver.findElements(By.id("trendLinesStep"));

			for (int i = 0; i < Trendlines_Span.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> elementToClick = Trendlines_Span.get(i).findElements(By.tagName("input"));
				elementToClick.get(3).click();
				elementToClick.get(3).clear();
				elementToClick.get(3).sendKeys("Test tip Text");
				Thread.sleep(1000);

				try {
					Assert.assertTrue(elementToClick.get(3).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Trend line Tool tip Text Value Not found: " + e.getMessage());
				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Tool_tip_Text() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 205, 2);
			ExcelWriter(ExecResult, ErrorDesc, 205, 3);

		}
	}

	public void Give_Opaqueness() throws InterruptedException, IOException {
		try {

			Thread.sleep(1200);
			List<WebElement> Trendlines_Span = driver.findElements(By.id("trendLinesStep"));

			for (int i = 0; i < Trendlines_Span.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> elementToClick = Trendlines_Span.get(i).findElements(By.tagName("input"));
				elementToClick.get(11).click();
				elementToClick.get(11).clear();
				elementToClick.get(11).sendKeys("13");

				try {
					Assert.assertTrue(elementToClick.get(11).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Give Opaqueness Value Not found: " + e.getMessage());
				}
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_Opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 206, 2);
			ExcelWriter(ExecResult, ErrorDesc, 206, 3);

		}

	}

	public void Trend_lines_Color() throws InterruptedException, IOException {
		try {
			Thread.sleep(1400);
			List<WebElement> Trendlines_Span = driver.findElements(By.id("trendLinesStep"));

			for (int i = 0; i < Trendlines_Span.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> elementToClick = Trendlines_Span.get(i).findElements(By.tagName("button"));
				elementToClick.get(0).click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" Trend_lines_Color()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 207, 2);
			ExcelWriter(ExecResult, ErrorDesc, 207, 3);

		}
	}

	public void thickness_of_the_trend_line() throws InterruptedException, IOException {
		try {

			Thread.sleep(1400);
			List<WebElement> Trendlines_Span = driver.findElements(By.id("trendLinesStep"));

			for (int i = 0; i < Trendlines_Span.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> elementToClick = Trendlines_Span.get(i).findElements(By.tagName("input"));
				elementToClick.get(10).click();
				elementToClick.get(10).clear();
				elementToClick.get(10).sendKeys("17");

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("thickness_of_the_trend_line() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 208, 2);
			ExcelWriter(ExecResult, ErrorDesc, 208, 3);

		}
	}

	public void as_filled_color_zone() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement Trendlines_Span = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> as_filled_color_zone = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					as_filled_color_zone.get(0).click();

					// Thread.sleep(1000);

					// as_filled_color_zone.get(0).click();
					try {
						Assert.assertTrue(as_filled_color_zone.get(0).isSelected());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("As filled color zone checkbox Not found: " + e.getMessage());
					}
					break;
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("as_filled_color_zone() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 209, 2);
			ExcelWriter(ExecResult, ErrorDesc, 209, 3);

		}
	}

	public void as_dashed() throws InterruptedException, IOException {// Thread.sleep(1000);
		try {

			Thread.sleep(1000);
			WebElement Trendlines_Span = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> as_filled_color_zone = fieldset.get(i).findElements(By.tagName("input"));

				if (fieldset.get(1).isDisplayed()) {
					as_filled_color_zone.get(0).click();
				}
			}
			WebElement Trendlines_Span1 = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset1 = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset1.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> as_dashed = fieldset1.get(i).findElements(By.tagName("input"));

				if (as_dashed.get(1).isDisplayed()) {
					as_dashed.get(1).click();

					try {
						Assert.assertTrue(as_dashed.get(1).isSelected());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("As dashed Checkbox Not found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("as_dashed() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 210, 2);
			ExcelWriter(ExecResult, ErrorDesc, 210, 3);

		}
	}

	public void over_data_plot() throws InterruptedException, IOException { // Thread.sleep(1600);
		try {
			WebElement Trendlines_Span = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> over_data_plot = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					over_data_plot.get(1).click();

					try {
						Assert.assertTrue(over_data_plot.get(1).isSelected());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Over data plot Checkbox Not found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("over_data_plot() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 211, 2);
			ExcelWriter(ExecResult, ErrorDesc, 211, 3);

		}
	}

	public void asdashed() throws InterruptedException, IOException {
		try {
			Thread.sleep(2000);
			WebElement Trendlines_Span = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> value_on_right_side = fieldset.get(i).findElements(By.tagName("input"));

				if (fieldset.get(2).isDisplayed()) {
					value_on_right_side.get(0).click();
					// value_on_right_side.get(0).click();
					// Dash_length.get(0).clear();
					// Dash_length.get(0).sendKeys("22");

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("asdashed() fail in catch" + ErrorDesc);
		} finally {
			//
			ExcelWriter(ExecResult, ErrorDesc, 212, 2);
			ExcelWriter(ExecResult, ErrorDesc, 212, 3);

		}
	}

	public void value_on_right_side() throws InterruptedException, IOException {
		try {
			WebElement Trendlines_Span = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> over_data_plot = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					over_data_plot.get(2).click();

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" value_on_right_side() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 213, 2);
			ExcelWriter(ExecResult, ErrorDesc, 213, 3);

		}
	}

	public void Dash_length() throws InterruptedException, IOException { // Thread.sleep(3000);
		try {
			WebElement Trendlines_Span = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> Dash_length = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					Dash_length.get(5).click();
					Dash_length.get(5).clear();
					Dash_length.get(5).sendKeys("8");

					try {
						Assert.assertTrue(Dash_length.get(4).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Dash length Not found: " + e.getMessage());
					}

					// value_on_right_side.get(0).click();
					// Dash_length.get(0).clear();
					// Dash_length.get(0).sendKeys("22");

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Dash_length() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 214, 2);
			ExcelWriter(ExecResult, ErrorDesc, 214, 3);

		}
	}

	public void Dash_gap_length() throws IOException {
		try {
			WebElement Trendlines_Span = driver.findElement(By.id("trendLinesStep"));
			List<WebElement> fieldset = Trendlines_Span.findElements(By.tagName("fieldset"));

			for (int i = 0; i < fieldset.size(); i++) {
				// String checkboxXPath = "//input[contains(@type,'checkbox')
				// and
				// contains(@title,'Show Plot Border')]";

				List<WebElement> Dash_length = fieldset.get(i).findElements(By.tagName("input"));

				if (i == 1) {
					Dash_length.get(6).click();
					Dash_length.get(6).clear();
					Dash_length.get(6).sendKeys("4");

					try {
						Assert.assertTrue(Dash_length.get(4).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Dash length Not found: " + e.getMessage());
					}

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Dash_gap_length() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 215, 2);
			ExcelWriter(ExecResult, ErrorDesc, 215, 3);

		}
	}
	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Other Settings\\\\\\\\\\

	public void Click_on_Other_Settings() throws InterruptedException, IOException { // Thread.sleep(1000);
		try {

			driver.manage().timeouts().implicitlyWait(200, TimeUnit.SECONDS);

			driver.findElement(By.xpath("//div/li[3]/ul/li[10]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" Click_on_Other_Settings() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 216, 2);
			ExcelWriter(ExecResult, ErrorDesc, 216, 3);

		}

	}

	public void Chart_Loading_Message() throws InterruptedException, IOException {
		try {

			WebElement PBarLoadingText = driver.findElement(By.id("PBarLoadingText"));
			PBarLoadingText.click();
			PBarLoadingText.sendKeys("Test Chart Loading Message");

			try {
				Assert.assertTrue(PBarLoadingText.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Chart Loading Message Not found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Chart_Loading_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 217, 2);
			ExcelWriter(ExecResult, ErrorDesc, 217, 3);

		}

	}

	public void Data_Loading_Message() throws InterruptedException, IOException {
		try {

			WebElement XMLLoadingText = driver.findElement(By.id("XMLLoadingText"));
			XMLLoadingText.click();
			XMLLoadingText.sendKeys("Test Data Loading Message");

			try {
				Assert.assertTrue(XMLLoadingText.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Data Loading Message Not found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Data_Loading_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 218, 2);
			ExcelWriter(ExecResult, ErrorDesc, 218, 3);

		}

	}

	public void Data_Processing_Message() throws InterruptedException, IOException {
		try {

			WebElement ParsingDataText = driver.findElement(By.id("ParsingDataText"));
			ParsingDataText.click();
			ParsingDataText.sendKeys("Test Data Processing Message");

			try {
				Assert.assertTrue(ParsingDataText.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Data Processing Message Not found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Data_Processing_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 219, 2);
			ExcelWriter(ExecResult, ErrorDesc, 219, 3);

		}
	}

	public void No_data_to_display_Message() throws InterruptedException, IOException {
		try {

			WebElement ChartNoDataText = driver.findElement(By.id("ChartNoDataText"));
			ChartNoDataText.click();
			ChartNoDataText.sendKeys("Test data to display Message");

			try {
				Assert.assertTrue(ChartNoDataText.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("	Chart No Data Text Message Not found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("No_data_to_display_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 220, 2);
			ExcelWriter(ExecResult, ErrorDesc, 220, 3);

		}
	}

	public void Pre_Rendering_Message() throws InterruptedException, IOException {
		try {

			WebElement RenderingChartText = driver.findElement(By.id("RenderingChartText"));
			RenderingChartText.click();
			RenderingChartText.sendKeys("Test Pre Rendering Message");

			try {
				Assert.assertTrue(RenderingChartText.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Rendering Chart Text Message Not found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Pre_Rendering_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 221, 2);
			ExcelWriter(ExecResult, ErrorDesc, 221, 3);

		}
	}

	public void Error_in_Loading_Message() throws InterruptedException, IOException {
		try {
			WebElement LoadDataErrorText = driver.findElement(By.id("LoadDataErrorText"));
			LoadDataErrorText.click();
			LoadDataErrorText.sendKeys("Test Error in Loading_Message");

			try {
				Assert.assertTrue(LoadDataErrorText.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Error in Loading Message Not found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Error_in_Loading_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 222, 2);
			ExcelWriter(ExecResult, ErrorDesc, 222, 3);

		}
	}

	public void Error_in_Data_Message() throws InterruptedException, IOException {
		try {

			WebElement InvalidXMLText = driver.findElement(By.id("InvalidXMLText"));
			InvalidXMLText.click();
			InvalidXMLText.sendKeys("Test Error in Data Message");

			try {
				Assert.assertTrue(InvalidXMLText.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Error in Data Message Not found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Error_in_Data_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 223, 2);
			ExcelWriter(ExecResult, ErrorDesc, 223, 3);

		}
	}

	public void CustomAttributes() throws InterruptedException, IOException {
		try {
			WebElement Axis = driver.findElement(By.cssSelector("div#settingsStep.x-panel.x-panel-noborder"));
			List<WebElement> span = Axis.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < span.size(); i++) {

				span.get(1).click();

				try {
					Assert.assertTrue(span.get(1).isEnabled());

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Error in Data Message Not found: " + e.getMessage());
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CustomAttributes()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 224, 2);
			ExcelWriter(ExecResult, ErrorDesc, 224, 3);

		}
	}

	public void Add() throws InterruptedException, IOException {
		try {
			driver.findElement(By.xpath("//button[text()='Add']")).click();
			// driver.findElement(By.xpath("//button[text()='Add']")).sendKeys("TestAttributeName");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Add()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 225, 2);
			ExcelWriter(ExecResult, ErrorDesc, 225, 3);

		}
	}

	public void AttributeName() throws InterruptedException, IOException {
		try {

			/*
			 * JavascriptExecutor js = (JavascriptExecutor) driver;
			 *
			 *
			 *
			 * String jQuerySelector =
			 * "$(\".x-grid3-col.x-grid3-cell.x-grid3-td-2.x-grid3-cell-last:eq(8)\").click()";
			 * String field = "return " + jQuerySelector ;
			 * //System.out.println(field);
			 *
			 *
			 * Thread.sleep(2000);
			 *
			 * WebElement AttributeName = (WebElement) js.executeScript(field);
			 * AttributeName.
			 */

			// dpCustomAttributesTab

			/*
			 * WebElement CustomAttributePage = driver.findElement(arg0)
			 * List<WebElement> Attributename=
			 * driver.findElements(By.xpath("//input[type='text']"));
			 * //Attributename.getAttribute("text"); Attributename.click();
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("AttributeName() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 226, 2);
			ExcelWriter(ExecResult, ErrorDesc, 226, 3);

		}
	}

	public void AttributeValue() throws InterruptedException, IOException {
		try {
			WebElement AttributeValue = driver.findElement(By.className("x-grid3-col-2"));
			AttributeValue.click();
			AttributeValue.sendKeys("TestAttributevalue");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("AttributeValue() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 227, 2);
			ExcelWriter(ExecResult, ErrorDesc, 227, 3);

		}
	}

	public void ConnectionFields() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Thread.sleep(2000);
			// WebElement Axis =
			// driver.findElement(By.cssSelector("div#settingsStep.x-panel.x-panel-noborder"));
			// List<WebElement> span =
			// Axis.findElements(By.cssSelector("span.x-tab-strip-text"));
			//
			// for (int i = 0; i < span.size(); i++) {
			//
			// span.get(1).click();
			// }
			// ConnectionFields
			// xpath://div[@id='settingsStep']/div[3]/div[1]/div/div/div/div/div[1]/div[1]/ul/li[3]/a[2]/em/span/span
			WebElement ConnectionFields = driver.findElement(By.xpath(
					"//div[@id='settingsStep']/div/div[1]/div/div/div/div/div[1]/div[1]/ul/li[3]/a[2]/em/span/span"));

			if (ConnectionFields.getText().equals("Connection Fields"))
				ConnectionFields.click();

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ConnectionFields() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 228, 2);
			ExcelWriter(ExecResult, ErrorDesc, 228, 3);

		}

	}

	public void rdChartFields() throws InterruptedException, IOException {
		try {
			WebElement rdChartFields = driver.findElement(By.id("rdChartFields"));
			rdChartFields.click();

			try {
				Assert.assertTrue(rdChartFields.isSelected());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Radio Button Chart Fields Not selected: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("rdChartFields() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 229, 2);
			ExcelWriter(ExecResult, ErrorDesc, 229, 3);

		}

	}

	public void rdAllFields() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement rdAllFields = driver.findElement(By.id("rdAllFields"));
			rdAllFields.click();

			try {
				Assert.assertTrue(rdAllFields.isSelected());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Radio Button All Fields Not Selected: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("rdAllFields()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 230, 2);
			ExcelWriter(ExecResult, ErrorDesc, 230, 3);

		}
	}

	/////////////////// SingleSeries 3D chart type///////////////
	public void ColumnThreeD_Chart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Column3D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(" Column2D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ColumnThreeD_Chart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 231, 2);
			ExcelWriter(ExecResult, ErrorDesc, 231, 3);

		}
	}

	/////////////////// Single Series BarTwoD chart type///////////////
	public void BarTwoD_Chart() throws IOException {
		try {

			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Bar2D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(" Column2D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("BarTwoD_Chart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 232, 2);
			ExcelWriter(ExecResult, ErrorDesc, 232, 3);

		}
	}

	/////////////////// Single Series BarTwoD chart type///////////////

	public void LineTwoD_Chart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Line.jpg')]"));

			/*
			 * try {
			 *
			 *
			 * Assert.assertTrue(Chart.isDisplayed());
			 * Assert.assertTrue(driver.getPageSource().contains("Column 2D"));
			 *
			 * } catch (AssertionError e) {
			 *
			 * // String message = e.getMessage(); System.out.println(
			 * " Column2D Chart Not Found: " + e.getMessage()); }
			 */
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("LineTwoD_Chart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 233, 2);
			ExcelWriter(ExecResult, ErrorDesc, 233, 3);

		}
	}

	/////////////////// Single Series LineTwoD Chart type......label,Values
	/////////////////// Tooltips -Anchors Tab page////////////////////////////
	public void Anchors_Tab_page() throws IOException {
		try {
			List<WebElement> panel = driver
					.findElements(By.cssSelector("div#dataPropertiesStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < panel.size(); i++) {
				List<WebElement> LabelValuesTooltipTabChange = panel.get(i)
						.findElements(By.cssSelector("span.x-tab-strip-text"));
				Assert.assertNotNull(LabelValuesTooltipTabChange.get(3));
				LabelValuesTooltipTabChange.get(3).click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Anchors_Tab_page() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 234, 2);
			ExcelWriter(ExecResult, ErrorDesc, 234, 3);

		}
	}

	public void Show_Anchors() throws InterruptedException, IOException {
		try {
			WebElement panel = driver.findElement(By.id("dpAnchorsTab"));
			WebElement Anchors = panel.findElement(By.cssSelector("label.x-fieldset-header"));
			Assert.assertNotNull(Anchors);
			Anchors.click();

			Thread.sleep(1000);
			Anchors.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_Anchors() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 235, 2);
			ExcelWriter(ExecResult, ErrorDesc, 235, 3);

		}

	}

	public void Configuration_Sides() throws InterruptedException, IOException {
		try {
			Thread.sleep(2000);
			WebElement panel1 = driver.findElement(By.id("dpAnchorsTab"));
			WebElement panel2 = driver.findElement(By.cssSelector("fieldset#drawAnchors.x-fieldset"));
			List<WebElement> LabelValuesTooltipTabChange = panel2.findElements(By.tagName("input"));

			for (int i = 0; i < LabelValuesTooltipTabChange.size(); i++) {

				LabelValuesTooltipTabChange.get(0).click();
				LabelValuesTooltipTabChange.get(0).clear();
				LabelValuesTooltipTabChange.get(0).sendKeys("11");
				Assert.assertNotNull(LabelValuesTooltipTabChange.get(0));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Configuration_Sides() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 236, 2);
			ExcelWriter(ExecResult, ErrorDesc, 236, 3);

		}
	}

	public void Configuration_Radius() throws InterruptedException, IOException {
		try {
			Thread.sleep(2500);
			WebElement panel1 = driver.findElement(By.id("dpAnchorsTab"));
			WebElement panel2 = driver.findElement(By.cssSelector("fieldset#drawAnchors.x-fieldset"));
			List<WebElement> LabelValuesTooltipTabChange = panel2.findElements(By.tagName("input"));

			for (int i = 0; i < LabelValuesTooltipTabChange.size(); i++) {

				LabelValuesTooltipTabChange.get(1).click();
				LabelValuesTooltipTabChange.get(1).clear();
				LabelValuesTooltipTabChange.get(1).sendKeys("4");
				Assert.assertNotNull(LabelValuesTooltipTabChange.get(1));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Configuration_Radius() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 237, 2);
			ExcelWriter(ExecResult, ErrorDesc, 237, 3);

		}
	}

	public void Configuration_Opaqueness() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);
			WebElement panel1 = driver.findElement(By.id("dpAnchorsTab"));
			WebElement panel2 = driver.findElement(By.cssSelector("fieldset#drawAnchors.x-fieldset"));
			List<WebElement> LabelValuesTooltipTabChange = panel2.findElements(By.tagName("input"));

			for (int i = 0; i < LabelValuesTooltipTabChange.size(); i++) {

				LabelValuesTooltipTabChange.get(2).click();
				LabelValuesTooltipTabChange.get(2).clear();
				LabelValuesTooltipTabChange.get(2).sendKeys("99");
				Assert.assertNotNull(LabelValuesTooltipTabChange.get(2));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Configuration_Opaqueness()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 238, 2);
			ExcelWriter(ExecResult, ErrorDesc, 238, 3);

		}
	}

	public void Border_color() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			WebElement panel1 = driver.findElement(By.id("dpAnchorsTab"));
			List<WebElement> button = panel1.findElements(By.tagName("button"));
			Assert.assertNotNull(button.get(0));
			button.get(0).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Border_color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 239, 2);
			ExcelWriter(ExecResult, ErrorDesc, 239, 3);

		}

	}

	public void Border_Thickness() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			WebElement panel1 = driver.findElement(By.id("dpAnchorsTab"));
			List<WebElement> button = panel1.findElements(By.tagName("input"));
			Assert.assertNotNull(button.get(4));
			button.get(4).click();
			button.get(4).clear();
			button.get(4).sendKeys(Keys.DELETE);
			button.get(4).sendKeys(Keys.BACK_SPACE);
			button.get(4).sendKeys(Keys.CLEAR);
			button.get(4).sendKeys(Keys.BACK_SPACE);
			button.get(4).sendKeys("2");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Border_Thickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 240, 2);
			ExcelWriter(ExecResult, ErrorDesc, 240, 3);

		}

	}

	public void Background_Opaqueness() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			WebElement panel1 = driver.findElement(By.id("dpAnchorsTab"));
			List<WebElement> button = panel1.findElements(By.tagName("input"));
			Assert.assertNotNull(button.get(5));
			button.get(5).click();
			button.get(5).clear();
			button.get(5).sendKeys(Keys.DELETE);
			button.get(5).sendKeys(Keys.BACK_SPACE);
			button.get(5).sendKeys(Keys.CLEAR);
			button.get(5).sendKeys(Keys.BACK_SPACE);
			button.get(5).sendKeys("99");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Background_Opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 241, 2);
			ExcelWriter(ExecResult, ErrorDesc, 241, 3);

		}
	}

	public void Background_color() throws InterruptedException, IOException {
		try {
			Thread.sleep(3500);
			WebElement panel1 = driver.findElement(By.id("dpAnchorsTab"));
			List<WebElement> button = panel1.findElements(By.tagName("button"));
			Assert.assertNotNull(button.get(1));
			button.get(1).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Background_color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 242, 2);
			ExcelWriter(ExecResult, ErrorDesc, 242, 3);

		}
	}

	///////// data plot///////
	public void spline_data_series_line_color() throws IOException {
		try {

			WebElement panel = driver.findElement(By.id("dpLineDataPlotTab"));
			List<WebElement> input = panel.findElements(By.tagName("button"));

			for (int i = 0; i < input.size(); i++) {

				input.get(0).click();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("spline_data_series_line_color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 243, 2);
			ExcelWriter(ExecResult, ErrorDesc, 243, 3);

		}

	}

	public void spline_data_series_line_thickness() throws IOException {
		try {
			WebElement panel = driver.findElement(By.id("dpLineDataPlotTab"));
			List<WebElement> input = panel.findElements(By.tagName("input"));

			for (int i = 0; i < input.size(); i++) {

				input.get(0).click();
				input.get(0).clear();
				input.get(0).sendKeys(Keys.DELETE);
				input.get(0).sendKeys(Keys.BACK_SPACE);
				input.get(0).sendKeys(Keys.CLEAR);
				input.get(0).sendKeys(Keys.BACK_SPACE);
				input.get(0).sendKeys("6");
				Assert.assertNotNull(input.get(0));
				break;

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("spline_data_series_line_thickness()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 244, 2);
			ExcelWriter(ExecResult, ErrorDesc, 244, 3);

		}
	}

	public void spline_data_series_line_opaqueness() throws IOException {
		try {

			WebElement panel = driver.findElement(By.id("dpLineDataPlotTab"));
			List<WebElement> input = panel.findElements(By.tagName("input"));

			for (int i = 0; i < input.size(); i++) {

				input.get(1).click();
				input.get(1).clear();
				input.get(1).sendKeys(Keys.DELETE);
				input.get(1).sendKeys(Keys.BACK_SPACE);
				input.get(1).sendKeys(Keys.CLEAR);
				input.get(1).sendKeys(Keys.BACK_SPACE);
				input.get(1).sendKeys("99");
				Assert.assertNotNull(input.get(1));
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("spline_data_series_line_opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 245, 2);
			ExcelWriter(ExecResult, ErrorDesc, 245, 3);

		}
	}

	/////////////////// Single Series LineTwoD Chart type......Axis -Vertical
	/////////////////// grid lines////////////////////////////

	public void Vertical_Grid_Lines() throws InterruptedException, IOException {
		try {
			Thread.sleep(5000);
			WebElement Axis = driver.findElement(By.cssSelector("div#divisionLinesStep.x-panel.x-panel-noborder"));
			List<WebElement> span = Axis.findElements(By.cssSelector("span.x-tab-strip-text"));

			for (int i = 0; i < span.size(); i++) {

				span.get(2).click();

				try {
					Assert.assertTrue(driver.getPageSource().contains("Show axis grid lines"));

				} catch (AssertionError e) {

					// String message = e.getMessage();
					System.out.println("Show Vertical grid lines Not Selected: " + e.getMessage());
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Vertical_Grid_Lines() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 246, 2);
			ExcelWriter(ExecResult, ErrorDesc, 246, 3);

		}
	}

	public void Show_Vertical_Grid_Lines() throws InterruptedException, IOException {
		try {
			Thread.sleep(6000);
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			WebElement span = Axis.findElement(By.cssSelector("label.x-fieldset-header"));

			span.click();

			Thread.sleep(1000);

			span.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_Vertical_Grid_Lines() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 247, 2);
			ExcelWriter(ExecResult, ErrorDesc, 247, 3);

		}
	}

	public void Number_of_Grid_Lines() throws InterruptedException, IOException {
		try {
			Thread.sleep(6200);
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(1).click();
				fieldset.get(1).clear();
				fieldset.get(1).sendKeys("10");
				Assert.assertNotNull(fieldset.get(1));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Number_of_Grid_Lines() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 248, 2);
			ExcelWriter(ExecResult, ErrorDesc, 248, 3);

		}

	}

	public void Grid_Line_thickness() throws InterruptedException, IOException {
		try {
			Thread.sleep(6400);

			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(2).click();
				fieldset.get(2).clear();
				fieldset.get(2).sendKeys("10");
				Assert.assertNotNull(fieldset.get(2));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Grid_Line_thickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 249, 2);
			ExcelWriter(ExecResult, ErrorDesc, 249, 3);

		}

	}

	public void Give_Color() throws IOException {
		try {
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("button"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(1).click();
				Assert.assertNotNull(fieldset.get(1));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_Color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 250, 2);
			ExcelWriter(ExecResult, ErrorDesc, 250, 3);

		}
	}

	public void Grid_Line_opaqueness() throws IOException {
		try {
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(3).click();
				fieldset.get(3).clear();
				fieldset.get(3).sendKeys("10");
				Assert.assertNotNull(fieldset.get(3));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Grid_Line_opaqueness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 251, 2);
			ExcelWriter(ExecResult, ErrorDesc, 251, 3);

		}
	}

	public void Show_Grid_Lines_As_Dashed() throws InterruptedException, IOException {
		try {

			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> input = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < input.size(); i++) {

				input.get(4).click();
				// input.get(4).click();
				// Thread.sleep(1000);
				// fieldset.get(4).click();
				// fieldset.get(4).clear();
				// fieldset.get(3).sendKeys("10");
				Assert.assertNotNull(input.get(4));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_Grid_Lines_As_Dashed() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 252, 2);
			ExcelWriter(ExecResult, ErrorDesc, 252, 3);

		}
	}

	public void Grid_Lines_Dash_Length() throws IOException {
		try {
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(5).click();
				fieldset.get(5).clear();
				fieldset.get(5).sendKeys("13");
				Assert.assertNotNull(fieldset.get(5));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Grid_Lines_Dash_Length() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 253, 2);
			ExcelWriter(ExecResult, ErrorDesc, 253, 3);

		}
	}

	public void Grid_lines_Dash_Gap() throws IOException {
		try {
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(6).click();
				fieldset.get(6).clear();
				fieldset.get(6).sendKeys("14");
				Assert.assertNotNull(fieldset.get(6));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Grid_lines_Dash_Gap()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 254, 2);
			ExcelWriter(ExecResult, ErrorDesc, 254, 3);

		}
	}

	public void Show_alternate_color_band() throws InterruptedException, IOException {
		try {

			Thread.sleep(1700);
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> button = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < button.size(); i++) {

				button.get(7).click();
				Assert.assertNotNull(button.get(7));
				// fieldset.get(6).clear();
				// fieldset.get(6).sendKeys("14");

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Show_alternate_color_band()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 255, 2);
			ExcelWriter(ExecResult, ErrorDesc, 255, 3);

		}
	}

	public void Give_alternate_color() throws IOException {
		try {
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("button"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(1).click();
				Assert.assertNotNull(fieldset.get(1));
				// fieldset.get(8).clear();
				// fieldset.get(8).sendKeys("11");

				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_alternate_color() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 256, 2);
			ExcelWriter(ExecResult, ErrorDesc, 256, 3);

		}
	}

	public void Give_alternate_color_Opaqueness() throws IOException {
		try {
			WebElement Axis = driver.findElement(By.id("dlVerticalGridLinesTab"));
			List<WebElement> fieldset = Axis.findElements(By.tagName("input"));

			for (int i = 0; i < fieldset.size(); i++) {

				fieldset.get(8).click();
				fieldset.get(8).clear();
				fieldset.get(8).sendKeys("14");
				Assert.assertNotNull(fieldset.get(8));
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Give_alternate_color_Opaqueness()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 257, 2);
			ExcelWriter(ExecResult, ErrorDesc, 257, 3);

		}
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Chart Type\\\\\\\\\\

	public void SplineArea_Chart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/SplineArea.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(" Column2D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SplineArea_Chart()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 258, 2);
			ExcelWriter(ExecResult, ErrorDesc, 258, 3);

		}
	}

	public void Area2D_Chart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Area2D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(" Column2D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Area2D_Chart()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 259, 2);
			ExcelWriter(ExecResult, ErrorDesc, 259, 3);

		}
	}

	public void Spline_Chart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Spline.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println(" Column2D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Spline_Chart()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 260, 2);
			ExcelWriter(ExecResult, ErrorDesc, 260, 3);

		}
	}

	///////////////////// User Interaction Drill Down//////////////////////

	public void ClickOnDrilldownBar1() throws InterruptedException, IOException {
		try {
			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// Thread.sleep(20000);
			// WebDriverWait wait = new WebDriverWait(driver, 60);// 1 minute

			// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='chartPanelContainer']//span//*[local-name()
			// = 'svg']/*[name()='g'][8]/*[name()='rect'][3]")));
			///////////////// ThailandSelection as country////////

			try {
				// Thread.sleep(20000);

				// WebDriverWait wait = new WebDriverWait(driver, 60);// 1
				// minute

				// WebElement SVGobject = driver.findElement(By.xpath(
				// "//div[@id='chartPanelContainer']//span//*[local-name() =
				// 'svg']/*[name()='g'][8]/*[name()='rect'][30]"));

				WebElement SVGobject = (WebElement) (new WebDriverWait(driver, 10))
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
								"//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[contains(@class,'red-hot')]/*[name()='rect'][30]")));
								//"//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][30]")));
				// Assert.assertNotNull(SVGobject);
				SVGobject.click();
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Click on Drilldown bar is not working: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickOnDrilldownBar1() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 261, 2);
			ExcelWriter(ExecResult, ErrorDesc, 261, 3);

		}

	}

	public void ClickOnDrilldownBar() throws InterruptedException, IOException {
		try {

			try {

				WebElement SVGobject = (WebElement) (new WebDriverWait(driver, 10))
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[contains(@class,'red-hot')]/*[name()='rect'][3]")));
						//.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//*[@id='ctl00_m_g_dceefc86_2c76_4a29_9e25_6e9513fb197d_ctl00_chartPanelChart']/svg/g[8]/rect[3]")));	
								//".//*[@id='ctl00_m_g_dceefc86_2c76_4a29_9e25_6e9513fb197d_ctl00_chartPanelChart']/svg/g[8]/rect[3]")));

				SVGobject.click();
				Assert.assertNotNull(driver.findElement(By.xpath("//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[contains(@class,'red-hot')]/*[name()='rect'][30]")));
						///+ ".//*[@id='ctl00_m_g_dceefc86_2c76_4a29_9e25_6e9513fb197d_ctl00_chartPanelChart']/svg/g[8]/rect[3]")));
						///**"//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][30]")));

			} catch (Exception e) {

				System.out.println("Click on Drilldown bar is not working: " + e.getMessage());

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickOnDrilldownBar() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 262, 2);
			ExcelWriter(ExecResult, ErrorDesc, 262, 3);

		}

	}

	public void ClickDrillDown() throws InterruptedException, IOException {
		try {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Thread.sleep(3000);
			try {

				Assert.assertNotNull(driver.findElement(By.xpath("//div/li[4]/ul/li[1]/div/a/span")));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Wizard Drilldown Menu Not Found " + e.getMessage());
			}
			driver.findElement(By.xpath("//div/li[4]/ul/li[1]/div/a/span")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickDrillDown() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 263, 2);
			ExcelWriter(ExecResult, ErrorDesc, 263, 3);

		}
	}

	public void DrillDownFirstOption() throws InterruptedException, IOException {
		try {

			// Thread.sleep(9000);
			WebElement rdMultiDrillDown = driver.findElement(By.id("rdMultiDrillDown"));

			try {

				Assert.assertNotNull(rdMultiDrillDown);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Wizard First Drilldown Option Not Found: " + e.getMessage());
			}
			rdMultiDrillDown.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DrillDownFirstOption()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 264, 2);
			ExcelWriter(ExecResult, ErrorDesc, 264, 3);

		}

	}

	public void subset_of_Data_dropdown() throws InterruptedException, IOException {
		try {
			WebElement dropdown = driver.findElement(By.id("drillDownFuncGroup"));
			dropdown.click();
			dropdown.sendKeys(Keys.ARROW_DOWN);
			dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("subset_of_Data_dropdown() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 265, 2);
			ExcelWriter(ExecResult, ErrorDesc, 265, 3);

		}
	}

	public void matched_field_for_the_subset_of_data() throws InterruptedException, IOException {
		try {
			WebElement dropdown = driver.findElement(By.id("drillDownGroupByGroup"));
			dropdown.click();
			dropdown.sendKeys(Keys.ARROW_DOWN);
			dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" matched_field_for_the_subset_of_data() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 266, 2);
			ExcelWriter(ExecResult, ErrorDesc, 266, 3);

		}
	}

	public void order_of_Data_Storing() throws InterruptedException, IOException {
		try {
			WebElement dropdown = driver.findElement(By.id("drillDownSortGroup"));
			dropdown.click();
			dropdown.sendKeys(Keys.ARROW_DOWN);
			dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("order_of_Data_Storing() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 267, 2);
			ExcelWriter(ExecResult, ErrorDesc, 267, 3);

		}
	}

	public void way_of_showing_chart_Data() throws InterruptedException, IOException {
		try {
			WebElement dropdown = driver.findElement(By.id("drillDownWhere"));
			dropdown.click();
			dropdown.sendKeys(Keys.ARROW_DOWN);
			dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("way_of_showing_chart_Data() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 268, 2);
			ExcelWriter(ExecResult, ErrorDesc, 268, 3);

		}
	}

	public void Configuration() throws InterruptedException, IOException {
		try {

			Thread.sleep(20000);
			WebElement button = driver.findElement(By.xpath("//button[text()='Configuration']"));

			try {

				Assert.assertNotNull(button);

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Configuration Button Not Found: " + e.getMessage());
			}

			button.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Configuration() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 269, 2);
			ExcelWriter(ExecResult, ErrorDesc, 269, 3);

		}

	}
	

	public void ChartTypeChoose() throws InterruptedException  {
	
		
		WebElement ChartTypedropdown1 = driver.findElement(By.id("drilldowncharttype"));
		ChartTypedropdown1.click();
		ChartTypedropdown1.sendKeys(Keys.ARROW_DOWN);
		ChartTypedropdown1.sendKeys(Keys.ARROW_DOWN);
		ChartTypedropdown1.sendKeys(Keys.ARROW_DOWN);
		ChartTypedropdown1.sendKeys(Keys.ARROW_DOWN);
		ChartTypedropdown1.sendKeys(Keys.ARROW_DOWN);
		ChartTypedropdown1.sendKeys(Keys.ENTER);
		/*Thread.sleep(1000);
		WebElement ChartTypedropdown2= driver.findElement(By.xpath("//div[2]"));
		ChartTypedropdown2.click();*/
		
		
		
		
	}
	
//////////////////////////////***************CCSP2.4 Modification ..............Drill down caption modification............../////////////////////////
	
	public void Chart_Type() {
		
		
		
	}
	
	
	
	
	public void Chart_Title() throws InterruptedException, IOException {
		try {

			WebElement field = driver.findElement(By.id("caption"));
			field.click();
			Thread.sleep(1000);
			field.clear();
			field.sendKeys("{wizardFilter}");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Chart_Title() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 270, 2);
			ExcelWriter(ExecResult, ErrorDesc, 270, 3);

		}

	}

	public void Chart_Sub_Title() throws InterruptedException, IOException {
		try {

			WebElement field = driver.findElement(By.id("subcaption"));
			field.click();
			Thread.sleep(1000);
			field.clear();
			field.sendKeys("{userFilter}");
			// field.sendKeys("f");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Chart_Sub_Title() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 271, 2);
			ExcelWriter(ExecResult, ErrorDesc, 271, 3);

			

		}

	}

	public void X_Axis_Title() throws InterruptedException, IOException {
		try {

			WebElement field = driver.findElement(By.id("xaxisname"));
			field.click();
			Thread.sleep(1000);
			field.clear();
			field.sendKeys("{clickedLabel}");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("X_Axis_Title() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 272, 2);
			ExcelWriter(ExecResult, ErrorDesc, 272, 3);

		}

	}

	public void Primary_Y_axis_Title() throws InterruptedException, IOException {
		try {

			WebElement field = driver.findElement(By.id("pyaxisname"));
			field.click();
			Thread.sleep(1000);
			field.clear();
			field.sendKeys(" ");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Primary_Y_axis_Title() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 273, 2);
			ExcelWriter(ExecResult, ErrorDesc, 273, 3);

		}

	}

	public void Rotate_Y_axis_Title() throws InterruptedException, IOException {
		try {

			WebElement field = driver.findElement(By.id("rotateyaxisname"));
			field.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Rotate_Y_axis_Title() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 475, 2);
			ExcelWriter(ExecResult, ErrorDesc, 475, 3);

		}

	}

	public void Y_axis_width() throws InterruptedException, IOException {
		try {

			WebElement field = driver.findElement(By.id("yaxisnamewidth"));
			field.click();
			field.sendKeys("10");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Y_axis_width() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 274, 2);
			ExcelWriter(ExecResult, ErrorDesc, 274, 3);

		}

	}

	public void DrillDownSecondOption() throws InterruptedException, IOException {
		try {

			WebElement rdShowData = driver.findElement(By.id("rdShowData"));
			try {

				Assert.assertNotNull(rdShowData);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Second Drilldown Option Not Found: " + e.getMessage());
			}

			rdShowData.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DrillDownSecondOption() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 275, 2);
			ExcelWriter(ExecResult, ErrorDesc, 275, 3);

		}

	}

	public void DrillDownThirdOption() throws InterruptedException, IOException {
		try {

			WebElement rdHotspot = driver.findElement(By.id("rdHotspot"));
			rdHotspot.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DrillDownThirdOption()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 276, 2);
			ExcelWriter(ExecResult, ErrorDesc, 276, 3);

		}
	}

	public void DrillDownFourthOption() throws InterruptedException, IOException {
		try {

			WebElement rdQueryString = driver.findElement(By.id("rdQueryString"));
			try {

				Assert.assertNotNull(rdQueryString);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("DrillDown Fourth Option Not Found: " + e.getMessage());
			}
			rdQueryString.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DrillDownFourthOption() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 277, 2);
			ExcelWriter(ExecResult, ErrorDesc, 277, 3);

		}

	}

	public void DrillDownFifthOption() throws InterruptedException, IOException {
		try {

			WebElement rdNoDrillDown = driver.findElement(By.id("rdNoDrillDown"));
			rdNoDrillDown.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DrillDownFifthOption() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 278, 2);
			ExcelWriter(ExecResult, ErrorDesc, 278, 3);

		}

	}

	public void ApplyYes() throws InterruptedException
	{
		Thread.sleep(1000);
		WebElement ApplyYes = driver.findElement(By.xpath("//tbody/tr[2]/td[2]"));
		ApplyYes.click();
	}
	
	public void OK() throws AWTException, InterruptedException{
		
		Robot rb =new Robot();
		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		
		
		/*String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			 driver.switchTo().window(winHandle);
		}
		    
		Thread.sleep(2050);
	driver.findElement(By.xpath("//*[contains(text(),'OK')]")).click();*/
	//x-toolbar-right-row
	//tr
	
	/*WebElement righttoolbar = driver.findElement(By.className("x-window-br"));
	
	List<WebElement> firsttablerow = righttoolbar.findElements(By.tagName("button"));*/

	//firsttablerow.click();
	
	
	
	
	}
	
	
   public void Cancel() throws AWTException
   {
	   
	   Robot rb =new Robot();
	   rb.keyPress(KeyEvent.VK_CANCEL);
   }
	
	public void ok() throws InterruptedException, IOException {
		try {
			
			Robot rb =new Robot();
			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			
		//	driver.findElement(By.tagName("span")).getText();
			
		//	Thread.sleep(4000);
			
			/*Alert alert = driver.switchTo().alert();
	
			alert.accept();*/


			// WebElement DrillDownfooter

			// driver.findElement(By.cssSelector("td.x-toolbar-right"));
		//	WebElement OkButton = driver.findElement(By.xpath("//button[text()='OK']"));
			
			/*WebElement OkButton =driver.findElement(By.cssSelector("td.x-btn-mc"));
			OkButton.click();*/
	/*		
			List<WebElement> OkButton =driver.findElements(By.tagName("button"));
			OkButton.get(3).click();
			OkButton.get(3).sendKeys(Keys.ENTER);
			*/
			//driver.findElement(By.xpath("//*[contains(text(),'OK')]")).click();
			
			
			
			//Thread.sleep(500);
			// OkButton.submit();
			////////////////OkButton.click();
			//Thread.sleep(1000);
			// OkButton.click();
			// OkButton.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ok123() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 477, 2);
			ExcelWriter(ExecResult, ErrorDesc, 477, 3);

		}
	}

	public void Group_By_Dates() throws IOException {
		try {
			WebElement Drill_Input = driver.findElement(By.id("drillDownGroupByGroup"));
			Drill_Input.click();

			WebElement Input = driver.findElement(By.xpath("//div/div/div[text()='Date']"));

			try {

				Assert.assertNotNull(Input);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Group By Date Radio Button Not Found: " + e.getMessage());
			}
			Input.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Group_By_Dates()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 280, 2);
			ExcelWriter(ExecResult, ErrorDesc, 280, 3);

		}

	}

	public void Group_By_Fields() throws IOException {
		try {
			WebElement Drilldown_Panel = driver.findElement(By.id("drilldownStep"));

			WebElement Input = Drilldown_Panel.findElements(By.tagName("input")).get(2);
			try {

				Assert.assertNotNull(Input);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Group By Date Radio Button Not Found: " + e.getMessage());
			}

			Input.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" Group_By_Fields() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 281, 2);
			ExcelWriter(ExecResult, ErrorDesc, 281, 3);

		}
	}

	
	
	public void Customize() throws InterruptedException
	{
		
		Thread.sleep(1000);
		WebElement Customize = driver.findElement(By.xpath("//button[text()='Customize']"));
		 
		Customize.click();
		 
	}
		
		
	
	
	
	
	
	public void First_DrillDown_Field() throws InterruptedException, IOException {
		try {
			/*
			 * // List<WebElement> First_DrillDown_Field = //
			 * driver.findElements(By.className("x-grid3-col-ColumnName"));
			 *
			 * // First_DrillDown_Field.get(0).click();
			 *
			 * // WebElement First_DrillDown_Field_Input = //
			 * driver.findElement(By.tagName("input")) //Thread.sleep(2000);
			 *
			 * WebElement Drilldown_Panel =
			 * driver.findElement(By.id("drilldownStep"));
			 *
			 * Drilldown_Panel.findElements(By.className(
			 * "x-grid3-col-ColumnName")). get(0).click();
			 *
			 * // Thread.sleep(3000);
			 *
			 *
			 */
			// WebElement Drilldown_Panel =
			// driver.findElement(By.id("drilldownStep"));
			//
			// Drilldown_Panel.findElements(By.className("x-grid3-col-ColumnName")).get(0).click();
			WebElement el = driver
					.findElement(By.xpath(".//*[@id='gdMultiLevelGridPanel']//div[1]/table/tbody/tr/td[2]/div"));
			el.click();
			Thread.sleep(1000);
			WebElement ElInput = driver.findElement(By.xpath(".//*[@id='gdMultiLevelGridPanel']//div/input"));
			ElInput.click();
			Thread.sleep(1000);
			ElInput.sendKeys(Keys.ARROW_DOWN);
			ElInput.sendKeys(Keys.ENTER);

			// JavascriptExecutor js = (JavascriptExecutor) driver;
			//
			// String jQuerySelector =
			// "CCSP.jQuery(\".x-combo-list-item:eq(6)\").click();";
			// String findDropdown = "return " + jQuerySelector;
			// //System.out.println(findDropdown);
			//
			// js.executeScript(findDropdown);

			try {

				// Assert.assertNotNull(findDropdown);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("First Drilldown Field Not Found: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("First_DrillDown_Field() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 282, 2);
			ExcelWriter(ExecResult, ErrorDesc, 282, 3);

		}

	}

	public void Second_DrillDown_Field() throws IOException {
		try {

			WebElement Second_DrillDown = driver.findElement(By.xpath("	.//*[@id='rdShowData']"));
			try {

				Assert.assertNotNull(Second_DrillDown);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Second Drilldown Field Not Found: " + e.getMessage());
			}

			Second_DrillDown.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Second_DrillDown_Field() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 283, 2);
			ExcelWriter(ExecResult, ErrorDesc, 283, 3);

		}

	}

	public void ok1() throws InterruptedException, IOException {
		try {
			// Thread.sleep(2000);
			// List<WebElement> OkButtonTag =
			// driver.findElements(By.tagName("em"));
			// List<WebElement> tag =
			// paneldiv.findElements(By.tagName("input"));
			// for(int i = 0; i < OkButtonTag.size(); i++) {
			// List<WebElement> OkButtonClass =
			// OkButtonTag.get(i).findElements(By.className("x-btn-text"));
			// WebElement OkButtonClass =
			// driver.findElement(By.xpath("//button[text()='OK']"));
			// OkButtonClass.click();
			/*
			 * driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			 *
			 *
			 * WebElement AxisTitles =
			 * driver.findElement(By.className("x-panel-btns"));
			 * List<WebElement> Button =
			 * AxisTitles.findElements(By.tagName("button"));
			 * //button[text()='OK']
			 *
			 *
			 * for(int i = 0; i < Button.size(); i++) {
			 *
			 * WebElement OkButton=
			 * Button.get(i).findElement(By.xpath("//button[text()='OK']"));
			 * OkButton.click();
			 *
			 * }
			 */
			Thread.sleep(2000);
			WebElement OkButton = driver.findElement(By.xpath("//button[text()='OK']"));
			OkButton.click();

			// OkButton.click();
			// OkButton.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ok1()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 284, 2);
			ExcelWriter(ExecResult, ErrorDesc, 284, 3);

		}
	}

	public void Enable_Multi_level_DrillDown() throws IOException {
		try {
			WebElement chkBoxMultiLevel = driver.findElement(By.id("chkBoxMultiLevel"));

			try {

				Assert.assertTrue(chkBoxMultiLevel.isDisplayed());
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi level Drilldown Checkbox Not Found: " + e.getMessage());
			}
			chkBoxMultiLevel.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Enable_Multi_level_DrillDown() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 285, 2);
			ExcelWriter(ExecResult, ErrorDesc, 285, 3);

		}

	}
	
////////////////////////////////CCSP 2.4 new feature************new radio button introduced......Modification////////////////////
	public void Enable_Chart_Type_View() throws IOException {
		try{
		
		WebElement Enable_Chart_Type_View = driver.findElement(By.id("chkChangeDrillChart"));
		
		Enable_Chart_Type_View.click();
		}
	   catch (Exception e) {
		ExecResult = "FAIL";
		ErrorDesc = e.getMessage();
		System.out.println("Enable_Chart_Type_View() fail in catch:" + ErrorDesc);
	} finally {
		// System.out.print("SelectExcelDataProvider() fail in catch:" +
		// ErrorDesc);
		ExcelWriter(ExecResult, ErrorDesc, 435, 2);
		ExcelWriter(ExecResult, ErrorDesc, 435, 3);
		// System.out.print(" SelectExcelDataProvider() finally passed ");

	}

}

		
	
////////////////////////////////CCSP 2.4 new feature****************End***//////////////////////////////////
	
	public void DrillDown_LinkUrl() throws IOException {
		try {
			WebElement clickURL = driver.findElement(By.id("clickURL"));

			try {

				Assert.assertNotNull(clickURL);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("DrillDown LinkUrl Field Not Found: " + e.getMessage());
			}
			clickURL.click();
			// clickURL.sendKeys("http://pc-nibir2010/SitePages/site%20page%2020th.aspx?Country={label}");
			clickURL.sendKeys("http://pc-nibir2010/SitePages/CCSPQueryStringDontTouch.aspx?Country={label}");
					//"http://ccsptestsvr/SitePages/QueryStringUniversalSourcePage.aspx?Country={label}");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DrillDown_LinkUrl() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 286, 2);
			ExcelWriter(ExecResult, ErrorDesc, 286, 3);

		}
	}

	public void Url() throws IOException {
		try {
			WebElement clickURL = driver.findElement(By.id("clickURL"));

			try {

				Assert.assertNotNull(clickURL);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("DrillDown LinkUrl Field Not Found: " + e.getMessage());
			}
			clickURL.click();
			clickURL.clear();
			// clickURL.sendKeys("http://pc-nibir2010/SitePages/site%20page%2020th.aspx?Country={label}");
			clickURL.sendKeys("https://www.google.co.in/");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Url() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 287, 2);
			ExcelWriter(ExecResult, ErrorDesc, 287, 3);

		}

	}

	public void DrillDown_openURLin() throws IOException {
		try {
			WebElement openURLin = driver.findElement(By.id("openURLin"));
			try {

				Assert.assertNotNull(openURLin);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("DrillDown OpenURL option Not Found: " + e.getMessage());
			}

			openURLin.click();
			openURLin.sendKeys(Keys.ARROW_DOWN);

			openURLin.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DrillDown_openURLin()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 288, 2);
			ExcelWriter(ExecResult, ErrorDesc, 288, 3);

		}

	}

	public void SortBy() throws IOException {
		try {
			WebElement SortBy = driver.findElement(By.id("drillDownSortGroup"));

			try {

				Assert.assertNotNull(SortBy);

			} catch (AssertionError e) {

				System.out.println("Sort By Dropdown Option Not Found: " + e.getMessage());
			}
			SortBy.click();
			driver.findElement(By.xpath("//div/div/div[text()='(None)']")).click();
			SortBy.click();
			SortBy.sendKeys(Keys.ARROW_DOWN);
			SortBy.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SortBy() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 289, 2);
			ExcelWriter(ExecResult, ErrorDesc, 289, 3);

		}
	}

	public void Apply_to_all() throws IOException {
		try {

			WebElement Apply_to_all = driver.findElement(By.xpath("//button[text()='Apply to all']"));
			Apply_to_all.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Apply_to_all() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 290, 2);
			ExcelWriter(ExecResult, ErrorDesc, 290, 3);

		}

	}
	///////////////////// User Interaction Dynamic Filter//////////////////////

	public void filter_data_dynamically() throws InterruptedException, IOException {
		try {
			// Thread.sleep(4000);
			WebElement dyFilterChk = driver.findElement(By.id("dyFilterChk"));
			dyFilterChk.click();
			Thread.sleep(1000);
			Assert.assertTrue(dyFilterChk.isEnabled());
			// dyFilterChk.sendKeys(Keys.ARROW_DOWN);;
			// openURLin.sendKeys(Keys.ENTER);;
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("filter_data_dynamically() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 291, 2);
			ExcelWriter(ExecResult, ErrorDesc, 291, 3);

		}

	}

	public void First_filter_Checkbox_Click() throws InterruptedException, IOException {
		try {
			Thread.sleep(1500);
			WebElement Dynamic_Filter_Panel = driver.findElement(By.cssSelector("div#FilterSettingsStep"));
			WebElement First_filter_Checkbox_Click = Dynamic_Filter_Panel
					.findElement(By.xpath("//div[1]/table/tbody/tr/td[1]/div/div"));
			First_filter_Checkbox_Click.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("First_filter_Checkbox_Click() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 292, 2);
			ExcelWriter(ExecResult, ErrorDesc, 292, 3);

		}
	}

	/*
	 * //Thread.sleep(2500); List<WebElement> Second_filter_Checkbox_Click =
	 * Dynamic_Filter_Panel.findElements(By.xpath(
	 * "//table/tbody/tr/td[1]/div/div"));
	 * Second_filter_Checkbox_Click.get(0).click();
	 * Second_filter_Checkbox_Click.get(1).click();
	 * Second_filter_Checkbox_Click.get(2).click();
	 * Second_filter_Checkbox_Click.get(3).click();
	 */
	public void Second_filter_Checkbox_Click() throws InterruptedException, IOException {
		try {

			Thread.sleep(1800);
			WebElement Dynamic_Filter_Panel = driver.findElement(By.cssSelector("div#FilterSettingsStep"));
			WebElement Second_filter_Checkbox_Click = Dynamic_Filter_Panel
					.findElement(By.xpath("//div[2]/table/tbody/tr/td[1]/div/div"));
			Second_filter_Checkbox_Click.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Second_filter_Checkbox_Click() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 293, 2);
			ExcelWriter(ExecResult, ErrorDesc, 293, 3);

		}
	}

	public void Forth_filter_Checkbox_Click() throws InterruptedException, IOException {
		try {

			Thread.sleep(2200);
			WebElement Dynamic_Filter_Panel = driver.findElement(By.cssSelector("div#FilterSettingsStep"));
			WebElement Forth_filter_Checkbox_Click = Dynamic_Filter_Panel
					.findElement(By.xpath("//div[4]/table/tbody/tr/td[1]/div/div"));
			Forth_filter_Checkbox_Click.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Forth_filter_Checkbox_Click() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 294, 2);
			ExcelWriter(ExecResult, ErrorDesc, 294, 3);

		}

	}

	public void filter_only_chart_fields_And_Data() throws InterruptedException, IOException {
		try {

			// Thread.sleep(4000);
			WebElement dyRgeScope2 = driver.findElement(By.xpath(".//*[@id='dyRgeScope2']"));
			// WebElement dyRgeScope2 =
			// driver.findElement(By.className("dyChartFieldsScope"));
			dyRgeScope2.click();
			// dyFilterChk.sendKeys(Keys.ARROW_DOWN);;
			// openURLin.sendKeys(Keys.ENTER);;
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("filter_only_chart_fields_And_Data() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 295, 2);
			ExcelWriter(ExecResult, ErrorDesc, 295, 3);

		}

	}

	public void filter_hdchecker() throws IOException {
		try {
			WebElement hdchecker = driver.findElement(By.cssSelector("div.cc-x-grid3-hdchecker"));
			hdchecker.click();
			// dyFilterChk.sendKeys(Keys.ARROW_DOWN);;
			// openURLin.sendKeys(Keys.ENTER);;
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("filter_hdchecker() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 296, 2);
			ExcelWriter(ExecResult, ErrorDesc, 296, 3);

		}

	}

	public void ClickDynamicFilter() throws InterruptedException, IOException {
		try {
			Thread.sleep(8000);

			WebDriverWait W1 = new WebDriverWait(driver, 20);
			W1.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div/li[4]/ul/li[2]/div/a/span")));
			WebElement DynamicFilterTab = driver.findElement(By.xpath("//div/li[4]/ul/li[2]/div/a/span"));
			DynamicFilterTab.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickDynamicFilter() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 297, 2);
			ExcelWriter(ExecResult, ErrorDesc, 297, 3);

		}
	}

	public void Export_Settings() throws InterruptedException, IOException {
		try {
			Thread.sleep(7000);
			WebElement export_settings = driver.findElement(By.xpath("//div/li[4]/ul/li[3]/div/a/span"));
			export_settings.click();
			// Assert.assertTrue(export_settings.getCssValue("css=td[bgcolor=#FFE2C0]"),
			// false);

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Export_Settings() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 298, 2);
			ExcelWriter(ExecResult, ErrorDesc, 298, 3);

		}
	}

	public void Enable_printing_of_chart() throws IOException {
		try {
			WebElement export_div = driver.findElement(By.id("exportStep"));

			List<WebElement> checkbox_div = export_div.findElements(By.tagName("input"));

			for (int i = 0; i < checkbox_div.size(); i++) {

				checkbox_div.get(0).click();
				Assert.assertTrue(checkbox_div.get(0).isEnabled());
				break;

				/* txtExportFileName */
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Enable_printing_of_chart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 299, 2);
			ExcelWriter(ExecResult, ErrorDesc, 299, 3);

		}

	}

	public void Revert_Button() throws IOException {
		try {

			driver.findElement(By.xpath("//button[text()='Revert']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Revert_Button() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 300, 2);
			ExcelWriter(ExecResult, ErrorDesc, 300, 3);

		}
	}

	public void Enable_exporting_chart_as_Excel() throws IOException {
		try {
			WebElement export_div = driver.findElement(By.id("exportStep"));

			List<WebElement> checkbox_div = export_div.findElements(By.tagName("input"));

			for (int i = 0; i < checkbox_div.size(); i++) {

				checkbox_div.get(1).click();

				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Enable_exporting_chart_as_Excel() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 301, 2);
			ExcelWriter(ExecResult, ErrorDesc, 301, 3);

		}
	}

	public void Enable_exporting_charts_as_image() throws InterruptedException, IOException {
		try {
			WebElement export_div = driver.findElement(By.id("exportStep"));

			List<WebElement> checkbox_div = export_div.findElements(By.tagName("input"));

			for (int i = 0; i < checkbox_div.size(); i++) {

				checkbox_div.get(2).click();
				// Thread.sleep(500);
				// checkbox_div.get(2).click();
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Enable_exporting_charts_as_image() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 302, 2);
			ExcelWriter(ExecResult, ErrorDesc, 302, 3);

		}
	}

	public void Export_file_name() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement export_div = driver.findElement(By.id("exportStep"));

			List<WebElement> checkbox_div = export_div.findElements(By.tagName("input"));
			// checkbox_div.get(4)/.c;

			// checkbox_div.click();
			checkbox_div.get(4).clear();
			checkbox_div.get(4).sendKeys("Collabion Charts for SharePoint");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Export_file_name() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 303, 2);
			ExcelWriter(ExecResult, ErrorDesc, 303, 3);

		}

	}

	public void ExportChartButton() throws InterruptedException, IOException {
		try {

			try {
				Thread.sleep(1200);
				WebElement ExportChartButton = driver.findElement(By.xpath(
						"//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][9]/*[name()='g'][1]/*[name()='rect'][1]"));
				ExportChartButton.click();

			} catch (Exception e) {

				System.out.println("Export Chart Button is not working: " + e.getMessage());
				// WebElement ExportExcelChartButton=
				// driver.findElement(By.xpath("//tspan[text()='Download as JPEG
				// image']"));

				// WebElement ExportExcelChartButton=
				// driver.findElement((By.xpath(".//*[@id='ext-gen3']/svg[2]/g/text[3]/tspan")));
				// ExportExcelChartButton.click();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExportChartButton() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 304, 2);
			ExcelWriter(ExecResult, ErrorDesc, 304, 3);

		}
	}
	///////////////////// Filter data dropdown selection//////////////////////

	public void IncludeAllDates() throws InterruptedException, IOException {
		try {
			Thread.sleep(4000);
			WebElement Date = driver.findElement(By.id("addEmptyRecords"));
			Date.click();
			Assert.assertTrue(Date.isSelected());
			try {
				Assert.assertTrue(Date.isSelected());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Include All Dates Checkbox is not selected: " + e.getMessage());
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("IncludeAllDates() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 305, 2);
			ExcelWriter(ExecResult, ErrorDesc, 305, 3);

		}
	}

	public void DeleteButton() throws IOException {
		try {
			WebElement Delete = driver.findElement(By.className("cc-delete-button"));

			try {
				Assert.assertTrue(Delete.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Filter Data Delete Button is not Enabled: " + e.getMessage());
			}
			Delete.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DeleteButton() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 306, 2);
			ExcelWriter(ExecResult, ErrorDesc, 306, 3);

		}
	}

	public void AddButton() throws InterruptedException, IOException {
		try {
			WebElement Add = driver.findElement(By.className("cc-add-button"));

			try {
				Assert.assertTrue(Add.isEnabled());

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Filter Data Add Button is not Enabled: " + e.getMessage());
			}
			Add.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("AddButton() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 307, 2);
			ExcelWriter(ExecResult, ErrorDesc, 307, 3);

		}
	}

	public void ColumnNameDropdown() throws InterruptedException, IOException {
		try {

			Thread.sleep(8000);

			// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			List<WebElement> MainPanel = driver
					.findElements(By.cssSelector("div#filteringStep.x-panel.x-panel-noborder"));
			// List<WebElement> checkbox1 =
			// MainPanel.findElements(By.tagName("img"));
			// List<WebElement> checkbox2 =
			// MainPanel.findElements(By.className("x-form-arrow-trigger"));
			// List<WebElement> checkbox3 =
			// MainPanel.findElements(By.className("x-combo-list-item"));

			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(2).isDisplayed()) {

					checkbox2.get(2).click();

					Thread.sleep(1200);
					// String findfield = "First Filter Dropdown Selection " +
					// checkbox2.get(2).isEnabled() ;
					// System.out.println(findfield);

					// WebElement
					// DropDownValue=driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[2]"));
					// DropDownValue.click();

					checkbox2.get(2).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(2).sendKeys(Keys.ARROW_DOWN);

					// checkbox2.get(0).click();
					checkbox2.get(2).sendKeys(Keys.ENTER);

					// checkbox2.get(0).sendKeys(Keys.ENTER);
					// String findfield = "return " +
					// checkbox2.get(2).isSelected()
					// ;
					// System.out.println(findfield);
					// System.out.println("First Filter Dropdown +
					// checkbox2.get(2).isSelected()");
					Thread.sleep(9000);
					try {
						Assert.assertTrue(checkbox2.get(2).isEnabled());
						Assert.assertNotNull(checkbox2.get(2));

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("First Filter Data Dropdown Option is not found: " + e.getMessage());
					}
				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ColumnNameDropdown() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 308, 2);
			ExcelWriter(ExecResult, ErrorDesc, 308, 3);

		}

		/*
		 * if (checkbox2.get(3).isDisplayed() ) { checkbox2.get(3).click();
		 * checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
		 * checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
		 * checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
		 * checkbox2.get(3).sendKeys(Keys.ARROW_DOWN); //
		 * checkbox2.get(1).click();
		 * //checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
		 * checkbox2.get(3).sendKeys(Keys.ENTER);
		 * //checkbox2.get(1).sendKeys(Keys.ENTER); }
		 *
		 *
		 * }
		 */
	}

	public void ColumnNameDropdownSecond() throws InterruptedException, IOException {
		try {
			Thread.sleep(8600);
			List<WebElement> MainPanel = driver
					.findElements(By.cssSelector("div#filteringStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(3).isDisplayed()) {
					checkbox2.get(3).click();

					Thread.sleep(3000);
					// String findfield = "Second Filter Dropdown Selection " +
					// checkbox2.get(3).isEnabled() ;
					// System.out.println(findfield);

					checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
					// checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
					// checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
					// checkbox2.get(1).click();
					// checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(3).sendKeys(Keys.ENTER);

					// checkbox2.get(1).sendKeys(Keys.ENTER);
					Thread.sleep(10000);
					try {
						Assert.assertTrue(checkbox2.get(3).isEnabled());
						Assert.assertNotNull(checkbox2.get(3));
					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Second Filter Data Dropdown Option is not found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ColumnNameDropdownSecond() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 309, 2);
			ExcelWriter(ExecResult, ErrorDesc, 309, 3);

		}
	}

	public void ColumnNameField() throws InterruptedException, IOException {
		try {
			List<WebElement> MainPanel = driver
					.findElements(By.cssSelector("div#filteringStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(4).isDisplayed()) {
					checkbox2.get(4).click();

					// checkbox2.get(1).click();
					// checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(4).sendKeys("9");
					// checkbox2.get(1).sendKeys(Keys.ENTER);

					Thread.sleep(4000);

					try {
						Assert.assertTrue(checkbox2.get(4).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Filter Data Field is not found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" ColumnNameField() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 310, 2);
			ExcelWriter(ExecResult, ErrorDesc, 310, 3);

		}
	}

	public void CSV_search_data() throws InterruptedException, IOException {
		try {
			List<WebElement> MainPanel = driver
					.findElements(By.cssSelector("div#filteringStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(4).isDisplayed()) {
					checkbox2.get(4).click();

					// checkbox2.get(1).click();
					// checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(4).sendKeys("Cha");
					// checkbox2.get(1).sendKeys(Keys.ENTER);

					Thread.sleep(4000);

					try {
						Assert.assertTrue(checkbox2.get(4).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Filter Data Field is not found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CSV_search_data() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 311, 2);
			ExcelWriter(ExecResult, ErrorDesc, 311, 3);

		}
	}

	public void SQL_server_search() throws InterruptedException, IOException {
		try {
			List<WebElement> MainPanel = driver
					.findElements(By.cssSelector("div#filteringStep.x-panel.x-panel-noborder"));
			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(4).isDisplayed()) {
					checkbox2.get(4).click();

					// checkbox2.get(1).click();
					// checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(4).sendKeys("5");
					// checkbox2.get(1).sendKeys(Keys.ENTER);

					Thread.sleep(4000);

					try {
						Assert.assertTrue(checkbox2.get(4).isEnabled());

					} catch (AssertionError e) {

						// String message = e.getMessage();
						System.out.println("Filter Data Field is not found: " + e.getMessage());
					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SQL_server_search() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 312, 2);
			ExcelWriter(ExecResult, ErrorDesc, 312, 3);

		}
	}
	///////////////////// Group data dropdown selection//////////////////////

	public void FirstSeriesGroupDataFunction() throws InterruptedException, IOException {
		try {
			
			WebElement DropDown = driver.findElement(By.xpath(
					"//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Revenue']/../../td[2]/div"));
			DropDown.click();
			WebElement Editor = new WebDriverWait(driver, 12).until(ExpectedConditions.elementToBeClickable(
					By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input")));
			Editor.click();
			driver.findElement(By.xpath("//div/span[text()='SUM']")).click();

		
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FirstSeriesGroupDataFunction() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 479, 2);
			ExcelWriter(ExecResult, ErrorDesc, 479, 3);

		}

	}

	public void SecondSeriesGroupDataFunction() throws InterruptedException, IOException {
		
		WebElement DropDown = driver.findElement(By.xpath(
				"//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Sales']/../../td[2]/div"));
		DropDown.click();
		WebElement Editor = new WebDriverWait(driver, 12).until(ExpectedConditions.elementToBeClickable(
				By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input")));
		Editor.click();
		driver.findElement(By.xpath("//div/span[text()='AVERAGE']")).click();
		
		
		
		
	}
	
	
	
	
	
	public void GroupDataFunctionn() throws InterruptedException, IOException {
		try {
			// List<WebElement> MainPanel =
			// driver.findElements(By.id("gd-gdGroupGrid"));
			//
			// for (int i = 0; i < MainPanel.size(); i++) {
			//
			// List<WebElement> checkbox2 = MainPanel.get(i)
			// .findElements(By.cssSelector("div.x-grid3-cell-inner.x-grid3-col-1"));
			//
			// if (checkbox2.get(1).isDisplayed()) {
			//
			// driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			// checkbox2.get(6).click();
			// checkbox2.get(6).click();
			driver.findElement(By.xpath("//div/span[text()='AVERAGE']")).click();
			// Thread.sleep(10900);
			/*
			 * //String jQuerySelector1 = "$(\":contains('Apply')\").click()";
			 * JavascriptExecutor js = (JavascriptExecutor) driver; String
			 * jQuerySelector1 = "$(\"td.x-btn-mc:eq(13)\").click()"; String
			 * findDropdown1 = "return " + jQuerySelector1 ;
			 * System.out.println(findDropdown1);
			 * js.executeScript(findDropdown1); break;
			 */

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("GroupDataFunctionn() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 314, 2);
			ExcelWriter(ExecResult, ErrorDesc, 314, 3);

		}
	}

	public void GroupDataFunction2() throws IOException {
		try {
			List<WebElement> DivClass = driver.findElements(By.tagName("div"));

			for (int a = 0; a < DivClass.size(); a++) {

				WebElement checkbox3 = DivClass.get(a).findElement(By.tagName("input"));
				// WebElement checkbox4=
				// DivClass.get(a).findElement(By.tagName("img"));

				// checkbox3.click();

				checkbox3.sendKeys(Keys.ARROW_DOWN);
				checkbox3.sendKeys(Keys.ENTER);
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("GroupDataFunction2() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 315, 2);
			ExcelWriter(ExecResult, ErrorDesc, 315, 3);

		}
	}

	public void ColumnNameDropdown1() throws IOException {
		try {
			// WebElement DropdownElement = (WebElement)
			// driver.findElements(By.xpath("//div[contains(@class,
			// 'x-combo-list-inner')]"));
			// DropdownElement.findElement(By.xpath("//*[contains(text(),
			// 'Sales')]")).click();

			// Select DropdownElement = new
			// Select(driver.findElement(By.className("x-form-arrow-trigger")));
			// DropdownElement.selectByVisibleText("Sales");

			// List<WebElement> SelectFirstColumnDropdowns =
			// driver.findElements(By.className("x-form-text"));
			WebElement webElementForMainTab = driver.findElement(By.xpath("//*[@id='filteringStep']"));
			WebElement DropdownChoose = webElementForMainTab.findElement(By.className("x-form-arrow-trigger"));
			List<WebElement> SelectFirstColumnDropdowns = DropdownChoose.findElements(By.className("x-form-text"));
			// for (WebElement SelectFirstColumnDropdown :
			// SelectFirstColumnDropdowns)
			// {
			// if("Sales".equals(SelectFirstColumnDropdown.getText()))
			// SelectFirstColumnDropdown.click();
			// break;
			// }

			for (WebElement SelectFirstColumnDropdown : SelectFirstColumnDropdowns) {
				if (SelectFirstColumnDropdowns.get(0).isDisplayed()) {

					SelectFirstColumnDropdown.click();
					break;
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ColumnNameDropdown1()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 316, 2);
			ExcelWriter(ExecResult, ErrorDesc, 316, 3);

		}

		// }

	}

	public void clickFirstComboItemlist() throws InterruptedException, IOException {
		try {

			Thread.sleep(5000);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();
			JavascriptExecutor js = (JavascriptExecutor) driver;

			// String jQuerySelector =
			// "CCSP.jQuery(\".x-combo-list-item:eq(10)\").click()";

			String jQuerySelector = "CCSP.jQuery(\".x-combo-list-item:eq(1)\").click()";

			String findDropdown = "return " + jQuerySelector;
			System.out.println(findDropdown);

			try {

				Assert.assertNotNull(findDropdown);
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Combo List Dropdown Option Not Found: " + e.getMessage());
			}
			// Thread.sleep(8000);
			// CCSP.jQuery(".x-combo-list-item:eq(18)").click();
			// js.executeScript(findDropdown);
			// WebElement ComboItem = (WebElemnt)
			// js.executeScript(findDropdown);
			js.executeScript(findDropdown);

			// WebElement ComboItem = (WebElement)
			// js.executeScript(findDropdown);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("clickComboItemlist() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 482, 2);
			ExcelWriter(ExecResult, ErrorDesc, 482, 3);

		}

	}

	
	public void clickSecondComboItemlist() throws InterruptedException, IOException {
	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String jQuerySelector = "CCSP.jQuery(\".x-combo-list-item:eq(2)\").click()";

		String findDropdown = "return " + jQuerySelector;
		System.out.println(findDropdown);

		
	}
	
	public void ChooseComboItemlist() throws InterruptedException, IOException {
		try {

			// Thread.sleep(2000);
			driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
			driver.switchTo().defaultContent();

			JavascriptExecutor js = (JavascriptExecutor) driver;

			String jQuerySelector = "$(\".x-combo-list-item:eq(2)\").click()";

			String findDropdownOption = "return " + jQuerySelector;
			// System.out.println(findDropdownOption);
			js.executeScript(findDropdownOption);

			// WebElement
			// DropDownValue=driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[2]"));
			// DropDownValue.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChooseComboItemlist() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 319, 2);
			ExcelWriter(ExecResult, ErrorDesc, 319, 3);

		}
	}

	public void ApplyButtonClick1() throws IOException {
		try {
			WebElement ApplyButtonelement = driver.findElement(By.className("x-btn-text"));
			List<WebElement> ApplyButton = driver.findElements(By.tagName("tr"));
			for (WebElement ApplyButtonelements : ApplyButton) {
				if ("Apply".equals(((WebElement) ApplyButton).getText()))
					((WebElement) ApplyButton).click();
				((WebElement) ApplyButton).click();
				((WebElement) ApplyButton).click();
				break;
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ApplyButtonClick1()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 319, 2);
			ExcelWriter(ExecResult, ErrorDesc, 319, 3);

		}
	}

	public void FilterData() throws IOException {
		try {
			WebElement webElementForMainTab = driver.findElement(By.xpath("//*[@id='filteringStep']"));

			int ForAllInputs = webElementForMainTab.findElements(By.className("x-trigger-noedit")).size();

			// List<IWebElement> elements = new
			// List<OpenQA.Selenium.IWebElement>();
			// Object p;
			// Object webElementsForAllInputsVisible =
			// webElementsForAllInputs.Where(p => p.notify()).ToList();

			for (int i = 0; i < ForAllInputs; i++) {
				if (i == 0) {

				}

				if (i == 1) {

				}
				// if(webElementsForAllImages[i].)
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FilterData() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 320, 2);
			ExcelWriter(ExecResult, ErrorDesc, 320, 3);

		}
	}

	public void Experiment() throws IOException

	{
		try {
			WebElement Topvalue = driver.findElement(By.id("txtTopValue"));
			Topvalue.clear();
			Topvalue.sendKeys("10");

			WebElement webElementForMainTab = driver.findElement(By.xpath("//*[@id='filteringStep']"));
			List<WebElement> webElementsForAllInputs = Arrays.asList();
			webElementsForAllInputs = webElementForMainTab.findElements(By.className("x-trigger-noedit"));

			// List <WebElement> webElementsForAllInputs = new
			// ArrayList<Element>(Arrays.asList(array));
			// List<WebElement> webElementsForAllInputs = Arrays.asList(array);

			// List <WebElement> webElementsForAllInputsVisible = null;

			if (!webElementsForAllInputs.isEmpty()) {
				// List<IWebElement> elements = new
				// List<OpenQA.Selenium.IWebElement>();

				/*
				 * for(int i = 0; i < webElementsForAllInputs.; i++) {
				 * if(webElementsForAllInputs.listIterator(i) != null)
				 *
				 * }
				 */

				for (int i = 0; i < webElementsForAllInputs.size(); i++) {
					if (i == 0) {
						webElementsForAllInputs.get(i).sendKeys(Keys.ARROW_DOWN);
						webElementsForAllInputs.get(i).sendKeys(Keys.ARROW_DOWN);
						webElementsForAllInputs.get(i).sendKeys(Keys.ENTER);
					}

					if (i == 1) {
						webElementsForAllInputs.get(i).sendKeys(Keys.ARROW_DOWN);
						webElementsForAllInputs.get(i).sendKeys(Keys.ARROW_DOWN);
						webElementsForAllInputs.get(i).sendKeys(Keys.ENTER);
					}
					// if(webElementsForAllImages[i].)
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Experiment() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 321, 2);
			ExcelWriter(ExecResult, ErrorDesc, 321, 3);

		}
	}

	// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	// List<WebElement> listImg =
	// driver.findElements(By.xpath("//img[contains(@src,'http://pc-nibir2010/_layouts/CollabionCharts/ext/resources/images/default/form/trigger.gif')]"));

	// listImg.get(-1).click();

	// listImg.get(2).click();

	// driver.findElement(By.xpath("//div[1]/table/tbody/tr/td[2]/div")).click();

	// Select Dropdown = new
	// Select(driver.findElement(By.cssSelector("div.x-combo-list-item")));
	// Dropdown.selectByVisibleText("Company Name");

	// driver.findElement(By.xpath("//input[@src
	// ='/_layouts/CollabionCharts/ext/resources/images/default/s.gif'and
	// @alt='s']")).click();

	//// Start These codes are correct/////

	///////////// driver.findElement(By.xpath("//button[text()='Clear all
	///////////// filter(s)']")).click();

	///////////// driver.findElement(By.xpath("//button[text()='Yes']")).click();

	/////////// driver.manage().timeouts().implicitlyWait(40,
	/////////// TimeUnit.SECONDS);Second Filter Dropdown Selection
	////////// driver.findElement(By.xpath("//button[text()='Finish']")).click();

	///// driver.findElement(By.xpath("//div[3]/div[1]/img")).click();
	///// ......using chart

	//// driver.findElement(By.xpath("//button[text()='Customize']")).click();.....clicking
	//// on customize button

	//// driver.findElement(By.xpath("//button[text()='Customize']")).click();
	///// driver.findElement(By.xpath("//button[text()='OK']")).click();

	//// End These codes are correct/////

	/*
	 * public void Experiment123()
	 *
	 * { // WebElement panel
	 * =driver.findElement(By.cssSelector("div.x-tree-root-node"));
	 * //List<WebElement> listmenu =
	 * driver.findElements(By.xpath("a.x-tree-node-anchor")); //
	 * List<WebElement> listmenu =
	 * driver.findElements(By.cssSelector("img.x-tree-node-icon"));
	 *
	 * /*WebElement panel =driver.findElement(By.tagName("li"));
	 * List<WebElement> listmenu =
	 * panel.findElements(By.className("x-tree-node-el"));
	 *
	 *
	 * for(int i = 0; i < listmenu.size(); i++) {
	 *
	 *
	 * if (listmenu.get(-1).isDisplayed() ) { listmenu.get(1).click();
	 */

	// WebElement panel
	// =driver.findElement(By.cssSelector("a.x-tree-node-anchor[tabindex=1])"));

	// WebElement panel
	// =driver.findElement(By.xpath("\\a[@className='x-tree-node-anchor' and
	// @tabindex='1']"));

	// String test= panel.getAttribute("tabindex");

	// driver.findElement(By.xpath("//a[contains(text(),'Select Fields') and
	// @tabindex='1']")).click();
	// panel.click();
	// listmenuitem.get(1).click();*/
	// }

	public void Experiment123() throws InterruptedException, IOException

	{
		try {
			// WebElement Menuitem =
			// driver.findElement(By.cssSelector("li.x-tree-node-leaf[ext:tree-node-id=’1’]"));

			// WebElement Menuitem=
			// driver.findElement(By.cssSelector("a[tabindex='1']"));
			// Menuitem.click();
			Thread.sleep(2000);
			// List<WebElement> Menuitem =
			// driver.findElements(By.cssSelector("li.x-tree-node-leaf"));

			// List<WebElement> Menuitem =
			// driver.findElements(By.cssSelector("img.x-tree-node-icon"));
			WebElement Menuitem = driver.findElement(By.xpath("//input[contains(@text,'Select Fields')]"));

			// for(int i = 0; i < Menuitem.size(); i++) {

			Menuitem.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Experiment123() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 322, 2);
			ExcelWriter(ExecResult, ErrorDesc, 322, 3);

		}

		// break;
	}

	public void ApplyDynamicFilter() throws InterruptedException, IOException

	{
		try {
			// Thread.sleep(2000);
			driver.findElement(
					By.xpath("//*[@id='ctl00_m_g_54088158_68eb_40b4_b4e1_491a8df40103_ctl00_imgApplyFilter']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ApplyDynamicFilter()fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 323, 2);
			ExcelWriter(ExecResult, ErrorDesc, 323, 3);

		}
	}

	public void preview_filter_first_Dropdown() throws IOException

	{
		try {

			List<WebElement> MainPanel = driver
					.findElements(By.id("filterOptionsg_54088158_68eb_40b4_b4e1_491a8df40103"));

			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(1).isDisplayed()) {

					checkbox2.get(1).click();

					checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
					// checkbox2.get(0).click();
					checkbox2.get(1).sendKeys(Keys.ENTER);
					// checkbox2.get(0).sendKeys(Keys.ENTER);

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("preview_filter_first_Dropdown() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 324, 2);
			ExcelWriter(ExecResult, ErrorDesc, 324, 3);

		}

	}

	public void preview_filter_second_Dropdown() throws IOException {
		try {
			List<WebElement> MainPanel = driver
					.findElements(By.id("filterOptionsg_54088158_68eb_40b4_b4e1_491a8df40103"));

			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(2).isDisplayed()) {

					checkbox2.get(2).click();

					checkbox2.get(2).sendKeys(Keys.ARROW_DOWN);
					checkbox2.get(2).sendKeys(Keys.ARROW_DOWN);
					// checkbox2.get(0).click();
					checkbox2.get(2).sendKeys(Keys.ENTER);
					// checkbox2.get(0).sendKeys(Keys.ENTER);

				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("preview_filter_second_Dropdown() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 325, 2);
			ExcelWriter(ExecResult, ErrorDesc, 325, 3);

		}
	}

	public void search_with_data() throws IOException

	{
		try {
			List<WebElement> MainPanel = driver
					.findElements(By.id("filterOptionsg_54088158_68eb_40b4_b4e1_491a8df40103"));

			for (int i = 0; i < MainPanel.size(); i++) {

				List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

				if (checkbox2.get(3).isDisplayed()) {

					checkbox2.get(3).click();
					checkbox2.get(3).sendKeys("2000");

					// checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);
					// checkbox2.get(3).sendKeys(Keys.ARROW_DOWN);1
					// checkbox2.get(0).click();
					// checkbox2.get(2).sendKeys(Keys.ENTER);
					// checkbox2.get(0).sendKeys(Keys.ENTER);

				}

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("search_with_data() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 326, 2);
			ExcelWriter(ExecResult, ErrorDesc, 326, 3);

		}
	}

	public void Preview_button() throws IOException {
		try {
			driver.findElement(By.xpath("//button[text()='Preview']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Preview_button() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 327, 2);
			ExcelWriter(ExecResult, ErrorDesc, 327, 3);

		}
	}

	public void Drilldown_Chart() throws InterruptedException, IOException {
		try {
			Thread.sleep(4500);
			// driver.navigate().refresh();
			driver.findElement(By.className("FusionCharts")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Drilldown_Chart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 405, 2);
			ExcelWriter(ExecResult, ErrorDesc, 405, 3);

		}
	}

	public void VerifyHomePageEntry() throws IOException {
		try {

			Assert.assertTrue(driver.getPageSource().contains("Dashboard"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("VerifyHomePageEntry() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 328, 2);
			ExcelWriter(ExecResult, ErrorDesc, 328, 5);

		}

	}

	/*
	 * try {
	 *
	 * WebElement fieldset =driver.findElement(By.cssSelector(
	 * "fieldset#dsc-fsconn.x-fieldset.x-fieldset-noborder")); List<WebElement>
	 * tagname =fieldset.findElements(By.tagName("label"));
	 *
	 * for (int i = 0; i < tagname.size(); i++) { WebElement label=
	 * tagname.get(1); Assert.assertTrue(label.getText().matches(
	 * "URL of Excel file")); } } catch (Error e) { System.out.println(
	 * "Element assertion failed"); }
	 *
	 * //Assert.assertEquals("Specified text is not available"
	 * ,DataProvider.getAttribute("Microsoft Office Excel File"));
	 *
	 * //Assert.assertTrue(DataProvider.getText().contains("SharePoint List") );
	 *
	 * //Assert.assertTrue(driver.getPageSource().contains(
	 * "Microsoft Office Excel File"));
	 * //Assert.assertEquals("Test",DataProvider.getText());
	 *
	 * /*try { Assert.assertEquals("Test",DataProvider.getText());
	 *
	 * } catch (AssertionError e) {
	 *
	 * String message = e.getMessage(); System.out.println(message); }
	 */
	public void RevertButton() throws InterruptedException, IOException {
		try {
			Thread.sleep(1000);
			WebElement panel = driver.findElement(By.id("connectionFields"));
			List<WebElement> field = panel.findElements(By.tagName("button"));

			for (int i = 0; i < field.size(); i++) {
				field.get(2).click();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("RevertButton() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 329, 2);
			ExcelWriter(ExecResult, ErrorDesc, 329, 3);

		}
	}
	/*
	 * //////////HTML attachment sending////////// public void HTMLAttachment()
	 * throws InterruptedException {
	 *
	 * String to="jayati.sen71@gmail.com";//change accordingly final String
	 * user="kumarijayati@gmail.com";//change accordingly final String
	 * password="P@ssw0rd123!!";//change accordingly
	 *
	 * //1) get the session object Properties props = new Properties();
	 * props.put("mail.smtp.host", "smtp.gmail.com");
	 * props.put("mail.smtp.socketFactory.port", "465");
	 * props.put("mail.smtp.socketFactory.class",
	 * "javax.net.ssl.SSLSocketFactory"); props.put("mail.smtp.auth", "true");
	 * props.put("mail.smtp.port", "465");
	 *
	 * Session session = Session.getDefaultInstance(props, new
	 * javax.mail.Authenticator() { protected javax.mail.PasswordAuthentication
	 * getPasswordAuthentication() { return new
	 * javax.mail.PasswordAuthentication(user,password); } });
	 *
	 * //2) compose message try{ MimeMessage message = new MimeMessage(session);
	 * message.setFrom(new InternetAddress(user));
	 * message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
	 * message.setSubject("Message Alert");
	 *
	 * //3) create MimeBodyPart object and set your message text BodyPart
	 * messageBodyPart1 = new MimeBodyPart(); messageBodyPart1.setText(
	 * "This is message body");
	 *
	 * //4) create new MimeBodyPart object and set DataHandler object to this
	 * object MimeBodyPart messageBodyPart2 = new MimeBodyPart();
	 *
	 * String filename = "super.html";//change accordingly DataSource source =
	 * new FileDataSource(filename); messageBodyPart2.setDataHandler(new
	 * DataHandler(source)); messageBodyPart2.setFileName(filename);
	 *
	 *
	 *
	 * //5) create Multipart object and add MimeBodyPart objects to this object
	 * Multipart multipart = new MimeMultipart();
	 * multipart.addBodyPart(messageBodyPart1);
	 * multipart.addBodyPart(messageBodyPart2);
	 *
	 * //6) set the multiplart object to the message object
	 * message.setContent(multipart );
	 *
	 * //7) send message Transport.send(message);
	 *
	 * System.out.println("message sent...."); }catch (MessagingException ex)
	 * {ex.printStackTrace();} }
	 *
	 * ///////////////////////Webpart
	 * TextFilter////////////////////////////////// public void TextFilter()
	 * throws InterruptedException { WebElement TextFilter
	 * =driver.findElement(By.name(
	 * "ctl00$m$g_34465bf7_9b1f_4689_8ce2_fe3c48f23dfa$SPTextSlicerValueTextControl"
	 * )); TextFilter.sendKeys("China"); Thread.sleep(3000);
	 * TextFilter.sendKeys(Keys.ENTER); } ////////////////Expected
	 * Screenshot///////// public void getscreenshot() throws Exception { File
	 * scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	 * //The below method will save the screen shot in d drive with name
	 * "screenshot.png" FileUtils.copyFile(scrFile, new
	 * File("D:\\screenshot.png")); } public void compareImages (String exp,
	 * String cur, String diff) { // This instance wraps the compare command
	 *
	 * String exp1 = "D:\\screenshot.png"; String cur1 =
	 * "C:\\Collabion\\CollabionChartsAutomationTestingProject\\ExpectedImage.png";
	 *
	 * CompareCmd compare = new CompareCmd();
	 *
	 * // For metric-output compare.setErrorConsumer(StandardStream.STDERR);
	 * IMOperation cmpOp = new IMOperation(); // Set the compare metric
	 * cmpOp.metric("mae");
	 *
	 * // Add the expected image cmpOp.addImage(exp1);
	 *
	 * // Add the current image cmpOp.addImage(cur1);
	 *
	 * // This stores the difference cmpOp.addImage(diff);
	 *
	 * /* try { // Do the compare compare.run(cmpOp); return true; } catch
	 * (Exception ex) { return false; }
	 */
	// }
	///////////////////////// MultiSeries column 3D-Legend/////////////////////

	public void MultiSeries2DChartChoose() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSColumn2D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Multi Series Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeries2DChartChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 330, 2);
			ExcelWriter(ExecResult, ErrorDesc, 330, 3);

		}

	}

	public void SharePoint2016MultiSeries2DChartChoose() throws IOException {
   try{
		WebElement Chart = driver.findElement(By.xpath(
				"//img[contains(@src,'http://ccsptestsvr//_layouts/15/CollabionCharts/resources/images/charts/MSColumn2D.jpg')]"));

		try {

			Assert.assertTrue(Chart.isDisplayed());
			Assert.assertTrue(driver.getPageSource().contains("Multi Series Column 2D"));

		} catch (AssertionError e) {

			// String message = e.getMessage();
			System.out.println("Multi Series Column2D Chart Not Found: " + e.getMessage());
		}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SharePoint2016MultiSeries2DChartChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 404, 2);
			ExcelWriter(ExecResult, ErrorDesc, 404, 3);

		}
	}

	public void MultiSeries3DChartChoose() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSColumn3D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Multi Series Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeries3DChartChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 331, 2);
			ExcelWriter(ExecResult, ErrorDesc, 331, 3);

		}

	}

	public void SharePoint2016MultiSeries3DChartChoose() throws IOException {

		WebElement Chart = driver.findElement(By.xpath(
				"//img[contains(@src,'http://ccsptestsvr//_layouts/15/CollabionCharts/resources/images/charts/MSColumn3D.jpg')]"));
		Chart.click();

	}

	public void MultiSeriesCategoryChoose() throws IOException {
		try {
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesCategoryChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 403, 2);
			ExcelWriter(ExecResult, ErrorDesc, 403, 3);

		}
	}

	public void MultiSeriesLegendClick() throws InterruptedException, IOException {
		try {

			Thread.sleep(1000);
			driver.findElement(By.xpath("//div/li[3]/ul/li[8]/div/a/span")).click();
			Assert.assertTrue(driver.getPageSource().contains("Legend"));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendClick() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 333, 2);
			ExcelWriter(ExecResult, ErrorDesc, 333, 3);

		}

	}

	public void MultiSeriesCaption() throws InterruptedException, IOException {
		try {
			Thread.sleep(1300);
			WebElement ButtonPanel = driver.findElement(By.id("legendStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Button.get(1));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart Caption Not Found: " + e.getMessage());
			}
			Button.get(1).click();
			Button.get(1).sendKeys("Region");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesCaption() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 334, 2);
			ExcelWriter(ExecResult, ErrorDesc, 334, 3);

		}

	}

	public void MultiSeriesPosition() throws InterruptedException, IOException {
		try {
			Thread.sleep(1600);
			WebElement ButtonPanel = driver.findElement(By.id("legendStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Button.get(2));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart Position Not Found: " + e.getMessage());
			}
			Button.get(2).click();
			Button.get(2).sendKeys(Keys.ARROW_DOWN);
			Button.get(2).sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesPosition() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 335, 2);
			ExcelWriter(ExecResult, ErrorDesc, 335, 3);

		}
	}

	public void MultiSeriesLegendSize() throws InterruptedException, IOException {
		try {
			Thread.sleep(2000);
			WebElement ButtonPanel = driver.findElement(By.id("legendStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Button.get(4));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Size Not Found: " + e.getMessage());
			}

			Button.get(4).click();
			Button.get(4).sendKeys(Keys.ARROW_DOWN);
			Button.get(4).sendKeys(Keys.ENTER);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendSize() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 336, 2);
			ExcelWriter(ExecResult, ErrorDesc, 336, 3);

		}

	}

	public void MultiSeriesLegendRows() throws InterruptedException, IOException {
		try {
			Thread.sleep(2200);
			WebElement ButtonPanel = driver.findElement(By.id("legendStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Button.get(5));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Rows Not Found: " + e.getMessage());
			}
			Button.get(5).click();
			Button.get(5).sendKeys("4");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendRows() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 337, 2);
			ExcelWriter(ExecResult, ErrorDesc, 337, 3);

		}

	}

	public void MultiSeriesLegendReverse() throws InterruptedException, IOException {
		try {
			Thread.sleep(2500);
			WebElement ButtonPanel = driver.findElement(By.id("legendStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));
			try {

				Assert.assertNotNull(Button.get(6));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Reverse Not Found: " + e.getMessage());
			}
			Button.get(6).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendReverse() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 338, 2);
			ExcelWriter(ExecResult, ErrorDesc, 338, 3);

		}

	}

	public void MultiSeriesLegendDrag() throws InterruptedException, IOException {
		try {
			Thread.sleep(2700);
			WebElement ButtonPanel = driver.findElement(By.id("legendStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Button.get(7));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Drag Not Found: " + e.getMessage());
			}
			Button.get(7).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendDrag() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 339, 2);
			ExcelWriter(ExecResult, ErrorDesc, 339, 3);

		}

	}

	public void MultiSeriesLegendBackgroungOpaqness() throws InterruptedException, IOException {
		try {
			Thread.sleep(2900);

			WebElement Panel = driver.findElement(By.id("legendStep"));
			List<WebElement> Opaqness = Panel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Opaqness.get(8));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out
						.println("Multi Series Column2D Chart legend Backgroung Opaqness Not Found: " + e.getMessage());
			}
			Opaqness.get(8).click();
			Opaqness.get(8).clear();
			Opaqness.get(8).sendKeys("99");

			/*
			 * WebElement Panel = driver.findElement(By.id("legendStep"));
			 * List<WebElement> paneldiv =
			 * Panel.findElements(By.tagName("fieldset")); for(int i = 0; i <
			 * paneldiv.size(); i++) { List<WebElement> BackgroungOpaqness=
			 * paneldiv.get(i).findElements(By.tagName("input"));
			 * BackgroungOpaqness.get(0).click();
			 * BackgroungOpaqness.get(0).clear();
			 * BackgroungOpaqness.get(0).sendKeys("99");
			 *
			 * }
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendBackgroungOpaqness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 339, 2);
			ExcelWriter(ExecResult, ErrorDesc, 339, 3);

		}

	}

	public void MultiSeriesLegendBackgroungShadow() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);

			WebElement Panel = driver.findElement(By.id("legendStep"));
			List<WebElement> Opaqness = Panel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Opaqness.get(9));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Backgroung Shadow Not Found: " + e.getMessage());
			}
			Opaqness.get(9).click();
			// Opaqness.get(8).clear();
			// Opaqness.get(8).sendKeys("99");

			/*
			 * WebElement Panel = driver.findElement(By.id("legendStep"));
			 * List<WebElement> fieldset =
			 * Panel.findElements(By.tagName("fieldset")); List<WebElement>
			 * input = fieldset.get(0).findElements(By.tagName("input"));
			 *
			 * input.get(1).click(); }
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendBackgroungShadow() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 341, 2);
			ExcelWriter(ExecResult, ErrorDesc, 341, 3);

		}

	}

	public void MultiSeriesLegendBorderOpaqness() throws InterruptedException, IOException {
		try {
			Thread.sleep(3200);

			WebElement Panel = driver.findElement(By.id("legendStep"));
			List<WebElement> Opaqness = Panel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Opaqness.get(10));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Border Opaqness Not Found: " + e.getMessage());
			}
			Opaqness.get(10).click();
			Opaqness.get(10).clear();
			Opaqness.get(10).sendKeys("99");

			/*
			 * WebElement Panel = driver.findElement(By.id("legendStep"));
			 * List<WebElement> fieldset =
			 * Panel.findElements(By.tagName("fieldset")); List<WebElement>
			 * input = fieldset.get(1).findElements(By.tagName("input"));
			 *
			 * input.get(0).click(); input.get(0).sendKeys("99");
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendBorderOpaqness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 342, 2);
			ExcelWriter(ExecResult, ErrorDesc, 342, 3);

		}

	}

	public void MultiSeriesLegendBorderThickness() throws InterruptedException, IOException {
		try {
			Thread.sleep(3400);

			WebElement Panel = driver.findElement(By.id("legendStep"));
			List<WebElement> Opaqness = Panel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Opaqness.get(11));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Border Thickness Not Found: " + e.getMessage());
			}
			Opaqness.get(11).click();
			Opaqness.get(11).clear();
			Opaqness.get(11).sendKeys("2");

			/*
			 * WebElement Panel = driver.findElement(By.id("legendStep"));
			 * List<WebElement> fieldset =
			 * Panel.findElements(By.tagName("fieldset")); List<WebElement>
			 * input = fieldset.get(0).findElements(By.tagName("input"));
			 *
			 * input.get(0).click(); input.get(0).clear();
			 * input.get(0).sendKeys("2");
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendBorderThickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 343, 2);
			ExcelWriter(ExecResult, ErrorDesc, 343, 3);

		}

	}

	public void MultiSeriesLegendApply() throws InterruptedException, IOException {
		try {
			Thread.sleep(3600);
			WebElement ButtonPanel = driver.findElement(By.id("legendStep"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("button"));
			try {

				Assert.assertNotNull(Button.get(3));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column2D Chart legend Apply Not Found: " + e.getMessage());
			}
			Button.get(3).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLegendApply() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 344, 2);
			ExcelWriter(ExecResult, ErrorDesc, 344, 3);

		}

	}

	///////////////////////// MultiSeries column
	///////////////////////// 3D-Cosmetics/////////////////////

	public void MultiSeries3DShowCanvas() throws InterruptedException, IOException {
		try {
			Thread.sleep(1300);
			WebElement ButtonPanel = driver.findElement(By.id("ccfs3DCanvas"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));
			try {

				Assert.assertNotNull(Button.get(0));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Show Canvas Not Found: " + e.getMessage());
			}
			Button.get(0).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeries3DShowCanvas() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 345, 2);
			ExcelWriter(ExecResult, ErrorDesc, 345, 3);

		}

	}

	public void MultiSeries3DShowBase() throws InterruptedException, IOException {
		try {
			Thread.sleep(1300);
			WebElement ButtonPanel = driver.findElement(By.id("ccfs3DCanvas"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));

			try {

				Assert.assertNotNull(Button.get(1));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Show Base Not Found: " + e.getMessage());
			}
			Button.get(1).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeries3DShowBase() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 346, 2);
			ExcelWriter(ExecResult, ErrorDesc, 346, 3);

		}

	}

	public void MultiSeries3DShowCanvasThickness() throws InterruptedException, IOException {
		try {
			Thread.sleep(1300);
			WebElement ButtonPanel = driver.findElement(By.id("ccfs3DCanvas"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));
			try {

				Assert.assertNotNull(Button.get(2));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Show Canvas Thickness Not Found: " + e.getMessage());
			}
			Button.get(2).click();
			Button.get(2).clear();
			Button.get(2).sendKeys("2");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeries3DShowCanvasThickness() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 347, 2);
			ExcelWriter(ExecResult, ErrorDesc, 347, 3);

		}

	}

	public void MultiSeries3DShowCanvasDepth() throws InterruptedException, IOException {
		try {
			Thread.sleep(1300);
			WebElement ButtonPanel = driver.findElement(By.id("ccfs3DCanvas"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));
			try {

				Assert.assertNotNull(Button.get(3));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Show Canvas Depth Not Found: " + e.getMessage());
			}
			Button.get(3).click();
			Button.get(3).clear();
			Button.get(3).sendKeys("5");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeries3DShowCanvasDepth() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 348, 2);
			ExcelWriter(ExecResult, ErrorDesc, 348, 3);

		}

	}

	public void MultiSeries3DZeroPlaneChartBorder() throws InterruptedException, IOException {
		try {
			Thread.sleep(1300);
			WebElement ButtonPanel = driver.findElement(By.id("ccZeroPlaneTab3DChart"));
			List<WebElement> Button = ButtonPanel.findElements(By.tagName("input"));
			try {

				Assert.assertNotNull(Button.get(0));
				// Assert.assertTrue(driver.getPageSource().contains("Multi
				// Series
				// Column 2D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Zero Plane Chart Border Not Found: " + e.getMessage());
			}
			Button.get(0).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(" MultiSeries3DZeroPlaneChartBorder()  fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 349, 2);
			ExcelWriter(ExecResult, ErrorDesc, 349, 3);

		}

	}

	/////////// close wizard///////////

	public void CloseWizard() throws InterruptedException, IOException {
		try {

			WebElement CloseWizard = driver.findElement(By.className("x-tool x-tool-close"));
			CloseWizard.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CloseWizard() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 351, 2);
			ExcelWriter(ExecResult, ErrorDesc, 351, 3);

		}

	}
	///////////////// Excel Report///////////

	/*
	 * static Workbook wbook; static WritableWorkbook workbook1,workbook2;
	 * static String ExecutedTestCasesSheet; static WritableSheet shSheet;
	 * static String TEST1="PASS";
	 *
	 * public static WebDriver driver = new FirefoxDriver();
	 *
	 * public void ExcelSheet(){
	 *
	 *
	 *
	 * try{ File ExcelFile=new File("D:\\sampletestfile.xls"); workbook1 =
	 * Workbook.createWorkbook(ExcelFile);
	 *
	 *
	 * shSheet = workbook1.createSheet("sheet1", 0);
	 *
	 * } catch(Exception e) { e.printStackTrace(); }
	 *
	 *
	 *
	 * try{
	 *
	 * // String baseUrl = "http://newtours.demoaut.com"; String expectedTitle =
	 * "Collabion Charts for SharePoint - v 2.1.0.6"; String actualTitle = "";
	 *
	 * // launch Firefox and direct it to the Base URL // driver.get(baseUrl);
	 *
	 * // get the actual value of the title actualTitle = driver.getTitle();
	 *
	 * ///////////console to Excel//////////// // int i = 0;
	 *
	 * PrintStream printStream = new PrintStream(new FileOutputStream(new
	 * File("D:\\sampletestfile.xls")));
	 *
	 * System.setOut(printStream);
	 *
	 *
	 *
	 *
	 * // while(i<1000){
	 *
	 * // System.out.print(i+++","+i+++","+i++);
	 *
	 * System.out.println();
	 *
	 * // } /* compare the actual title of the page witht the expected one and
	 * print the result as "Passed" or "Failed"
	 */
	/*
	 * if (actualTitle.contentEquals(expectedTitle)){ System.out.println(
	 * "Test Passed!"); } else { System.out.println("Test Failed"); } }
	 * catch(Exception e) { e.printStackTrace(); TEST1="FAIL"; }
	 *
	 *
	 *
	 *
	 * // @Test //public void test002() {
	 *
	 *
	 * // }
	 *
	 * WritableSheet wshTemp = workbook1.getSheet(1); Label labTemp = new
	 * Label(1,1,TEST1); //WritableCell cell =shSheet.getWritableCell(1, 2); try
	 * { wshTemp.addCell(labTemp); } catch (Exception e) { e.printStackTrace();
	 * }
	 *
	 *
	 */

	/*
	 * public void ReportGeneration() throws Exception { CucumberResultsOverview
	 * results = new CucumberResultsOverview();
	 * results.setOutputDirectory("target1/");
	 * results.setOutputName("cucumber"); results.setSourceFile(
	 * "C:/Collabion/CollabionChartsAutomationTestingProject/target.json");
	 * results.executeFeaturesOverviewReport();
	 *
	 * }
	 */

	public static final void prepareToPressEnterKey(int seconds, int tabs, WebDriver driverr) {

		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
		ScheduledFuture scheduledFuture = scheduledExecutorService.schedule(new Runnable() {

			public void run() {

				try {
					Robot robot = new Robot();

					System.out.println("Waiting for popup");

					Set<String> MainWindow = driver.getWindowHandles();
					System.out.println(MainWindow);
					String[] MainWinArr = MainWindow.toArray(new String[MainWindow.size()]);

					driver.switchTo().window(MainWinArr[1]);

					// Thread.sleep(1200);
					driver.findElement(By.xpath(".//*[@id='2']")).click();

					driver.findElement(By.xpath(".//*[@id='ctl00_OkButton']")).click();

				} catch (Exception e) {
					e.printStackTrace();
					// System.out.println("Prepare to Press Enter Exception");
				}
				try {
					Assert.assertTrue(DriverFactoryPage.driver
							.findElement(By
									.xpath(".//div[@id='chartPanelContainer']/div[2]/div[1]/span/*[name()='svg']/*[name()='g'][3]/*[name()='g'][7]//*[name()='tspan']"))
							.getText().contentEquals("China"));
				} catch (Exception e) {
					System.out.println(e.getStackTrace());
				}

			}
		}, seconds, TimeUnit.SECONDS);
		scheduledExecutorService.shutdown();
	}

	///////////////////////////////////////// Combination of Dynamic & Choice
	///////////////////////////////////////// Filter
	///////////////////////////////////////// /////////////////////////////

	public void ChoiceFilterSelection() throws InterruptedException, IOException {
		try {
			Thread.sleep(3000);
			WebElement ChoiceButton = driver
					.findElement(By.xpath(".//*[contains(@id,'WebPartWPQ')]//img[@alt='Browse']"));

			System.out.println(driver.getWindowHandle());

			prepareToPressEnterKey(5, 4, driver);

			ChoiceButton.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(
					"prepareToPressEnterKey(int seconds, int tabs, WebDriver driverr) fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc); ExcelWriter(ExecResult, ErrorDesc,
			// 1, 5);
			ExcelWriter(ExecResult, ErrorDesc, 2, 5);

		}

	}

	///////////////////////////////////////// Combination of Dynamic & Choice
	///////////////////////////////////////// Filter/////////////////////////////////////////
	///////////////////////////////////////// /////////////////////////////

	// ************************************************This part is for
	// MultiLevelDrillDownUI****************************************************************************************************************************************************************************************************************************

	public void OpenDrillDown() throws InterruptedException, IOException {
		Thread.sleep(2000);
		try {
			// driver.findElement(By.xpath("//a[text()='CollabionChartsAutomationTesting']")).click();
			driver.findElement(By.xpath("//a[text()='CCSPAutomationTestingDontTouch']")).click();

			// public void FirstLevelDrillDown()throws InterruptedException
			// WebElement FirstLevelOfDrill = (WebElement) (new
			// WebDriverWait(driver, 5))
			// .until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@id='chartPanelContainer']//span//*[local-name()
			// = 'svg']/*[name()='g'][8]/*[name()='rect'][3]")));
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("OpenDrillDown() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc); ExcelWriter(ExecResult, ErrorDesc,
			// 355, 2);
			ExcelWriter(ExecResult, ErrorDesc, 355, 3);

		}
	}

	public void OpenSitePages() throws InterruptedException, IOException {
		try {
			WebElement SpanText = driver.findElement(By.xpath("//span[text()='Site Pages']"));
			SpanText.click();
		} catch (Exception e) {

			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("OpenSitePages() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc); ExcelWriter(ExecResult, ErrorDesc,
			// 356, 2);
			ExcelWriter(ExecResult, ErrorDesc, 355, 2);
			ExcelWriter(ExecResult, ErrorDesc, 355, 3);

		}
	}

	public void barclicker(String Xpath, int BarIndex, String ClassIdentifier) throws Exception {
		/* Xpath Generalization */
		try {
			List<WebElement> First = (List<WebElement>) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(Xpath)));
			int G_counter = 0;
			for (WebElement E : First) {
				if (E.getAttribute("class").contains(ClassIdentifier)) {
					List<WebElement> FirstRectContainer = E.findElements(By.xpath("node()[name()='rect']"));
					for (WebElement F : FirstRectContainer) {
						if (F != null && G_counter == BarIndex) {

							System.out.println("yoohoo" + F.getTagName() + G_counter);
							F.click();

						}
						G_counter++;

					}
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println(
					"barclicker(String Xpath, int BarIndex, String ClassIdentifier) fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc); ExcelWriter(ExecResult, ErrorDesc,
			// 357, 2);
			ExcelWriter(ExecResult, ErrorDesc, 357, 3);

		}
	}

	public void ExcelFirstLevelDrillDown() throws Exception {

	}

	public void ExcelSecondLevelDrillDown() throws Exception {

	}

	public void Excel_FirstLevelDrillDown() throws Exception {
		try {
			Thread.sleep(5000);

			datavalidation("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SalesDataSheet.xlsx",
					"EXCEL_FIRST_DRILL", 0);
			barclicker(".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g']", 2, "red-hot");
			Thread.sleep(10000);
			datavalidation("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SalesDataSheet.xlsx",
					"EXCEL_SECOND_DRILL", 1);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Excel_FirstLevelDrillDown() fail in catch" + ErrorDesc);
		} finally {
			// System.out.print(ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 357, 2);
			ExcelWriter(ExecResult, ErrorDesc, 357, 3);

		}
	}

	public void SharepointList_FirstLevelDrillDown() throws Exception {
		try {
			Thread.sleep(5000);
			datavalidation("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SalesDataSheet.xlsx",
					"SharePointList_FirstLevelDrill", 0);
			barclicker(".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g']", 2, "red-hot");
			Thread.sleep(10000);
			datavalidation("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SalesDataSheet.xlsx",
					"SharePointList_SecondLevelDrill", 1);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SharepointList_FirstLevelDrillDown() failed in catch:" + ErrorDesc);

		} finally {
			// System.out.print("TextDataDrivenTesting(String InputText) fail in
			// finally:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 393, 2);
			ExcelWriter(ExecResult, ErrorDesc, 393, 3);
			// System.out.print(" TextDataDrivenTesting(String InputText)
			// finally passed ");

		}
	}

	public void SharepointList_SecondLevelDrillDown() throws Exception {
		try {
			barclicker(".//span[@id='chartobject-1']/*[local-name() = 'svg']/*[name()='g']", 2, "red-hot");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SharepointList_SecondLevelDrillDown() failed in catch:" + ErrorDesc);

		} finally {
			// System.out.print("TextDataDrivenTesting(String InputText) fail in
			// finally:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 394, 2);
			ExcelWriter(ExecResult, ErrorDesc, 394, 3);
			// System.out.print(" TextDataDrivenTesting(String InputText)
			// finally passed ");

		}
	}

	public void SQLServerFirstLevelDrillDown() throws Exception {
		try {
			Thread.sleep(5000);
			datavalidation("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SalesDataSheet.xlsx",
					"SQLServer_FirstLevelDrill", 0);
			barclicker(".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g']", 30, "red-hot");
			
			Thread.sleep(10000);
			
			datavalidation("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SalesDataSheet.xlsx",
					"SQLServer_SecondLevelDrill", 1);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SQLServerFirstLevelDrillDown() failed in catch:" + ErrorDesc);

		} finally {
			// System.out.print("TextDataDrivenTesting(String InputText) fail in
			// finally:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 361, 2);
			ExcelWriter(ExecResult, ErrorDesc, 361, 3);
			// System.out.print(" TextDataDrivenTesting(String InputText)
			// finally passed ");

		}
	}

	public void SQLServerSecondLevelDrillDown() throws Exception {
		try {
			barclicker(".//span[@id='chartobject-1']/*[local-name() = 'svg']/*[name()='g']", 1, "red-hot");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SQLServerSecondLevelDrillDown() failed in catch:" + ErrorDesc);

		} finally {
			// System.out.print("TextDataDrivenTesting(String InputText) fail in
			// finally:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 362, 2);
			ExcelWriter(ExecResult, ErrorDesc, 362, 3);
			// System.out.print(" TextDataDrivenTesting(String InputText)
			// finally passed ");

		}
	}

	/*
	 * // HtmlUnitDriver htmlUnitDriver = new HtmlUnitDriver(true); //
	 * WebElement FirstLevelOfDrill = (WebElement) (new WebDriverWait(driver,
	 * 5)) // .until(ExpectedConditions.presenceOfElementLocated(By.xpath( //
	 * ".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][7]/*[name()='rect'][3]"
	 * ))); // ( (HtmlUnitDriver)htmlUnitDriver ).isJavascriptEnabled(); ​ // //
	 * String DrilldownChart = (String) //
	 * ((JavascriptExecutor)htmlUnitDriver).executeScript("var arr= []; //
	 * for(var obj in FusionCharts.items) { //
	 * if(obj.toString().startsWith('chartobject')) //
	 * arr.push(parseInt(obj.toString().substring(12)));} //
	 * FusionCharts.items['chartobject-'+Math.max.apply(null, //
	 * arr)].getXML()"); ​ // System.out.println(DrilldownChart);
	 */
	/*
	 * public void FirstLevelDrillDown()throws InterruptedException { try{ //
	 * HtmlUnitDriver htmlUnitDriver = new HtmlUnitDriver(true); WebElement
	 * FirstLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
	 * .until(ExpectedConditions.presenceOfElementLocated(By.xpath(
	 * ".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][3]"
	 * )));
	 *
	 *
	 * if(FirstLevelOfDrill.isDisplayed()) { System.out.println(
	 * "I am in first level drill click"); FirstLevelOfDrill.click();
	 *
	 * }
	 *
	 * } catch (Exception e) {
	 *
	 * System.out.println("First Level Drilldown not working: " +
	 * e.getMessage()); } }
	 */
	//////////////////////////// Important-Dont Delete this block of
	//////////////////////////// code///////////////////////////
	// ( (HtmlUnitDriver)htmlUnitDriver ).isJavascriptEnabled();

	// String DrilldownChart = (String)
	// ((JavascriptExecutor)htmlUnitDriver).executeScript("var arr= []; for(var
	// obj in FusionCharts.items) { if(obj.toString().startsWith('chartobject'))
	// arr.push(parseInt(obj.toString().substring(12)));}
	// FusionCharts.items['chartobject-'+Math.max.apply(null, arr)].getXML()");

	// System.out.println(DrilldownChart);

	/* end of Generalization */

	/*
	 * public void FirstLevelDrillDown() throws Exception {
	 *
	 * { Thread.sleep(5000);
	 * datavalidation("C://Users//sayan.sikdar//Desktop//SalesDataSheet.xlsx",
	 * "Sheet3", 0); barclicker(
	 * ".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g']"
	 * ,2,"red-hot"); Thread.sleep(10000);
	 * datavalidation("C://Users//sayan.sikdar//Desktop//SalesDataSheet.xlsx",
	 * "Sheet4", 1); // ( (HtmlUnitDriver)htmlUnitDriver
	 * ).isJavascriptEnabled();
	 *
	 * // String DrilldownChart = (String) //
	 * ((JavascriptExecutor)htmlUnitDriver).executeScript("var arr= []; //
	 * for(var obj in FusionCharts.items) { //
	 * if(obj.toString().startsWith('chartobject')) //
	 * arr.push(parseInt(obj.toString().substring(12)));} //
	 * FusionCharts.items['chartobject-'+Math.max.apply(null, //
	 * arr)].getXML()");
	 *
	 * // System.out.println(DrilldownChart);
	 *
	 *
	 *
	 * {
	 *
	 * // WebElement ParentSpan=(WebElement) (new WebDriverWait(driver, 20)) //
	 * .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
	 * ".//div[@id='chartPanelContainer']/div[2]/div/span")));
	 *
	 * // System.out.println(ParentSpan.getAttribute("style")); //
	 * System.out.println(ParentSpan.getAttribute("id")); barclicker(
	 * ".//span[@id='chartobject-1']/*[local-name() = 'svg']/*[name()='g']"
	 * ,2,"red-hot"); // WebElement SecondLevelOfDrill = (WebElement) (new
	 * WebDriverWait(driver, 10)) //
	 * .until(ExpectedConditions.presenceOfElementLocated(By.xpath( //
	 * ".//span[@id='chartobject-1']/*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][3]"
	 * ))); // // while (!SecondLevelOfDrill.isDisplayed()) { // } // // //
	 * FirstLevelOfDrill.click(); // System.out.println(
	 * "I am in Second level drill click"); // if
	 * (SecondLevelOfDrill.isDisplayed()) { // // Actions builder = new
	 * Actions(driver); // //
	 * builder.click(SecondLevelOfDrill).build().perform(); // //
	 * System.out.println(SecondLevelOfDrill); // SecondLevelOfDrill.click(); //
	 * // }
	 *
	 * while (!SecondLevelOfDrill.isDisplayed()) { }
	 *
	 * // FirstLevelOfDrill.click(); System.out.println(
	 * "I am in Second level drill click"); if
	 * (SecondLevelOfDrill.isDisplayed()) { // Actions builder = new
	 * Actions(driver); // builder.click(SecondLevelOfDrill).build().perform();
	 * // System.out.println(SecondLevelOfDrill); SecondLevelOfDrill.click();
	 *
	 * }
	 *
	 * }
	 */

	public void SharepointlistDrilldown() throws InterruptedException, IOException {
		try {
			WebElement FirstLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][2]")));
			if (FirstLevelOfDrill.isDisplayed()) {
				System.out.println("I am in first level drill click");
				FirstLevelOfDrill.click();
			}

			Thread.sleep(2000);

			WebElement SecondLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][1]")));
			if (SecondLevelOfDrill.isDisplayed()) {
				System.out.println("I am in Second level drill click");
				SecondLevelOfDrill.click();

			}

			Thread.sleep(2000);

			WebElement ThirdLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][1]")));
			if (ThirdLevelOfDrill.isDisplayed()) {
				System.out.println("I am in Third level drill click");
				ThirdLevelOfDrill.click();

			}

			Thread.sleep(2000);

			WebElement ForthLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][8]/*[name()='rect'][1]")));
			if (ForthLevelOfDrill.isDisplayed()) {
				System.out.println("I am in Forth level drill click");
				ForthLevelOfDrill.click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SharepointlistDrilldown() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("SQLServerDatabaseDrilldown() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 363, 2);
			ExcelWriter(ExecResult, ErrorDesc, 363, 3);
			// System.out.print(" SQLServerDatabaseDrilldown() finally passed
			// ");

		}
	}

	public void SQLServerDatabaseDrilldown() throws InterruptedException, IOException {
		try {
			WebElement FirstLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][7]/*[name()='rect'][3]")));
			if (FirstLevelOfDrill.isDisplayed()) {
				System.out.println("I am in first level drill click");
				FirstLevelOfDrill.click();
			}

			Thread.sleep(3000);
			WebElement SecondLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][7]/*[name()='rect'][1]")));
			if (SecondLevelOfDrill.isDisplayed()) {
				System.out.println("I am in Second level drill click");
				SecondLevelOfDrill.click();
			}

			Thread.sleep(3000);
			WebElement ThirdLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][7]/*[name()='rect'][1]")));
			if (ThirdLevelOfDrill.isDisplayed()) {
				System.out.println("I am in Third level drill click");
				ThirdLevelOfDrill.click();
			}

			Thread.sleep(3000);
			WebElement ForthLevelOfDrill = (WebElement) (new WebDriverWait(driver, 5))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][7]/*[name()='rect'][1]")));
			if (ForthLevelOfDrill.isDisplayed()) {
				System.out.println("I am in Forth level drill click");
				ThirdLevelOfDrill.click();
			}

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SQLServerDatabaseDrilldown() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("SQLServerDatabaseDrilldown() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 364, 2);
			ExcelWriter(ExecResult, ErrorDesc, 364, 3);
			// System.out.print(" SQLServerDatabaseDrilldown() finally passed
			// ");

		}
		/*
		 * Thread.sleep(3000); WebElement FifthLevelOfDrill = (WebElement) (new
		 * WebDriverWait(driver, 5))
		 * .until(ExpectedConditions.presenceOfElementLocated(By.xpath(
		 * ".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g'][7]/*[name()='rect'][1]"
		 * ))); if(FifthLevelOfDrill.isDisplayed()) { System.out.println(
		 * "I am in Fifth level drill click"); ThirdLevelOfDrill.click(); }
		 */
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////// End
	///////////////////////////////////////////////////////////////////////////////////////////////////// Conflict////////////////////////////////

	// public void ThirdLevelDrillDown() throws InterruptedException {

	// ************************************************This part is for Text and
	// Dynamic Filter
	// Combination****************************************************************************************************************************************************************************************************************************
	public void TextFilterValue() throws InterruptedException, IOException {
		try {
			Thread.sleep(5635);
			WebElement Element = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(
							By.xpath("//div[contains(@id,'WebPartWPQ')]/div/div/input[@type='text']")));
			System.out.println(Element);

			Element.click();
			Element.sendKeys("China");
			Element.submit();

			// Assert.assertTrue(driver
			// .findElement(By
			// .xpath(".//div[@id='chartPanelContainer']/div[2]/div[1]/span/*[name()='svg']/*[name()='g'][3]/*[name()='g'][7]//*[name()='tspan']"))
			// .getText().contentEquals("China"));

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TextFilterValue() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("TextFilterValue() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 365, 2);
			ExcelWriter(ExecResult, ErrorDesc, 365, 3);
			// System.out.print(" TextFilterValue() finally passed ");

		}

		// Actions action1=new Actions(driver);
		// WebElement
		// EditIco=driver.findElement(By.xpath(".//img[@src='/_layouts/images/wpmenuarrow.png']"));
		// action1.moveToElement(EditIco).click();

	}

	public void DynamicFilterValue() throws InterruptedException {

		try {
			int count = 0;
			// WebElement Element = (WebElement) (new WebDriverWait(driver, 10))
			// .until(ExpectedConditions.presenceOfElementLocated(By
			// .xpath(".//*[@id='ctl00_m_g_65cfa9f0_5d68_48b8_a23d_f67e870c2d89_ctl00_imgApplyFilter']")));
			//
			WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));
			Element.click();
			List<WebElement> Field2nd;
			WebElement FilterField = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/input")));
			FilterField.click();

			if ((Field2nd = new WebDriverWait(driver, 30).until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='Country Name']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input")));
			FilterField2.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='Not Equal']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterFieldDrop = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[4]/div/div/img")));
			FilterFieldDrop.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath("//div[text()='Czech Republic']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			WebElement AddFilter = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//*[@class='x-window-mr']/div/div/div/div/div/div[1]/div /div[1]/div/table/tbody/tr[2]/td[2]/em/button")));
			AddFilter.click();
			WebElement OperatorField = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[1]/div/input")));
			OperatorField.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(" //div[text()='Or']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2nd = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/input")));
			FilterField2nd.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='Country Name']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2nd2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[3]/input")));
			FilterField2nd2.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='Equal']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterCoiceSec = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[4]//img")));
			FilterCoiceSec.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[text()='China']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			WebElement applyButton = driver.findElement(By.xpath(".//button[text()='Apply']"));
			applyButton.click();

			WebElement CloseButton = driver.findElement(By.xpath(".//button[text()='Close']"));
			CloseButton.click();
		} catch (Exception e) {

			System.out.println("   DynamicFilterValue() failed    " + e.getMessage());

		}
	}

	public void DynamicFilterValueSQL() throws InterruptedException {

		try {
			int count = 0;
			// WebElement Element = (WebElement) (new WebDriverWait(driver, 10))
			// .until(ExpectedConditions.presenceOfElementLocated(By
			// .xpath(".//*[@id='ctl00_m_g_65cfa9f0_5d68_48b8_a23d_f67e870c2d89_ctl00_imgApplyFilter']")));
			//
			WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));
			Element.click();
			List<WebElement> Field2nd;
			WebElement FilterField = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/input")));
			FilterField.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='CountryName']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input")));
			FilterField2.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='Equal']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterFieldDrop = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[4]/div/div/img")));
			FilterFieldDrop.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath("//div[text()='Czech Republic']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			WebElement AddFilter = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//*[@class='x-window-mr']/div/div/div/div/div/div[1]/div /div[1]/div/table/tbody/tr[2]/td[2]/em/button")));
			AddFilter.click();
			WebElement OperatorField = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[1]/div/input")));
			OperatorField.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(" //div[text()='Or']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2nd = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/input")));
			FilterField2nd.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(ExpectedConditions
					.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='CountryName']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2nd2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[3]/input")));
			FilterField2nd2.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(".//div[text()='Equal']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterCoiceSec = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[4]//img")));
			FilterCoiceSec.click();

			if ((Field2nd = new WebDriverWait(driver, 10).until(
					ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[text()='China']")))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			// Thread.sleep(1000);
			WebElement applyButton = driver
					.findElement(By.xpath("//div[contains(@id,'winOuterFilterg')]//button[text()='Apply']"));
			applyButton.click();

			Thread.sleep(2000);

			// WebElement CloseButton =
			// driver.findElement(By.xpath("//table[contains(@id,'Closeg')]//button[text()='Close']"));
			// CloseButton.click();
		} catch (Exception e) {

			System.out.println("   DynamicFilterValueSQL() failed    " + e.getMessage());

		}
	}

	//////////////////////////////////// QueryStringFilter///////////////////////

	// public void QueryStringFilter() throws InterruptedException {

	// }

	// ChoiceButton.click();

	// driver.switchTo().activeElement();
	// Set<String> MainWindow=driver.getWindowHandles();

	// driver.switchTo().window(MainWindow).findElement(By.cssSelector("input#1")).click();

	// String WindowTitle=driver.getCurrentUrl();
	// System.out.println(WindowTitle);
	// Set<String> MainWindow=driver.getWindowHandles();
	// String[] MainWinArr=(String[]) MainWindow.toArray();
	//
	// driver.switchTo().window(MainWinArr[1]);

	// driver.switchTo().window("Select Filter Value(s)");

	/////////////////// WebElement
	/////////////////// RadioButton=driver.findElement(By.cssSelector("input#1"));
	/////////////////// RadioButton.click();

	/*
	 * // Storing parent window reference into a String Variable String
	 * Parent_Window = driver.getWindowHandle(); // Switching from parent window
	 * to child window for (String Child_Window : driver.getWindowHandles()) {
	 * driver.switchTo().window(Child_Window);
	 * driver.findElement(By.cssSelector("input[value='China']")).click();
	 * driver.findElement(By.xpath(".//*[@id='ctl00_OkButton']")).click();
	 *
	 * }
	 *
	 *
	 * //Switching back to Parent Window
	 * driver.switchTo().window(Parent_Window);
	 *
	 */
	/*
	 * Set <String> set1=driver.getWindowHandles(); Iterator <String>
	 * win1=set1.iterator(); String parent=win1.next(); String
	 * child=win1.next(); driver.switchTo().window(child);
	 *
	 */

	/*
	 * driver.switchTo().window(currentWindowHandle); WebElement
	 * ChinaRadioButtonSelection= driver.findElement(By.id("bodyMain"));
	 *
	 * List<WebElement> input=
	 * ChinaRadioButtonSelection.findElements(By.id("input"));
	 *
	 * WebElement RadioButton=
	 * input.get(2).findElement(By.xpath(".//*[@id='2']"));
	 *
	 * RadioButton.click();
	 *
	 *
	 *
	 *
	 *
	 * ChinaRadioButtonSelection.click();
	 *
	 *
	 *
	 */

	public void TextDataDrivenTesting(String InputText) throws Exception {
		// String FilterType="";

		// System.out.println("The Row count is " + ex.excel_get_rows());
		// System.out.println("the columnCount is"+ex.excel_get_columns());
		// FilterType=ex.getCellDataasstring(1,0);

		// String Textdata1 = ex.getCellDataasstring(1, 1);

		try {
			// WebElement Textdata2 =
			// driver.findElement(By.xpath("//div[@id='WebPartWPQ4']/div/div/input"));
			WebElement Textdata2 = driver
					.findElement(By.xpath("//div[contains(@id,'WebPartWPQ')]/div/div/input[@type='text']"));
			Textdata2.sendKeys(InputText);
			// Assert.assertTrue("Textdata2.sendKeys(Textdata3)", false);
			Textdata2.sendKeys(Keys.ENTER);
			// ExcelWSheet.getRow(1).createCell(5).setCellValue("Pass");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TextDataDrivenTesting(String InputText) fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("TextDataDrivenTesting(String InputText) fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 367, 2);
			ExcelWriter(ExecResult, ErrorDesc, 367, 3);
			// System.out.print(" TextDataDrivenTesting(String InputText)
			// finally passed ");

		}

	}

	public void ExcTextDataDrivenTesting() throws Exception {
		// String FilterType="";

		// System.out.println("The Row count is " + ex.excel_get_rows());
		// System.out.println("the columnCount is"+ex.excel_get_columns());
		// FilterType=ex.getCellDataasstring(1,0);

		// String Textdata1 = ex.getCellDataasstring(1, 1);

		try {
			ExcelUtil ex = new ExcelUtil(
					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\UIAutomationTestData.xlsx",
					"Sheet1");

			String Textdata3 = ex.getCellDataasstring(1, 2).toString().trim();
			// String Resultdata=ex.getCellDataasstring(1,5).toString().trim();
			// WebElement Textdata2 =
			// driver.findElement(By.xpath("//div[@id='WebPartWPQ4']/div/div/input"));
			WebElement Textdata2 = driver
					.findElement(By.xpath("//div[contains(@id,'WebPartWPQ')]/div/div/input[@type='text']"));

			Textdata2.sendKeys(Textdata3);
			// Assert.assertTrue("Textdata2.sendKeys(Textdata3)", false);
			Textdata2.sendKeys(Keys.ENTER);
			// ExcelWSheet.getRow(1).createCell(5).setCellValue("Pass");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcTextDataDrivenTesting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("ExcTextDataDrivenTesting() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 397, 2);
			ExcelWriter(ExecResult, ErrorDesc, 397, 3);
			// System.out.print(" ExcTextDataDrivenTesting() finally passed ");

		}
	}

	public static final void ChoiceFilterRadioButtonSelection(int seconds, final String ValueString,
			WebDriver driverr) {
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
		ScheduledFuture scheduledFuture = scheduledExecutorService.schedule(new Runnable() {

			public void run() {

				try {
					Robot robot = new Robot();

					System.out.println("Waiting for popup");

					Set<String> MainWindow = driver.getWindowHandles();
					System.out.println(MainWindow);
					String[] MainWinArr = MainWindow.toArray(new String[MainWindow.size()]);

					driver.switchTo().window(MainWinArr[1]);

					// Thread.sleep(1200);

					ExcelUtil ex = new ExcelUtil(
							"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\NewDataDrivenTesting.xlsx",
							"Sheet1");
					int Textdata3 = (int) ex.getCellDataasnumber(4, 2);
					List<WebElement> select = driver.findElements(By.xpath("//label"));
					for (WebElement TestLabel : select) {
						if (ValueString.equals(TestLabel.getAttribute("innerHTML"))) {
							TestLabel.click();
						}

					}

					System.out.println(select.size());

					// driver.findElement(By.xpath(".//*[@id='i']")).click();

					// select.get((int) Textdata3).click();
					// System.out.println((radio.getSize().equals(Textdata3)));

					// if ((radio.getSize().equals(Textdata3)));
					// if ((radio.equals(Textdata3)));
					// driver.findElement(By.xpath("//button[text()='Load']")).click();
					// radio.click();
					// }

					driver.findElement(By.xpath(".//*[@id='ctl00_OkButton']")).click();
				} catch (Exception e) {
					e.printStackTrace();

					System.out.println(
							"ChoiceFilterRadioButtonSelection(int seconds, final String ValueString,WebDriver driverr) failed");

					// System.console().flush();
					// System.out.println("Exception Found");

					// System.out.println("Prepare to Press Enter Exception");
				}
				/*
				 * try{
				 * Assert.assertTrue(DriverFactoryPage.driver.findElement(By.
				 * xpath(
				 * ".//div[@id='chartPanelContainer']/div[2]/div[1]/span/*[name()='svg']/*[name()='g'][3]/*[name()='g'][7]//*[name()='tspan']"
				 * )).getText().contentEquals("China")); }catch(Exception e) {
				 * System.out.println(e.getStackTrace()); }
				 *
				 * }
				 */
			}
		}, seconds, TimeUnit.SECONDS);
		scheduledExecutorService.shutdown();
		// driver.switchTo().activeElement();
		// driver.close();
	}

	public void ChoiceFilterDataDrivenTesting(String input) throws Exception {

		try {
			WebElement ChoiceButton = driver
					.findElement(By.xpath("//div[contains(@id,'WebPartWPQ')]//table//a/img[@alt='Browse']"));

			System.out.println(driver.getWindowHandle());

			ChoiceFilterRadioButtonSelection(5, input, driver);

			ChoiceButton.click();

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ChoiceFilterDataDrivenTesting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("ChoiceFilterDataDrivenTesting() fail in catch:"
			// + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 369, 2);
			ExcelWriter(ExecResult, ErrorDesc, 369, 3);
			// System.out.print(" ChoiceFilterDataDrivenTesting() finally passed
			// ");

		}
		// driver.navigate().back();

		/*
		 * ByteArrayOutputStream baos = new ByteArrayOutputStream(); PrintStream
		 * ps = new PrintStream(baos); // IMPORTANT: Save the old System.out!
		 * PrintStream old = System.out; // Tell Java to use your special stream
		 * System.setOut(ps); // Print some output: goes to your special stream
		 * System.out.println("e"); // Put things back System.out.flush();
		 * System.setOut(old); // Show what happened System.out.println("Here: "
		 * + baos.toString());
		 */
	}

	public void ExcChoiceFilterDataDrivenTesting() throws Exception {

		try {
			WebElement ChoiceButton = driver
					.findElement(By.xpath("//div[contains(@id,'WebPartWPQ')]//table//a/img[@alt='Browse']"));

			System.out.println(driver.getWindowHandle());

			ChoiceFilterRadioButtonSelection(5, 4, driver);

			ChoiceButton.click();
			// driver.navigate().back();

			/*
			 * ByteArrayOutputStream baos = new ByteArrayOutputStream();
			 * PrintStream ps = new PrintStream(baos); // IMPORTANT: Save the
			 * old System.out! PrintStream old = System.out; // Tell Java to use
			 * your special stream System.setOut(ps); // Print some output: goes
			 * to your special stream System.out.println("e"); // Put things
			 * back System.out.flush(); System.setOut(old); // Show what
			 * happened System.out.println("Here: " + baos.toString());
			 */
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcChoiceFilterDataDrivenTesting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("ExcChoiceFilterDataDrivenTesting() fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 396, 2);
			ExcelWriter(ExecResult, ErrorDesc, 396, 3);
			// System.out.print(" ExcChoiceFilterDataDrivenTesting() finally
			// passed ");

		}
	}

	public static final void ChoiceFilterRadioButtonSelection(int seconds, int tabs, WebDriver driverr) {
		ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
		ScheduledFuture scheduledFuture = scheduledExecutorService.schedule(new Runnable() {

			public void run() {

				try {
					Robot robot = new Robot();

					System.out.println("Waiting for popup");

					Set<String> MainWindow = driver.getWindowHandles();
					System.out.println(MainWindow);
					String[] MainWinArr = MainWindow.toArray(new String[MainWindow.size()]);

					driver.switchTo().window(MainWinArr[1]);

					// Thread.sleep(1200);

					ExcelUtil ex = new ExcelUtil(
							// "D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx",
							"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\UIAutomationTestData.xlsx",
							"Sheet1");
					int Textdata3 = (int) ex.getCellDataasnumber(4, 2);
					List<WebElement> select = driver.findElements(By.xpath("//input[@type='radio']"));
					int i = Textdata3;

					System.out.println(select.size());
					select.get(i).click();

					// driver.findElement(By.xpath(".//*[@id='i']")).click();

					// select.get((int) Textdata3).click();
					// System.out.println((radio.getSize().equals(Textdata3)));

					// if ((radio.getSize().equals(Textdata3)));
					// if ((radio.equals(Textdata3)));
					// driver.findElement(By.xpath("//button[text()='Load']")).click();
					// radio.click();
					// }

					driver.findElement(By.xpath(".//*[@id='ctl00_OkButton']")).click();
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println(
							"ChoiceFilterRadioButtonSelection(int seconds, int tabs, WebDriver driverr) failed");

					// System.console().flush();
					// System.out.println("Exception Found");

					// System.out.println("Prepare to Press Enter Exception");
				}
				/*
				 * try{
				 * Assert.assertTrue(DriverFactoryPage.driver.findElement(By.
				 * xpath(
				 * ".//div[@id='chartPanelContainer']/div[2]/div[1]/span/*[name()='svg']/*[name()='g'][3]/*[name()='g'][7]//*[name()='tspan']"
				 * )).getText().contentEquals("China")); }catch(Exception e) {
				 * System.out.println(e.getStackTrace()); }
				 *
				 * }
				 */
			}
		}, seconds, TimeUnit.SECONDS);
		scheduledExecutorService.shutdown();
		// driver.switchTo().activeElement();
		// driver.close();
	}

	public void ExcDynamicFilterDataDrivenTesting() throws Exception {
		ExcelUtil ex;
		List<WebElement> Field2nd;
		int CurrentRow;
		String FieldName1, FieldName2, FieldValue1, FiledValue2, OperatorType1, OperatorType2, JOperator1,
				ExecResult = "PASS", ErrorDesc = " ";
		try {
			ex = new ExcelUtil(
					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\UIAutomationTestData.xlsx",
					// "D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx",
					"Sheet1");
			// ex=new
			// ExcelUtil("D:\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx",
			// "Sheet1");
			FieldName1 = ex.getCellDataasstring(1, 1);
			FieldName2 = ex.getCellDataasstring(2, 1);
			FieldValue1 = ex.getCellDataasstring(1, 2);
			FiledValue2 = ex.getCellDataasstring(2, 2);
			OperatorType1 = ex.getCellDataasstring(1, 3);
			OperatorType2 = ex.getCellDataasstring(2, 3);
			JOperator1 = ex.getCellDataasstring(2, 4);

			WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));
			Element.click();

			WebElement OptionSelection1 = driver.findElement(By.xpath(
					" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/input"));
			OptionSelection1.click();

			System.out.println(
					FieldName1 + FieldName2 + FieldValue1 + FiledValue2 + OperatorType1 + OperatorType2 + JOperator1);
			String FieldNameXpath = ".//div[text()='" + FieldName1 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			WebElement FilterField2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input")));
			FilterField2.click();

			String FieldValueXpath = ".//div[text()='" + OperatorType2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValueXpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField3 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input")));
			FilterField3.click();

			String FirstOperatorXpath1 = ".//div[text()='" + OperatorType1 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FirstOperatorXpath1)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			WebElement FilterFieldDrop = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[4]/div/div/img")));
			FilterFieldDrop.click();

			String FieldValue1Xpath = ".//div[text()='" + FieldValue1 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValue1Xpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			WebElement AddFilter = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//*[@class='x-window-mr']/div/div/div/div/div/div[1]/div /div[1]/div/table/tbody/tr[2]/td[2]/em/button")));
			AddFilter.click();
			WebElement OperatorField = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[1]/div/input")));
			OperatorField.click();

			String JoperatorXpath = " //div[text()='" + JOperator1 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(JoperatorXpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2nd = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/input")));
			FilterField2nd.click();

			String FieldNameXpath1 = ".//div[text()='" + FieldName2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath1)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterField2nd2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[3]/input")));
			FilterField2nd2.click();
			String OperatorXpath2 = ".//div[text()='" + OperatorType2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(OperatorXpath2)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			WebElement FilterCoiceSec = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[4]//img")));
			FilterCoiceSec.click();
			String FiledValue2Xpath = ".//div[text()='" + FiledValue2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FiledValue2Xpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			// int Textdata3=(int) ex.getCellDataasnumber(1,2);
			// List<WebElement> selectdropdown
			// =driver.findElements(By.xpath("//input[@type='text']"));
			// selectdropdown.get(Textdata3).click();
			// Thread.sleep(2000);

			// sendKeys(Keys.ENTER);
			/*
			 * WebElement OptionSelection=
			 * driver.findElement(By.tagName("div"));
			 * OptionSelection.sendKeys(Textdata1);
			 * OptionSelection.sendKeys(Keys.ENTER); OptionSelection.click();
			 *
			 * String Textdata2=ex.getCellDataasstring(1,3).toString().trim();
			 * WebElement OptionSelection2 = driver.findElement(By.xpath(
			 * " //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input"
			 * )); ((Select) OptionSelection2).selectByVisibleText(Textdata2);
			 */

			WebElement applyButton = driver.findElement(By.xpath(".//button[text()='Apply']"));
			applyButton.click();

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcDynamicFilterDataDrivenTesting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("ExcDynamicFilterDataDrivenTesting() fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 395, 2);
			ExcelWriter(ExecResult, ErrorDesc, 395, 3);
			// System.out.print(" ExcDynamicFilterDataDrivenTesting() finally
			// passed ");

		}

	}

	public void DateFilterDataDrivenTesting() throws Exception {
		try {
			WebElement date = driver.findElement(By.xpath(
					".//*[@id='ctl00_m_g_d349eb70_0195_4b1e_8378_43d1753085e5_DateFilterPicker_DateFilterPickerDate']"));
			date.sendKeys("1/16/1995");
			date.sendKeys(Keys.ENTER);

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DateFilterDataDrivenTesting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("DynamicFilterDataDrivenTesting() fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 370, 2);
			ExcelWriter(ExecResult, ErrorDesc, 370, 3);
			// System.out.print(" DynamicFilterDataDrivenTesting() finally
			// passed ");

		}

	}

	public void SharepointlistFilterDataDrivenTesting() throws Exception {

		try {
			WebElement date = driver.findElement(By
					.xpath(".//*[@id='ctl00_m_g_0d117ed5_ba6a_4639_91e4_cf1adcf8c601_SPTextSlicerValueTextControl']"));
			date.sendKeys("Ravioli Angelo");
			date.sendKeys(Keys.ENTER);

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SharepointlistFilterDataDrivenTesting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("SharepointlistFilterDataDrivenTesting() fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 371, 2);
			ExcelWriter(ExecResult, ErrorDesc, 371, 3);
			// System.out.print(" SharepointlistFilterDataDrivenTesting()
			// finally passed ");

		}

	}

	public void DynamicFilterDataDrivenTesting(String Col1, String Opt1, String Val1, String Joperator, String Col2,
			String Opt2, String Val2) throws Exception {
		ExcelUtil ex;
		List<WebElement> Field2nd;
		int CurrentRow;
		String FieldName1, FieldName2, FieldValue1, FiledValue2, OperatorType1, OperatorType2, JOperator1,
				ExecResult = "PASS", ErrorDesc = " ";
		try {

			Thread.sleep(2000);
			WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));

			// .xpath(".//*[@id='ctl00_m_g_65cfa9f0_5d68_48b8_a23d_f67e870c2d89_ctl00_imgApplyFilter']")));
			Element.click();

			WebElement OptionSelection1 = driver.findElement(By
					.xpath("//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/input"));
			OptionSelection1.click();

			System.out.println(Col1 + Opt1 + Val1 + Col2 + Opt2 + Val2);
			String FieldNameXpath = ".//div[text()='" + Col1 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			Thread.sleep(4500);
			WebElement FilterField2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input")));
			FilterField2.click();

			String FieldValueXpath = ".//div[text()='" + Opt1 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValueXpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			/*Thread.sleep(2600);
			WebElement FilterField3 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input")));
			FilterField3.click();

			String FirstOperatorXpath1 = ".//div[text()='" + Opt2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FirstOperatorXpath1)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}*/
			Thread.sleep(5500);
			WebElement FilterFieldDrop = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							" //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[4]/div/div/img")));
			FilterFieldDrop.click();

			String FieldValue1Xpath = ".//div[text()='" + Val1 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValue1Xpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			Thread.sleep(6500);
			WebElement AddFilter = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							".//*[@class='x-window-mr']/div/div/div/div/div/div[1]/div /div[1]/div/table/tbody/tr[2]/td[2]/em/button")));
			AddFilter.click();
			WebElement OperatorField = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[1]/div/input")));
			OperatorField.click();

			String JoperatorXpath = " //div[text()='" + Joperator + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(JoperatorXpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			Thread.sleep(7500);
			WebElement FilterField2nd = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[2]/input")));
			FilterField2nd.click();

			String FieldNameXpath1 = ".//div[text()='" + Col2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath1)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			Thread.sleep(8500);
			WebElement FilterField2nd2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[3]/input")));
			FilterField2nd2.click();
			String OperatorXpath2 = ".//div[text()='" + Opt2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(OperatorXpath2)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			Thread.sleep(9500);
			WebElement FilterCoiceSec = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div[2]/div/div[4]//img")));
			FilterCoiceSec.click();
			String FiledValue2Xpath = ".//div[text()='" + Val2 + "']";
			if ((Field2nd = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FiledValue2Xpath)))) != null) {
				int length = Field2nd.size();
				WebElement[] Field2ndArr = Field2nd.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}

			// int Textdata3=(int) ex.getCellDataasnumber(1,2);
			// List<WebElement> selectdropdown
			// =driver.findElements(By.xpath("//input[@type='text']"));
			// selectdropdown.get(Textdata3).click();
			// Thread.sleep(2000);

			// sendKeys(Keys.ENTER);
			/*
			 * WebElement OptionSelection=
			 * driver.findElement(By.tagName("div"));
			 * OptionSelection.sendKeys(Textdata1);
			 * OptionSelection.sendKeys(Keys.ENTER); OptionSelection.click();
			 *
			 * String Textdata2=ex.getCellDataasstring(1,3).toString().trim();
			 * WebElement OptionSelection2 = driver.findElement(By.xpath(
			 * " //div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input"
			 * )); ((Select) OptionSelection2).selectByVisibleText(Textdata2);
			 */

			WebElement applyButton = driver.findElement(By.xpath(".//button[text()='Apply']"));
			applyButton.click();

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DynamicFilterDataDrivenTesting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("DynamicFilterDataDrivenTesting() fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 372, 2);
			ExcelWriter(ExecResult, ErrorDesc, 372, 3);
			// System.out.print(" DynamicFilterDataDrivenTesting() finally
			// passed ");

		}

	}

	/////////////////////////// *****************Excel
	/////////////////////////// Writing****************////////////////////
	public void Excelwriting() throws Exception {
		try {

			// Specify the file path which you want to create or write

			File src = new File("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx");
			// ExcelUtil ex=new
			// ExcelUtil("D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx",
			// "Sheet1");
			// Load the file

			FileInputStream fis = new FileInputStream(src);

			// load the workbook

			XSSFWorkbook wb = new XSSFWorkbook(fis);

			// get the sheet which you want to modify or create

			XSSFSheet sh1 = wb.getSheetAt(0);

			// getRow specify which row we want to read and getCell which column
			System.out.println(sh1.getRow(0).getCell(0).getStringCellValue());

			System.out.println(sh1.getRow(0).getCell(1).getStringCellValue());

			System.out.println(sh1.getRow(1).getCell(0).getStringCellValue());

			System.out.println(sh1.getRow(1).getCell(1).getStringCellValue());

			System.out.println(sh1.getRow(2).getCell(0).getStringCellValue());

			System.out.println(sh1.getRow(2).getCell(1).getStringCellValue());

			// here createCell will create column

			// and setCellvalue will set the value

			sh1.getRow(1).createCell(5).setCellValue("Pass");

			sh1.getRow(2).createCell(5).setCellValue("Fail");

			sh1.getRow(3).createCell(5).setCellValue("Pass");

			// here we need to specify where you want to save file

			// FileOutputStream fout=new FileOutputStream(new File("location of
			// file/filename.xlsx"));

			FileOutputStream fout = new FileOutputStream(
					new File("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx"));
			// finally write content

			wb.write(fout);

			// close the file

			fout.close();

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Excelwriting() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("Excelwriting() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 374, 2);
			ExcelWriter(ExecResult, ErrorDesc, 374, 3);
			// System.out.print(" Excelwriting() finally passed ");

		}

	}

	/////////////////////////////// Drilldown Chart XML Data
	/////////////////////////////// Rendering///////////////////////////

	public void Drilldown_Chart_XML12() throws IOException {
		try {
			HtmlUnitDriver htmlUnitDriver = new HtmlUnitDriver(true);

			htmlUnitDriver.get("http://www.google.co.in");
			((HtmlUnitDriver) htmlUnitDriver).isJavascriptEnabled();

			String DrilldownChart = (String) ((JavascriptExecutor) htmlUnitDriver).executeScript(
					"var arr= []; for(var obj in FusionCharts.items) { if(obj.toString().startsWith('chartobject')) arr.push(parseInt(obj.toString().substring(12)));} FusionCharts.items['chartobject-'+Math.max.apply(null, arr)].getXML()");

			System.out.println(DrilldownChart);

		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Drilldown_Chart_XML12() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("Drilldown_Chart_XML12() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 373, 2);
			ExcelWriter(ExecResult, ErrorDesc, 373, 3);
			// System.out.print(" Drilldown_Chart_XML12() finally passed ");

		}

	}

	public void Drilldown_Chart_XML_FirstLevel()
			throws InterruptedException, ParserConfigurationException, SAXException, IOException, TransformerException {
		Thread.sleep(10000);
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			driver.manage().timeouts().setScriptTimeout(55, TimeUnit.SECONDS);
			// Object obj = js.executeScript("var arr= [1,2]; for(var obj in
			// FusionCharts.items) {
			// if(obj.toString().startsWith('chartobject'))
			// arr.push(parseInt(obj.toString().substring(12)));} return
			// FusionCharts.items['chartobject-'+Math.max.apply(null,
			// arr)].getXML();");

			// String sText =
			// js.executeScript("FusionCharts.items['chartobject-2'].getXML()").toString();
			Object obj = js.executeScript("return FusionCharts.items['chartobject-1'].getXML();");
			String sText = obj.toString();
			// String sText = js.executeScript("FusionCharts.items").toString();
			// String sText = js.executeAsyncScript("var arr= []; for(var obj in
			// FusionCharts.items) {
			// if(obj.toString().startsWith('chartobject'))
			// arr.push(parseInt(obj.toString().substring(12)));}
			// FusionCharts.items['chartobject-'+Math.max.apply(null,
			// arr)].getXML()").toString();

			System.out.println("Here Javascript Response:" + sText);

			/////////// save XML response in XMl file///////////
			// Element element = null;
			// String Value = null;
			// Parse the given input
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Node doc = (Node) builder
					.parse(new InputSource(new StringReader(obj.toString().trim().replaceFirst("^([\\W]+)<", "<"))));

			doc.getAttributes();
			System.out.println(doc.getAttributes());
			// Write the parsed document to an xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("SpecficDataFinderFirst.XML"));
			transformer.transform(source, result);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Drilldown_Chart_XML_FirstLevel() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("Drilldown_Chart_XML_FirstLevel() fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 375, 2);
			ExcelWriter(ExecResult, ErrorDesc, 375, 3);
			// System.out.print(" Drilldown_Chart_XML_FirstLevel() finally
			// passed ");

		}

	}

	public void Drilldown_Chart_XML_SecondLevel()
			throws InterruptedException, ParserConfigurationException, SAXException, IOException, TransformerException {

		Thread.sleep(15000);
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// driver.manage().timeouts().setScriptTimeout(56,
			// TimeUnit.SECONDS);
			// Object obj = js.executeScript("var arr= [1,2]; for(var obj in
			// FusionCharts.items) {
			// if(obj.toString().startsWith('chartobject'))
			// arr.push(parseInt(obj.toString().substring(12)));} return
			// FusionCharts.items['chartobject-'+Math.max.apply(null,
			// arr)].getXML();");

			// String sText =
			// js.executeScript("FusionCharts.items['chartobject-2'].getXML()").toString();
			Object obj = js.executeScript("return FusionCharts.items['chartobject-2'].getXML();");
			String sText = obj.toString();
			// String sText = js.executeScript("FusionCharts.items").toString();
			// String sText = js.executeAsyncScript("var arr= []; for(var obj in
			// FusionCharts.items) {
			// if(obj.toString().startsWith('chartobject'))
			// arr.push(parseInt(obj.toString().substring(12)));}
			// FusionCharts.items['chartobject-'+Math.max.apply(null,
			// arr)].getXML()").toString();

			System.out.println("Here Javascript Response:" + sText);

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Node doc = (Node) builder
					.parse(new InputSource(new StringReader(obj.toString().trim().replaceFirst("^([\\W]+)<", "<"))));

			doc.getAttributes();
			System.out.println(doc.getAttributes());
			// Write the parsed document to an xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File("SpecficDataFinderSecond.XML"));
			transformer.transform(source, result);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Drilldown_Chart_XML_SecondLevel() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("Drilldown_Chart_XML_SecondLevel() fail in
			// catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 376, 2);
			ExcelWriter(ExecResult, ErrorDesc, 376, 3);
			// System.out.print(" Drilldown_Chart_XML_SecondLevel() finally
			// passed ");

		}

	}

	public void Drilldown_Chart_XML()
			throws InterruptedException, SAXException, IOException, ParserConfigurationException, TransformerException {
		try {
			// driver.getWindowHandles();
			// driver.switchTo().defaultContent();
			Set<String> MainWindow = driver.getWindowHandles();
			System.out.println(MainWindow);
			String[] MainWinArr = MainWindow.toArray(new String[MainWindow.size()]);
			System.out.println(MainWinArr);
			driver.switchTo().window(MainWinArr[0]);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			driver.manage().timeouts().setScriptTimeout(56, TimeUnit.SECONDS);
			Object obj = js.executeScript(
					"var a=FusionCharts.items;for(i in a){if(a[i].hasOwnProperty('id')){var c=a[i].id;var d=a[c].getXML(); return d;}}");
			String sText = obj.toString();
			System.out.println("Here Javascript Response:" + sText);

			/////////// save XML response in XMl file///////////
			// Element element = null;
			// String Value = null;
			// Parse the given input
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Node doc = (Node) builder
					.parse(new InputSource(new StringReader(obj.toString().trim().replaceFirst("^([\\W]+)<", "<"))));

			// Write the parsed document to an xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(new File(
					"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SpecficDataFinder.XML"));
			transformer.transform(source, result);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Drilldown_Chart_XML() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("Drilldown_Chart_XML() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 377, 2);
			ExcelWriter(ExecResult, ErrorDesc, 377, 3);
			// System.out.print(" Drilldown_Chart_XML() finally passed ");

		}

	}

	// System.setProperty("webdriver.chrome.driver",
	// "C:\\Users\\me\\Downloads\\chromedriver.exe");
	// ChromeDriver driver = new ChromeDriver();

	/*
	 * JavascriptExecutor executor = (JavascriptExecutor) driver;
	 *
	 *
	 *
	 * Object js ="var arr= []; for(var obj in FusionCharts.items)"+ "{
	 * if(obj.toStrin
	 *
	 *
	 *
	 *
	 *
	 * g().startsWith('chartobject'))"+
	 * "arr.push(parseInt(obj.toString().substring(12)));}"+
	 * "FusionCharts.items['chartobject-'+Math.max.apply(null, arr)]"+
	 * ".getXML()";
	 *
	 * Long a = (Long) executor.toString().st.ex.executeScript(js);
	 * System.out.println(a); }
	 */

	/*
	 *
	 * if (driver instanceof JavascriptExecutor) { //
	 * ((JavascriptExecutor)driver).executeScript(
	 * "var arr= []; for(var obj in FusionCharts.items) { if(obj.toString().startsWith('chartobject')) arr.push(parseInt(obj.toString().substring(12)));} FusionCharts.items['chartobject-'+Math.max.apply(null, arr)].getXML();"
	 * );
	 *
	 *
	 *
	 *
	 *
	 * JavascriptExecutor js;
	 *
	 * js = (JavascriptExecutor)driver; // String Response= (String)
	 * js.executeScript(
	 * "var arr= []; for(var obj in FusionCharts.items) { if(obj.toString().startsWith('chartobject')) arr.push(parseInt(obj.toString().substring(12)));} FusionCharts.items['chartobject-'+Math.max.apply(null, arr)].getXML()"
	 * ); // js.executeScript(
	 * "function showAlert() { alert('success'); };var arr= []; for(var obj in FusionCharts.items) { if(obj.toString().startsWith('chartobject')) arr.push(parseInt(obj.toString().substring(12)));} FusionCharts.items['chartobject-'+Math.max.apply(null, arr)].getXML()"
	 * ); String Response= (String) js.executeScript(
	 * "var arr= []; for(var obj in FusionCharts.items) { if(obj.toString().startsWith('chartobject')) arr.push(parseInt(obj.toString().substring(12)));} FusionCharts.items['chartobject-'+Math.max.apply(null, arr)].getXML()"
	 * ).toString();
	 *
	 * String findDrilldown = "return " + Response;
	 * System.out.println(findDrilldown);
	 *
	 *
	 *
	 * } else { throw new IllegalStateException(
	 * "This driver does not support JavaScript!"); }
	 *
	 *
	 */

	//////// *********************************************************************************Excel
	//////// Writing****************************************************************************************************************************************************************************************************************************
	public void ExcelWriter(String ExecutionStatus, String ErrorMessage, int CurrentRowNumber, int ColumnOffset)
			throws IOException {

		// ex=new
		// ExcelUtil("D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx",
		// "Sheet1");
		File src = new File(
				"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\functionerror.xlsx");
		// "//D:\\collabionCharts\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\functionerror.xlsx");
		// "D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\UIAutomationTestData.xlsx");

		FileInputStream fis = new FileInputStream(src);
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		XSSFSheet sh1 = wb.getSheetAt(0);

		Cell c1 = sh1.getRow(CurrentRowNumber).createCell(1);
		c1.setCellValue(wb.getCreationHelper().createRichTextString(ExecutionStatus));
		sh1.getRow(CurrentRowNumber).createCell(2)
				.setCellValue(wb.getCreationHelper().createRichTextString(ErrorMessage));

		FileOutputStream outFile = new FileOutputStream(
				new File("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\functionerror.xlsx"));
		// "D:\\collabionCharts\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\functionerror.xlsx"));
		wb.write(outFile);
		outFile.close();

	}

	public void ExcelWriter(int Row, int Col, String ExcelPath, String SheetName, String Data) throws IOException {

		File src = new File(ExcelPath);

		FileInputStream fis = new FileInputStream(src);

		XSSFWorkbook wb = new XSSFWorkbook(fis);

		XSSFSheet sh1 = wb.getSheet(SheetName);

		Cell c1 = sh1.getRow(Row).createCell(Col);
		c1.setCellValue(Data);
		;

		FileOutputStream outFile = new FileOutputStream(new File(ExcelPath));

		wb.write(outFile);

		outFile.close();

	}

	///////// *************************************************************************************DATA
	///////// VALIDATION*************************************************************************************************************************************************************************************************************************

	public void datavalidation(String ExcelPath, String SheetName, int depth) throws Exception {

		WebElement TestElement;
		List<WebElement> LabelName = null;
		List<WebElement> ValueList;
		List<WebElement> LabelContainer = null;
		int count = 0, IndexMeasurer[], index_counter = 0, RowNumber, RowIndex = 1;
		String[] ExcelLabelContainerArray;
		WebElement ValueContainer;

		/*
		 * This part fetches all the Labels from the canvas and stores them into
		 * an List,for furthur processing,different Xpaths are used as after
		 * drill down changes in xpath happens depth: is an integer variable
		 * which measures the level of drill down LabelContainer:is a List of
		 * WebElement collection which holds all the Labels in the background of
		 * the chart. ValueList:is a List of WebElement collection which holds
		 * the collection of the WebElement of the values of the respective
		 * Labels.
		 */

		if (depth == 0) {
			LabelContainer = (List<WebElement>) new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(
							"//*[@id='chartPanelContainer']/div[2]/div[1]/span/*[name()='svg']/*[name()='g'][3]/*[name()='g'][7]/*[name()='text']")));
			ValueContainer = new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(
					By.xpath("//*[@id='chartPanelContainer']/div[2]/div[1]/span/*[name()='svg']/*[name()='g'][6]")));
			ValueList = ValueContainer.findElements(By.xpath(
					"//*[@id='chartPanelContainer']/div[2]/div[1]//*[name()='svg']/*[name()='g'][6]/*[name()='text']/*[name()='tspan']"));

		}

		/*
		 * if drill down happens then xpath changes the span id changes ,which
		 * is re-genrated Xpath:is a string which holds the Xpath of the Labels.
		 * XpathValue:is a string which holds the Xpath of the Values.
		 */

		else {
			String Xpath = "//span[@id=" + "'chartobject-" + depth
					+ "']/*[name()='svg']/*[name()='g'][3]/*[name()='g'][7]//*[name()='text']";
			LabelContainer = (List<WebElement>) new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(Xpath)));
			String XpathValue = "//span[@id=" + "'chartobject-" + depth
					+ "']/*[name()='svg']/*[name()='g'][6]/*[name()='text']/*[name()='tspan']";
			System.out.println(Xpath);
			ValueList = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(XpathValue)));
		}

		// End of fetching of labeldata

		// this part fetches all the data from Excelsheet for furthur processing

		ExcelUtil ex = new ExcelUtil(ExcelPath, SheetName);
		RowNumber = ex.excel_get_rows();
		ExcelLabelContainerArray = new String[RowNumber - 1];// need to update
		for (int i = 0; i < RowNumber - 1; i++) {

			ExcelLabelContainerArray[i] = ex.getCellDataasstring(RowIndex, 0);
			RowIndex++;

		}
		// end of fetching data from excel sheet

		// This part measures the index of the label-span and fetches the
		// correspondng value

		index_counter = 0;
		for (WebElement E : LabelContainer) {

			int ExcelLabelIndex;
			LabelName = E.findElements(By.xpath("*[name()='tspan']"));
			StringBuilder LabelText = null;

			// when the label Name is long the tspan contains the text a=in to
			// text svg classes insted of one ,
			// and then appends it. here the child text elements are stored and
			// appeneded.

			for (WebElement E1 : LabelName) {
				if (LabelText != null)
					LabelText.append(E1.getText());
				else
					LabelText = new StringBuilder(E1.getText());

			}

			/*
			 * Once the entire label is extracted and concated,the label is
			 * matched with all the label values provided in the ExcelSheet
			 */

			for (int i = 0; i < ExcelLabelContainerArray.length; i++) {

				// System.out.println("Compare: " + LabelText.toString() + ":" +
				// ExcelLabelContainerArray[i]);
				if (LabelText.toString().equalsIgnoreCase(ExcelLabelContainerArray[i])) {

					WebElement[] ValueListArr = ValueList.toArray(new WebElement[ValueList.size()]);
					String ExactVal = ValueListArr[index_counter].getText();

					System.out.println(LabelText.toString() + ":" + ExactVal);
					ExcelWriter(i + 1, 2,
							"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\SalesDataSheet.xlsx",
							SheetName, ExactVal);
					index_counter++;
					break;

				}

			}

		}

	}// *[@id='chartPanelContainer']/div[2]/div[1]/span/*[name()='svg']/*[name()='g'][6]/

	/// ************************************************************************List
	/// Filter************************************************************************************************************************
	public void openListFilterPage() throws IOException {
		try {
			driver.navigate().to("http://pc-nibir2010/SitePages/SharePointListFilter.aspx");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("openListFilterPage() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("openListFilterPage() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 381, 2);
			ExcelWriter(ExecResult, ErrorDesc, 381, 3);
			// System.out.print(" openListFilterPage() finally passed ");

		}
	}

	public void ClickToChoose() throws IOException {
		try {
			WebElement ChoiceButton = driver
					.findElement(By.xpath("//div[@id='WebPartWPQ3']/div[1]/div/span/table/tbody/tr/td[2]/a/img"));
			System.out.println(driver.getWindowHandle());
			prepareToPressEnterKey(5, 4, driver);
			ChoiceButton.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickToChoose() fail in catch:" + ErrorDesc);

		} finally {
			// System.out.print("ClickToChoose() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 382, 2);
			ExcelWriter(ExecResult, ErrorDesc, 382, 3);
			// System.out.print(" ClickToChoose() finally passed ");

		}
	}

	// *****************************************************Wizard Filter
	// *************************************************************************************

	public void CheckToAddFilter() throws IOException {
		try {
			List<WebElement> DeleteIcons = (List<WebElement>) new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("cc-delete-button")));

			if (DeleteIcons != null) {
				for (WebElement El1 : DeleteIcons)
					El1.click();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CheckToAddFilter() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 400, 2);
			ExcelWriter(ExecResult, ErrorDesc, 400, 3);

		}
	}

	public void AddFilterCondition() throws IOException {
		try {
			WebElement AddButtons = driver.findElement(By.className("cc-add-button"));
			AddButtons.click();

			List<WebElement> firstButton = driver.findElements(By.className("x-form-trigger"));
			for (WebElement El1 : firstButton) {
				if (El1.getAttribute("class").equals("x-form-trigger")) {
					System.out.print(El1.getAttribute("class"));
					El1.click();
				}
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("AddFilterCondition() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 410, 2);
			ExcelWriter(ExecResult, ErrorDesc, 410, 3);

		}

	}

	// ***********************************************************************SQL
	// DATASOURCE NEGATIVE
	// TESTINGS**************************************************************************************************************************iI

	public void SelectAutheticationMethod() throws IOException {
		try {
			driver.findElement(By.xpath("//div[@id='x-form-el-winAuthnentication']/div/input")).click();
			WebElement Authetication = driver.findElement(By.xpath("//div/div[text()='Domain Authentication']"));
			Authetication.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SelectAutheticationMethod() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 411, 2);
			ExcelWriter(ExecResult, ErrorDesc, 411, 3);

		}

	}

	public void clickconnect() throws IOException {
		try {
			driver.findElement(By.xpath("//table[@id='connectBtn']//button[text()='Connect']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("clickconnect() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 412, 2);
			ExcelWriter(ExecResult, ErrorDesc, 412, 3);

		}

	}

	public void WriteServerName() throws IOException {
		try {
			WebElement Server = driver.findElement(By.xpath("//input[contains(@id, 'sqlServer')]"));
			Assert.assertNotNull(Server);
			// Server.click();
			Server.clear();
			// Server.sendKeys("(local)");
			Server.sendKeys(".");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WriteServerName() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 413, 2);
			ExcelWriter(ExecResult, ErrorDesc, 413, 3);

		}

	}

	public void WriteDomainName() throws IOException {
		try {

			WebElement DomainName = driver.findElement(By.cssSelector("input#sqlDomain.x-form-text.x-form-field"));
			DomainName.clear();
			DomainName.sendKeys(".");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WriteDomainName() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 414, 2);
			ExcelWriter(ExecResult, ErrorDesc, 414, 3);

		}
	}

	public void WriteUserName() throws IOException {
		try {
			WebElement UserName = driver.findElement(By.cssSelector("input#sqlLogin.x-form-text.x-form-field"));
			UserName.clear();
			UserName.sendKeys("Administrator");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WriteUserName() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 415, 2);
			ExcelWriter(ExecResult, ErrorDesc, 415, 3);

		}
	}

	public void WritePassWord() throws IOException {
		try {
			WebElement Password = driver.findElement(By.cssSelector("input#sqlPass.x-form-text.x-form-field"));
			Password.clear();
			Password.sendKeys("P@ssw0rd");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WritePassWord() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 416, 2);
			ExcelWriter(ExecResult, ErrorDesc, 416, 3);

		}
	}

	public void WriteDB() throws IOException {
		try {
			WebElement Dbase = driver.findElement(By.xpath("//input[contains(@id, 'sqlDbase')]"));
			Assert.assertNotNull(Dbase);
			// Dbase.click();
			Dbase.clear();
			Dbase.sendKeys("fusionchartdb");
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("WriteDB() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 417, 2);
			ExcelWriter(ExecResult, ErrorDesc, 417, 3);

		}
	}

	public void CheckErrorBackGroundColor() throws IOException {
		try {
			// border-color
			// Class<? extends WebElement>
			// clas=driver.findElement(By.xpath("//*[@id='sqlDomain']")).getClass();
			String clas = driver.findElement(By.xpath("//*[@id='sqlDomain']")).getAttribute("class");
			String eXpeced = "url(" + '"'
					+ "http://Administrator:P%40ssw0rd@pc-nibir2010/_layouts/CollabionCharts/ext/resources/images/default/grid/invalid_line.gif"
					+ '"' + ")";
			String COLOR = driver.findElement(By.xpath("//*[contains(@class,'x-form-invalid')]"))
					.getCssValue("background-image");
			Assert.assertEquals(eXpeced, COLOR);
			System.out.println("Background color is :" + COLOR + clas);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CheckErrorBackGroundColor() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 418, 2);
			ExcelWriter(ExecResult, ErrorDesc, 418, 3);

		}
	}

	public void clearAllfields() throws IOException {
		try {
			WebElement Server = driver.findElement(By.xpath("//input[contains(@id, 'sqlServer')]"));
			WebElement DomainName = driver.findElement(By.cssSelector("input#sqlDomain.x-form-text.x-form-field"));
			WebElement UserName = driver.findElement(By.cssSelector("input#sqlLogin.x-form-text.x-form-field"));
			WebElement Password = driver.findElement(By.cssSelector("input#sqlPass.x-form-text.x-form-field"));
			WebElement Dbase = driver.findElement(By.xpath("//input[contains(@id, 'sqlDbase')]"));
			Server.clear();
			DomainName.clear();
			UserName.clear();
			Password.clear();
			Dbase.clear();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("clearAllfields() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 419, 2);
			ExcelWriter(ExecResult, ErrorDesc, 419, 3);

		}
	}

	public void CheckErrDial() throws IOException {
		try {
			WebElement DialogBox = driver.findElement(By.xpath("//button[text()='OK']"));
			Assert.assertTrue(PresenceOfElementFound("//button[text()='OK']"));
			if (PresenceOfElementFound("//button[text()='OK']")) {
				DialogBox.click();
			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CheckErrDial() fail in catch:" + ErrorDesc);
		} finally {
			ExcelWriter(ExecResult, ErrorDesc, 420, 2);
			ExcelWriter(ExecResult, ErrorDesc, 420, 3);
			//System.out.println("CheckErrDial() fail in finally:" + ErrorDesc);

		}

	}

	private boolean PresenceOfElementFound(String Xpath) {
		try {
			driver.findElement(By.xpath(Xpath));
		} catch (NoSuchElementException e) {
			return false;
		}
		return true;

	}

	// Excel Password
	// automation********************************************************************************************************************************************************
	public void ExcelPath()throws IOException {
	try{
		WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
		Assert.assertNotNull("Urlfield");
		Urlfield.clear();
		Urlfield.click();
		Urlfield.sendKeys("http://pc-nibir2010/Shared Documents/Stocks.xlsx");
		}catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelPath() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 469, 2);
			ExcelWriter(ExecResult, ErrorDesc, 469, 3);
		//	System.out.println("ExcelPath() fail in finally:" + ErrorDesc);
			
	}
		
	}

	public void SheetSelection() throws IOException {
	try{
		WebElement SheetChoose = driver.findElement(By.xpath("//input[contains(@id,'xlsSheetName')]"));
		Assert.assertNotNull("SheetChoose");
		SheetChoose.click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[text()='Sheet1']")).click();
		}catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SheetSelection() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 468, 2);
			ExcelWriter(ExecResult, ErrorDesc, 468, 3);
	//		System.out.println("SheetSelection() fail in finally:" + ErrorDesc);
			
	}
	}

	public void ExcelPassword() throws IOException{
	try{
		driver.findElement(By.xpath(".//*[@id='xlPass']")).clear();
		driver.findElement(By.xpath(".//*[@id='xlPass']")).sendKeys("abc123");
		}catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelPassword() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 467, 2);
			ExcelWriter(ExecResult, ErrorDesc, 467, 3);
	//	System.out.println("ExcelPassword() fail in finally:" + ErrorDesc);
			
	}
		
	}

	public void aggregatefunctionSelection()throws IOException {
	try{
		WebElement DropDown = driver.findElement(By.xpath(
				"//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Volume']/../../td[2]/div"));
		DropDown.click();
		WebElement Editor = new WebDriverWait(driver, 12).until(ExpectedConditions.elementToBeClickable(
				By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input")));
		Editor.click();
		driver.findElement(By.xpath("//div/span[text()='SUM']")).click();
		
		
		}catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("aggregatefunctionSelection() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 466, 2);
			ExcelWriter(ExecResult, ErrorDesc, 466, 3);
		//	System.out.println("aggregatefunctionSelection() fail in finally:" + ErrorDesc);
			
	}
		
	}

	public void ClickDrillDownBar() throws IOException{
		try {
			barclicker(".//div[@id='chartPanelContainer']//span//*[local-name() = 'svg']/*[name()='g']", 2, "red-hot");
		 }catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ClickDrillDownBar() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 465, 2);
			ExcelWriter(ExecResult, ErrorDesc, 465, 3);
		//	System.out.println("ClickDrillDownBar() fail in finally:" + ErrorDesc);
			
	}
		
	}

	public void providefilterdata() throws InterruptedException,IOException{
		try{
		List<WebElement> MainPanel = driver.findElements(By.cssSelector("div#filteringStep.x-panel.x-panel-noborder"));
		for (int i = 0; i < MainPanel.size(); i++) {

			List<WebElement> checkbox2 = MainPanel.get(i).findElements(By.tagName("input"));

			if (checkbox2.get(4).isDisplayed()) {
				checkbox2.get(4).click();

				// checkbox2.get(1).click();
				// checkbox2.get(1).sendKeys(Keys.ARROW_DOWN);
				checkbox2.get(4).sendKeys("200");
				// checkbox2.get(1).sendKeys(Keys.ENTER);
     }}}
     catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("providefilterdata() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 464, 2);
			ExcelWriter(ExecResult, ErrorDesc, 464, 3);
	//	System.out.println("providefilterdata() fail in finally:" + ErrorDesc);
			
	}
    
	}

	public void ExcelWrongPassword()throws InterruptedException,IOException {
	try{
		driver.findElement(By.xpath(".//*[@id='xlPass']")).clear();
		driver.findElement(By.xpath(".//*[@id='xlPass']")).sendKeys("abc12");
		
		  } catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelWrongPassword() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 463, 2);
			ExcelWriter(ExecResult, ErrorDesc, 463, 3);
	//	System.out.println("ExcelWrongPassword() fail in finally:" + ErrorDesc);
			
	}
	}

	public void ExcelNoPassword() throws InterruptedException,IOException {
	try{
		driver.findElement(By.xpath(".//*[@id='xlPass']")).clear();
		Thread.sleep(1000);

     } catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelNoPassword() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 462, 2);
			ExcelWriter(ExecResult, ErrorDesc, 462, 3);
		//	System.out.println("ExcelNoPassword() fail in finally:" + ErrorDesc);
			
	}
	}

	public void ExcelWrongPasswordPopUpverification()throws IOException {
	try{
		WebElement POPUP = new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(
				"//div//div/span[text()='Access Denied! The password you have provided is not correct, please enter the correct password and try again.']")));
		String txt = POPUP.getText();
		System.out.println(txt);
		Assert.assertTrue("POP UP mismatchg", txt.contains(
				"Access Denied! The password you have provided is not correct, please enter the correct password and try again."));
	
	} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelWrongPasswordPopUpverification() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 461, 2);
			ExcelWriter(ExecResult, ErrorDesc, 461, 3);
	//	System.out.println("ExcelWrongPasswordPopUpverification() fail in finally:" + ErrorDesc);
			
	}
	
	}

	public void ExcelNoPasswordPopUpverification()throws IOException
	 {
	try{
		WebElement POPUP = new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(
				"//div//div/span[text()='The Excel file is password protected, please enter the password and try again.']")));
		String txt = POPUP.getText();
		System.out.println(txt);
		Assert.assertTrue("POP-up mismatch in ExcelNoPassword",
				txt.contains("The Excel file is password protected, please enter the password and try again."));
				
				} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelNoPasswordPopUpverification() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 460, 2);
			ExcelWriter(ExecResult, ErrorDesc, 460, 3);
	//	System.out.println("ExcelNoPasswordPopUpverification() fail in finally:" + ErrorDesc);
			
	}
	}
	
	
	public void Oracle_Database() throws IOException
	{
	
		
		try {
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			Thread.sleep(1000);
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[3]"));
			DropDownValue.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Oracle_Database() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 459, 2);
			ExcelWriter(ExecResult, ErrorDesc, 459, 3);
		//	System.out.println("Oracle_Database() fail in finally:" + ErrorDesc);
			
	}
}	
	public void Oracle_ServerName()throws IOException
	{
	try{
		WebElement OracleServerName = driver.findElement(By.xpath(".//*[@id='orcServer']"));
		OracleServerName.clear();
		OracleServerName.sendKeys("(ADDRESS = (PROTOCOL = TCP)(HOST = PC-NIBIR2010)(PORT = 1521))");
		}	catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Oracle_ServerName() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 458, 2);
			ExcelWriter(ExecResult, ErrorDesc, 458, 3);
		//	System.out.println("Oracle_ServerName() fail in finally" + ErrorDesc);

		}
	}
	
	public void Oracle_UserName()throws IOException
	{
	try{
		WebElement OracleUserName = driver.findElement(By.xpath(".//*[@id='orcLogin']"));
		OracleUserName.clear();
		OracleUserName.sendKeys("FusionChartsDB");
		}	catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Oracle_UserName() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 457, 2);
			ExcelWriter(ExecResult, ErrorDesc, 457, 3);
	//		System.out.println("Oracle_UserName() fail in finally" + ErrorDesc);

		}
		
	}
	public void Oracle_UserPassword()throws IOException
	{
	try{
		WebElement OracleUserPassword = driver.findElement(By.xpath(".//*[@id='orcPass']"));
		OracleUserPassword.clear();
		OracleUserPassword.sendKeys("FusionChartsDB");
	     }	catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Oracle_UserPassword() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 456, 2);
			ExcelWriter(ExecResult, ErrorDesc, 456, 3);
	//		System.out.println("Oracle_UserPassword() fail in finally" + ErrorDesc);

		}
	}
//////////////START////////////////////////////////https://fusioncharts.jira.com/browse/SPC-1579////////////////////////////////////
	public void Column(String Column) throws IOException, InterruptedException
	{
	try{	
		List<WebElement> Field;
		Thread.sleep(2000);
		ExecResult = "PASS"; ErrorDesc = " ";	
		

		WebElement OptionSelection1 = driver.findElement(By
				.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[2]/input"));
		
		OptionSelection1.click();
		
		String FieldNameXpath = ".//div[text()='" + Column + "']";
		if ((Field = new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
			int length = Field.size();
			WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
			Field2ndArr[length - 1].click();
	}} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Column(String Column) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 455, 2);
			ExcelWriter(ExecResult, ErrorDesc, 455, 3);
	//		System.out.println("Column(String Column) fail in finally" + ErrorDesc);

		}
	
	}
	public void Fieldvalue(String FieldValue) throws IOException, InterruptedException
	{
		try{
		List<WebElement> Field;
		Thread.sleep(2000);
		ExecResult = "PASS"; ErrorDesc = " ";	
		//WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
				//.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));

		// .xpath(".//*[@id='ctl00_m_g_65cfa9f0_5d68_48b8_a23d_f67e870c2d89_ctl00_imgApplyFilter']")));
		//Element.click();

		WebElement OptionSelection2 = driver.findElement(By
				.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[3]/input"));
		OptionSelection2.click();

		
		String FieldNameXpath = ".//div[text()='" + FieldValue + "']";
		if ((Field = new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
			int length = Field.size();
			WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
			Field2ndArr[length - 1].click();
	}
	} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Fieldvalue(String FieldValue) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 454, 2);
			ExcelWriter(ExecResult, ErrorDesc, 454, 3);
	//		System.out.println("Fieldvalue(String FieldValue) fail in finally" + ErrorDesc);

		}
	}
	
	
	public void Searchvalue(String Searchvalue) throws IOException, InterruptedException
	{
		try{
		List<WebElement> Field;
		Thread.sleep(2000);
		ExecResult = "PASS"; ErrorDesc = " ";	
		

		WebElement OptionSelection3 = driver.findElement(By
				.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[4]/div/div/img"));

		OptionSelection3.click();
		
		String FieldNameXpath = ".//div[text()='" + Searchvalue + "']";
		if ((Field = new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
			int length = Field.size();
			WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
			Field2ndArr[length - 1].click();
	}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Searchvalue(String Searchvalue) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 453, 2);
			ExcelWriter(ExecResult, ErrorDesc, 453, 3);
	//		System.out.println("Searchvalue(String Searchvalue) fail in finally" + ErrorDesc);

		}
	}
	
	public void SpecificSearch(int SpecificSearch) throws IOException, InterruptedException
	{
		try{
		List<WebElement> Field;
		Thread.sleep(2000);
		ExecResult = "PASS"; ErrorDesc = " ";	
		

		WebElement OptionSelection4 = driver.findElement(By
				.xpath("//div[4]/div/input"));
		OptionSelection4.click();
		OptionSelection4.clear();
		
		String FieldNameXpath = ".//div[text()='" + SpecificSearch + "']";
		OptionSelection4.sendKeys(FieldNameXpath);
		
		/*if ((Field = new WebDriverWait(driver, 20)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
			int length = Field.size();
			WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
			Field2ndArr[length - 1].click();*/
	} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SpecificSearch(int SpecificSearch) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 452, 2);
			ExcelWriter(ExecResult, ErrorDesc, 452, 3);
		//	System.out.println("SpecificSearch(int SpecificSearch) fail in finally" + ErrorDesc);

		}
	
	}
	public void Particulars(String Particulars) throws IOException, InterruptedException
	{
		try{
			
			List<WebElement> Field;
			Thread.sleep(2000);
			ExecResult = "PASS"; ErrorDesc = " ";	
			//WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
					//.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));

			// .xpath(".//*[@id='ctl00_m_g_65cfa9f0_5d68_48b8_a23d_f67e870c2d89_ctl00_imgApplyFilter']")));
			//Element.click();

			WebElement OptionSelection4 = driver.findElement(By
					.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[4]/div/div/input"));
			OptionSelection4.click();
			//OptionSelection4.sendKeys("Particulars");
			
			String FieldNameXpath = ".//div[text()='" + Particulars + "']";
			if ((Field = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
				int length = Field.size();
				WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();
		}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Particulars(String Particulars) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 451, 2);
			ExcelWriter(ExecResult, ErrorDesc, 451, 3);
	//		System.out.println("Particulars(String Particulars) fail in finally" + ErrorDesc);

		}
		
	}
/*	public void Column1(String field1) throws IOException
	{
		 //Initialize data table 
		DataTable table = null;
		 List<List<String>> data = table.raw();
	      System.out.println(data.get(1).get(1)); 
		List<WebElement> Field;
		int CurrentRow;
		ExecResult = "PASS"; ErrorDesc = " ";	
		try{
			WebElement OptionSelection1 = driver.findElement(By
					.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[2]/input"));
			OptionSelection1.click();
			OptionSelection1.sendKeys(data.get(1).get(0));

			WebElement Field1_Date = OptionSelection1.findElement(By.xpath(".//div[text()='" + field1 + "']"));
			Field1_Date.click();
			String FieldNameXpath = ".//div[text()='" + field1 + "']";
			
			
			
			if ((Field = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
		WebElement Field1_Date = driver.findElement(By.xpath(".//div[text()='" + field1 + "']"));
		Field1_Date.click();
				
				int length = Field.size();
				WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			//}
			WebElement FilterField2 = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[3]/input")));
			FilterField2.click();

			String FieldValueXpath = ".//div[text()='" + field2 + "']";
			if ((Field = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValueXpath)))) != null) {
				
				WebElement Field2_Date = driver.findElement(By.xpath(".//div[text()='" + field2 + "']"));
				Field2_Date.click();
	
				int length = Field.size();
				WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
			WebElement FilterFieldDrop = (WebElement) (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
							"//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[4]/div/div/input")));
			FilterFieldDrop.click();

			String FieldValue1Xpath = ".//div[text()='" + field3 + "']";
			if ((Field = new WebDriverWait(driver, 10)
					.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValue1Xpath)))) != null) {
				
				WebElement Field3_Date = driver.findElement(By.xpath(".//div[text()='" + field3 + "']"));
				Field3_Date.click();
				int length = Field.size();
				WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
				Field2ndArr[length - 1].click();

			}
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Error_in_Data_Message() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 421, 2);
			ExcelWriter(ExecResult, ErrorDesc, 421, 3);

		}
		
	}*/
	
//////////////END////////////////////////////////https://fusioncharts.jira.com/browse/SPC-1579////////////////////////////////////

	public void List_Dropdown() throws IOException
	{
	try{
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		WebElement DataSelectionDropdown = driver.findElement(By.xpath("//input[contains(@id,'dsEntity')]"));
		// Assert.assertNotNull(DataSelectionDropdown);

		DataSelectionDropdown.click();

		Thread.sleep(4500);
		driver.findElement(By.xpath("html/body/div/div/div[text()='Stocks']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("List_Dropdown() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 449, 2);
			ExcelWriter(ExecResult, ErrorDesc, 449, 3);
		//	System.out.print("List_Dropdown() finally passed ");

		}
	}
	
	
	public void List_View_Dropdown()throws IOException
	{
	try{
		
		WebElement SelectListView = driver.findElement(By.id("dsView"));
		// Assert.assertNotNull("SelectListView");
		SelectListView.click();
		driver.findElement(By.xpath("html/body/div/div/div[text()='WithoutMS']")).click();
		
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("List_View_Dropdown() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 448, 2);
			ExcelWriter(ExecResult, ErrorDesc, 448, 3);
		//	System.out.print("List_View_Dropdown() finally passed ");

		}

	}
	public void Another_Chart_Navigation() throws InterruptedException
	{
		Thread.sleep(2000);
		driver.navigate().to("http://pc-nibir2010/SitePages/GroupData_Test.aspx");
	}
	
	public void First_Date(String FirstDate) throws IOException
	{
	try{
		List<WebElement> Field;
		Thread.sleep(2000);
		ExecResult = "PASS"; ErrorDesc = " ";	
		//String FieldNameXpath = ".//div[text()='" + FirstDate + "']";
		
		//WebElement div = driver.findElement(By.id("filteringStep"));
		//div.click();

		List<WebElement> OptionSelection4 = driver.findElements(By.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[4]/div/div/input"));
		OptionSelection4.get(0).click();
		OptionSelection4.get(0).clear();
		OptionSelection4.get(0).sendKeys(FirstDate);
	
		//OptionSelection4.get(3).sendKeys(FieldNameXpath);
		
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("First_Date(String FirstDate)fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 447, 2);
			ExcelWriter(ExecResult, ErrorDesc, 447, 3);
		//	System.out.print("First_Date(String FirstDate) finally passed ");

		}
	}
	
	public void Second_Date(String SecondDate) throws IOException
	{
	try{
		List<WebElement> Field;
		Thread.sleep(2000);
		ExecResult = "PASS"; ErrorDesc = " ";	
		

		WebElement OptionSelection4 = driver.findElement(By
				.xpath("//div[4]/div/div[2]/input"));
		OptionSelection4.click();
		OptionSelection4.clear();
		
		//String FieldNameXpath = ".//div[text()='" + SecondDate + "']";
		OptionSelection4.sendKeys(SecondDate);
			} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Second_Date(String SecondDate)fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 446, 2);
			ExcelWriter(ExecResult, ErrorDesc, 446, 3);
	//		System.out.print("Second_Date(String SecondDate) finally passed ");

		}
	}
	
	public void Date(String Date) throws IOException
	{
	try{
		List<WebElement> Field;
		Thread.sleep(2000);
		ExecResult = "PASS"; ErrorDesc = " ";	
		

		WebElement OptionSelection4 = driver.findElement(By
				.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[4]/div/div/input"));
		OptionSelection4.click();
		OptionSelection4.clear();
		
		//String FieldNameXpath = ".//div[text()='" + Date + "']";
		OptionSelection4.sendKeys(Date);
		
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Date(String Date)fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 445, 2);
			ExcelWriter(ExecResult, ErrorDesc, 445, 3);
			//System.out.print("Date(String Date) finally passed ");

		}
	}
	public void Contains_BeginsWith(String With) throws IOException
	{
	try{
	List<WebElement> Field;
	Thread.sleep(2000);
	ExecResult = "PASS"; ErrorDesc = " ";	
	//WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
			//.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));

	// .xpath(".//*[@id='ctl00_m_g_65cfa9f0_5d68_48b8_a23d_f67e870c2d89_ctl00_imgApplyFilter']")));
	//Element.click();

	WebElement OptionSelection2 = driver.findElement(By
			.xpath("//div[@id='filteringStep']/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div/div[4]/div/input"));
	OptionSelection2.click();
	OptionSelection2.clear();
	
	//String FieldNameXpath = ".//div[text()='" + Value + "']";
	OptionSelection2.sendKeys(With);
	
	} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Contains_BeginsWith(String With)fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 444, 2);
			ExcelWriter(ExecResult, ErrorDesc, 444, 3);
			//System.out.print("Contains_BeginsWith(String With) finally passed ");

		}
}
	public void PublishingSiteNavigate()
	{
		driver.navigate().to("http://pc-nibir2010:8080/Pages/Test666.aspx");

	}
	
	public void PublishingSiteNavigateAdminSignIn() throws IOException
	{
	try{
		driver.navigate().to("http://pc-nibir2010:8080/Pages/Test666.aspx");
		Runtime.getRuntime().exec("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\AdminSignIn.exe");
       } catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("PublishingSiteNavigateAdminSignIn()fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 443, 2);
			ExcelWriter(ExecResult, ErrorDesc, 443, 3);
			//System.out.print("PublishingSiteNavigateAdminSignIn() finally passed ");

		}
	}
	
	public void CaptureImage() throws IOException, InterruptedException
	{
	try{
		Thread.sleep(2000);
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		  //The below method will save the screen shot in d drive with name
		 FileUtils.copyFile(scrFile, new File("D:\\screenshot.png")); 
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("CaptureImage()fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("Sharepoint2016SignIn() fail in catch:" +
			// ErrorDesc);
			//ExcelWriter(ExecResult, ErrorDesc, 442, 2);
			//ExcelWriter(ExecResult, ErrorDesc, 442, 3);
			// System.out.print("CaptureImage() finally passed ");

		}
	}

	public void ArkaSignIn()
	{
		try{
			driver.findElement(By.xpath(".//*[@id='ctl00_IdWelcome_ExplicitLogin']")).click();
			Runtime.getRuntime().exec("D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\ArkaSignIn.exe");
		    Thread.sleep(2000);
		    
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Sharepoint2016SignIn()fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ArkaSignIn() fail in catch:" +
			// ErrorDesc);
			//ExcelWriter(ExecResult, ErrorDesc, 441, 2);
			//ExcelWriter(ExecResult, ErrorDesc, 441, 3);
			// System.out.print(" ArkaSignIn() finally passed ");

		}
		
		}
	public void ExcelUrlPS() throws IOException
	{
		try {
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://pc-nibir2010:8080/Documents/Best Selling Music Artist.xlsx");
			/// Urlfield.sendKeys(ExcelName);
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ExcelUrlPS() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ExcelUrlPS() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 440, 2);
			ExcelWriter(ExecResult, ErrorDesc, 440, 3);
		//	System.out.print(" ExcelUrlPS() finally passed ");

		}
			
	}
	public void SheetNmaePS() throws IOException
	{
	try{
		WebElement SheetChoose = driver.findElement(By.xpath("//input[contains(@id,'xlsSheetName')]"));
		Assert.assertNotNull("SheetChoose");
		SheetChoose.click();
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[text()='Artists']")).click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SheetNmaePS() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ExcelUrlPS() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 439, 2);
			ExcelWriter(ExecResult, ErrorDesc, 439, 3);
		//	System.out.print(" SheetNmaePS() finally passed ");

		}
	}
	
	public void FirstDynamicFilter(String DynamicFilterFirst) throws IOException
	{
	try{
		ExcelUtil ex;
		List<WebElement> Field;
		//int CurrentRow;
		
		Thread.sleep(2000);
		WebElement Element = (WebElement) (new WebDriverWait(driver, 10)).until(ExpectedConditions
				.presenceOfElementLocated(By.xpath("//div[@id='chartPanelContainer']/div/div/img[2]")));

		
		Element.click();

		WebElement OptionSelection1 = driver.findElement(By
				.xpath("//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/input"));
		OptionSelection1.click();

	
		String FieldNameXpath = ".//div[text()='" + DynamicFilterFirst + "']";
		if ((Field = new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldNameXpath)))) != null) {
			int length = Field.size();
			WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
			Field2ndArr[length - 1].click();

		}} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("FirstDynamicFilter(String DynamicFilterFirst) fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ExcelUrlPS() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 438, 2);
			ExcelWriter(ExecResult, ErrorDesc, 438, 3);
		//	System.out.print(" FirstDynamicFilter(String DynamicFilterFirst) finally passed ");

		}
	}
	
	public void SecondDynamicFilter(String DynamicFilterSecond) throws IOException
	{
	try{
	Thread.sleep(4500);
	ExcelUtil ex;
	List<WebElement> Field;
	WebElement FilterField2 = (WebElement) (new WebDriverWait(driver, 10))
			.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
					"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[3]/input")));
	FilterField2.click();

	String FieldValueXpath = ".//div[text()='" + DynamicFilterSecond + "']";
	if ((Field = new WebDriverWait(driver, 10)
			.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValueXpath)))) != null) {
		int length = Field.size();
		WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
		Field2ndArr[length - 1].click();

	}} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SecondDynamicFilter(String DynamicFilterSecond) fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("ExcelUrlPS() fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 437, 2);
			ExcelWriter(ExecResult, ErrorDesc, 437, 3);
		//	System.out.print(" SecondDynamicFilter(String DynamicFilterSecond) finally passed ");

		}
		
	}
	public void DynamicFilterSearch(String DynamicFilterSearch)throws IOException
	{
		try{
		Thread.sleep(4500);
		ExcelUtil ex;
		List<WebElement> Field;
		WebElement FilterField2 = (WebElement) (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
						"//div[@class='x-window-mr']/div/div/div/div/div/div/div/div/div/div/div/div/div[4]/div/div/img")));
		FilterField2.click();

		String FieldValueXpath = ".//div[text()='" + DynamicFilterSearch + "']";
		if ((Field = new WebDriverWait(driver, 10)
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(FieldValueXpath)))) != null) {
			int length = Field.size();
			WebElement[] Field2ndArr = Field.toArray(new WebElement[length]);
			Field2ndArr[length - 1].click();
			}
			} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("DynamicFilterSearch(String DynamicFilterSearch) fail in catch:" + ErrorDesc);
		} finally {
			/// System.out.print("DynamicFilterSearch(String DynamicFilterSearch) fail in catch:" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 436, 2);
			ExcelWriter(ExecResult, ErrorDesc, 436, 3);
		//	System.out.print(" DynamicFilterSearch(String DynamicFilterSearch) finally passed ");

		}
	}
	
	
	
	/////////////////////ODBC Database connection///////////////////START/////////////////
	
	public void ODBC_data_provider() throws IOException
	{
	try {
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			Thread.sleep(1000);
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[8]"));
			DropDownValue.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ODBC_data_provider() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 483, 2);
			ExcelWriter(ExecResult, ErrorDesc, 483, 3);
		//	System.out.println("Oracle_Database() fail in finally:" + ErrorDesc);
			
	}
	}
	
	
	public void ODBC_ConnectionString()throws IOException
	{
	
	try{
	     Thread.sleep(1000);
		driver.findElement(By.id("connectionString")).clear();
		driver.findElement(By.id("connectionString")).sendKeys("Driver={SQL Server};Server=ccsptestsvr;Database=FusionChartsDB;Uid=sa;Pwd=P@ssw0rd;");
	} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ODBC_ConnectionString() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 484, 2);
			ExcelWriter(ExecResult, ErrorDesc, 484, 3);
		//	System.out.println("Oracle_Database() fail in finally:" + ErrorDesc);
			
	}
	}
	
	
	public void ODBC_Query()throws IOException
	{   
	try{    
	       // Thread.sleep(2000);
	       driver.findElement(By.id("dsOdbcSelQuery")).clear();
	       driver.findElement(By.id("dsOdbcSelQuery")).sendKeys("select * from dbo.FC_Products");
	       } catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ODBC_Query() fail in catch:" + ErrorDesc);
		} finally {
			// System.out.print("GoToUrl() fail in catch :" + ErrorDesc);
			ExcelWriter(ExecResult, ErrorDesc, 485, 2);
			ExcelWriter(ExecResult, ErrorDesc, 485, 3);
		//	System.out.println("Oracle_Database() fail in finally:" + ErrorDesc);
			
	}
	
	}
	
	
	
	
	/////////////////////ODBC Database connection///////////////////END/////////////////
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	/////////////////////SingleSeriesPie2D/////////////
	
	public void SingleSeriesPie2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Pie2D')]"));
       //img src="http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Pie2D.jpg
			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesPie2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 486, 2);
			ExcelWriter(ExecResult, ErrorDesc, 486, 3);

		}

	}
	
	/////////////////////SingleSeriesPie3D/////////////

public void SingleSeriesPie3DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Pie3D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesPie3DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 487, 2);
			ExcelWriter(ExecResult, ErrorDesc, 487, 3);

		}

   }


	/////////////////////SingleSeriesDoughnut2D/////////////

	public void SingleSeriesDoughnut2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Doughnut2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesDoughnut2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 488, 2);
			ExcelWriter(ExecResult, ErrorDesc, 488, 3);

		}

	}


	/////////////////////SingleSeriesFunnel/////////////
	
	
	
	public void SingleSeriesFunnelChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Funnel.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesFunnelChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 489, 2);
			ExcelWriter(ExecResult, ErrorDesc, 489, 3);

		}

	}
	
		/////////////////////SingleSeriesWaterfall2D/////////////
	
	
	public void SingleSeriesWaterfall2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Waterfall2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesWaterfall2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 490, 2);
			ExcelWriter(ExecResult, ErrorDesc, 490, 3);

		}

	}

		/////////////////////SingleSeriesPareto2D/////////////
	
	public void SingleSeriesPareto2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Pareto2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesPareto2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 491, 2);
			ExcelWriter(ExecResult, ErrorDesc, 491, 3);

		}

	}
	
	/////////////////////SingleSeriesPareto3D////////////////
	
	public void SingleSeriesPareto3DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Pareto3D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Multi Series Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesPareto3DChart()) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 492, 2);
			ExcelWriter(ExecResult, ErrorDesc, 492, 3);

		}

	}
	
	
	
	/////////////////////SingleSeriesKagi////////////////
	
	public void SingleSeriesKagiChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Kagi.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesKagiChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 493, 2);
			ExcelWriter(ExecResult, ErrorDesc, 493, 3);

		}

	}
	
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
		/////////////////////MultiSeriesLine2D////////////////
	
	
	public void MultiSeriesLine2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSLine.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLine2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 494, 2);
			ExcelWriter(ExecResult, ErrorDesc, 494, 3);

		}

	}
	
			/////////////////////MultiSeriesArea2D////////////////
	
	


	public void SingleSeriesMSAreaChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSArea.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("SingleSeriesMSAreaChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 495, 2);
			ExcelWriter(ExecResult, ErrorDesc, 495, 3);

		}

	}
	
	
	
			/////////////////////MultiSeriesBar2D////////////////
	
	


	public void MultiSeriesBar2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSBar2D.jpg')]"));

			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesBar2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 496, 2);
			ExcelWriter(ExecResult, ErrorDesc, 496, 3);

		}

	}
	
				/////////////////////MultiSeriesBar3D////////////////
	
	
	
	public void MultiSeriesBar3DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSBar3D.jpg')]"));

			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesBar3DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 497, 2);
			ExcelWriter(ExecResult, ErrorDesc, 497, 3);

		}

	}
	
	

	
			/////////////////////MultiSeriesSpline////////////////
	
	
	
	public void MultiSeriesSplineChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSSpline.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesSplineChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 498, 2);
			ExcelWriter(ExecResult, ErrorDesc, 498, 3);

		}

	}
		/////////////////////MultiSeriesSplineArea////////////////
	
	
	
	
	public void MultiSeriesSplineAreaChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSSplineArea.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesSplineAreaChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 499, 2);
			ExcelWriter(ExecResult, ErrorDesc, 499, 3);

		}

	}
	
		/////////////////////MultiSeriesLogarithmline////////////////
	
	
	
	
	
		public void MultiSeriesLogarithmlineChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/LogMSLine.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLogarithmlineChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 500, 2);
			ExcelWriter(ExecResult, ErrorDesc, 500, 3);

		}

	}
	
			/////////////////////MultiSeriesLogarithmicColumn////////////////
	
		public void MultiSeriesLogarithmicColumnChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/LogMSColumn2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesLogarithmicColumnChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 501, 2);
			ExcelWriter(ExecResult, ErrorDesc, 501, 3);

		}

	}
				/////////////////////MultiSeriesRadar////////////////
				
		public void MultiSeriesRadarChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Radar.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesRadarChart()) fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 502, 2);
			ExcelWriter(ExecResult, ErrorDesc, 502, 3);

		}

	}
					/////////////////////MultiSeriesInverseArea////////////////
	
	
		public void MultiSeriesInverseAreaChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/InverseMSArea.jpg')]"));

			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesInverseAreaChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 503, 2);
			ExcelWriter(ExecResult, ErrorDesc, 503, 3);

		}

	}
		
	
	
			/////////////////////MultiSeriesInverse2DColumn////////////////
	
	
	public void MultiSeriesInverse2DColumnChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/InverseMSColumn2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesInverse2DColumnChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 504, 2);
			ExcelWriter(ExecResult, ErrorDesc, 504, 3);

		}

	}
	
				/////////////////////MultiSeriesInverse2DLine////////////////
	
	
	
	
	public void MultiSeriesInverse2DLineChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/InverseMSLine.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesInverse2DLineChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 505, 2);
			ExcelWriter(ExecResult, ErrorDesc, 505, 3);

		}

	}
	

	
	/////////////////////MultiSeriesMarimekko////////////////
	
	
	
	
	public void MultiSeriesMarimekkoChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Marimekko.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesMarimekkoChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 506, 2);
			ExcelWriter(ExecResult, ErrorDesc, 506, 3);

		}

	}
	
/////////////////////MultiSeriesZoomLine////////////////
	
	
	
	
	public void MultiSeriesZoomLineChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/ZoomLine.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesZoomLineChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 507, 2);
			ExcelWriter(ExecResult, ErrorDesc, 507, 3);

		}

	}
		
	
		
/////////////////////MultiSeriesStepLine////////////////
	
	
	
	
	public void MultiSeriesStepLineChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSStepLine.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeriesStepLineChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 508, 2);
			ExcelWriter(ExecResult, ErrorDesc, 508, 3);

		}

	}
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/////////////////////StackedColumn2D////////////////
	
	
	
	
	public void StackedColumn2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/StackedColumn2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("StackedColumn2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 509, 2);
			ExcelWriter(ExecResult, ErrorDesc, 509, 3);

		}

	}
	
	
	/////////////////////StackedColumn3D////////////////



	public void StackedColumn3DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/StackedColumn3D.jpg')]"));
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("StackedColumn3DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 510, 2);
			ExcelWriter(ExecResult, ErrorDesc, 510, 3);

		}

	}






	
		/////////////////////StackedArea2D////////////////
	
	
	
	
	public void StackedArea2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/StackedArea2D.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("StackedArea2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 511, 2);
			ExcelWriter(ExecResult, ErrorDesc, 511, 3);

		}

	}
	
			/////////////////////StackedBar2D////////////////
	
	public void StackedBar2DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/StackedBar2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("StackedBar2DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 512, 2);
			ExcelWriter(ExecResult, ErrorDesc, 512, 3);

		}

	}
	
				/////////////////////StackedBar3D////////////////
	
	
	
	public void StackedBar3DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/StackedBar3D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Multi Series Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("StackedBar3DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 513, 2);
			ExcelWriter(ExecResult, ErrorDesc, 513, 3);

		}

	}
	
	
	
	
			/////////////////////2DSingleYCombination////////////////
	//MSCombi2D
	
		public void TwoDSingleYCombinationChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSCombi2D.jpg')]"));

	

			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TwoDSingleYCombinationChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 514, 2);
			ExcelWriter(ExecResult, ErrorDesc, 514, 3);

		}

	}
				/////////////////////3DSingleYCombination////////////////
	
	
		public void ThreeDSingleYCombinationChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSCombi3D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ThreeDSingleYCombinationChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 515, 2);
			ExcelWriter(ExecResult, ErrorDesc, 515, 3);

		}

	}
	
	//MSColumnLine3D
					/////////////////////Column 3D+Line Single Y////////////////
	
	
		public void MSColumnLine3DChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSColumnLine3D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MSColumnLine3DChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 516, 2);
			ExcelWriter(ExecResult, ErrorDesc, 516, 3);

		}

	}
	
		/////////////////////Column 3D + line Dual y////////////////
	
	public void Column3DLineDYChart() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSColumn3DLineDY.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Column3DLineDYChart() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 517, 2);
			ExcelWriter(ExecResult, ErrorDesc, 517, 3);

		}

}
	
			/////////////////////Stacked Column 3D+Line Dual Y////////////////
	
	
		public void StackedColumn3DLineDualY() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSStackedColumn2DLineDY.jpg')]"));


			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("StackedColumn3DLineDualY() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 518, 2);
			ExcelWriter(ExecResult, ErrorDesc, 518, 3);

		}

}
	/////////////////////2D Dual Y Combination////////////////



		public void TwoDDualYCombination() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSCombiDY2D.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TwoDDualYCombination() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 519, 2);
			ExcelWriter(ExecResult, ErrorDesc, 519, 3);

		}

}








	/////////////////////2D Stacked Column line(Dual Y Axis)////////////////
	
	
	//StackedColumn2DLine
	
	public void TwoDStackedColumnlineDualY() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/MSStackedColumn2DLineDY.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TwoDStackedColumnlineDualY() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 520, 2);
			ExcelWriter(ExecResult, ErrorDesc, 520, 3);

		}

}

	/////////////////////2D Stacked Column line(Single Y Axis)////////////////
	
	
	//StackedColumn2DLine
	
	public void TwoDStackedColumnSingleY() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/StackedColumn2DLine.jpg')]"));

			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("TwoDStackedColumnSingleY() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 521, 2);
			ExcelWriter(ExecResult, ErrorDesc, 521, 3);

		}

}

	/////////////////////3D Stacked Column line(Single Y Axis)////////////////


public void ThreeDStackedColumnLineSingleY() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/StackedColumn3DLine.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ThreeDStackedColumnLineSingleY() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 522, 2);
			ExcelWriter(ExecResult, ErrorDesc, 522, 3);

		}

}
//////////////////////////////////////////////////////////////////////////////////////

	/////////////////////Scatter (XY Plot)/////////////////////////


public void ScatterXYPlot() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Scatter.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ScatterXYPlot() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 523, 2);
			ExcelWriter(ExecResult, ErrorDesc, 523, 3);

		}

}


	/////////////////////////////Bubble/////////////////////////////

	//Bubble
	public void Bubble() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/Bubble.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("Bubble() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 524, 2);
			ExcelWriter(ExecResult, ErrorDesc, 524, 3);

		}

}

///////////////////////////////////////////////////////////////////////////////////////////////////////

///ScrollColumn2D

public void ScrollColumn2D() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/ScrollColumn2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ScrollColumn2D() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 525, 2);
			ExcelWriter(ExecResult, ErrorDesc, 525, 3);

		}

}

////ScrollLine2D

public void ScrollLine2D() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/ScrollLine2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ScrollLine2D() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 526, 2);
			ExcelWriter(ExecResult, ErrorDesc, 526, 3);

		}

}

///ScrollArea2D

public void ScrollArea2D() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/ScrollArea2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ScrollArea2D() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 527, 2);
			ExcelWriter(ExecResult, ErrorDesc, 527, 3);

		}

}


///ScrollStackedColumn2D

public void ScrollStackedColumn2D() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/ScrollStackedColumn2D.jpg')]"));

		
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ScrollStackedColumn2D() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 528, 2);
			ExcelWriter(ExecResult, ErrorDesc, 528, 3);

		}

}

/////ScrollCombi2D
	
	
	public void ScrollCombi2D() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/ScrollCombi2D.jpg')]"));

			
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("ScrollCombi2D() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 529, 2);
			ExcelWriter(ExecResult, ErrorDesc, 529, 3);

		}

}
////////ScrollCombiDY2D

	public void ScrollCombiDY2D() throws IOException {
		try {
			WebElement Chart = driver.findElement(By.xpath(
					"//img[contains(@src,'http://pc-nibir2010//_layouts/CollabionCharts/resources/images/charts/ScrollCombiDY2D.jpg')]"));

			try {

				Assert.assertTrue(Chart.isDisplayed());
				Assert.assertTrue(driver.getPageSource().contains("Multi Series Column 3D"));

			} catch (AssertionError e) {

				// String message = e.getMessage();
				System.out.println("Multi Series Column3D Chart Not Found: " + e.getMessage());
			}
			Chart.click();
		} catch (Exception e) {
			ExecResult = "FAIL";
			ErrorDesc = e.getMessage();
			System.out.println("MultiSeries3DChartChoose() fail in catch" + ErrorDesc);
		} finally {

			ExcelWriter(ExecResult, ErrorDesc, 530, 2);
			ExcelWriter(ExecResult, ErrorDesc, 530, 3);

		}

}


}	



















