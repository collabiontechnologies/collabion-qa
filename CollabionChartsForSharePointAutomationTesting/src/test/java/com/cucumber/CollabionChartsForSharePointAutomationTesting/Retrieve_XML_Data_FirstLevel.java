package com.cucumber.CollabionChartsForSharePointAutomationTesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.w3c.dom.Document;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class Retrieve_XML_Data_FirstLevel {

	// private static final int CurrentRowNumber = 0;

	public static void ExcelWriter(int Row, int Col, String ExcelPath, String SheetName, String Data)
			throws IOException {
		File src = new File(ExcelPath);

		FileInputStream fis = new FileInputStream(src);

		XSSFWorkbook wb = new XSSFWorkbook(fis);

		XSSFSheet sh1 = wb.getSheet(SheetName);

		Cell c1 = sh1.getRow(Row).createCell(Col);
		c1.setCellValue(Data);
		;

		FileOutputStream outFile = new FileOutputStream(new File(ExcelPath));

		wb.write(outFile);

		outFile.close();
	}

	private static final String ValueListArr = null;
	private static final int[] Revenues = null;

	public static void main(String argv[]) {
		int[] ValueListArr = null;
		try {

			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File("SpecficDataFinderFirst.XML"));
			// Document doc = docBuilder.parse (new
			// File("SpecficDataFinderSecond.XML"));

			// normalize text representation
			doc.getDocumentElement().normalize();
			System.out.println("Root element of the doc is " + doc.getDocumentElement().getNodeName());

			NodeList categories = doc.getElementsByTagName("category");
			System.out.println("Total no of categories : " + categories.getLength());

			for (int x = 0, size = categories.getLength(); x < size; x++) {
				System.out.println(categories.item(x).getAttributes().getNamedItem("label").getNodeValue());
			}

			NodeList revenues = doc.getElementsByTagName("set");
			System.out.println("Total no of revenues : " + revenues.getLength());

			int i = 0;
			while (i < revenues.getLength()) {

				// System.out.println(categories.item(x).getAttributes().getNamedItem("label").getNodeValue()
				// + "-" +
				// revenues.item(x).getAttributes().getNamedItem("value").getNodeValue());
				String Revenue = revenues.item(i++).getAttributes().getNamedItem("value").getNodeValue();

				System.out.println(Revenue);

				ExcelWriter(i + 1, 3,
						"D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\AutomationTest.xlsx",
						"Sheet1", Revenue);

				// ExcelWriter(i+3,3,
				// "D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\AutomationTest.xlsx","Sheet2",Revenue);

			}

		} catch (SAXParseException err) {
			System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());

		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();

		} catch (Throwable t) {
			t.printStackTrace();
		}
		// System.exit (0);

	}
}// end of main

/*
 * 
 * 
 * // Create an array with room for 100 integers int[] nums = new
 * int[revenues.getLength()]; for(int x=0,size= revenues.getLengt h(); x<size;
 * x++) {
 * 
 * 
 * 
 * String Revenue =
 * revenues.item(nums[0]).getAttributes().getNamedItem("value").getNodeValue();
 * // System.out.println(Revenue);
 * 
 * String Revenue1 =
 * revenues.item(nums[1]).getAttributes().getNamedItem("value").getNodeValue();
 * // System.out.println(Revenue1);
 * 
 * String Revenue2 =
 * revenues.item(nums[2]).getAttributes().getNamedItem("value").getNodeValue();
 * // System.out.println(Revenue2);
 * 
 * 
 * switch (x) { case 1 : //String Revenue1 =
 * revenues.item(0).getAttributes().getNamedItem("value").getNodeValue();
 * System.out.println(Revenue1);
 * 
 * case 2 : //String Revenue2 =
 * revenues.item(1).getAttributes().getNamedItem("value").getNodeValue();
 * System.out.println(Revenue1);
 * 
 * case 3 : //String Revenue3 =
 * revenues.item(1).getAttributes().getNamedItem("value").getNodeValue();
 * System.out.println(Revenue2); break; // default: return "";
 * 
 * // break;
 * 
 * // Fill it with numbers using a for-loop // for (int i = 0; i < nums.length;
 * i++) // nums[i] = i + 1;
 * 
 * 
 * 
 * 
 * //ex=new ExcelUtil(
 * "D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\Book1.xlsx",
 * "Sheet1"); File src=new File(
 * "D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\AutomationTest.xlsx"
 * ); FileInputStream fis=new FileInputStream(src); XSSFWorkbook wb=new
 * XSSFWorkbook(fis); XSSFSheet sh1= wb.getSheetAt(0);
 * 
 * sh1.getRow(0).getCell(5).setCellValue(Revenue);
 * //sh1.getRow(CurrentRowNumber).createCell(6).setCellValue(wb.
 * getCreationHelper().createRichTextString(ErrorMessage));;
 * 
 * 
 * FileOutputStream outFile =new FileOutputStream(new File(
 * "D:\\Work\\QA-Collabion\\CollabionChartsForSharePointAutomationTesting\\AutomationTest.xlsx"
 * )); wb.write(outFile); outFile.close();
 * 
 * 
 * 
 * 
 * //break;
 * 
 * //System.out.println(revenues.item(1).getAttributes().getNamedItem("value").
 * getNodeValue()); //break;
 * 
 * //System.out.println(revenues.item(x).getAttributes().getNamedItem("value").
 * getNodeValue());
 * //System.out.println(categories.item(x).getAttributes().getNamedItem("label")
 * .getNodeValue() + "-" +
 * revenues.item(x).getAttributes().getNamedItem("value").getNodeValue());
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * Node node = null; HSSFRow datarow = null; //for (int i = 0; i <
 * nodeList.getLength(); i++) { // On each loop you get the value of node item
 * node = revenues.item(x); //For every new node list you will create a row
 * datarow = spreadSheet.createRow(x); //Finally set the node value to the
 * columns of the newly created Row
 * 
 * 
 * // }
 * 
 * 
 * //}
 * 
 * 
 * 
 * // }
 * 
 * 
 * 
 * // }
 */
		