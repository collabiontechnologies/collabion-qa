package com.cucumber.CollabionChartsForSharePointAutomationTesting;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;




@RunWith(Cucumber.class)
@CucumberOptions(

strict = false, format = { "pretty", "html:target/html/", "json:target/cucumber.json" },

features ="src/test/resource",
//////////////START////////////SP2016////////CCSP-Pro Version///////////////////////

///
tags={"@SP16CCSP2.4Regression,@SP2016ExcelListRegression,@SP2016SharePointListRegression,@SP2016CSVListRegression,@SP2016ExcelNT"}

//////////////END////////////SP2016////////CCSP-Pro Version///////////////////////


//////////START////////////////SP2010///////CCSP-Pro Version////////////////////////

///tags={"@ExcelNew2.4Regression,@CSVRegression,@SharePointListRegression,@DataSourceOracleDatabase,@DataSourceODBCDatabase,@ChartCosmeticsRegression,@JiraRegression"}

///////////END///////////////SP2010///////CCSP-Pro Version////////////////////////



 ///tags={"@ChartTypeAutomation"}
///tags={"@ChartTypeAutomation"}
//tags={"@DataSourceODBCDatabase"}

//tags={"@ChartCosmeticsRegression"}

///////////////////////////////////////SharePoint 2010 -CCSP Standard Version///////////////

/*tags={"@DataSourceSelection2010,@Grouping2010,@TOP-N-RECORDS2010,@chartConfiguration2010,@VerifyConnectionString2010,@DrilldownNonClickableFieldCheck2010,@WizardDynamicFilterNonClickableFieldCheck2010,"
+"@ExportSettingsNonClickableFieldCheck2010,@DrillDownFeatureNotAvailableVerification2010,@DynamicFilterFeatureNotAvailableVerification2010,@ExportSettingsFeatureNotAvailableVerification2010,"
+"@DrillDownSelectionBarColorVerification2010,@DynamicFilterSelectionBarColorVerification2010,@ExportSettingsSelectionBarColorVerification2010"}

*/
//////////***********2010

/*tags={   "@DataSourceExcelListWithCurrentSite,@WizardFilterDataDrivenTesting,@SeriesFunction,@WizardFirstDrilldownSetWithGroupByFields,@ExcelListMultiLevelDrillDownUI,@TextFilterDataDrivenTesting,@ChoiceFilterDataDrivenTesting,@DynamicFilterDataDrivenTesting, @WizardDynamicFilterSecond, @CombinationOfText&DynamicFilter-UI,@ChoiceFilter&DynamicFilterCombination,"
		+ "@Hotspot,@DrillDownNavigateToLink, @QuerStringFilterWebPart&WizardDrilldownQueryStringCombination,@NoDrillDown,@ExportSettings,@Macro/DynamicTitle,@DataSourceExcel,@WizardFirstDrilldownSetWithGroupByDate,@WizardDynamicFilterFirst,@SharepointListFilter,"
        +"@DataSourceSharepointlist,@SeriesFunction(Sharepointlist),@WizardFirstDrilldownSetWithGroupByFields(Sharepointlist),@MultiLevelDrillDownUI(Sharepointlist),@TextFilterDataDrivenTesting(Sharepointlist)2010,@ChoiceFilterDataDrivenTesting(Sharepointlist)2010,@DynamicFilterDataDrivenTesting(Sharepointlist),@CombinationOfText&DynamicFilter-UI(Sharepointlist),@ChoiceFilter&DynamicFilterCombination(Sharepointlist),"
     //   + "@Hotspot(Sharepointlist),@DrillDownNavigateToLink(Sharepointlist),@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(Sharepointlist),@NoDrillDown(Sharepointlist),@ExportSettings(Sharepointlist),@Macro/DynamicTitle(Sharepointlist),"
        +"@DataSourceSQLServer,@SeriesFunction(SQLServerDatabase),@WizardFirstDrilldownSetWithGroupByFields(SQLServerDatabase),@SQLServerDatabaseMultiLevelDrillDownUI(SQLServerDatabase),@TextFilterDataDrivenTesting(SQLServerDatabase)2010,@ChoiceFilterDataDrivenTesting(SQLServerDatabase)2010,@CombinationOfText&DynamicFilter-UI(SQLServerDatabase),@ChoiceFilter&DynamicFilterCombination(SQLServerDatabase),"
     //   + "@Hotspot(SQLServerDatabase),@DrillDownNavigateToLink(SQLServerDatabase),@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(SQLServerDatabase),@NoDrillDown(SQLServerDatabase),@ExportSettings(SQLServerDatabase),@Macro/DynamicTitle(SQLServerDatabase),@WizardFirstDrilldownSetWithGroupByDate(SQLServerDatabase)c,@SQLServerNegativeDataSourceTesting,"
       +"@MultiSeriesColumn3D,@SingleSeriesColumn2D,@ExcelDataSelectionRegression,@SharepointListDataSelectionRegression,@CSVDataSelectionRegression,@MicrosoftSQlServerDataSelectionRegression,@SQLServerNegativeDataSourceTesting,@ExcelDataNegativeTestCase,@SharepointListDataNegativeTestCase,@CSVDataNegativeTestCase"}
   //@MultiSeriesColumn3D,@MultiSeriesColumn2D,@SingleSeriesColumn3D,@SingleSeriesColumn2D



*/

/*tags={"@DataSourceSelection,@Grouping,@TOP-N-RECORDS,@chartConfiguration,@VerifyConnectionString,@DrilldownNonClickableFieldCheck,@WizardDynamicFilterNonClickableFieldCheck,"
+"@ExportSettingsNonClickableFieldCheck,@DrillDownFeatureNotAvailableVerification,@DynamicFilterFeatureNotAvailableVerification,@ExportSettingsFeatureNotAvailableVerification,"
+"@DrillDownSelectionBarColorVerification,@DynamicFilterSelectionBarColorVerification,@ExportSettingsSelectionBarColorVerification,@CollabionChartLicenseActivation"
*/



      
        
//////////////////////////////***************CCSP Standard Version-SharePoint 2016 Server***********************///////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//tags={"@SeriesFunction(SharePoint2016)"}

//tags ={"@DrilldownNonClickableFieldCheck,@WizardDynamicFilterNonClickableFieldCheck,@ExportSettingsNonClickableFieldCheck,@DrillDownFeatureNotAvailableVerification,@DynamicFilterFeatureNotAvailableVerification,@ExportSettingsFeatureNotAvailableVerification,@DrillDownSelectionBarColorVerification,@DynamicFilterSelectionBarColorVerification,@ExportSettingsSelectionBarColorVerification,@CollabionChartLicenseActivation"}

//tags={"@DataSourceSelection,@Grouping,@TOP-N-RECORDS,@chartConfiguration,@VerifyConnectionString,@DrilldownNonClickableFieldCheck,@WizardDynamicFilterNonClickableFieldCheck,@ExportSettingsNonClickableFieldCheck,@DrillDownFeatureNotAvailableVerification,@DynamicFilterFeatureNotAvailableVerification,@ExportSettingsFeatureNotAvailableVerification,@DrillDownSelectionBarColorVerification,@DynamicFilterSelectionBarColorVerification,@ExportSettingsSelectionBarColorVerification,@CollabionChartLicenseActivation"}



////////////////////*********************tags for SharePoint 2016 Server********************//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////2016
//tags={"@DataSourceExcelList(SharePoint2016),@SeriesFunction(SharePoint2016),@WizardFirstDrilldownSetWithGroupByFields(SharePoint2016),@ExcelListMultiLevelDrillDownUI(SharePoint2016),@TextFilterDataDrivenTesting(SharePoint2016),@ChoiceFilterDataDrivenTesting(SharePoint2016),@WizardDynamicFilterSecond(SharePoint2016),@CombinationOfText&DynamicFilter-UI(SharePoint2016),@DynamicFilterDataDrivenTesting(SharePoint2016),@ChoiceFilter&DynamicFilterCombination(SharePoint2016),@ChoiceFilter&DynamicFilterCombination(SharePoint2016),@Hotspot(SharePoint2016),@DrillDownNavigateToLink(SharePoint2016),@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(SharePoint2016),@NoDrillDown(SharePoint2016),@SharepointlistDataSource(SharePoint2016),"
//		+ "@SeriesFunction(Sharepointlist)(SharePoint2016),@WizardFirstDrilldownSetWithGroupByFields(Sharepointlist)(SharePoint2016),@MultiLevelDrillDownUI(Sharepointlist)(SharePoint2016),@TextFilterDataDrivenTesting(Sharepointlist)(SharePoint2016),@ChoiceFilterDataDrivenTesting(Sharepointlist)(SharePoint2016),@WizardDynamicFilterSecond(Sharepointlist)(SharePoint2016),@CombinationOfText&DynamicFilter-UI(Sharepointlist)(SharePoint2016),@DynamicFilterDataDrivenTesting(Sharepointlist)(SharePoint2016),@ChoiceFilter&DynamicFilterCombination(Sharepointlist)(SharePoint2016),@Hotspot(Sharepointlist)(SharePoint2016),@DrillDownNavigateToLink(Sharepointlist)(SharePoint2016),@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination(Sharepointlist)(SharePoint2016),@NoDrillDown(Sharepointlist)(SharePoint2016),@CSVDataSelectionRegression(SharePoint2016),"
//        + "@CSVDataNegativeTestCase(SharePoint2016),@CSVDataNegativeTestCase(SharePoint2016),@ExcelDataSelectionRegression(SharePoint2016),"
//        + "@ExcelDataNegativeTestCase(SharePoint2016),@ExcelDataNegativeTestCase(SharePoint2016),@ExcelDataNegativeTestCase(SharePoint2016),@SharepointListDataSelectionRegression(SharePoint2016),@SharepointListDataNegativeTestCase(SharePoint2016),@ChartTypeMultiSeriesColumn2D(SharePoint2016)"}
//       
//        //+ "@MultiSeriesColumn2D(SharePoint2016),@MultiSeriesColumn3D(SharePoint2016),@SingleSeriesColumn2D(SharePoint2016),@SingleSeriesColumn3D(SharePoint2016)"}
//



////////////////////************tags for SharePoint 2010 Server********************//////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*

tags = { "@DataSourceExcelList,@SeriesFunction,@WizardFirstDrilldownSetWithGroupByFields,@MultiLevelDrillDownUI,@TextFilterDataDrivenTesting,@ChoiceFilterDataDrivenTesting,@DynamicFilterDataDrivenTesting,@WizardDynamicFilterSecond,@CombinationOfText&DynamicFilterUI,@ChoiceFilter&DynamicFilterCombination,"

+ "@Hotspot,@DrillDownNavigateToLink,@QuerStringFilterWebPart&WizardDrilldownQueryStringCombination ,@NoDrillDown, @ExportSettings,@Macro/DynamicTitle,"

+ "@DataSourceSharepointlist,@SeriesFunction(Sharepointlist),@WizardFirstDrilldownSetWithGroupByFields(Sharepointlist),@MultiLevelDrillDownUI(Sharepointlist),@TextFilterDataDrivenTesting(Sharepointlist),@ChoiceFilterDataDrivenTesting(Sharepointlist),@DynamicFilterDataDrivenTesting(Sharepointlist),@WizardDynamicFilterSecond(Sharepointlist),@CombinationOfText&DynamicFilterUI(Sharepointlist),@ChoiceFilter&DynamicFilterCombination(Sharepointlist),"

+ "@DataSourceSQLServer,@SeriesFunction(SQLServerDatabase),@WizardFirstDrilldownSetWithGroupByFields(SQLServerDatabase),@MultiLevelDrillDownUI(SQLServerDatabase),@TextFilterDataDrivenTesting(SQLServerDatabase),@ChoiceFilterDataDrivenTesting(SQLServerDatabase),@DynamicFilterDataDrivenTesting(SQLServerDatabase),@WizardDynamicFilterSecond(SQLServerDatabase),@CombinationOfText&DynamicFilterUI(SQLServerDatabase),@ChoiceFilter&DynamicFilterCombination(SQLServerDatabase),"

+ "@MultiSeriesColumn3D,@MultiSeriesColumn2D,@SingleSeriesColumn3D,@SingleSeriesColumn2D,@ExcelDataSelectionRegression,@SharepointListDataSelectionRegression,@CSVDataSelectionRegression,@MicrosoftSQlServerDataSelectionRegression,@SQLServerDataNegativeTestCase,@ExcelDataNegativeTestCase,@SharepointListDataNegativeTestCase,@CSVDataNegativeTestCase" }


*/



)
public class RunnerTest {

}