package com.cucumber.CollabionChartsForSharePointAutomationTesting;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

//import javax.mail.BodyPart;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.Multipart;
//import javax.mail.Session;
//import javax.mail.Transport;
//import javax.mail.internet.InternetAddress;
//import javax.mail.internet.MimeBodyPart;
//import javax.mail.internet.MimeMessage;
//import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
//import org.xml.sax.InputSource;
//import org.xml.sax.SAXException;
import org.xml.sax.SAXException;

import org.im4java.core.CompareCmd;
import org.im4java.process.StandardStream;
import org.im4java.core.IMOperation;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

import jxl.Workbook;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WriteException;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import cucumber.api.java.it.E;

import java.util.*;
//import javax.mail.*;  
//import javax.mail.internet.*;  
import javax.activation.*;
import com.cucumber.CollabionChartsForSharePointAutomationTesting.DriverFactoryPage;
import com.cucumber.CollabionChartsForSharePointAutomationTesting.ExcelUtil;








public class CCSP_StandardVersion_POM extends DriverFactoryPage {
	
	public void CCSP() throws InterruptedException, IOException {
		

		WebElement rdShowData = driver.findElement(By.id("rdShowData"));
		
			Assert.assertNotNull(rdShowData);
			
			try {
				Assert.assertTrue(rdShowData.isEnabled());
			}
			catch (Exception e) {
				System.out.println("This radio button is disabled" + e.getMessage());
			}
			Thread.sleep(4000);
			rdShowData.click();
	}





	public void filter_data_dynamically() throws InterruptedException, IOException {
	

		Thread.sleep(4000);
		WebElement dyFilterChk = driver.findElement(By.id("dyFilterChk"));
		dyFilterChk.click();
		Assert.assertTrue(dyFilterChk.isEnabled());
		// dyFilterChk.sendKeys(Keys.ARROW_DOWN);;
		// openURLin.sendKeys(Keys.ENTER);;
		

	}


	public void Enable_printing_of_chart() throws IOException {
		
		WebElement export_div = driver.findElement(By.id("exportStep"));

		List<WebElement> checkbox_div = export_div.findElements(By.tagName("input"));

		for (int i = 0; i < checkbox_div.size(); i++) {

			checkbox_div.get(0).click();
            Assert.assertTrue(checkbox_div.get(0).isEnabled());
			break;

			/* txtExportFileName */
		}
	}
	
	
    public void CCSPLicense() {
			
    	driver.navigate().to("http://Administrator:P@ssw0rd@ccsptestsvr:2016/_admin/CollabionCharts/CollabionChartActivationPage.aspx");
			
		}




	public void CCSPLicenseAdd() {
		CCSPLicense();
		WebElement AddLicense = driver.findElement(By.cssSelector("a.ms-addnew"));
    	AddLicense.click();
		
	}

	public void CCSPLicenseSelectServer() throws InterruptedException {
		WebElement SelectServerdropdown = driver.findElement(By.cssSelector("input#cmbServer.x-form-text.x-form-field.x-trigger-noedit"));
		SelectServerdropdown.click();
		Thread.sleep(500);
		WebElement SelectServerName = driver.findElement(By.cssSelector("input#cmbServer.x-form-text.x-form-field.x-trigger-noedit"));
		SelectServerName.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(600);
		SelectServerName.sendKeys(Keys.ENTER);
		//SelectServer.sendKeys("ccsptestsvr");
		
	}
	public void CCSPLicenseKey() {
		WebElement LicenseKey = driver.findElement(By.cssSelector("input#txtLicenseKey.x-form-text.x-form-field"));
		LicenseKey.sendKeys("keys");
		
	}
	
	public void Add(){
		
		WebElement LicenseKey = driver.findElement(By.xpath("//button[text()='Add']"));
		LicenseKey.click();
	}

	public void SQLAuthentication()
	{
		WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'winAuthnentication')]"));
		DataProvider.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		String jQuerySelector = "CCSP.jQuery(\".x-combo-list-item:eq(8)\").click();";
		String findDropdown = "return " + jQuerySelector;
		System.out.println(findDropdown);
		js.executeScript(findDropdown);
		
	}
	
	public void WindowsAuthentication() throws InterruptedException
	{
		WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'winAuthnentication')]"));
		DataProvider.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		String jQuerySelector = "CCSP.jQuery(\".x-combo-list-item:eq(9)\").click();";
		String findDropdown = "return " + jQuerySelector;
		System.out.println(findDropdown);
		js.executeScript(findDropdown);
		
	}
	
    public void DomainAuthentication()
	{
    	WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'winAuthnentication')]"));
		DataProvider.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;

		String jQuerySelector = "CCSP.jQuery(\".x-combo-list-item:eq(10)\").click();";
		String findDropdown = "return " + jQuerySelector;
		System.out.println(findDropdown);
		js.executeScript(findDropdown);
	}
    public void DomainName()
   	{
    	WebElement DomainName = driver.findElement(By.cssSelector("input#sqlDomain.x-form-text.x-form-field"));
    	DomainName.clear();
    	DomainName.sendKeys(".");
   	}
    public void UserName()
   	{
    	WebElement UserName = driver.findElement(By.cssSelector("input#sqlLogin.x-form-text.x-form-field"));
    	UserName.clear();
    	UserName.sendKeys("Administrator");
   	}
    public void Password()
   	{
    	WebElement Password = driver.findElement(By.cssSelector("input#sqlPass.x-form-text.x-form-field"));
    	Password.clear();
    	Password.sendKeys("P@ssw0rd");
   	} 
    public void getCssValue_Color() throws InterruptedException
    {
    	/*WebElement googleSearchBtn = driver.findElement(By.xpath("//div/li[4]/ul/li[1]/div/a/span")); 
		System.out.println("Color of a button before mouse hover: "	+ googleSearchBtn.getCssValue("color"));*/
		
		
		
		String color = driver.findElement(By.cssSelector("div.x-tree-node-el.x-tree-node-leaf.x-unselectable.step-locked-node.x-tree-selected")).getCssValue("background-color");
		   
		System.out.println("The background color of Drill Down  "+ color);
    }
    
    public void VerificationAlert() throws InterruptedException
    {
    	WebElement VerificationAlert = driver.findElement(By.cssSelector("div.overlayFeatures-child2"));
    	VerificationAlert.getText();
    	Assert.assertTrue(driver.getPageSource().contains("Sorry! This feature is available in"));
    	System.out.println("Sorry! This feature is available in");
    	Thread.sleep(500);
    	Assert.assertTrue(driver.getPageSource().contains("Pro Mode "));
    	System.out.println("Pro Mode ");
    	Thread.sleep(550);
    	Assert.assertTrue(driver.getPageSource().contains("only"));
    	System.out.println("only");
    
    	
    	//VerificationAlert.getText();
    	
    	/*String expected_String = "Sorry! This feature is available in";

    	String actual_String = driver.findElement(By.xpath("//span[@class='overlayFeatures-txt']")).getText();

    	if(expected_String.equals(actual_String))

    	{

    	System.out.println("Text is Matched");

    	}

    	else

    	{

    	System.out.println("Text is not Matched");

    	}*/
    	
    	
    	
    }
    /*This function is responsible for the chart editing, This corresponds to the background Step
	 * Xpath1: "//span[text()='Edit']" :is the sharepoint edit button Xpath.
	 * Xpath2: "//*[@id='WebPartWPQ2_MenuLink']": is the Edit arrow Xpath.
	 * Xpath3: "//span[text()='Edit This Chart']": Edit Chart Option Xpath of collabion Charts.
	 * */	
		public void ChartEditing()
		{
			driver.findElement(By.xpath("//span[text()='Edit']")).click();
		    driver.findElement(By.xpath("//*[@id='WebPartWPQ2_MenuLink']")).click();
			driver.findElement(By.xpath("//span[text()='Edit This Chart']")).click();
		}
		
	//**************************************** Excel Data Source Selection*********************************************************************
		
		/* public void ExcelDataselection():This method is responsible for the Selection of Excel File from the Data Provider Dropdown. 
		 		*Relevant XPATHS:
		 		  XPATH1: "//input[contains(@id, 'dsProvider')]" =The drop down arrow Xpath.
		 		  XPATH2: ".//*[@class='x-combo-list-inner']/div[6]"=The Excel file option is a div.
		 
		 *public void ExcelPathSelection():This method provides the path of the excel.
		         *Relevant XPATHS:
		            XPATH1: "//input[contains(@id,'xlsFileUrl')]":Text Box for Inputing the URL.
		 *public void ClickLoad() LOads the excel.
		 		*Relevant Xpaths:
		 		   Xpath1:  "//button[text()='Load']"=Load button Xpath.   
		            
		 */

		public void ExcelDataselection()
		{
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			WebElement DataProvider = driver.findElement(By.xpath("//input[contains(@id, 'dsProvider')]"));
			Assert.assertNotNull(DataProvider);
			DataProvider.click();
			WebElement DropDownValue = driver.findElement(By.xpath(".//*[@class='x-combo-list-inner']/div[6]"));
			DropDownValue.click();
		}
		public void ExcelPathSelection()
		{
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://ccsptestsvr/Shared Documents/SalesDataSheet.xlsx");
		}
		public void ExcelPathSelection2010()
		{
			WebElement Urlfield = driver.findElement(By.xpath("//input[contains(@id,'xlsFileUrl')]"));
			Assert.assertNotNull("Urlfield");
			Urlfield.clear();
			Urlfield.click();
			Urlfield.sendKeys("http://pc-nibir2010/Shared Documents/SalesDataSheet.xlsx");
		}
		public void ClickLoad() throws InterruptedException
		{
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			Thread.sleep(1000);
			driver.findElement(By.xpath("//button[text()='Load']")).click();
			Thread.sleep(1000);
		}
		/*	public void OkClick():Clicks the ok buton which appears in a dialgue after clicking the LOAD button.
		 *  Ok BUtton Xpath:"//button[text()='OK']"
		 * 
		 */
		
		public void OkClick()
		{
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Assert.assertNotNull(driver.findElement(By.xpath("//button[text()='OK']")));
			driver.findElement(By.xpath("//button[text()='OK']")).click();
		}
		
		/*public void sheetSelection() selects the required sheet after loadind data.
		 * SHEETSELECTION BY CLASSNAME:"x-panel-header"
		 * SHEETSELECTION INPUT XPATH:"//input[contains(@id,'xlsSheetName')]"
		 */
		public void sheetSelection() throws InterruptedException
		{
			WebElement SheetChoose = driver.findElement(By.xpath("//input[contains(@id,'xlsSheetName')]"));
			Assert.assertNotNull("SheetChoose");
			SheetChoose.click();
			Thread.sleep(2000);
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div/div/div[text()='MOCK_DATA']"))).click();
		
			/*SheetChoose.sendKeys(Keys.ENTER);
			Assert.assertNotNull(driver.findElement(By.className("x-panel-header")));
			driver.findElement(By.className("x-panel-header")).click();
			// SheetChoose.sendKeys(Keys.ARROW_DOWN);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			SheetChoose.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			
			//Assert.assertNotNull(driver.findElement(By.xpath("//div[text()='MOCK_DATA']")));
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div/div/div[text()='MOCK_DATA']"))).click();
		*/}
		
		/*public void ConnectingExcel() CLICKS THE CONNECT BUTTON
		 * CONNECT button Xpath:"//button[text()='Connect']"
		 * 
		 */
		
		public void ConnectingExcel() throws InterruptedException
		{
			Thread.sleep(5000);
			driver.findElement(By.xpath("//button[text()='Connect']")).click();
			
		}
		
	//***************************************************************************************************************************************************************************************
		
	//***************************************************************************Field Selection**********************************************************************************************
		
		
		/*public void selecFields()  selects all the fields 
		 * Select Fields Xpath: //div/li[1]/ul/li[2]/div/a/span
		 * All field Checking Checkbox CSSPath:div.cc-x-grid3-hdchecker
		 * Apply Button Xpath://button[text()='Apply']
		 */
		public void selecFields() throws InterruptedException
		{

			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			Thread.sleep(5000);
			Assert.assertNotNull(driver.findElement(By.xpath("//div/li[1]/ul/li[2]/div/a/span")));
			driver.findElement(By.xpath("//div/li[1]/ul/li[2]/div/a/span")).click();
			Thread.sleep(5000);
			WebElement field = driver.findElement(By.cssSelector("div.cc-x-grid3-hdchecker"));
			field.click();
			driver.findElement(By.xpath("//button[text()='Apply']")).click();
		}
	//******************************************************************************************************************************************************************************************
	 
		
	//******************************************************************************Grouping Function**************************************************************************************************
		
		/*public void groupingStatutorymessage() This function is responsible for navigating to the group BY menu and checking whether the Statutory " Available in Pro Mode only" message is being displayed.
		 * The Group By menu left panel navigator XPath: //div/li[2]/ul/li[2]/div/a/span
		 * The Message Xpath: //div[@id='groupingStep']//div[@class='stndrdMsg']//../span
		 */
		public void groupingStatutorymessage() throws InterruptedException
		{
			Thread.sleep(5000);
			driver.findElement(By.xpath("//div/li[2]/ul/li[2]/div/a/span")).click();;
			WebElement Msg= driver.findElement(By.xpath("//div[@id='groupingStep']//div[@class='stndrdMsg']//../span"));
			String MSG1=Msg.getAttribute("innerHTML");
			System.out.println(MSG1);
			System.out.println(MSG1.equals(" Available in Pro Mode only"));
			
			Assert.assertTrue("Correct Message displayed", MSG1.equals(" Available in Pro Mode only"));
		}
		
		/*public void enableGrouping()  Clicks the Grouping and clears all grouping applied previously.
		 * Clear Grouping Button XPATH://button[text()='Yes']
		 * Pop-up Yes Button XPATH: //button[text()='Yes']
		 * The Enable Grouping Checkbox lOCATOR:By.name("groupBy-checkbox")
		 */
		
		public void enableGrouping() throws InterruptedException
		{
			WebElement button = (WebElement)new WebDriverWait(driver,10).until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Clear Grouping']")));
			if(button!=null)
			button.click();
			else
				;
			
			WebDriverWait waitList = new WebDriverWait(driver, 60);
			waitList.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[text()='Yes']")));
	        
			WebElement button1= driver.findElement(By.xpath("//button[text()='Yes']"));
			button1.click();
			
			
			WebElement FirstCheckBox = driver.findElement(By.name("groupBy-checkbox"));

			FirstCheckBox.click();

			Thread.sleep(5000);
			
		}
		/*public void GroupByFieldSelection()Selects the group BY field.
		 * The field Dropdown Locator is :By.name("gd-combogroup")
		 */
		public void GroupByFieldSelection()
		{
			

			WebElement FirstDropdown = driver.findElement(By.name("gd-combogroup"));

			try {
				Assert.assertTrue(FirstDropdown.isEnabled());
				Assert.assertNotNull(FirstDropdown);
				} catch (AssertionError e) {

						System.out.println("Groupdata First Dropdown Not Found: " + e.getMessage());
				  }
				FirstDropdown.click();
					
				FirstDropdown.sendKeys(Keys.ARROW_DOWN);
					
				FirstDropdown.sendKeys(Keys.ENTER);
		}

		/*
		 * public void NegativeGroupSeriesTest() :clicks the radio button which is not supposed to work.
		 * Negative Test Input radio xpath://input[@id='//input[@id='radioSplit']
		 * Dialogue Box Text Span Xpath://div[@class='x-window-mr']/div/div/div/div[2]/span
		 */
		public void NegativeGroupSeriesTest()
		{
			WebElement TempTester;
			WebElement NegativeRadio=driver.findElement(By.xpath("//input[@id='radioSplit']"));
			NegativeRadio.click();
			TempTester=new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='x-window-mr']/div/div/div/div[2]/span")));
			
			Assert.assertTrue("Dialgue Box APPEARED", TempTester.getText().equals("This feature is not supported with the current license type."));
			driver.findElement(By.xpath("//div[@class='x-window-br']/div/div/div/table/tbody/tr/td/table//button[text()='OK']")).click();
			
		}
		/* public void negativeAgrregateFunctionTest() checks the negative test cases for aggregate function
		 * Revenue Aggregate Function dropdown selector XPATH://div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Revenue']/../../td[2]/div /// editor://div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input
		 * Average function div xpath:"//span[text()='AVERAGE']/.."
		 * Min function div XPATH:"//span[text()='MIN']/.."
		 * MAX fucntion div XPATH:"//span[text()='MAX']/.."
		 * OK button XPATH: "//div[@class='x-window-br']/div/div/div/table/tbody/tr/td/table//button[text()='OK']"
		 */
	public void negativeAgrregateFunctionAVGTest() throws InterruptedException
	{
		//Clicks on the revenue dowpdown.
		WebElement DropDown=driver.findElement(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Revenue']/../../td[2]/div"));
		DropDown.click();
		
		//Clicks on the revenue drop down Editor.
		WebElement Editor=new WebDriverWait(driver, 12).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input")));
		Editor.click();
		
		//Picks the Color of the three AGGREGATE fucntion DIV
	    String COLORAVG=driver.findElement(By.xpath("//span[text()='AVERAGE']/..")).getCssValue("background-color");
	    String COLORMIN=driver.findElement(By.xpath("//span[text()='MIN']/..")).getCssValue("background-color");
	    String COLORMAX=driver.findElement(By.xpath("//span[text()='MAX']/..")).getCssValue("background-color");
	    if(COLORAVG.equals(COLORMAX) && COLORMAX.equals(COLORMIN))
	    Assert.assertEquals("THE BACK GROUND COLOR FOR AVERGE,MIN,MAX IS CORRECT", COLORMAX, "rgba(255, 226, 192, 1)");
	    System.out.println(COLORAVG);
	    //Selectes Average Function.
	    
//		Editor.sendKeys(Keys.ARROW_DOWN);
//		Editor.sendKeys(Keys.ARROW_DOWN);
//		Editor.sendKeys(Keys.ENTER);
	    driver.findElement(By.xpath("//div/span[text()='AVERAGE']")).click();
		
		//Checks for the POP-UP MESSAGE AND VERIFIES THE DISPLAYED TEXT.->
		WebElement TempTester=new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='x-window-mr']/div/div/div/div[2]/span")));
		
		Assert.assertTrue("Average Function is working", TempTester.getText().equals("This feature is not supported with the current license type."));
		driver.findElement(By.xpath("//div[@class='x-window-br']/div/div/div/table/tbody/tr/td/table//button[text()='OK']")).click();
		
		

	}
	public void negativeAgrregateFunctionMINTest() throws InterruptedException
	{
		
		WebElement DropDown=driver.findElement(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Revenue']/../../td[2]/div"));
		DropDown.click();
		
		//Clicks on the revenue drop down Editor.
		WebElement Editor=new WebDriverWait(driver, 12).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input")));
		Editor.click();
		
		//Picks the Color of the three AGGREGATE fucntion DIV
	    String COLORMIN=driver.findElement(By.xpath("//span[text()='MIN']/..")).getCssValue("background-color");
	    
	    
	    Assert.assertEquals("THE BACK GROUND COLOR FOR AVERGE,MIN,MAX IS CORRECT", COLORMIN, "rgba(255, 226, 192, 1)");

		//CHECKS FOR MIN FUNCTITON
	
		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(2000);
		Editor.sendKeys(Keys.ENTER);
		WebElement TempTester=new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='x-window-mr']/div/div/div/div[2]/span")));
        Assert.assertTrue("Min Function is working", TempTester.getText().equals("This feature is not supported with the current license type."));
		driver.findElement(By.xpath("//div[@class='x-window-br']/div/div/div/table/tbody/tr/td/table//button[text()='OK']")).click();
		
	}
	
	public void negativeAgrregateFunctionMAXTest() throws InterruptedException
	{
		WebElement DropDown=driver.findElement(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Revenue']/../../td[2]/div"));
		DropDown.click();
		
		//Clicks on the revenue drop down Editor.
		WebElement Editor=new WebDriverWait(driver, 12).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input")));
		Editor.click();
		
		//Picks the Color of the three AGGREGATE fucntion DIV
	   	String COLORMAX=driver.findElement(By.xpath("//span[text()='MAX']/..")).getCssValue("background-color");
	   
	    Assert.assertEquals("THE BACK GROUND COLOR FOR AVERGE,MIN,MAX IS CORRECT", COLORMAX, "rgba(255, 226, 192, 1)");


		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ARROW_DOWN);
		
		Thread.sleep(2000);
		Editor.sendKeys(Keys.ENTER);
		WebElement TempTester=new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='x-window-mr']/div/div/div/div[2]/span")));
		Assert.assertTrue("Max Function is working", TempTester.getText().equals("This feature is not supported with the current license type."));
		driver.findElement(By.xpath("//div[@class='x-window-br']/div/div/div/table/tbody/tr/td/table//button[text()='OK']")).click();
		

	}
	
	/*public void PositiveAggregateFunctionTesting() aggregate function positive test cases checking.
	 * Revenue Dropdown FIELD XPATH://div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Revenue']/../../td[2]/div
	 * Revenue Editor Xpath: //div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input
	 */

	public void PositiveAggregateFunctionTesting() throws InterruptedException
	{
		WebElement DropDown=driver.findElement(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]//table//tr/td[1]/div[text()='Revenue']/../../td[2]/div"));
		DropDown.click();
		WebElement Editor=new WebDriverWait(driver, 12).until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='gd-gdGroupGrid']/div/div/div/div[1]/div[2]/div[2]/div/input")));
		Editor.click();
		Editor.sendKeys(Keys.ARROW_DOWN);
		Editor.sendKeys(Keys.ENTER);
		Thread.sleep(5000);
		WebElement ButtonPanel = driver.findElement(By.id("groupingStep"));
		List<WebElement> Button = ButtonPanel.findElements(By.className("x-btn"));
		Assert.assertNotNull(Button.get(5));
		Button.get(5).click();
		Button.get(5).click();
		Button.get(5).click();
		WebElement FinishButton = driver.findElement(By.xpath("//button[text()='Finish']"));
		FinishButton.click();

	}
	
/*
 * public void topnrecordsOptionbackgroundCheck() checks whether the background color of the top n records are orange.
 * The options selector div XPATH://div/li[2]/ul/li[3]/div/a/span/../..
 */
	public void topnrecordsOptionbackgroundCheck() throws InterruptedException
	{
		Thread.sleep(2000);
		WebElement Topnrec_div=driver.findElement(By.xpath("//div/li[2]/ul/li[3]/div/a/span/../.."));
		String background=Topnrec_div.getCssValue("background-color");
		System.out.println(Topnrec_div.getClass());
		System.out.println("TOP N COLOR"+background);
		Assert.assertTrue(background.equals("rgba(255, 226, 192, 1)"));
		
	}
	/*public void TopnRecordsFuncltyDisbldCheck() checks whether the top n records functionalities are displaed.
	 * By.id("toprecordsStep" selector for the top n records input field 1.
	 * "x-btn" apply button selector in TOP-N records.
	 */
	public void TopnRecordsFuncltyDisbldCheck() throws InterruptedException
	{
		Thread.sleep(3500);
		WebElement Topvalue = driver.findElement(By.id("txtTopValue"));
		
		if(Topvalue.isEnabled())
		{
			Assert.assertTrue("Top n Records functionalities are working",false);
		}
		
	 }

//************************************************************************************************************************************************************************
    
    public void DrillDownNonClickableFieldCheck() throws InterruptedException, IOException {
		

		WebElement rdShowData = driver.findElement(By.id("rdShowData"));
		rdShowData.click();
		try {

			Assert.assertTrue(rdShowData.isEnabled());
			// Assert.assertTrue(driver.getPageSource().contains("Multi Series
			// Column 3D"));

		} catch (AssertionError e) {

			// String message = e.getMessage();
			System.out.println("Drill Down Field Is Not Clickable: " + e.getMessage());
		}

	
		
    }
    
    
    
    
    public void DynamicFilterNonClickableFieldCheck()  throws InterruptedException, IOException {
	
		//Thread.sleep(4000);
		WebElement dyFilterChk = driver.findElement(By.id("dyFilterChk"));
		dyFilterChk.click();
		try{
		Assert.assertTrue(dyFilterChk.isEnabled());
	
		}catch (AssertionError e) {

			// String message = e.getMessage();
			System.out.println("Dynamic Filter Field Is Not Clickable: " + e.getMessage());
		}

		

	}
    
	public void ExportSettingsNonClickableFieldCheck() throws IOException {
	
		WebElement export_div = driver.findElement(By.id("exportStep"));

		List<WebElement> checkbox_div = export_div.findElements(By.className("x-form-checkbox"));

		

			checkbox_div.get(0).click();

		
			try{
	            Assert.assertTrue(checkbox_div.get(0).isEnabled());
			}
		
		catch (AssertionError e) {

			// String message = e.getMessage();
			System.out.println("Export Settings Field Is Not Clickable: " + e.getMessage());
		}

		}
	
	
//******************************************************************************************Auto Refresh**************************************************************************************************************
	public void InitializeAutorefeshProcess()
	{
	
		driver.get("http://ccsptestsvr/");
	}
/*public void ClickDocumentLibrary() clicks on document library 
 * Document Library Xpath://span[text()='Documents']
 * 
 */
	
	public void ClickDocumentLibrary()
	{
		driver.findElement(By.xpath("//span[text()='Documents']")).click();
	}
	/*
	 * public void UploadExcel() clicks on the upload button.
	 *  Upload button Xpath: html/body/form/div[12]/div/div[2]/div[2]/div[3]/div[1]/div/div/div/table/tbody/tr/td/table[1]/tbody/tr/td/div/div/ul[1]/li[2]/button
	 *  Iframe container DIv:"div.ms-dlgFrameContainer" 
	 *
	 */
	
	public void UploadExcel() throws InterruptedException
	{
		driver.findElement(By.xpath("html/body/form/div[12]/div/div[2]/div[2]/div[3]/div[1]/div/div/div/table/tbody/tr/td/table[1]/tbody/tr/td/div/div/ul[1]/li[2]/button")).click();
	    WebElement FrameDiv=driver.findElement(By.cssSelector("div.ms-dlgFrameContainer"));
		WebElement Frame=FrameDiv.findElement(By.tagName("iframe"));
	    driver.switchTo().frame(Frame);
	    		
	    Thread.sleep(2000);
	    //((JavascriptExecutor)driver).executeScript("CheckAssetLibMediaExtension()");
       
	   driver.findElement(By.xpath("html/body/form/div[12]/div/div[2]/div[2]/div[3]/table/tbody/tr[2]/td[2]/table/tbody/tr[1]/td[2]/table/tbody/tr[3]/td[2]/table/tbody/tr[1]/td/input")).click();
	}
	/*public void RunAutoItScript() Runs the AutoIT script for handing windows dialogue.
	 * 
	 */
	
	public void RunAutoItScript() throws IOException
	{
		Runtime.getRuntime().exec("D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\ExcelUpload.exe");
	}
	
	/*	public void ClickOkButton() clicks on the okay button
	 * 
	 * 
	 */
	public void ClickOkButton() throws InterruptedException
	{
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/form/div[12]/div/div[2]/div[2]/div[3]/table/tbody/tr[8]/td/table/tbody/tr/td[2]/input[1]")).click();;
	}
	/*
	 * 
	 */
	
	public void screencapturer() throws IOException, InterruptedException
	{
		driver.get("http://ccsptestsvr/_layouts/15/start.aspx#/SitePages/");
		driver.findElement(By.xpath("html/body/form/div[12]/div/div[2]/div[2]/div[3]/div[1]/div/div/div/table/tbody/tr/td/table[2]/tbody/tr/td/div/a[text()='CollabionStandardAutomation']")).click();
		Thread.sleep(6000);
		WebElement element = driver.findElement(By.xpath("html/body/form/div[12]/div/div[2]/div[2]/div[3]/div[4]/div/div/table/tbody/tr/td/div/div"));
		((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
		//D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\ScreenShots\\Actualscreenshot.png
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File("D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\ScreenShots\\Actualscreenshot.png"));

	}
	public void ccspStndardRdrctn()
	{
		driver.get("http://ccsptestsvr/SitePages/CollabionStandardAutomation.aspx");
		//driver.findElement(By.xpath("//span[text()='Site Pages']")).click(); 
		//driver.findElement(By.xpath("html/body/form/div[12]/div/div[2]/div[2]/div[3]/div[1]/div/div/div/table/tbody/tr/td/table[2]/tbody/tr/td/div/a[text()='CollabionStandardAutomation']")).click();
	}
	public void screenComparerer() throws IOException, InterruptedException
	{
		Thread.sleep(6000);
		WebElement element = driver.findElement(By.xpath("html/body/form/div[12]/div/div[2]/div[2]/div[3]/div[4]/div/div/table/tbody/tr/td/div/div"));
		((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileUtils.copyFile(scrFile, new File("D:\\collabion-qa\\CollabionChartsForSharePointAutomationTesting\\ScreenShots\\Alteredscreenshot.png"));

	}
	
	
	
	
}





	


