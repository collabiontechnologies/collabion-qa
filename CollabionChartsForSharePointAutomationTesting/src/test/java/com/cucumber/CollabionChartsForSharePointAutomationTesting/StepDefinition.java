package com.cucumber.CollabionChartsForSharePointAutomationTesting;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import com.cucumber.CollabionChartsForSharePointAutomationTesting.ExcelUtil;

public class StepDefinition {

	StringBuffer verificationErrors;

	// Initialize your error SringBuffer here

	@Before
	public void initialize() {
		verificationErrors = new StringBuffer();

	}
	// The following is one of the steps in the test case where I need to assert
	// something

	// @When("^the value is (\\d+)$")
	/*
	 * public void the_result_should_be(int arg1) { try { assertEquals(arg1, 0);
	 * } catch(AssertionError ae) { verificationErrors.append(
	 * "Value is incorrect- "+ae.getMessage()); } }
	 */

	@After
	public void afterScenario() {
		if (!(verificationErrors.length() == 0)) {
			verificationErrors.toString();
		}
	}
	///////////////////*********Select Chart Type-to chang the drilldown chart at any phase*************///////////////////////////////////////////////

@Given("^Click on Select Chart type icon$")
public void click_on_Select_Chart_type_icon() throws Throwable {
	POM Chart = new POM();
	Chart.SelectChartTypeAfterDrilldown();
	
}
///////////////////END*********Select Chart Type-to chang the drilldown chart at any phase*************///////////////////////////////////////////////

////////////////////SharePoint2010 Sign In///////////////////

@Given("^Click on Sign In$")
public void click_on_Sign_In() throws Throwable {
	POM Chart = new POM();
	Chart.SharePoint2010SignIn();
}


////////////////////End..........SharePoint2010 Sign In///////////////////


@Then("^go to another site$")
public void go_to_another_site() throws Throwable {
	
	POM Chart = new POM();
	Chart.Another_Chart_Navigation();
}
	////////////////////////// SharePoint 2016///////////////////////////////

   @Then("^Click on SharePoint (\\d+) Chart Import/Export$")
   public void click_on_SharePoint_Chart_Import_Export(int arg1) throws Throwable {
	POM Chart = new POM();
	Chart.SP16Import_ExportChartDropdown();
}

	@Given("^Click on SharePoint SignIn$")
	public void Click_on_Sharepoint_SignIn() throws Throwable {
		POM Chart = new POM();
		Chart.Sharepoint2016SignIn();

	}


	@Then("^Click on SharePoint Page$")
	public void Click_on_Sharepoint_Page() throws Throwable {
		POM Chart = new POM();
		Chart.Sharepoint2016Page();
		Chart.Sharepoint2016Edit();
	}

	@Then("^Click on SharePoint (\\d+) Chart Edit$")
	public void Click_on_SharePoint_Chart_Edit(int arg1) throws Throwable {

		POM Chart = new POM();
		Chart.Sharepoint2016ChartEdit();

	}

	@Given("^Edit Chart$")
	public void edit_Chart() throws Throwable {
		POM Chart = new POM();
		Chart.ChartEdit();
	}

	@Then("^Give New Url of Excel$")
	public void Give_New_Url_of_Excel() throws Throwable {
		Thread.sleep(2000);
		POM Url = new POM();
		Url.NewUrlOfExcel();
	}

	////////// #Data source-Microsoft Excel/////////

	@Given("^Configuration of Web Part$")
	public void Configuration_of_Web_Part() throws Throwable {
		POM WebPart_Configuration = new POM();
		WebPart_Configuration.WebPart_Configuration();
	}

	@Given("^Click on Chart Wizard Dropdown$")
	public void I_click_on_Chart_Dropdown() throws Throwable {
		Thread.sleep(2500);
		// POM CSV_Data=new POM();
		// CSV_Data.CSVData();

		POM Chart = new POM();
		Chart.DropdownClick();

		Thread.sleep(1500);
		// Chart.ExcelSheet();

	}

	
	
///////////Import/Export from Chart Wizard Dropdown///////////////////	

@Given("^Click on Chart Wizard Dropdown of Export/Import$")
public void click_on_Chart_Wizard_Dropdown_of_Export_Import() throws Throwable {
	POM Chart = new POM();
	Chart.Import_ExportChartDropdown();
}

@Then("^Click on Import from Dropdown$")
public void click_on_Import_from_Dropdown() throws Throwable {
	POM Chart = new POM();
	Chart.ImportFromDropdown();
}
@Then("^Select file for import$")
public void select_file_for_import() throws Throwable {
	POM Chart = new POM();
	Chart.SelectfileforImport();
//hread.sleep(1000);
Chart.ImportButtonClick();
}

@Then("^Click on Export Tab$")
public void click_on_Export() throws Throwable {
	POM Chart = new POM();
	Chart.ExportChartTabClick();
}
@Then("^Click on Export Button$")
public void click_on_Export_Button() throws Throwable {
	POM Chart = new POM();
	Chart.ExportButtonClick();
}





///////////Import/Export from Chart Wizard Dropdown///////////////////	
	/*
	 * @Then("^format the data source$") public void format_the_data_source()
	 * throws Throwable {
	 * 
	 * }
	 */
	/*
	 * @Then("^format the Excel to Sharepoint List data source$") public void
	 * format_the_Excel_to_Sharepoint_List_data_source() throws Throwable { POM
	 * format = new POM(); format.CSVtoExcel(); }
	 */
	@Then("^format the Sharepoint list to CSV data source$")
	public void format_the_Sharepoint_list_to_CSV_data_source() throws Throwable {
		POM format = new POM();
		format.SharepointlisttoCSV();

	}

	@Then("^format the CSV to Excel data source$")
	public void format_the_CSV_to_Excel_data_source() throws Throwable {
		POM format = new POM();
		format.CSVtoExcel();

	}

	@Then("^format the Excel to Microsoft SQl server data source$")
	public void format_the_Excel_to_Microsoft_SQl_server_data_source() throws Throwable {
		POM format = new POM();
		format.ExceltoSQL();

	}

	@Then("^format the SQL to Sharepoint List data source$")
	public void format_the_SQL_to_Sharepoint_List_data_source() throws Throwable {
		POM format = new POM();
		format.SQLtoSharepointList();

	}

	/*
	 * @Then("^format the CSV to Microsoft SQl server data source$") public void
	 * format_the_CSV_to_Microsoft_SQl_server_data_source() throws Throwable {
	 * 
	 * Thread.sleep(4000); POM format = new POM(); format.;
	 * 
	 * 
	 * }
	 * 
	 * @Then("^format the Microsoft SQl server to WebParts data source$") public
	 * void format_the_Microsoft_SQl_server_to_WebParts_data_source() throws
	 * Throwable { POM format = new POM();
	 * format.MicrosoftSQlServertoWebParts();
	 * 
	 * 
	 * 
	 * 
	 * }
	 */

	@Then("^choose Excel Data Provider$")
	public void Choose_Source_Data_Provider() throws Throwable {
		Thread.sleep(1000);
		POM Source = new POM();
		Source.SelectExcelDataProvider();
	}

	@Then("^select Network radio button$")
	public void select_Network_radio_button() throws Throwable {
		Thread.sleep(1000);
		POM Source = new POM();
		Source.NetworkRadioButton();
	}

	@Then("^Give User Name of Network$")
	public void give_User_Name_of_Network() throws Throwable {
		Thread.sleep(1000);
		POM Source = new POM();
		Source.NetworkUserName();
	}

	@Then("^Give Password of Network$")
	public void give_Password_of_Network() throws Throwable {
		Thread.sleep(1000);
		POM Source = new POM();
		Source.NetworkPassword();
	}

	@Then("^Give Password of File$")
	public void give_Password_of_File() throws Throwable {
		Thread.sleep(1000);
		POM Source = new POM();
		Source.FilePassword();
	}
	@Then("^Give Url of Network Excel$")
	public void give_Url_of_Network_Excel() throws Throwable {
		Thread.sleep(1000);
		POM Source = new POM();
		Source.UrlOfNetworkExcel();
	}


	@Then("^choose network sheet name$")
	public void choose_network_sheet_name() throws Throwable {
		Thread.sleep(1000);
		POM Source = new POM();
		Source.NetworkSheetNameChoose();
	}
	
	@Then("^Give Url of Excel$")
	public void Give_Url_of_Excel() throws Throwable {
		Thread.sleep(2000);
		POM Url = new POM();
		Url.UrlOfExcel();
	}

	@Then("^click on Load Button$")
	public void click_on_Load_Button() throws Throwable {
		POM Load = new POM();
		Load.LoadButtonClick();

	}

	@Then("^click on ok button$")
	public void click_on_ok_button() throws Throwable {
		POM OK = new POM();
		OK.OKClick();
	}

	@Then("^select Sheet name$")
	public void select_Sheet_name() throws Throwable {
		Thread.sleep(500);
		POM Sheet = new POM();
		Sheet.SheetChoose();
	}

	@Then("^Give Wrong Sheet Range$")
	public void Give_Wrong_Sheet_Range() throws Throwable {
		POM Sheet = new POM();
		Sheet.SheetRange();
	}

	@Then("^Give Wrong URL$")
	public void Give_Wrong_URL() throws Throwable {
		POM Wrong_URL = new POM();
		Wrong_URL.WrongUrlOfExcel();

	}

	@Then("^verify URL$")
	public void verify_URL() throws Throwable {
		POM Wrong_URL = new POM();
		Wrong_URL.Verify_URL();
	}

	@Then("^click on connect button$")
	public void click_on_connect_button() throws Throwable {
		Thread.sleep(1000);
		POM Connect = new POM();
		Connect.connectbutton();
		// Thread.sleep(3000);

	}

	@Then("^click on OK button$")
	public void click_on_OK_button() throws Throwable {
		Thread.sleep(1000);
		POM OK = new POM();
		OK.OKClick();
		// OK.OKClick();
	}

	@Then("^Click View Data button$")
	public void Click_View_Data_button() throws Throwable {
		POM View_Data_button_check = new POM();
		View_Data_button_check.ViewData();
		Thread.sleep(500);

	}

	@Then("^Verify Sheet range$")
	public void Verify_Sheet_range() throws Throwable {
		POM Verify_Sheet_range = new POM();
		Verify_Sheet_range.VerifySheetRange();
		Thread.sleep(500);

	}

	@Then("^go to Select Fields$")
	public void go_to_Select_Fields() throws Throwable {
		Thread.sleep(9000);
		POM button = new POM();
		// button.Experiment123();
		button.ClickSelecfields();
		// button.Experiment123();
	}

	@Then("^ticked all Select Field Checkbox$")
	public void Select_Field_Checkbox() throws Throwable {
		Thread.sleep(1000);
		POM Checkbox = new POM();
		Checkbox.ClickField();
	}

	@Then("^uncheck First field Checkbox$")
	public void uncheck_First_field_Checkbox() throws Throwable {
		Thread.sleep(1500);
		POM FirstCheckbox = new POM();
		FirstCheckbox.FirstClickField();
	}

	@Then("^uncheck fourth field Checkbox$")
	public void uncheck_fourth_field_Checkbox() throws Throwable {
		Thread.sleep(2000);
		POM FourthCheckbox = new POM();
		FourthCheckbox.FourthClickField();
	}

	@Then("^uncheck fifth field Checkbox$")
	public void uncheck_fifth_field_Checkbox() throws Throwable {
		Thread.sleep(2500);
		POM FifthCheckbox = new POM();
		FifthCheckbox.FifthClickField();
	}

	@Then("^uncheck Sixth field Checkbox$")
	public void uncheck_Sixth_field_Checkbox() throws Throwable {
		Thread.sleep(3000);
		POM SixthCheckbox = new POM();
		SixthCheckbox.SixthClickField();
	}

	@Then("^click on Apply button$")
	public void click_on_Apply_button() throws Throwable {
		Thread.sleep(4500);
		POM Button = new POM();
		// Button.Applyclick();
		Button.ApplyButtonClick();
	}

	@Then("^click on Filter Data Apply button$")
	public void click_on_Filter_Data_Apply_button() throws Throwable {
		Thread.sleep(4500);
		POM Button = new POM();
		// Button.Applyclick();
		Button.Applyclick();
		Thread.sleep(2500);
	}

	@Then("^click on Finish button$")
	public void click_on_Finish_button() throws Throwable {
		Thread.sleep(5500);
		POM Button = new POM();
		Button.FinishButtonClick();
		Thread.sleep(6500);
	}

	@Then("^save chart$")
	public void save_chart() throws Throwable {
		POM save_chart = new POM();
		save_chart.save_chart();
	}

	@Then("^delete chart$")
	public void delete_chart() throws Throwable {
		POM delete_chart = new POM();
		delete_chart.delete_chart();
	}
	////////// #SharePoint list source/////////

	@Then("^choose SharePoint List Data Provider$")
	public void choose_SharePoint_List_Data_Provider() throws Throwable {
		POM SharePointDataSource = new POM();
		SharePointDataSource.SelectSharePointDataProvider();
	}

	@Then("^choose data in Select List Dropdown$")
	public void choose_data_in_Select_List_Dropdown() throws Throwable {
		Thread.sleep(2500);
		POM Dropdown = new POM();
		Dropdown.SharePointSelectList();
	}

	@Then("^choose data of SharePoint 2016 in Select List Dropdown$")
	public void choose_data_of_SharePoint_2016_in_Select_List_Dropdown() throws Throwable {
		Thread.sleep(2500);
		POM Dropdown = new POM();
		Dropdown.SharePointSelectList2016();
	}

	@Then("^choose data in Select List View Dropdown$")
	public void choose_data_in_Select_List_View_Dropdown() throws Throwable {

		POM Dropdown = new POM();
		Dropdown.SharePointSelectListView();
	}

	@Then("^Click on Drill Down$")
	public void Click_on_Drill_Down() throws Throwable {
		Thread.sleep(32000);
		POM DrillDown = new POM();
		DrillDown.ClickDrillDown();
	}

	////////////// Micrisoft SQL Server Database////////////////

	@Then("^choose Microsoft SQL Server$")
	public void choose_Microsoft_SQL_Server() throws Throwable {
		POM SQLServer = new POM();
		SQLServer.ChooseSQLServer();
	}

	@Then("^tick on Windows Authentication$")
	public void tick_on_Windows_Authentication() throws Throwable {
		Thread.sleep(1000);
		POM Windows_Authentication = new POM();
		Windows_Authentication.Windows_Authentication();
	}

	@Then("^Click Select Field Apply button$")
	public void Click_Select_Field_Apply_button() throws Throwable {
		POM Select_Field_Apply = new POM();
		Select_Field_Apply.SelectFieldApply();

	}

	@Then("^Verify QueryString Error$")
	public void Verify_QueryString_Error() throws Throwable {
		POM QueryString_Error = new POM();
		QueryString_Error.QueryString_Error();
	}

	@Then("^Give Data in Server Field$")
	public void Give_Data_in_Server_Field() throws Throwable {
		Thread.sleep(10000);
		POM Server_Field = new POM();
		Server_Field.sqlServer();
	}

	@Then("^untick all the checkbox$")
	public void untick_all_the_checkbox() throws Throwable {
		Thread.sleep(10000);
		POM Checkbox = new POM();
		Checkbox.ExcelUntickSelectFields();
	}

	@Then("^Untick all the Checkbox$")
	public void untick_all_the_Checkbox() throws Throwable {
		POM Checkbox = new POM();
		Checkbox.CSVUntickSelectFields();
	}

	@Then("^go to Configuration Page$")
	public void go_to_Configuration_Page() throws Throwable {
		POM Configuration = new POM();
		Configuration.ConfigureSourcePage();
	}

	@Then("^Give Wrong UserName$")
	public void Give_Wrong_UserName() throws Throwable {
		POM Configuration = new POM();
		Configuration.username();
	}

	@Then("^Give Wrong Password$")
	public void Give_Wrong_Password() throws Throwable {
		POM Configuration = new POM();
		Configuration.password();
	}

	@Then("^Give Wrong Server Name$")
	public void Give_Wrong_Server_Name() throws Throwable {
		POM Server_Name = new POM();
		Server_Name.WrongServerName();

	}

	@Then("^Give Wrong Database Name$")
	public void Give_Wrong_Database_Name() throws Throwable {
		POM Database_Name = new POM();
		Database_Name.WrongDatabaseName();
	}

	@Then("^Give Data in Database field$")
	public void Give_Data_in_Database_field() throws Throwable {
		Thread.sleep(10000);
		POM Database_field = new POM();
		Database_field.sqlDbase();
	}

	@Then("^Select data table$")
	public void Select_data_table() throws Throwable {
		POM data_table = new POM();
		data_table.DataTable();
	}
	/////////////////////// CSV File////////////////////////////

	@Then("^Choose CSV File$")
	public void Choose_CSV_File() throws Throwable {
		Thread.sleep(5000);
		POM CSV_File = new POM();
		CSV_File.ChooseCSVFile();
	}

	@Then("^Select Radio Button of static CSV data$")
	public void Select_Radio_Button_of_static_CSV_data() throws Throwable {

		POM Radio_Button = new POM();
		Radio_Button.CSVDataStoredRadioButton();

	}

	@Then("^Give data in CSV Data Field$")
	public void Give_data_in_CSV_Data_Field() throws Throwable {

		POM CSV_Data = new POM();
		CSV_Data.CSVScanner();
	}

	@Then("^Click on Parse Data Button$")
	public void Click_on_Parse_Data_Button() throws Throwable {

		POM Parse_Data = new POM();
		Parse_Data.Parse_Data();
	}

	@Then("^Close Wizard$")
	public void Close_Wizard() throws Throwable {
		POM Parse_Data = new POM();
		Parse_Data.CloseWizard();
	}

	@Then("^Clear CSVData Field$")
	public void Clear_CSVData_Field() throws Throwable {
		POM Clear_CSVData_Field = new POM();
		Clear_CSVData_Field.ClearCSVDataField();
	}

	@Then("^Verify CSV Data field$")
	public void Verify_CSV_Data_field() throws Throwable {
		POM Verify = new POM();
		Verify.Verify_CSV();
	}

	/////////////////////// Web Parts in Current Page////////////

	@Then("^choose Web Parts In Current Page Data Provider$")
	public void choose_Web_Parts_In_Current_Page_Data_Provider() throws Throwable {
		POM Current_Page_Data_Provider = new POM();
		Current_Page_Data_Provider.Current_Page_Data_Provider();
	}

	@Then("^Click on Get Provider Parts$")
	public void Click_on_Get_Provider_Parts() throws Throwable {
		POM Get_Provider_Parts = new POM();
		Get_Provider_Parts.Get_Provider_Parts();
	}

	@Then("^Select Current Web part$")
	public void Select_Current_Web_part() throws Throwable {
		POM Current_Web_part = new POM();
		Current_Web_part.Current_Web_part();

	}

	//////// #Filter Data////////

	@Then("^I click on Select Fields$")
	public void I_click_on_Checkbox() throws Throwable {
		POM SF = new POM();
		SF.ClickSelecfields();
		// SF.ClickSelecfields();

	}

	@Then("^Click on Filter Data$")
	public void Click_on_Filter_Data() throws Throwable {

		Thread.sleep(1000);
		POM FD = new POM();
		FD.ClickfilterData();
		// FD.ClickfilterData();
	}

	@Then("^click on first Dropdown$")
	public void click_on_Dropdowns() throws Throwable {

		// Thread.sleep(5000);
		POM Dropdown = new POM();
		Dropdown.ColumnNameDropdown();
		// Dropdown.ColumnNameDropdown();
	}

	@Then("^click on second Dropdown$")
	public void click_on_second_Dropdown() throws Throwable {
		Thread.sleep(7000);
		POM Dropdown = new POM();
		Dropdown.ColumnNameDropdownSecond();
	}

	@Then("^send search data$")
	public void send_search_data() throws Throwable {
		Thread.sleep(8000);
		POM Dropdown = new POM();
		Dropdown.ColumnNameField();
	}

	@Then("^send SQL server search data$")
	public void send_SQL_server_search_data() throws Throwable {
		Thread.sleep(8000);
		POM Dropdown = new POM();
		Dropdown.SQL_server_search();
	}

	@Then("^send CSV search data$")
	public void send_CSV_search_data() throws Throwable {
		Thread.sleep(8000);
		POM Dropdown = new POM();
		Dropdown.CSV_search_data();
	}

	@Then("^Click on Delete Button$")
	public void Click_on_Delete_Button() throws Throwable {
		POM DeleteButton = new POM();
		DeleteButton.DeleteButton();
	}

	@Then("^Click on Add Button$")
	public void Click_on_Add_Button() throws Throwable {
		POM Add_Button = new POM();
		Add_Button.AddButton();
	}

	@Then("^includes all dates$")
	public void includes_all_dates() throws Throwable {
		POM dates = new POM();
		dates.IncludeAllDates();
	}

	//////// #Group Data////////

	@Then("^click on Group Data$")
	public void click_on_Group_Data() throws Throwable {
		Thread.sleep(2000);
		POM GD = new POM();
		GD.ClickGroupdata();
		// GD.ClickGroupdata();
	}

	@Then("^click on first checkbox$")
	public void click_on_first_checkbox() throws Throwable {
		Thread.sleep(2000);

		POM Checkbox = new POM();
		Checkbox.GroupdataFirstCheckBox();
		// Thread.sleep(3000);
	}

	@Then("^click on first dropdown$")
	public void click_on_first_dropdown() throws Throwable {
		Thread.sleep(3000);
		POM dropdown1 = new POM();
		dropdown1.GroupdataFirstDropdown();
	}

	@Then("^click on Group By dropdown$")
	public void click_on_Group_By_dropdown() throws Throwable {
		POM Group_By_Dropdown = new POM();
		Group_By_Dropdown.Group_By_Dropdown();
		Thread.sleep(3000);
	}

	
	
	
	@Then("^click on series function dropdown$")
	public void click_on_series_function_dropdown() throws Throwable {
		POM functionDropdown = new POM();
		functionDropdown.functionDropdown();
	}

	
	
	@Then("^click on radiobutton 'distinct values from field'$")
	public void click_on_radiobutton_distinct_values_from_field() throws Throwable {
		POM Distinct_values_from_field = new POM();
		Distinct_values_from_field.Distinct_values_from_field();
	}

	@Then("^Choose parallel dropdown$")
	public void Choose_parallel_dropdown() throws Throwable {
		POM Choose_parallel_dropdown = new POM();
		Choose_parallel_dropdown.Choose_parallel_dropdown();
	}

	@Then("^Choose Series Display first dropdown$")
	public void Choose_Series_Display_first_dropdown() throws Throwable {
		POM Series_Display_first_dropdown = new POM();
		Series_Display_first_dropdown.Series_Display_first_dropdown();
	}

	@Then("^Choose Series Display second dropdown$")
	public void Choose_Series_Display_second_dropdown() throws Throwable {
		POM Series_Display_second_dropdown = new POM();
		Series_Display_second_dropdown.Series_Display_second_dropdown();
	}

	@Then("^click on Series Function Dropdown$")
	public void click_on_Series_Function_Dropdown() throws Throwable {
		// Thread.sleep(3000);
		POM Func = new POM();
		Func.FirstSeriesGroupDataFunction();
		// Func.GroupDataFunctionn();
		// Thread.sleep(3000);
		// Func.GroupDataFunction2();
	}
	

@Then("^click on series function 'Revenue' dropdown$")
public void click_on_series_function_Revenue_dropdown() throws Throwable {
	POM Func = new POM();
	Func.FirstSeriesGroupDataFunction();
}

@Then("^click on Series Function 'Sales' dropdown$")
public void click_on_Series_Function_Sales_dropdown() throws Throwable {
	POM Func = new POM();
	Func.SecondSeriesGroupDataFunction();
}


	@Then("^click combo dropdown item$")
	public void click_combo_dropdown_item() throws Throwable {
		Thread.sleep(6000);
		POM ComboItem = new POM();
		 ComboItem.clickFirstComboItemlist();
	}

	@Then("^choose combo dropdown option$")
	public void choose_combo_dropdown_option() throws Throwable {

		POM combo_dropdown_option = new POM();
		combo_dropdown_option.ChooseComboItemlist();
	}

	@Then("^click on clear grouping button$")
	public void click_on_clear_grouping_button() throws Throwable {
		// Thread.sleep(3000);
		POM ClrGrp = new POM();
		ClrGrp.ClearGroupingButton();

	}

	@Then("^click on Yes button$")
	public void click_on_Yes_button() throws Throwable {
		// Thread.sleep(7500);
		POM ClrGrp = new POM();
		ClrGrp.ClearGroupingYesButton();

	}

	@Then("^click on No button$")
	public void click_on_No_button() throws Throwable {
		POM NoButton = new POM();
		NoButton.NoButton();

	}

	@Then("^Click Apply button$")
	public void Click_Apply_button() throws Throwable {
		POM Button = new POM();
		Button.Apply1();
	}

	@Then("^click on Clear All Filters$")
	public void click_on_Clear_All_Filters() throws Throwable {
		Thread.sleep(1000);
		POM field = new POM();
		field.Clear_All_Filters();
	}
	/////// Top N Records\\\\\\\\

	@Then("^click on Top N Records$")
	public void click_on_Top_N_Records() throws Throwable {
		Thread.sleep(1000);
		POM field = new POM();
		field.Top_N_Records();

	}

	@Then("^click on Show top first Dropdown$")
	public void click_on_Show_top_first_Dropdown() throws Throwable {

		POM drpdwn = new POM();
		drpdwn.ShowTopFirstField();

	}

	@Then("^click on Show top Second Dropdown$")
	public void click_on_Show_top_Second_Dropdown() throws Throwable {

		POM drpdwn = new POM();
		drpdwn.ShowTopDropdown();

	}

	@Then("^click Apply button$")
	public void click_Apply_button() throws Throwable {
		// Thread.sleep(7000);
		POM Button = new POM();
		Button.TopNRecordApplyclick();

	}

	@Then("^click on clear Top N Records$")
	public void click_on_clear_Top_N_Records() throws Throwable {
		POM AllclearRecords = new POM();
		AllclearRecords.AllclearRecords();
	}

	@Then("^Compare SQLServerData XML$")
	public void Compare_SQLServerData_XML() throws Throwable {
		POM CopyXML = new POM();
		CopyXML.XMLSQLServerDataResponse();
	}

	@Then("^Compare CSVData XML$")
	public void Compare_CSVData_XML() throws Throwable {
		POM CopyXML = new POM();
		CopyXML.XMLCSVDataResponse();
	}

	@Then("^Compare SharePointListData XML$")
	public void Compare_SharePointListData_XML() throws Throwable {
		POM CopyXML = new POM();
		CopyXML.XMLSharePointListDataResponse();
	}

	@Then("^Compare XML$")
	public void Compare_XML() throws Throwable {
		POM CopyXML = new POM();
		CopyXML.XMLResponse();
	}

	@Then("^click on Source XML$")
	public void click_on_Source_XML() throws Throwable {
		POM SourceXML = new POM();
		SourceXML.SourceXML();

	}

	@Then("^Copy the XML$")
	public void Copy_the_XML() throws Throwable {
		POM CopyXML = new POM();
		CopyXML.XMLResponse();
	}

	@Then("^click Finish button$")
	public void click_Finish_button() throws Throwable {
		Thread.sleep(9000);
		POM Finish = new POM();
		Finish.FinishClick();

	}

	@Then("^click Apply Button$")
	public void click_Apply_Button() throws Throwable {
		Thread.sleep(2500);
		POM Button = new POM();
		Button.ApplyClick();
	}
	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Chart Type\\\\\\\\\\

	@Then("^click on Chart Type$")
	public void click_on_Chart_Type() throws Throwable {
		// Thread.sleep(1000);
		POM Chart = new POM();
		Chart.ChartTypeClick();
	}

	@Given("^Choose Single Series from Chart Category Dropdown$")
	public void Choose_Single_Series_from_Chart_Category_Dropdown() throws Throwable {
		Thread.sleep(2500);
		POM CC = new POM();
		CC.ChartcategorySingleSeriesChoose();

	}

	@Then("^Choose Column(\\d+)D Chart$")
	public void Choose_Column_D_Chart(int arg1) throws Throwable {
		POM twoD = new POM();
		// twoD.Column2DChoose();
		twoD.ccsptestsvrColumn2DChoose();
	}

	/*
	 * @Then("^Click on Apply$") public void Click_on_Apply() throws Throwable {
	 * Thread.sleep(9000); POM button = new POM(); button.Apply();
	 * Thread.sleep(1000); }
	 */

	@Then("^Click on Chart Type Apply Button$")
	public void Click_on_Chart_Type_Apply_Button() throws Throwable {
		POM CaptionsApply = new POM();
		CaptionsApply.ChartTypeApplyClick();
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Captions\\\\\\\\\\
	@Then("^Click on Captions$")
	public void Click_on_Captions() throws Throwable {
		// Thread.sleep(2000);
		POM menu = new POM();
		menu.Captions();
	}

	@Then("^Enter text in Chart Title Field$")
	public void Enter_text_in_Chart_Title_Field() throws Throwable {
		Thread.sleep(2000);
		POM Title = new POM();
		Title.ChartTitle();

	}
	
	
	


	@Then("^Enter text in Chart Sub Title Field$")
	public void Enter_text_in_Chart_Sub_Title_Field() throws Throwable {
		Thread.sleep(4000);
		POM SubTitle = new POM();
		SubTitle.ChartSubTitle();

	}

	@Then("^enter text in X-Axis Title Field$")
	public void enter_text_in_X_Axis_Title_Field() throws Throwable {
		POM Axis = new POM();
		Axis.XAxisTitle();
	}

	@Then("^enter text in Primary Y-axis Title Field$")
	public void enter_text_in_Primary_Y_axis_Title_Field() throws Throwable {
		POM Axis = new POM();
		Axis.PrimaryYAxisTitle();
	}

	@Then("^Untick Rotate Y-Axis Title Checkbox$")
	public void Untick_Rotate_Y_Axis_Title_Checkbox() throws Throwable {
		POM YAxis = new POM();
		YAxis.YAxisTitle();
	}

	@Then("^Click Captions Apply Button$")
	public void Click_Series_Customization_Apply_Button() throws Throwable {
		POM CaptionsApply = new POM();
		CaptionsApply.CaptionsApplyClick();
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Series Customization\\\\\\\\\\
	@Then("^Click Series Customization$")
	public void Click_Series_Customization() throws Throwable {
		// Thread.sleep(3000);
		POM Series = new POM();
		Series.SeriesCustomization();
		Thread.sleep(6578);
	}

	@Then("^Choose XAxis Label Column$")
	public void Choose_XAxis_Label_Column() throws Throwable {
		POM XAxisLabel = new POM();
		XAxisLabel.XAxisLabelColumn();

	}

	@Then("^Choose XAxis Sort Column$")
	public void Choose_XAxis_Sort_Column() throws Throwable {
		POM XAxisSortColumn = new POM();
		XAxisSortColumn.XAxisSortColumn();
	}

	@Then("^Choose XAxis Sort Order$")
	public void Choose_XAxis_Sort_Order() throws Throwable {
		POM XAxisSortOrder = new POM();
		XAxisSortOrder.XAxisSortOrder();
	}

	@Then("^Click on Series$")
	public void Click_on_Series() throws Throwable {
		POM Series = new POM();
		Series.SeriesCustomizationTabChange();
	}

	@Then("^Choose Series value Column$")
	public void Choose_Series_value_Column() throws Throwable {
		POM SeriesvalueColumn = new POM();
		SeriesvalueColumn.SeriesValueColumn();

	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Labels,Values & Tool-tips\\\\\\\\\\

	@Then("^Click on Labels,Values & Tool-tips$")
	public void Click_on_Labels_Values_Tool_tips() throws Throwable {
		// Thread.sleep(4000);
		POM ValueTooltip = new POM();
		ValueTooltip.LabelsValuesTooltips();
	}

	@Then("^Choose Display Type$")
	public void Choose_Display_Type() throws Throwable {
		POM Display = new POM();
		Display.Display();
	}

	@Then("^choose nth Label$")
	public void choose_nth_Label() throws Throwable {
		POM nthlabel = new POM();
		nthlabel.nthLabel();
	}

	@Then("^choose Number of StaggerLines$")
	public void choose_Number_of_StaggerLines() throws Throwable {
		POM Stagger = new POM();
		Stagger.stagger();

	}

	@Then("^tick on checkbox Rotate_Values_When_Displayed$")
	public void tick_on_checkbox_Rotate_Values_When_Displayed() throws Throwable {
		POM RotateValues = new POM();
		RotateValues.RotateValuesWhenDisplayed();
	}

	@Then("^tick on checkbox Place_Values_Inside_Data_Plot$")
	public void tick_on_checkbox_Place_Values_Inside_Data_Plot() throws Throwable {
		POM PlaceValues = new POM();
		PlaceValues.PlaceValuesInsideDataPlot();
	}

	@Then("^Click on Data Plot$")
	public void Click_on_Data_Plot() throws Throwable {
		POM DataPlot = new POM();
		DataPlot.LabelValuesTooltipTabChange();
	}

	@Then("^tick on checkbox Use_Round_Edges$")
	public void tick_on_checkbox_Use_Round_Edges() throws Throwable {
		Thread.sleep(1000);
		POM RoundEdges = new POM();
		RoundEdges.UseRoundEdges();

	}

	@Then("^Select Plot Fill Angle$")
	public void Select_Plot_Fill_Angle() throws Throwable {
		POM Angle = new POM();
		Angle.PlotFillAngle();
	}

	@Then("^Select Fill Opaqueness$")
	public void Select_Fill_Opaqueness() throws Throwable {
		POM Opaqueness = new POM();
		Opaqueness.PlotFillOpaqueness();
	}

	@Then("^tick on ShowShadow$")
	public void tick_on_ShowShadow() throws Throwable {
		POM ShowShadow = new POM();
		ShowShadow.ShowShadow();
	}

	@Then("^Choose Gradient Color$")
	public void Choose_GradientColor() throws Throwable {
		POM GradientColor = new POM();
		GradientColor.GradientColor();
	}

	@Then("^Give Data Plot Color$")
	public void Give_DataPlotColor() throws Throwable {
		Thread.sleep(1000);
		POM DataPlotColor = new POM();
		DataPlotColor.DataPlotColor();
	}

	@Then("^select on Show Plot Border$")
	public void select_on_Show_Plot_Border() throws Throwable {
		// Thread.sleep(1000);
		POM Plot_Border = new POM();
		Plot_Border.Show_Plot_Border();
	}

	@Then("^Choose Border Color$")
	public void Choose_Border_Color() throws Throwable {
		POM Color = new POM();
		Color.BorderColor();
	}

	@Then("^Give Border Thickness$")
	public void Give_Border_Thickness() throws Throwable {
		Thread.sleep(2000);
		POM Border = new POM();
		Border.BorderThickness();
	}

	@Then("^Plot Border Opaqueness$")
	public void Plot_Border_Opaqueness() throws Throwable {
		POM Border = new POM();
		Border.BorderOpaqueness();
	}

	@Then("^Show Plot Border$")
	public void Show_Plot_Border() throws Throwable {
		POM Plot_Border = new POM();
		Plot_Border.Show_Plot_Border();
	}

	@Then("^tick on Show_Dashed_Plot_Border$")
	public void tick_on_Show_Dashed_Plot_Border() throws Throwable {
		POM Border = new POM();
		Border.DashedPlotBorder();
	}

	@Then("^Give Dash Length$")
	public void Give_Dash_Length() throws Throwable {
		POM Dash = new POM();
		Dash.DashLength();
	}

	@Then("^Give Dash Gap Length$")
	public void Give_Dash_Gap_Length() throws Throwable {
		POM Dash = new POM();
		Dash.DashGapLength();
	}

	@Then("^Give spline data series line color$")
	public void Give_spline_data_series_line_color() throws Throwable {

		POM spline_data_series_line_color = new POM();
		spline_data_series_line_color.spline_data_series_line_color();
	}

	@Then("^Give spline data series line thickness$")
	public void Give_spline_data_series_line_thickness() throws Throwable {
		POM spline_data_series_line_thickness = new POM();
		spline_data_series_line_thickness.spline_data_series_line_thickness();
	}

	@Then("^Give spline data series line Opaqueness$")
	public void Give_spline_data_series_line_Opaqueness() throws Throwable {
		POM spline_data_series_line_Opaqueness = new POM();
		spline_data_series_line_Opaqueness.spline_data_series_line_thickness();
	}

	@Then("^click on Dashed line checkbox$")
	public void click_on_Dashed_line_checkbox() throws Throwable {

	}

	@Then("^Give spline data series Dash length$")
	public void Give_spline_data_series_Dash_length() throws Throwable {

	}

	@Then("^Give spline data series Dash Gap length$")
	public void Give_spline_data_series_Dash_Gap_length() throws Throwable {

	}

	@Then("^click on Tool-Tips tab$")
	public void click_on_Tool_Tips_tab() throws Throwable {
		Thread.sleep(3000);
		POM Tooltipstabchange = new POM();
		Tooltipstabchange.TooltipsTabChange();
	}

	@Then("^Show Tool-Tips$")
	public void Show_Tool_Tips() throws Throwable {
		POM Tooltip = new POM();
		Tooltip.Show_Tool_Tips();
	}

	@Then("^click Include Shadow$")
	public void click_Include_Shadow() throws Throwable {
		// Thread.sleep(9000);
		POM Include = new POM();
		Include.IncludeShadow();
	}

	@Then("^Give Tool-Tips Border Color$")
	public void Give_Tool_Tips_Border_Color() throws Throwable {
		POM Color = new POM();
		Color.Tool_Tips_Border_Color();
	}

	@Then("^Give Tool-Tips Background Color$")
	public void Give_Tool_Tips_Background_Color() throws Throwable {
		POM Color = new POM();
		Color.Tool_Tips_Background_Color();

	}
	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Cosmetics\\\\\\\\\\

	@Given("^Click on Cosmetics$")
	public void Click_on_Cosmetics() throws Throwable {
		// Thread.sleep(5000);
		POM Cosmetics = new POM();
		Cosmetics.Click_on_Cosmetics();
	}

	@Then("^Give Border Data$")
	public void Give_Opaqueness_value() throws Throwable {

		POM Give_Opaqueness_value = new POM();
		Give_Opaqueness_value.Opaqueness_value();
	}

	@Then("^Give Border Cosmetics Color$")
	public void Give_Border_Cosmetics_Color() throws Throwable {
		POM Border_Cosmetics_Color = new POM();
		Border_Cosmetics_Color.Border_Cosmetics_Color();
	}

	@Then("^Give Chart Background$")
	public void Give_Image_SWF_URL() throws Throwable {

		POM Image_SWF_URL = new POM();
		Image_SWF_URL.Image_SWF_URL();
	}
	/*
	 * @Then("^Give Image/SWF Opaqueness$") public void
	 * Give_Image_SWF_Opaqueness() throws Throwable { POM Image_SWF_Opaqueness =
	 * new POM(); Image_SWF_Opaqueness.Image_SWF_Opaqueness(); }
	 * 
	 * @Given("^Give Border Opaqueness$") public void Give_Border_Opaqueness()
	 * throws Throwable { POM Border_Opaqueness = new POM();
	 * Border_Opaqueness.Border_Opaqueness(); }
	 */

	@Then("^Click on Canvas$")
	public void Click_on_Canvas() throws Throwable {

		POM Canvas = new POM();
		Canvas.Canvas();
	}

	@Then("^Give First Opaqueness$")
	public void Give_First_Opaqueness() throws Throwable {
		Thread.sleep(2000);
		POM Opaqueness = new POM();
		Opaqueness.CanvasBackgroundOpaqueness();
	}

	@Then("^Give Thickness$")
	public void Give_Thickness() throws Throwable {
		Thread.sleep(2100);
		POM Thickness = new POM();
		Thickness.CanvasThickness();
	}

	@Then("^Give Second Opaqueness$")
	public void Give_Second_Opaqueness() throws Throwable {
		Thread.sleep(2200);
		POM Opaqueness = new POM();
		Opaqueness.CanvasBorderOpaqueness();
	}

	@Given("^Give Background Color$")
	public void Give_Background_Color() throws Throwable {
		Thread.sleep(2300);
		POM Background_Color = new POM();
		Background_Color.CanvasBackgroundColor();
	}

	@Given("^Give Border Color$")
	public void Give_Border_Color() throws Throwable {
		Thread.sleep(2400);
		POM Border_Color = new POM();
		Border_Color.CanvasBorderColor();
	}

	@Given("^Click on Margin & Padding$")
	public void Click_on_Margin_Padding() throws Throwable {
		POM Margin_Padding = new POM();
		Margin_Padding.Margin_Padding();
	}

	@Given("^Give Left margin$")
	public void Give_Left_margin() throws Throwable {
		POM Left_margin = new POM();
		Left_margin.LeftMargin();
	}

	@Then("^Give Right Margin$")
	public void Give_Right_Margin() throws Throwable {
		POM Right_Margin = new POM();
		Right_Margin.RightMargin();
	}

	@Then("^Give Top Margin$")
	public void Give_Top_Margin() throws Throwable {
		POM Top_Margin = new POM();
		Top_Margin.TopMargin();
	}

	@Then("^Give Bottom Margin$")
	public void Give_Bottom_Margin() throws Throwable {
		POM Top_Margin = new POM();
		Top_Margin.BottomMargin();
	}

	@Then("^Give Title Padding$")
	public void Give_Title_Padding() throws Throwable {
		POM Title_Padding = new POM();
		Title_Padding.Title_Padding();
	}

	@Then("^Give Y Axis Title Padding$")
	public void Give_Y_Axis_Title_Padding() throws Throwable {
		POM Y_Axis_Title_Padding = new POM();
		Y_Axis_Title_Padding.Y_Axis_Title_Padding();
	}

	@Then("^Give Label Padding$")
	public void Give_Label_Padding() throws Throwable {
		POM Label_Padding = new POM();
		Label_Padding.Label_Padding();
	}

	@Then("^Give X Axis Title Padding$")
	public void Give_X_Axis_Title_Padding() throws Throwable {
		POM X_Axis_Title_Padding = new POM();
		X_Axis_Title_Padding.X_Axis_Title_Padding();
	}

	@Then("^Give Y Axis Value Padding$")
	public void Give_Y_Axis_Value_Padding() throws Throwable {
		POM Y_Axis_Value_Padding = new POM();
		Y_Axis_Value_Padding.Y_Axis_Value_Padding();
	}

	@Then("^Give Value Padding$")
	public void Give_Value_Padding() throws Throwable {
		POM Value_Padding = new POM();
		Value_Padding.Value_Padding();
	}

	@Given("^Click on Fonts$")
	public void Click_on_Fonts() throws Throwable {
		POM Fonts = new POM();
		Fonts.Fonts();
	}

	@Then("^Give Customize Font Name$")
	public void Give_Customize_Font_Name() throws Throwable {

		// Thread.sleep(20000);
		POM Customize_Font_Name = new POM();
		Customize_Font_Name.Customize_Font_Name();
	}

	@Then("^Give Customize Font Size$")
	public void Give_Customize_Font_Size() throws Throwable {
		POM Customize_Font_Size = new POM();
		Customize_Font_Size.Customize_Font_Size();
	}

	@Then("^Give Customize Font Color$")
	public void Give_Customize_font_Color() throws Throwable {
		Thread.sleep(20500);
		POM Customize_Font_Color = new POM();
		Customize_Font_Color.Customize_font_Color();
	}

	@Then("^Give side canvas Font Name$")
	public void Give_side_canvas_Font_Name() throws Throwable {
		POM canvas_Font_Name = new POM();
		canvas_Font_Name.canvas_Font_Name();
	}

	@Then("^Give side canvas Font Size$")
	public void Give_side_canvas_Font_size() throws Throwable {
		POM canvas_Font_Size = new POM();
		canvas_Font_Size.canvas_Font_Size();
	}

	@Then("^Give side canvas Font Color$")
	public void Give_side_canvas_Font_Color() throws Throwable {
		POM canvas_Font_Color = new POM();
		canvas_Font_Color.canvas_Font_Color();
	}

	@Then("^Give (\\d+)D chart color$")
	public void Give_D_chart_color(int arg1) throws Throwable {
		POM TwoD_chart_color = new POM();
		TwoD_chart_color.TwoD_chart_color();
	}

	@Then("^Give (\\d+)D chart Thickness$")
	public void Give_D_chart_Thickness(int arg1) throws Throwable {
		POM TwoD_chart_Thickness = new POM();
		TwoD_chart_Thickness.TwoD_chart_thickness();
	}

	@Then("^Give (\\d+)D chart Opaqueness$")
	public void Give_D_chart_Opaqueness(int arg1) throws Throwable {
		POM TwoD_chart_Thickness = new POM();
		TwoD_chart_Thickness.TwoD_chart_Opaqueness();
	}

	@Given("^Click on Zero Plane$")
	public void Click_on_Zero_Plane() throws Throwable {
		POM Zero_Plane = new POM();
		Zero_Plane.Zero_Plane();
	}

	@Then("^Tick on Show Zero pLane$")
	public void Tick_on_Show_Zero_pLane() throws Throwable {
		POM Show_Zero_pLane = new POM();
		Show_Zero_pLane.Show_Zero_pLane();
	}

	@Given("^Click on Custom Branding$")
	public void Click_on_Custom_Branding() throws Throwable {
		POM Custom_Branding = new POM();
		Custom_Branding.Custom_Branding();
	}

	@Then("^Specify logo URL$")
	public void Specify_logo_URL() throws Throwable {
		POM Specify_logo_URL = new POM();
		Specify_logo_URL.Specify_logo_URL();
	}

	@Then("^Give Link$")
	public void Give_Link() throws Throwable {
		POM Give_Link = new POM();
		Give_Link.Give_Link();
	}

	@Then("^Give Position$")
	public void Give_Position() throws Throwable {
		POM Give_Position = new POM();
		Give_Position.Give_Position();
	}

	@Then("^Give Branding Opaqueness$")
	public void Give_Branding_Opaqueness() throws Throwable {
		POM Branding_Opaqueness = new POM();
		Branding_Opaqueness.Branding_Opaqueness();
	}

	@Then("^Give Branding Scale$")
	public void Give_Branding_Scale() throws Throwable {
		POM Branding_Scale = new POM();
		Branding_Scale.Branding_Scale();
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Number Formatting\\\\\\\\\\

	@Given("^Click on Number Formatting$")
	public void Click_on_Number_Formatting() throws Throwable {
		Thread.sleep(1000);
		POM Number_Formatting = new POM();
		Number_Formatting.Click_on_Number_Formatting();
	}

	@Then("^Give Data in Prefix & Suffix Field$")
	public void Give_Data_in_Prefix_Field() throws Throwable {
		// Thread.sleep(35000);
		POM Data = new POM();
		Data.field();
	}

	@Then("^click on European numbers$")
	public void click_on_European_numbers() throws Throwable {
		POM European_numbers = new POM();
		European_numbers.European_numbers();
	}

	@Then("^click on Format numbers$")
	public void click_on_Format_numbers() throws Throwable {
		Thread.sleep(1700);
		POM Format_numbers = new POM();
		Format_numbers.Format_numbers();
	}

	@Then("^Give Scales to be used$")
	public void Give_Scales_to_be_used() throws Throwable {
		POM Scales_to_be_used = new POM();
		Scales_to_be_used.Default_Scale_Unit();
	}

	@Then("^Give Round numbers$")
	public void Give_Round_numbers() throws Throwable {
		POM Round_numbers = new POM();
		Round_numbers.Round_numbers_to_how_many_decimals();
	}

	@Then("^Give Round axis$")
	public void Give_Round_axis() throws Throwable {
		POM Round_axis = new POM();
		Round_axis.Round_axis();
	}

	@Then("^Click on Force exact number$")
	public void Click_on_Force_exact_number() throws Throwable {
		POM Force_exact_number = new POM();
		Force_exact_number.Force_exact_number_of_decimals();
	}

	@Then("^Click on Force Y Axis decimal values$")
	public void Click_on_Force_Y_Axis_decimal_values() throws Throwable {
		POM Y_Axis_decimal_values = new POM();
		Y_Axis_decimal_values.Force_Y_Axis_decimal_values();
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Axis\\\\\\\\\\

	@Given("^Click on Axis$")
	public void Click_on_Axis() throws Throwable {
		Thread.sleep(5500);

		POM Axis = new POM();
		Axis.Click_on_Axis();

	}

	@Then("^Give Y-Axis minimum value,maximum value,set lower limit,primary y-axis value$")
	public void Give_Y_Axis_minimum_value() throws Throwable {
		Thread.sleep(6000);
		POM value = new POM();
		value.Y_Axis_minimum_value();
	}

	@Then("^Set adaptive lower limit for axis$")
	public void Set_adaptive_lower_limit_for_axis() throws Throwable {

	}

	@Then("^Set Force decimals on primary y-axis values$")
	public void Set_Force_decimals_on_primary_y_axis_values() throws Throwable {

	}

	@Then("^Click on Axis Grid Lines$")
	public void Click_on_Axis_Grid_Lines() throws Throwable {
		POM Axis_Grid_Lines = new POM();
		Axis_Grid_Lines.Axis_Grid_Lines();
	}

	@Then("^Give Number of grid lines$")
	public void Give_Number_of_grid_lines() throws Throwable {
		POM grid_lines = new POM();
		grid_lines.Show_grid_lines();
	}

	@Then("^Give Grid Line Thickness$")
	public void Give_Grid_Line_Thickness() throws Throwable {
		POM Grid_Line_Thickness = new POM();
		Grid_Line_Thickness.Grid_Line_Thickness();
	}

	@Then("^Choose Grid Line color$")
	public void Choose_color() throws Throwable {
		POM Choose_color = new POM();
		Choose_color.Grid_Line_color();
	}

	@Then("^Give Grid Line Opaqueness$")
	public void Give_Grid_Line_Opaqueness() throws Throwable {
		POM Line_Opaqueness = new POM();
		Line_Opaqueness.Grid_Line_Opaqueness();
	}

	@Then("^Show grid lines as dashed$")
	public void Show_grid_lines_as_dashed() throws Throwable {
		Thread.sleep(2500);
		POM grid_lines_as_dashed = new POM();
		grid_lines_as_dashed.grid_lines_as_dashed();
	}

	@Then("^Give Dash length$")
	public void Give_Dash_length() throws Throwable {
		POM Dash_length = new POM();
		Dash_length.Give_Dash_length();
	}

	@Then("^Give Dash gap$")
	public void Give_Dash_gap() throws Throwable {
		POM Dash_length = new POM();
		Dash_length.Give_Dash_gap();
	}

	@Then("^Tick Show alternate color bands$")
	public void Tick_Show_alternate_color_bands() throws Throwable {
		POM Alternate_color = new POM();
		Alternate_color.show_alternate_color_bands();
	}

	@Then("^give alternate color$")
	public void give_alternate_color() throws Throwable {
		POM Alternate_color = new POM();
		Alternate_color.give_alternate_color();
	}

	@Then("^give alternate Opaqueness$")
	public void give_alternate_Opaqueness() throws Throwable {
		POM Alternate_color = new POM();
		Alternate_color.give_alternate_Opaqueness();
	}
	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Trend Lines\\\\\\\\\\

	@Given("^Click on Trend Lines$")
	public void Click_on_Trend_Lines() throws Throwable {

		Thread.sleep(2200);
		POM Trend_Lines = new POM();
		Trend_Lines.Click_on_Trend_Lines();
	}

	@Then("^Give Start Value$")
	public void Give_Start_Value() throws Throwable {
		POM Value = new POM();
		Value.Start_Value();
	}

	@Then("^Give End Value$")
	public void Give_End_Value() throws Throwable {
		POM Value = new POM();
		Value.End_Value();
	}

	@Then("^Give Display Text$")
	public void Give_Display_Text() throws Throwable {
		POM Value = new POM();
		Value.Display_Text();
	}

	@Then("^give Tool-tip Text$")
	public void give_Tool_tip_Text() throws Throwable {
		POM Value = new POM();
		Value.Tool_tip_Text();
	}

	@Then("^Give Trend lines Opaqueness$")
	public void Give_Opaqueness() throws Throwable {
		Thread.sleep(12000);
		POM Value = new POM();
		Value.Give_Opaqueness();

	}

	@Then("^Give Trend lines Color$")
	public void Give_Trend_lines_Color() throws Throwable {
		POM Trend_lines_Color = new POM();
		Trend_lines_Color.Trend_lines_Color();
	}

	@Then("^Set the thickness of the trend line$")
	public void Set_the_thickness_of_the_trend_line() throws Throwable {
		Thread.sleep(12000);
		POM Value = new POM();
		Value.thickness_of_the_trend_line();
	}

	@Then("^Click on as filled color zone$")
	public void Click_on_as_filled_color_zone() throws Throwable {
		POM color_zone = new POM();
		color_zone.as_filled_color_zone();
	}

	@Then("^Click on as dashed$")
	public void Click_on_as_dashed() throws Throwable {
		POM asdashed = new POM();
		asdashed.asdashed();
	}

	@Then("^Click on over data plot$")
	public void Click_on_over_data_plot() throws Throwable {
		POM over_data_plot = new POM();
		over_data_plot.over_data_plot();
	}

	@Then("^Click on value on right side$")
	public void Click_on_value_on_right_side() throws Throwable {
		POM value_on_right_side = new POM();
		value_on_right_side.value_on_right_side();
	}

	@Then("^Give Dash Lenth$")
	public void Give_Dash_Lenth() throws Throwable {
		POM Dash_length = new POM();
		Dash_length.Dash_length();
	}

	@Then("^Dash gap length$")
	public void Dash_gap_length() throws Throwable {
		POM Dash_gap_length = new POM();
		Dash_gap_length.Dash_gap_length();
	}

	////////////////// Single Series Column2D Collabion Chart
	////////////////// Configuration-Other Settings\\\\\\\\\\

	@Given("^Click on Other Settings$")
	public void Click_on_Other_Settings() throws Throwable {
		Thread.sleep(4000);
		POM Settings = new POM();
		Settings.Click_on_Other_Settings();
	}

	@Then("^Give Chart Loading Message$")
	public void Give_Chart_Loading_Message() throws Throwable {
		POM Chart_Loading_Message = new POM();
		Chart_Loading_Message.Chart_Loading_Message();
	}

	@Then("^Give Data Loading Message$")
	public void Give_Data_Loading_Message() throws Throwable {
		POM Give_Data_Loading_Message = new POM();
		Give_Data_Loading_Message.Data_Loading_Message();
	}

	@Then("^Give Data Processing Message$")
	public void Give_Data_Processing_Message() throws Throwable {
		POM Data_Processing_Message = new POM();
		Data_Processing_Message.Data_Processing_Message();
	}

	@Then("^Give No-data to display Message$")
	public void Give_No_data_to_display_Message() throws Throwable {
		POM No_data_to_display_Message = new POM();
		No_data_to_display_Message.No_data_to_display_Message();
	}

	@Then("^Give Pre-Rendering Message$")
	public void Give_Pre_Rendering_Message() throws Throwable {
		POM Pre_Rendering_Message = new POM();
		Pre_Rendering_Message.Pre_Rendering_Message();
	}

	@Then("^Give Error in Loading Message$")
	public void Give_Error_in_Loading_Message() throws Throwable {
		POM Error_in_Loading_Message = new POM();
		Error_in_Loading_Message.Error_in_Loading_Message();
	}

	@Then("^Give Error in Data Message$")
	public void Give_Error_in_Data_Message() throws Throwable {
		POM Error_in_Data_Message = new POM();
		Error_in_Data_Message.Error_in_Data_Message();
	}

	@Then("^click on Custom Attribute$")
	public void click_on_Custom_Attribute_tab() throws Throwable {
		POM Custom_Attribute = new POM();
		Custom_Attribute.CustomAttributes();
	}

	@Then("^click on Add Button$")
	public void click_on_Add_Button() throws Throwable {
		POM Add_Button = new POM();
		Add_Button.Add();
	}

	@Then("^Give Attribute Name$")
	public void Give_Attribute_Name() throws Throwable {
		POM AttributeName = new POM();
		AttributeName.AttributeName();
	}

	@Then("^Give Attribute Value$")
	public void Give_Attribute_Value() throws Throwable {
		POM AttributeValue = new POM();
		AttributeValue.AttributeValue();
	}

	@Then("^Click on Connection Fields$")
	public void Click_on_Connection_Fields() throws Throwable {
		Thread.sleep(4000);
		POM Connection_Fields = new POM();
		Connection_Fields.ConnectionFields();
	}

	@Then("^Select Fields used in chart$")
	public void Select_Fields_used_in_chart() throws Throwable {
		POM ChartFields = new POM();
		ChartFields.ConnectionFields();
	}

	@Then("^Select All fields returned from data source$")
	public void Select_All_fields_returned_from_data_source() throws Throwable {
		POM AllFields = new POM();
		AllFields.rdChartFields();
	}
	/////////////////////// User Interaction-Drill
	/////////////////////// Down///////////////////////////////////////////

	@Then("^click on Wizard Drilldown Apply button$")
	public void click_on_Wizard_Drilldown_Apply_button() throws Throwable {
		POM DrilldownApply = new POM();
		DrilldownApply.WizardDrilldownApply();
	}

	@Then("^Select First DrillDown option$")
	public void Select_First_DrillDown_option() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.DrillDownFirstOption();
	}

	@Then("^Choose subset of Data dropdown$")
	public void Choose_subset_of_Data_dropdown() throws Throwable {
		Thread.sleep(9000);
		POM FirstRd = new POM();
		FirstRd.subset_of_Data_dropdown();
	}

	@Then("^Choose matched field for the subset of data$")
	public void Choose_matched_field_for_the_subset_of_data() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.matched_field_for_the_subset_of_data();
	}

	@Then("^Choose order of Data Storing$")
	public void Choose_order_of_Data_Storing() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.order_of_Data_Storing();
	}

	@Then("^Choose the way of showing chart Data$")
	public void Choose_the_way_of_showing_chart_Data() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.way_of_showing_chart_Data();
	}

	@Given("^Click on Configuration$")
	public void Click_on_Configuration() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.Configuration();
	}
	
	
	@Given("^Select Chart Type from dropdown$")
	public void select_Chart_Type_from_dropdown() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.ChartTypeChoose();
	}

	@Then("^Give Chart Title$")
	public void Give_Chart_Title() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.Chart_Title();
	}

	@Then("^Give Chart Sub Title$")
	public void Give_Chart_Sub_Title() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.Chart_Sub_Title();
	}

	@Then("^Give X-Axis Title$")
	public void Give_X_Axis_Title() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.X_Axis_Title();
	}

	@Then("^Give Primary Y-axis Title$")
	public void Give_Primary_Y_axis_Title() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.Primary_Y_axis_Title();
	}

	@Then("^Rotate Y-axis Title$")
	public void Rotate_Y_axis_Title() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.Rotate_Y_axis_Title();
	}

	@Then("^Filled Y-axis width$")
	public void Filled_Y_axis_width() throws Throwable {
		POM FirstRd = new POM();
		FirstRd.Y_axis_width();
	}

	
	@Then("^click on Apply Yes button$")
	public void click_on_Apply_Yes_button() throws Throwable {
		POM ApplyYes = new POM();
		ApplyYes.ApplyYes();
		
		}
	
	@Then("^Click on ok button$")
	public void Click_on_ok_button() throws Throwable {
		Thread.sleep(8500);
		POM FirstRd = new POM();
		FirstRd.ok();
	}

	@Then("^Enable Multi level DrillDown$")
	public void Enable_Multi_level_DrillDown() throws Throwable {
		Thread.sleep(5500);
		POM FirstRd = new POM();
		FirstRd.Enable_Multi_level_DrillDown();
	}
	
	//
	@Then("^Enable to change chart type at view mode$")
	public void enable_to_change_chart_type_at_view_mode() throws Throwable {
		Thread.sleep(5500);
		POM FirstRd = new POM();
		FirstRd.Enable_Chart_Type_View();
		
	}
	
	@Then("^click on 'Customize' button$")
	public void click_on_Customize_button() throws Throwable {
		Thread.sleep(5500);
		POM Customize = new POM();
		Customize.Customize();
		
	}

	@Then("^Select First DrillDown Field$")
	public void Select_First_DrillDown_Field() throws Throwable {
		POM First_DrillDown_Field = new POM();
		First_DrillDown_Field.First_DrillDown_Field();

	}
	
	@Then("^Select Second DrillDown Field$")
	public void Select_Second_DrillDown_Field() throws Throwable {
		POM Second_DrillDown_Field = new POM();
		Second_DrillDown_Field.Second_DrillDown_Field();
	}

	@Then("^Select Group by dates$")
	public void Select_Group_by_dates() throws Throwable {
		POM GroupByDates = new POM();
		GroupByDates.Group_By_Dates();
	}

	@Then("^Select Group by Fields$")
	public void Select_Group_by_Fields() throws Throwable {
		POM GroupByFields = new POM();
		GroupByFields.Group_By_Fields();
	}

	@Then("^Select Second DrillDown option$")
	public void Select_Second_DrillDown_option() throws Throwable {
		POM SecondRd = new POM();
		SecondRd.DrillDownSecondOption();
	}

	@Then("^Select Third DrillDown option$")
	public void Select_Third_DrillDown_option() throws Throwable {
		POM ThirdRd = new POM();
		ThirdRd.DrillDownThirdOption();
	}

	@Then("^Give Link Url$")
	public void Give_Link_Url() throws Throwable {
		POM Link_Url = new POM();
		Link_Url.DrillDown_LinkUrl();
	}

	@Then("^Give Url$")
	public void Give_Url() throws Throwable {
		POM Link_Url = new POM();
		Link_Url.Url();
	}

	@Then("^Choose open url option$")
	public void Choose_open_url_option() throws Throwable {
		POM open_url = new POM();
		open_url.DrillDown_openURLin();
	}

	@Then("^Select Fourth DrillDown option From Wizard$")
	public void Select_Fourth_DrillDown_option_From_Wizard() throws Throwable {
		POM FourthRd = new POM();
		FourthRd.DrillDownFourthOption();
	}

	@Then("^Select Fifth DrillDown option$")
	public void Select_Fifth_DrillDown_option() throws Throwable {
		POM FifthRd = new POM();
		FifthRd.DrillDownFifthOption();
	}

	@Given("^Click on Dynamic filter$")
	public void Click_on_Dynamic_filter() throws Throwable {
		POM filter = new POM();
		filter.ClickDynamicFilter();

	}

	@Then("^Allow viewers to filter data dynamically$")
	public void Allow_viewers_to_filter_data_dynamically() throws Throwable {
		POM Dynamic_filter = new POM();
		Dynamic_filter.filter_data_dynamically();
	}

	@Then("^Tick on Specific Filter$")
	public void Tick_on_Specific_Filter() throws Throwable {
		POM filter_Checkbox = new POM();
		filter_Checkbox.First_filter_Checkbox_Click();
		filter_Checkbox.Second_filter_Checkbox_Click();
		filter_Checkbox.Forth_filter_Checkbox_Click();
	}

	@Then("^Allow filtering only on the fields and data shown on chart$")
	public void Allow_filtering_only_on_the_fields_and_data_shown_on_chart() throws Throwable {
		POM showndatachart = new POM();
		showndatachart.filter_only_chart_fields_And_Data();
	}

	@Then("^Select fields on which user can apply filters$")
	public void Select_fields_on_which_user_can_apply_filters() throws Throwable {
		POM showndatachart = new POM();
		showndatachart.filter_hdchecker();
	}

	@Given("^Click on Export Settings$")
	public void Click_on_Export_Settings() throws Throwable {
		POM Export_Settings = new POM();
		Export_Settings.Export_Settings();
	}

	@Then("^Click on Revert Button$")
	public void Click_on_Revert_Button() throws Throwable {
		POM Revert_Button = new POM();
		Revert_Button.Revert_Button();

	}

	@Then("^tick on Enable printing of chart$")
	public void tick_on_Enable_printing_of_chart() throws Throwable {
		POM Enable_printing_of_chart = new POM();
		Enable_printing_of_chart.Enable_printing_of_chart();
	}

	@Then("^tick on Enable exporting chart data as Excel Spreadsheet$")
	public void tick_on_Enable_exporting_chart_data_as_Excel_Spreadsheet() throws Throwable {
		POM Enable_exporting_chart_as_Excel = new POM();
		Enable_exporting_chart_as_Excel.Enable_exporting_chart_as_Excel();
	}

	@Then("^tick on Enable exporting charts as image or PDF$")
	public void tick_on_Enable_exporting_charts_as_image_or_PDF() throws Throwable {
		POM Enable_exporting_charts_as_image = new POM();
		Enable_exporting_charts_as_image.Enable_exporting_charts_as_image();
	}

	@Then("^give export file name$")
	public void give_export_file_name() throws Throwable {
		POM Export_file_name = new POM();
		Export_file_name.Export_file_name();
	}

	@Then("^Choose Sort By Dropdown$")
	public void Choose_Sort_By_Dropdown() throws Throwable {

		POM SortBy = new POM();
		SortBy.SortBy();
	}

	@Then("^Click on Apply to all$")
	public void Click_on_Apply_to_all() throws Throwable {
		POM Apply_to_all = new POM();
		Apply_to_all.Apply_to_all();
	}
	

   @Then("^Click on  Drilldown caption ok button$")
    public void click_on_Drilldown_caption_ok_button() throws Throwable {
	   POM DrilldowncaptionOK = new POM();
	   DrilldowncaptionOK.OK();
	 
   }
   
   
   
   @Then("^Click on  Drilldown caption cancel$")
   public void click_on_Drilldown_caption_cancel() throws Throwable {
	   POM DrilldowncaptionCancel = new POM();
	   DrilldowncaptionCancel.Cancel();
   }
   
   

	@Then("^click on Export Chart Button$")
	public void click_on_Export_Chart_Button() throws Throwable {
		POM ExportChartButton = new POM();
		ExportChartButton.ExportChartButton();
	}
	//////////////////// Apply Dynamic filter\\\\\\\\\\\\\\\\\\\\\\\\\

	@Given("^Click on Dynamic Filter$")
	public void Click_on_Dynamic_Filter() throws Throwable {

		POM ApplyDynamicFilter = new POM();
		ApplyDynamicFilter.ApplyDynamicFilter();
	}

	@Then("^click on preview filter first Dropdown$")
	public void click_on_preview_filter_first_Dropdown() throws Throwable {
		POM preview_filter_first_Dropdown = new POM();
		preview_filter_first_Dropdown.preview_filter_first_Dropdown();
	}

	@Then("^click on preview filter second Dropdown$")
	public void click_on_preview_filter_second_Dropdown() throws Throwable {
		POM preview_filter_second_Dropdown = new POM();
		preview_filter_second_Dropdown.preview_filter_second_Dropdown();

	}

	@Then("^search with data$")
	public void search_with_data() throws Throwable {
		POM search_with_data = new POM();
		search_with_data.search_with_data();
	}

	@Then("^Click on Preview button$")
	public void Click_on_Preview_button() throws Throwable {
		POM Preview_button = new POM();
		Preview_button.Preview_button();
	}

	@Given("^Click on Drilldown Chart$")
	public void Click_on_Drilldown_Chart() throws Throwable {
		POM Drilldown_Chart = new POM();
		Drilldown_Chart.Drilldown_Chart();
	}

	/////////////// Single Series Column3D Chart type////////////////////

	@Then("^Choose ColumnThreeD Chart$")
	public void Choose_ColumnThreeD_Chart() throws Throwable {
		POM ColumnThreeD_Chart = new POM();
		ColumnThreeD_Chart.ColumnThreeD_Chart();

	}

	/////////////// Single Series BarTwoD Chart type////////////////////

	@Then("^Choose BarTwoD Chart$")
	public void Choose_BarTwoD_Chart() throws Throwable {
		POM BarTwoD_Chart = new POM();
		BarTwoD_Chart.BarTwoD_Chart();
	}
	/////////////// Single Series LineTwoD Chart type////////////////////

	@Then("^Choose LineTwoD Chart$")
	public void Choose_LineTwoD_Chart() throws Throwable {
		POM Line2D = new POM();
		Line2D.LineTwoD_Chart();
	}

	/////////////////// Single Series LineTwoD Chart type......label,Values
	/////////////////// Tooltips -Anchors Tab page////////////////////////////

	@Given("^Click on Anchors Tab page$")
	public void Click_on_Anchors_Tab_page() throws Throwable {
		POM Anchors_Tab_page = new POM();
		Anchors_Tab_page.Anchors_Tab_page();
	}

	@Then("^Click on Show Anchors$")
	public void Click_on_Show_Anchors() throws Throwable {
		POM Show_Anchors = new POM();
		Show_Anchors.Show_Anchors();

	}

	@Then("^Give Visual Configuration Sides$")
	public void Give_Visual_Configuration_Sides() throws Throwable {
		POM Configuration_Sides = new POM();
		Configuration_Sides.Configuration_Sides();

	}

	@Then("^Give Visual Configuration Radius$")
	public void Give_Visual_Configuration_Radius() throws Throwable {
		POM Configuration_Radius = new POM();
		Configuration_Radius.Configuration_Radius();
	}

	@Then("^Give Visual Configuration Opaqueness$")
	public void Give_Visual_Configuration_Opaqueness() throws Throwable {
		POM Configuration_Opaqueness = new POM();
		Configuration_Opaqueness.Configuration_Opaqueness();
	}

	@Then("^Give Border color$")
	public void Give_Border_color() throws Throwable {
		POM Border_color = new POM();
		Border_color.Border_color();
	}

	@Then("^Give Border Tickness$")
	public void Give_Border_Tickness() throws Throwable {
		POM Border_Thickness = new POM();
		Border_Thickness.Border_Thickness();
	}

	@Then("^Give Background Opaqueness$")
	public void Give_Background_Opaqueness() throws Throwable {
		POM Background_Opaqueness = new POM();
		Background_Opaqueness.Background_Opaqueness();
	}

	@Then("^Give Background color$")
	public void Give_Background_color() throws Throwable {
		POM Background_color = new POM();
		Background_color.Background_color();
	}

	/////////////////// Single Series LineTwoD Chart type......Axis -Vertical
	/////////////////// grid lines////////////////////////////

	@Given("^Click on The Vertical Grid Lines$")
	public void Click_on_The_Vertical_Grid_Lines() throws Throwable {
		POM Vertical_Grid_Lines = new POM();
		Vertical_Grid_Lines.Vertical_Grid_Lines();
	}

	@Then("^Show Vertical Grid Lines$")
	public void Show_Vertical_Grid_Lines() throws Throwable {
		POM Show_Vertical_Grid_Lines = new POM();
		Show_Vertical_Grid_Lines.Show_Vertical_Grid_Lines();
	}

	@Then("^Give Number of Grid Lines$")
	public void Give_Number_of_Grid_Lines() throws Throwable {
		POM Number_of_Grid_Lines = new POM();
		Number_of_Grid_Lines.Number_of_Grid_Lines();
	}

	@Then("^Give Grid Line thickness$")
	public void Give_Grid_Line_thickness() throws Throwable {
		POM Grid_Line_thickness = new POM();
		Grid_Line_thickness.Grid_Line_thickness();
	}

	@Then("^Give Grid Line color$")
	public void Give_Grid_Line_color() throws Throwable {
		POM Grid_Line_color = new POM();
		Grid_Line_color.Give_Color();
	}

	@Then("^Give Grid Line opaqueness$")
	public void Give_Grid_Line_opaqueness() throws Throwable {
		POM Grid_Line_opaqueness = new POM();
		Grid_Line_opaqueness.Grid_Line_opaqueness();
	}

	@Then("^Show Grid Lines As Dashed$")
	public void Show_Grid_Lines_As_Dashed() throws Throwable {
		POM Grid_Lines_As_Dashed = new POM();
		Grid_Lines_As_Dashed.Show_Grid_Lines_As_Dashed();
	}

	@Then("^Give Grid Lines Dash Length$")
	public void Give_Grid_Lines_Dash_Length() throws Throwable {
		POM Grid_Lines_As_Dashed = new POM();
		Grid_Lines_As_Dashed.Grid_Lines_Dash_Length();
	}

	@Then("^Give Grid lines Dash Gap$")
	public void Give_Grid_lines_Dash_Gap() throws Throwable {
		POM Grid_Lines_As_Dashed = new POM();
		Grid_Lines_As_Dashed.Grid_lines_Dash_Gap();
	}

	@Then("^Show alternate color band$")
	public void Show_alternate_color_band() throws Throwable {
		POM Show_alternate_color_band = new POM();
		Show_alternate_color_band.Show_alternate_color_band();
	}

	@Then("^Give alternate color$")
	public void Give_alternate_color() throws Throwable {
		POM Give_alternate_color = new POM();
		Give_alternate_color.Give_alternate_color();
	}

	@Then("^Give alternate color Opaqueness$")
	public void Give_alternate_color_Opaqueness() throws Throwable {
		POM Give_alternate_color_Opaqueness = new POM();
		Give_alternate_color_Opaqueness.Give_alternate_color_Opaqueness();
	}
	/////////////////// Single Series SplineArea Chart
	/////////////////// type..////////////////////////////

	@Then("^Choose Spline Chart$")
	public void Choose_Spline_Chart() throws Throwable {
		POM Spline_Chart = new POM();
		Spline_Chart.Spline_Chart();
	}

	/////////////////// Single Series Area2D Chart
	/////////////////// type..////////////////////////////
	@Then("^Choose Area(\\d+)D Chart$")
	public void Choose_Area_D_Chart(int arg1) throws Throwable {
		POM Area2D_Chart = new POM();
		Area2D_Chart.Area2D_Chart();
	}

	/////////////////// Single Series SplineArea Chart
	/////////////////// type..////////////////////////////

	@Then("^Choose Spline Area Chart$")
	public void Choose_Spline_Area_Chart() throws Throwable {
		POM Spline_Area_Chart = new POM();
		Spline_Area_Chart.SplineArea_Chart();
	}

	@Then("^click on Revert button$")
	public void click_on_Revert_button() throws Throwable {
		Thread.sleep(1000);
		POM RevertButton = new POM();
		RevertButton.RevertButton();
	}

	//////////////////////// MultiSeries -column 2D////////////////////

	@Given("^Choose MultiSeries from Chart Category Dropdown$")
	public void Choose_MultiSeries_from_Chart_Category_Dropdown() throws Throwable {

	}

	@Given("^Click on Legend$")
	public void Click_on_Legend() throws Throwable {
		POM LegendClick = new POM();
		LegendClick.MultiSeriesLegendClick();
	}

	@Then("^Give Captions$")
	public void Give_Captions() throws Throwable {
		POM LegendCaptions = new POM();
		LegendCaptions.MultiSeriesCaption();
	}

	@Then("^Choose Position$")
	public void Choose_Position() throws Throwable {
		POM LegendPosition = new POM();
		LegendPosition.MultiSeriesPosition();
	}

	@Then("^Give size of legend key$")
	public void Give_size_of_legend_key() throws Throwable {
		POM Legendkey = new POM();
		Legendkey.MultiSeriesLegendSize();
	}

	@Then("^Give number of rows in legend$")
	public void Give_number_of_rows_in_legend() throws Throwable {
		POM LegendRows = new POM();
		LegendRows.MultiSeriesLegendRows();
	}

	@Then("^tick on Reverse order$")
	public void tick_on_Reverse_order() throws Throwable {
		POM LegendReverse = new POM();
		LegendReverse.MultiSeriesLegendReverse();
	}

	@Then("^tick Enable drag operation on legend$")
	public void tick_Enable_drag_operation_on_legend() throws Throwable {
		POM LegendDrag = new POM();
		LegendDrag.MultiSeriesLegendDrag();
	}

	@Then("^Give Legend Background Color$")
	public void Give_Legend_Background_Color() throws Throwable {

	}

	@Then("^Give Legend Background Opaqueness$")
	public void Give_Legend_Background_Opaqueness() throws Throwable {
		POM LegendOpaqness = new POM();
		LegendOpaqness.MultiSeriesLegendBackgroungOpaqness();
	}

	@Then("^Give Legend Background Shadow$")
	public void Give_Legend_Background_Shadow() throws Throwable {
		POM LegendShadow = new POM();
		LegendShadow.MultiSeriesLegendBackgroungShadow();
	}

	@Then("^Give Legend Border Color$")
	public void Give_Legend_Border_Color() throws Throwable {

	}

	@Then("^Give Legend Border Opaqueness$")
	public void Give_Legend_Border_Opaqueness() throws Throwable {
		POM BorderOpaqness = new POM();
		BorderOpaqness.MultiSeriesLegendBorderOpaqness();
	}

	@Then("^Give Legend Border Thickness$")
	public void Give_Legend_Border_Thickness() throws Throwable {
		POM BorderThickness = new POM();
		BorderThickness.MultiSeriesLegendBorderThickness();
	}

	@Then("^Click on Legend Apply Button$")
	public void Click_on_Legend_Apply_Button() throws Throwable {
		POM Apply = new POM();
		Apply.MultiSeriesLegendApply();
	}

	///////////////// Multiseries-Column 3D/////////////////

	@Then("^Choose MultiSeries Column(\\d+)D Chart$")
	public void Choose_MultiSeries_Column_D_Chart(int arg1) throws Throwable {
		Thread.sleep(3000);
		POM twoD = new POM();
		twoD.MultiSeries2DChartChoose();

	}

	@Then("^Choose MultiSeries Column(\\d+)D Chart for SharePoint (\\d+)$")
	public void choose_MultiSeries_Column_D_Chart_for_SharePoint(int arg1, int arg2) throws Throwable {
		POM twoD = new POM();
		twoD.SharePoint2016MultiSeries2DChartChoose();
	}

	@Then("^Click Chart Category Apply Button$")
	public void Click_Chart_Category_Apply_Button() throws Throwable {

		Thread.sleep(2000);
		POM twoD = new POM();
		twoD.Chart_Category_Apply_Button();

	}

	@Then("^Choose MultiSeries ColumnThreeD Chart$")
	public void Choose_MultiSeries_ColumnThreeD_Chart() throws Throwable {
		POM ColumnThreeD_Chart = new POM();
		ColumnThreeD_Chart.MultiSeries3DChartChoose();

	}

	@Then("^Choose MultiSeries ColumnThreeD Chart for SharePoint (\\d+)$")
	public void choose_MultiSeries_ColumnThreeD_Chart_for_SharePoint(int arg1) throws Throwable {
		POM ColumnThreeD_Chart = new POM();
		ColumnThreeD_Chart.SharePoint2016MultiSeries3DChartChoose();
	}

	@Then("^Give on (\\d+)D Show Canvas$")
	public void Give_on_D_Show_Canvas(int arg1) throws Throwable {
		POM Canvas = new POM();
		Canvas.MultiSeries3DShowCanvas();
	}

	@Then("^Give on (\\d+)D Show Base$")
	public void Give_on_D_Show_Base(int arg1) throws Throwable {
		POM Base = new POM();
		Base.MultiSeries3DShowBase();
	}

	@Then("^Give on (\\d+)D Canvas Thickness$")
	public void Give_on_D_Canvas_Thickness(int arg1) throws Throwable {
		POM Thickness = new POM();
		Thickness.MultiSeries3DShowCanvasThickness();
	}

	@Then("^Give on (\\d+)D Base Depth$")
	public void Give_on_D_Base_Depth(int arg1) throws Throwable {
		POM Depth = new POM();
		Depth.MultiSeries3DShowCanvasDepth();
	}

	@Then("^Give (\\d+)D chart Border$")
	public void Give_D_chart_Border(int arg1) throws Throwable {
		POM ZeroPlaneBorder = new POM();
		ZeroPlaneBorder.MultiSeries3DZeroPlaneChartBorder();
	}

	///////////// Webpart TextFilter Related Test////////////////////
	@Given("^filter Data in 'TextFilter' box$")
	public void filter_Data_in_TextFilter_box() throws Throwable {
		POM TextFilter = new POM();
		// TextFilter.TextFilter();
	}

	@Then("^Capture Current Image$")
	public void Capture_Current_Image() throws Throwable {
		POM Capture_Image = new POM();
		// Capture_Image.getscreenshot();
	}

	@Then("^Compare Expected Image with Current Image$")
	public void Compare_Expected_Image_with_Current_Image() throws Throwable {
		POM Compare_Image = new POM();
		String exp = null;
		String cur = null;
		String diff = null;
		// Compare_Image.compareImages(exp, cur, diff);
	}

	@Then("^Generate TestScenario Result Report$")
	public void Generate_TestScenario_Result_Report() throws Throwable {
		POM ReportGeneration = new POM();
		// ReportGeneration.ReportGeneration();
	}

	@Then("^Send Html Report in mail$")
	public void Send_Html_Report_in_mail() throws Throwable {
		POM HTML = new POM();
		// HTML.HTMLAttachment();
	}

	@Then("^click on DrillDown Bar$")
	public void click_on_DrillDown_Bar() throws Throwable {
		// Thread.sleep(2000);
		POM DrillDown = new POM();
		DrillDown.ClickOnDrilldownBar();
	}

	//////////////////////////////// ChoiceFilter&DynamicFilterCombination////////////////////

	@Given("^Choose Choice Filter$")
	public void Choose_Choice_Filter() throws Throwable {
		POM Choice_Filter = new POM();
		// Choice_Filter.DynamicFilterValue();
		Choice_Filter.ChoiceFilterSelection();

	}

	// ***************************************************MultiLevel
	// DrillDOWN********************************************************************************************************************************************
	@Then("^Click on the Site Pages HyperLink$")
	public void Click_on_the_Site_Pages_HyperLink() throws Throwable {
		POM drill1 = new POM();
		drill1.OpenSitePages();
	}

	@Then("^Click on the DrillDownPage HyperLink$")
	public void Click_on_the_DrillDownPage_HyperLink() throws Throwable {
		POM drill2 = new POM();
		drill2.OpenDrillDown();
	}

	@Then("^Click on ExcelList First Level bar in the chart$")
	public void Click_on_ExcelList_First_Level_bar_in_the_chart() throws Throwable {
		POM drill3 = new POM();
		drill3.Excel_FirstLevelDrillDown();
		drill3.Drilldown_Chart_XML_FirstLevel();
	}

	@Then("^Click on ExcelList Second Level bar in the chart$")
	public void Click_on_ExcelList_Second_Level_bar_in_the_chart() throws Throwable {
		POM drill4 = new POM();
		drill4.SharepointList_SecondLevelDrillDown();
		drill4.Drilldown_Chart_XML_FirstLevel();
	}

	@Then("^Click on any bar in the chart$")
	public void Click_on_any_bar_in_the_chart() throws Throwable {
		POM drill3 = new POM();
		drill3.SQLServerFirstLevelDrillDown();
		drill3.Drilldown_Chart_XML_FirstLevel();
		// drill3.Drilldown_Chart_XML_FirstLevel();

		// drill3.SQLServerFirstLevelDrillDown() ;
		// Thread.sleep(20000);
		// drill3.Drilldown_Chart_XML_FirstLevel();
		// Thread.sleep(5000);

	}

	@Then("^Click on a bar on the Second level DrillDown$")
	public void Click_on_a_bar_on_the_Second_level_DrillDown() throws Throwable {
		POM drill4 = new POM();
		drill4.SQLServerSecondLevelDrillDown();
		// drill4.SQLServerFirstLevelDrillDown();
		// drill4.Drilldown_Chart_XML_FirstLevel();
	}

	@Then("^Click on SharePointList First Level bar in the chart$")
	public void Click_on_SharePointList_First_Level_bar_in_the_chart() throws Throwable {
		POM drill3 = new POM();
		drill3.SharepointList_FirstLevelDrillDown();
		drill3.Drilldown_Chart_XML_FirstLevel();
	}

	@Then("^Click on SharePointList Second Level bar in the chart$")
	public void Click_on_SharePointList_Second_Level_bar_in_the_chart() throws Throwable {
		POM drill4 = new POM();
		drill4.SharepointList_SecondLevelDrillDown();
		drill4.Drilldown_Chart_XML_FirstLevel();
	}

	@Then("^Click on SharepointListData Multi Level bars Upto last level$")
	public void Click_on_SharepointListData_Multi_Level_bars_Upto_last_level() throws Throwable {
		POM SharepointListDrill = new POM();
		SharepointListDrill.SharepointlistDrilldown();
	}

	@Then("^Click on SQLServerDatabase Multi Level bars Upto last level$")
	public void Click_on_SQLServerDatabase_Multi_Level_bars_Upto_last_level() throws Throwable {
		POM SQLServerDatabaseDrill = new POM();
		SQLServerDatabaseDrill.SQLServerDatabaseDrilldown();
	}

	// *****************************************************END
	// MultiLevelDrillDown***********************************************************************************************************************************************

	// ****************************************************CombinationOFTextAndDynamicFilter*************************************************************************************************************************

	/*
	 * @Given("^provide a dynamic Filter.$") public void
	 * provide_a_dynamic_Filter() throws Throwable {
	 * 
	 * POM dynamicFilter=new POM(); dynamicFilter.DynamicFilterValue();
	 * 
	 * 
	 * 
	 * }
	 */

	@Given("^provide a dynamic Filter$")
	public void provide_a_dynamic_Filter() throws Throwable {

		// POM TextDataDrivenTesting = new POM();
		// TextDataDrivenTesting.TextDataDrivenTesting();

		POM dynamicFilter = new POM();
		dynamicFilter.DynamicFilterValue();

		// Express the Regexp above with the code you wish you had

	}

	@Given("^provide a dynamic Filter SQL$")
	public void provide_a_dynamic_Filter_SQL() throws Throwable {
		POM dynamicFilter = new POM();
		dynamicFilter.DynamicFilterValueSQL();
	}

	@Then("^Provide a value for the TextFilter.$")
	public void Provide_a_value_for_the_TextFilter() throws Throwable {

		POM textfilter = new POM();
		textfilter.TextFilterValue();

	}

	// ***********************************************************END
	// C&D***************************************************************************************************************************************************
	// **************************************************Share Point List
	// Filter******************************************************************************************************************************************
	@Given("^go to the Sharepoint List Page$")
	public void go_to_the_Sharepoint_List_Page() throws Throwable {

		new POM().openListFilterPage();
		;
	}

	//////////////////////// *****************Data are there in Excel sheet-From
	//////////////////////// that Excel sheet passing the data to
	//////////////////////// application******************************/////////////////////////
	///////////////////////////////// Data Driven Testing-Text Filter search
	//////////////////////// with multiple
	//////////////////////// data/////////////////////////////////////////////////////////
	@Then("^First Give data in Text filter And Search$")
	public void First_Give_data_in_Text_filter_And_Search() throws Throwable {

		POM TextDataDrivenTesting = new POM();
		TextDataDrivenTesting.ExcTextDataDrivenTesting();
		// TextDataDrivenTesting.Drilldown_Chart_XML();

		// TextDataDrivenTesting.Excelwriting();

	}

	//////////////////////// *****************Data are there in Excel sheet-From
	//////////////////////// that Excel sheet passing the data to
	//////////////////////// application******************************/////////////////////////
	///////////////////////////////// Data Driven Testing-Choice Filter search
	//////////////////////// with multiple
	//////////////////////// data/////////////////////////////////////////////////////////
	@Then("^First Give data in Choice Filter And Search$")
	public void First_Give_data_in_Choice_Filter_And_Search() throws Throwable {

		POM ChoiceFilterDataDrivenTesting = new POM();
		ChoiceFilterDataDrivenTesting.ExcChoiceFilterDataDrivenTesting();
		// Thread.sleep(3400);
		// ChoiceFilterDataDrivenTesting.Drilldown_Chart_XML();
	}

	//////////////////////// *****************Data are there in Excel sheet-From
	//////////////////////// that Excel sheet passing the data to
	//////////////////////// application******************************/////////////////////////
	///////////////////////////////// Data Driven Testing-Dynamic Filter search
	//////////////////////// with multiple
	//////////////////////// data/////////////////////////////////////////////////////////
	@Then("^First Give data in Dynamic filter From Excel$")
	public void First_Give_data_in_Dynamic_filter_From_Excel() throws Throwable {

		POM DynamicFilterDataDrivenTesting = new POM();
		DynamicFilterDataDrivenTesting.ExcDynamicFilterDataDrivenTesting();
		DynamicFilterDataDrivenTesting.Drilldown_Chart_XML();
	}

	////////////////////
	///////////////////////////////// Data Driven Testing-Text Filter search
	///////////////////////////////// with multiple
	///////////////////////////////// Cucumber Data
	//////////////////// Driven/////////////////////////////////////////////////////////
	@Then("^User provides \"([^\"]*)\" to the TextFilter$")
	public void User_provides_to_the_TextFilter(String arg1) throws Throwable {
		// Express the Regexp above with the code you wish you had
		POM TextDataDrivenTesting = new POM();
		TextDataDrivenTesting.TextDataDrivenTesting(arg1);
		// TextDataDrivenTesting.Drilldown_Chart_XML();

		// TextDataDrivenTesting.Excelwriting();

	}

	///////////////////////////////// Data Driven Testing-Choice Filter search
	///////////////////////////////// with multiple
	///////////////////////////////// Cucumber Data
	///////////////////////////////// Driven/////////////////////////////////////////////////////////
	@Then("^The user Selects the \"([^\"]*)\" for the Choice Filter$")
	public void The_user_Selects_the_for_the_Choice_Filter(String arg1) throws Throwable {
		POM ChoiceFilterDataDrivenTesting = new POM();
		ChoiceFilterDataDrivenTesting.ChoiceFilterDataDrivenTesting(arg1);
		// Thread.sleep(3400);
		// ChoiceFilterDataDrivenTesting.Drilldown_Chart_XML();
	}

	///////////////////////////////// Data Driven Testing-Dynamic Filter search
	///////////////////////////////// with multiple
	///////////////////////////////// Cucumber Data
	///////////////////////////////// Driven/////////////////////////////////////////////////////////

	@Then("^User selects the \"([^\"]*)\" and \"([^\"]*)\" and  \"([^\"]*)\" joins by \"([^\"]*)\" with \"([^\"]*)\" and \"([^\"]*)\" and  \"([^\"]*)\"$")
	public void User_selects_the_and_and_joins_by_with_and_and(String arg1, String arg2, String arg3, String arg4,
			String arg5, String arg6, String arg7) throws Throwable {

		POM DynamicFilterDataDrivenTesting = new POM();
		DynamicFilterDataDrivenTesting.DynamicFilterDataDrivenTesting(arg1, arg2, arg3, arg4, arg5, arg6, arg7);
		// DynamicFilterDataDrivenTesting.Drilldown_Chart_XML();
	}
	


	///////////////////////////////// Data Driven Testing-Date Filter search
	///////////////////////////////// with multiple
	///////////////////////////////// data/////////////////////////////////////////////////////////

	@Then("^First Give data in Date Filter And Search$")
	public void First_Give_data_in_Date_Filter_And_Search() throws Throwable {
		POM Date = new POM();
		Date.DateFilterDataDrivenTesting();
	}

	///////////////////////////////// Data Driven Testing-Text Filter search
	///////////////////////////////// with multiple
	///////////////////////////////// data/////////////////////////////////////////////////////////

	@Then("^Give data in Text filter And Search$")
	public void Give_data_in_Text_filter_And_Search() throws Throwable {
		POM TextFilter = new POM();
		TextFilter.SharepointlistFilterDataDrivenTesting();
	}

	////// ***********************************************Data
	////// Comparison***********************************************************************************************************************************************
	@Then("^Pick the  desired data and put it into excel File$")
	public void Pick_the_desired_data_and_put_it_into_excel_File() throws Throwable {
		// Express the Regexp above with the code you wish you had
		POM datacomp = new POM();
		datacomp.datavalidation("C://Users//sayan.sikdar//Desktop//SalesDataSheet.xlsx", "Sheet3", 0);
	}

	@Then("^Click on the button to choose from options$")
	public void Click_on_the_button_to_choose_from_options() throws Throwable {
		new POM().ClickToChoose();

	}

	@Then("^Select the proper choice.$")
	public void Select_the_proper_choice() throws Throwable {
		// Express the Regexp above with the code you wish you had

	}

	@Then("^Click on the 'APPLY' button.$")
	public void Click_on_the_APPLY_button() throws Throwable {
		// Express the Regexp above with the code you wish you had

	}

	// **********************************************************************************************************************************************************************************************************************

	// *************************************************************WIZARD
	// FILTER DATA DRIVEN
	// TESTING************************************************************************************************************************
	@Given("^User clicks on the Filter Data section$")
	public void user_clicks_on_the_Filter_Data_section() throws Throwable {

	}

	@Then("^User Checks if any filter is applied already and deletes them$")
	public void user_Checks_if_any_filter_is_applied_already_and_deletes_them() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		POM P1 = new POM();
		P1.CheckToAddFilter();
	}

	@Then("^User adds a new filter and provides the \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\" and Uses \"([^\"]*)\" to join with \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_adds_a_new_filter_and_provides_the_and_and_and_Uses_to_join_with_and_and(String arg1, String arg2,
			String arg3, String arg4, String arg5, String arg6, String arg7) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		POM P1 = new POM();
		P1.AddFilterCondition();
	}

	@Then("^User Clicks on The apply button\\.$")
	public void user_Clicks_on_The_apply_button() throws Throwable {
		// Write code here that turns the phrase above into concrete actions

	}

	///////////////////////////////////////////////////// ***********CCSP
	///////////////////////////////////////////////////// Standard Version
	///////////////////////////////////////////////////// sharePoint2016********************************/////////////////////////////////////////////

	@Given("^go to CCSP License Page$")
	public void go_to_CCSP_License_Page() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.CCSPLicense();
	}

	@Then("^click on 'Add License'$")
	public void click_on_Add_License() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.CCSPLicenseAdd();
	}

	@Then("^select server$")
	public void select_server() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.CCSPLicenseSelectServer();
	}

	@Given("^enter license key$")
	public void enter_license_key() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.CCSPLicenseKey();
	}

	@Then("^Click on 'Add' button$")
	public void click_on_Add_button() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.Add();
	}

	//////////////////////////////// *************************SQL Authentication
	//////////////////////////////// SharePoint
	//////////////////////////////// 2016*****************************////////////////////////////
	@Then("^Choose Windows Authentication from Authentication Dropdown$")
	public void choose_Windows_Authentication_from_Authentication_Dropdown() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.WindowsAuthentication();
	}

	@Then("^Choose SQL Authentication from Authentication Dropdown$")
	public void choose_SQL_Authentication_from_Authentication_Dropdown() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.SQLAuthentication();
	}

	@Then("^Choose Domain Authentication from Authentication Dropdown$")
	public void choose_Domain_Authentication_from_Authentication_Dropdown() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.DomainAuthentication();
	}

	@Then("^Give Domain Name$")
	public void give_Domain_Name() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.DomainName();
	}

	@Then("^Give User name$")
	public void give_User_name() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.UserName();
	}

	@Then("^Give Password$")
	public void give_Password() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.Password();
	}

	////////////////////// *************In User interaction "Sorry! This feature
	////////////////////// is available in Pro Mode only!" message
	////////////////////// verification******************/////////////////////////////

	@Then("^Verify Feature Not Available Message$")
	public void verify_Feature_Not_Available_Message() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.VerificationAlert();
	}

	///////////////// *******************User Interaction Selection Bar
	///////////////// Color(Orange)
	///////////////// Verification***************////////////////////////////

	@Then("^Verify Selection Bar color$")
	public void verify_Selection_Bar_color() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.getCssValue_Color();
	}

	///////////////// *******************User Interaction Non Clickable Field
	///////////////// Verification***************////////////////////////////

	@Then("^Check Drill Down Non-Clickable Field$")
	public void check_Drill_Down_Non_Clickable_Field() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.DrillDownNonClickableFieldCheck();
	}

	@Then("^Check Dynamic Filter Non-Clickable Field$")
	public void check_Dynamic_Filter_Non_Clickable_Field() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.DynamicFilterNonClickableFieldCheck();
	}

	@Then("^Check Export Settings Non-Clickable Field$")
	public void check_Export_Settings_Non_Clickable_Field() throws Throwable {
		CCSP_StandardVersion_POM site = new CCSP_StandardVersion_POM();
		site.ExportSettingsNonClickableFieldCheck();
	}

	// *************************************************************************************************************************************************************************************************************************

	CCSP_StandardVersion_POM PomIns1 = new CCSP_StandardVersion_POM();

	/*
	 * The following Given is The background for all the Tests that are going to
	 * be run ,The Text in the given is self explanatory First , It clicks on
	 * the Edit Link ,Then it Clicks the arrow and Clicks on the Edit Chart
	 * Option.
	 */
	@Given("^The wizard is open as the user has clicked on the Edit link and Edit arrow\\.$")
	public void the_wizard_is_open_as_the_user_has_clicked_on_the_Edit_link_and_Edit_arrow() throws Throwable {

		PomIns1.ChartEditing();
	}

	// ***************************************************************Backgorund*********************************************************************************************

	// ************************************************************Excel as
	// DataSource****************************************************************************************

	@Then("^Selects Dataprovider as ExcelSheet$")
	public void selects_Dataprovider_as_ExcelSheet() throws Throwable {
		PomIns1.ExcelDataselection();
	}

	@Then("^Provides the path of (\\d+) ExcelFile\\.$")
	public void provides_the_path_of_ExcelFile(int arg1) throws Throwable {
		PomIns1.ExcelPathSelection2010();
	}

	@Then("^Provides the path of ExcelFile\\.$")
	public void provides_the_path_of_ExcelFile() throws Throwable {
		PomIns1.ExcelPathSelection();
	}

	@Then("^Click on the load button\\.$")
	public void click_on_the_load_button() throws Throwable {
		PomIns1.ClickLoad();
	}

	@Then("^Click on the OK button\\.$")
	public void click_on_the_OK_button() throws Throwable {
		PomIns1.OkClick();
	}

	@Then("^Select the Sheet Name\\.$")
	public void select_the_Sheet_Name() throws Throwable {
		PomIns1.sheetSelection();
	}

	@Then("^Click on the Connect button\\.$")
	public void click_on_the_Connect_button() throws Throwable {

		PomIns1.ConnectingExcel();
	}
	// *****************************************************************************************************************************************************************************

	// ************************************************************************************Field
	// selection**************************************************************************

	@Then("^Click on SelectFields$")
	public void click_on_SelectFields() throws Throwable {
		PomIns1.selecFields();
	}
	// ***************************************************************************************Grouping*******************************************************************************

	@Given("^Click On Group Data and verify the statutory Message\\.$")
	public void click_On_Group_Data_and_verify_the_statutory_Message() throws Throwable {
		PomIns1.groupingStatutorymessage();

	}

	@Then("^Enable Group Data\\.$")
	public void enable_Group_Data() throws Throwable {

		PomIns1.enableGrouping();
	}

	@Then("^Select the Group By Field\\.$")
	public void select_the_Group_By_Field() throws Throwable {
		PomIns1.GroupByFieldSelection();
	}

	@Then("^Verify The 'Distinct Values' Option is not clickable\\.$")
	public void verify_The_Distinct_Values_Option_is_not_clickable() throws Throwable {
		PomIns1.NegativeGroupSeriesTest();
	}

	@Then("^Selects the 'one or more fields' Option\\.$")
	public void selects_the_one_or_more_fields_Option() throws Throwable {
	}

	@Then("^Verify that aggregate by 'Average' is not allowed\\.$")
	public void verify_that_aggregate_by_Average_is_not_allowed() throws Throwable {
		PomIns1.negativeAgrregateFunctionAVGTest();
	}

	@Then("^Verify that aggregate by 'MIN' is not allowed\\.$")
	public void verify_that_aggregate_by_MIN_is_not_allowed() throws Throwable {
		PomIns1.negativeAgrregateFunctionMINTest();
	}

	@Then("^Verify that aggregate by 'MAX' is not allowed\\.$")
	public void verify_that_aggregate_by_MAX_is_not_allowed() throws Throwable {
		PomIns1.negativeAgrregateFunctionMAXTest();
	}

	@Then("^Verify that aggregate by 'Sum' is allowed and performed succesfully\\.$")
	public void verify_that_aggregate_by_Sum_is_allowed_and_performed_succesfully() throws Throwable {
		PomIns1.PositiveAggregateFunctionTesting();
	}

	// ***************************************************************TOP-N-RECORDS****************************************************************************************************************
	@Then("^Check The Option Background Color\\.$")
	public void check_The_Option_Background_Color() throws Throwable {
		PomIns1.topnrecordsOptionbackgroundCheck();
	}

	@Then("^Check whether the TOP-N-RECORDS Funtionalities are displaed\\.$")
	public void check_whether_the_TOP_N_RECORDS_Funtionalities_are_displaed() throws Throwable {
		PomIns1.TopnRecordsFuncltyDisbldCheck();

	}

	// *******************************************************AutoRefreshUsing
	// Excel**********************************************************
	@Given("^Take ScreenShot of the actual Chart\\.$")
	public void take_ScreenShot_of_the_actual_Chart() throws Throwable {
		Thread.sleep(5000);
		PomIns1.screencapturer();
	}

	@Given("^Redirection to home page is complete$")
	public void redirection_to_home_page_is_complete() throws Throwable {
		PomIns1.InitializeAutorefeshProcess();
	}

	@Then("^Click on Documents Hyperlink$")
	public void click_on_Documents_Hyperlink() throws Throwable {
		PomIns1.ClickDocumentLibrary();
	}

	@Then("^Click On Upload Option$")
	public void click_On_Upload_Option() throws Throwable {
		PomIns1.UploadExcel();
		Thread.sleep(2000);

	}

	@Then("^Run the AutoIT script$")
	public void run_the_AutoIT_script() throws Throwable {
		PomIns1.RunAutoItScript();
	}

	@Then("^Click OK\\.$")
	public void click_OK() throws Throwable {

		PomIns1.ClickOkButton();

	}

	@Then("^Redirect TO ccsp standard automation page$")
	public void redirect_TO_ccsp_standard_automation_page() throws Throwable {
		PomIns1.ccspStndardRdrctn();
	}

	@Then("^Take Screenshotafter alteration of datasource\\.$")
	public void take_Screenshotafter_alteration_of_datasource() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		PomIns1.screenComparerer();
	}

	@Then("^Give Wrong URL (\\d+)$")
	public void give_Wrong_URL(int arg1) throws Throwable {
		new POM().WrongUrlOfExcel2010();
	}

	@Then("^Give Url of Excel of \"([^\"]*)\" version$")
	public void give_Url_of_Excel_of_version(String arg1) throws Throwable {
		new POM().NewUrlOfExcel2010();
	}
	// ****************************************************************************************************************SQL
	// SERVER NEGATIVE DATASOURCE
	// TESTING**********************************************************************************************

	@Given("^Click on the Authetication DropDown and Select Domain Autheticaion$")
	public void click_on_the_Authetication_DropDown_and_Select_Domain_Autheticaion() throws Throwable {
		new POM().SelectAutheticationMethod();
	}

	@Then("^Click on Connects$")
	public void click_on_Connects() throws Throwable {
		new POM().clickconnect();

	}

	@Then("^Check for the red border color\\.$")
	public void check_for_the_red_border_color() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new POM().CheckErrorBackGroundColor();

	}

	@Then("^Provide Incorrect Domain Name$")
	public void provide_Incorrect_Domain_Name() throws Throwable {
		POM PP = new POM();
		PP.WriteDomainName();
		PP.WriteServerName();
	}

	@Then("^Provide incorrect User Name$")
	public void provide_incorrect_User_Name() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		POM PP = new POM();
		PP.WriteUserName();
	}

	@Then("^Provide incorrect password$")
	public void provide_incorrect_password() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		POM PP = new POM();
		PP.WritePassWord();
	}

	@Then("^Provide incorrect DB Name$")
	public void provide_incorrect_DB_Name() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		POM PP = new POM();
		PP.WrongDatabaseName();
	}

	@Then("^Check for the Error Dialogue\\.$")
	public void check_for_the_Error_Dialogue() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new POM().CheckErrDial();
	}

	@Then("^leave all the input fields empty\\.$")
	public void leave_all_the_input_fields_empty() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new POM().clearAllfields();
	}

	// **************************************************************************************************************************************************************************************************************************************************************

	// *************************************Excel
	// Password*************************************************************************************************************
	@Then("^Give ExcelPath$")
	public void give_ExcelPath() throws Throwable {
		new POM().ExcelPath();
	}

	@Then("^give ExcelPassword$")
	public void give_ExcelPassword() throws Throwable {
		new POM().ExcelPassword();
	}

	@Then("^select Sheet$")
	public void select_Sheet() throws Throwable {
		new POM().SheetSelection();
	}

	@Then("^select aggregate function field and function$")
	public void select_aggregate_function_field_and_function() throws Throwable {
		new POM().aggregatefunctionSelection();
	}

	@Then("^click on any bar on chart$")
	public void click_on_any_bar_on_chart() throws Throwable {
		new POM().ClickDrillDownBar();
	}

	@Then("^provide data in the input field$")
	public void provide_data_in_the_input_field() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		new POM().providefilterdata();
	}

	@Then("^give wrong  ExcelPassword$")
	public void give_wrong_ExcelPassword() throws Throwable {
		new POM().ExcelWrongPassword();
	}

	@Then("^Check of the dialog box and click OK\\.$")
	public void check_of_the_dialog_box_and_click_OK() throws Throwable {

		new POM().ExcelWrongPasswordPopUpverification();
	}

	@Then("^Check of the Enter password dialog box and click OK\\.$")
	public void check_of_the_Enter_password_dialog_box_and_click_OK() throws Throwable {

		new POM().ExcelNoPasswordPopUpverification();
	}

	@Then("^give empty ExcelPassword$")
	public void give_empty_ExcelPassword() throws Throwable {
		new POM().ExcelNoPassword();

	}
/////////////////START////////////////Data source-Oracle Database//////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////

@Then("^Select Oracle Database as DataSource$")
public void select_Oracle_Database_as_DataSource() throws Throwable {
	POM Oracle = new POM();
	Oracle.Oracle_Database();
}

@Then("^Give Oracle Server Name$")
public void give_Oracle_Server_Name() throws Throwable {
	POM Oracle = new POM();
	Oracle.Oracle_ServerName();
}

@Then("^Give Oracle User Name$")
public void give_Oracle_User_Name() throws Throwable {
	POM Oracle = new POM();
	Oracle.Oracle_UserName();
}

@Then("^Give Oracle User Password$")
public void give_Oracle_User_Password() throws Throwable {
	POM Oracle = new POM();
	Oracle.Oracle_UserPassword();
}
////////////////END////////////////Data source-Oracle Database//////////////////////////

//////////////START////////////////////////////////https://fusioncharts.jira.com/browse/SPC-1579////////////////////////////////////

/*@Then("^filter data select the \"([^\"]*)\" and \"([^\"]*)\" with search  \"([^\"]*)\"$")
public void filter_data_select_the_and_with_search(String arg1, String arg2, String arg3) throws Throwable {
	POM DynamicFilterDataDrivenTesting = new POM();
	DynamicFilterDataDrivenTesting.FilterDataDrivenTesting(arg1, arg1, arg3);
}*/

/*@Then("^filter data select the \"([^\"]*)\" and \"([^\"]*)\" with search  \"([^\"]*)\"$")
public void filter_data_select_the_and_with_search(String field1, String field2, String field3,DataTable table) throws Throwable {
	POM DynamicFilterDataDrivenTesting = new POM();
	DynamicFilterDataDrivenTesting.FilterDataDrivenTesting(field1, field2, field3,table);
}*/
@Then("^filter with \"([^\"]*)\"$")
public void filter_with(String arg1) throws Throwable {
	POM Column = new POM();
	Column.Column(arg1);
}

@Then("^select the \"([^\"]*)\"$")
public void select_the(String arg1) throws Throwable {
	POM Fieldvalue = new POM();
	Fieldvalue.Fieldvalue(arg1);
}

@Then("^search with \"([^\"]*)\"$")
public void search_with(String arg1) throws Throwable {
	POM Searchvalue = new POM();
	Searchvalue.Searchvalue(arg1);
}

@Then("^give \"([^\"]*)\"$")
public void give(int arg1) throws Throwable {
	POM SpecificSearch = new POM();
	SpecificSearch.SpecificSearch(arg1);
}



@Then("^search \"([^\"]*)\"$")
public void search(String arg1) throws Throwable {
	POM Particulars = new POM();
	Particulars.Particulars(arg1);
}

//////////////END////////////////////////////////https://fusioncharts.jira.com/browse/SPC-1579////////////////////////////////////


@Then("^choose options from Select List Dropdown$")
public void choose_options_from_Select_List_Dropdown() throws Throwable {
	POM Select_List_Dropdown = new POM();
	Select_List_Dropdown.List_Dropdown();
}

@Then("^choose options from Select List View Dropdown$")
public void choose_options_from_Select_List_View_Dropdown() throws Throwable {
	POM Select_List_View_Dropdown = new POM();
	Select_List_View_Dropdown.List_View_Dropdown();
}

@Then("^give first date\"([^\"]*)\"$")
public void give_first_date(String arg1) throws Throwable {
	POM FirstDate = new POM();
	FirstDate.First_Date(arg1);
}

@Then("^give second date\"([^\"]*)\"$")
public void give_second_date(String arg1) throws Throwable {
	POM SecondDate = new POM();
	SecondDate.Second_Date(arg1);
}

@Then("^search with date\"([^\"]*)\"$")
public void search_with_date(String arg1) throws Throwable {
	POM Date = new POM();
	Date.Date(arg1);
}

/*@Then("^Contains or Begins With <value>$")
public void contains_or_Begins_With_value(String arg1) throws Throwable {
	POM CB = new POM();
	CB.Contains_BeginsWith(arg1);
}*/

@Then("^Contains or Begins \"([^\"]*)\"$")
public void contains_or_Begins(String arg1) throws Throwable {
	POM CB = new POM();
	CB.Contains_BeginsWith(arg1);
}

@Then("^go to publishing site$")
public void go_to_publishing_site() throws Throwable {
	POM PS = new POM();
	PS.PublishingSiteNavigate();
}
@Then("^go to publishing site and Sign In$")
public void go_to_publishing_site_and_Sign_In() throws Throwable {
	POM PS = new POM();
	PS.PublishingSiteNavigate();
}
@Then("^capture Image$")
public void capture_Image() throws Throwable {
	POM PS = new POM();
	PS.CaptureImage();
}
@Given("^Click on Arka Sign In$")
public void click_on_Arka_Sign_In() throws Throwable {
	POM PS = new POM();
	PS.ArkaSignIn();
}
@Then("^Give Url of Excel of Publishing Site$")
public void give_Url_of_Excel_of_Publishing_Site() throws Throwable {
	POM PS = new POM();
	PS.ExcelUrlPS();
}

@Then("^select Sheet name of Publishing Site$")
public void select_Sheet_name_of_Publishing_Site() throws Throwable {
	POM PS = new POM();
	PS.SheetNmaePS();
}


@Then("^select Dynamic filter with \"([^\"]*)\"$")
public void select_Dynamic_filter_with(String arg1) throws Throwable {
    POM DynamicFilter = new POM();
    DynamicFilter.FirstDynamicFilter(arg1);
}

@Then("^select Dynamic filter the \"([^\"]*)\"$")
public void select_Dynamic_filter_the(String arg1) throws Throwable {
	POM DynamicFilter = new POM();
    DynamicFilter.SecondDynamicFilter(arg1);
}

@Then("^search with Dynamic Filter \"([^\"]*)\"$")
public void search_with_Dynamic_Filter(String arg1) throws Throwable {
	POM DynamicFilter = new POM();
    DynamicFilter.DynamicFilterSearch(arg1);
}
/////////////////////////////////////////////////////////////////////////////////////////

///////////////////ODBC database connection//////////////
@Then("^Select ODBC Database as DataSource$")
public void select_ODBC_Database_as_DataSource() throws Throwable {
	POM ODBC = new POM();
	ODBC.ODBC_data_provider();
}

@Then("^Give Connection String$")
public void give_Connection_String() throws Throwable {
	POM ODBC = new POM();
	ODBC.ODBC_ConnectionString();
}

@Then("^Give Query$")
public void give_Query() throws Throwable {
	POM ODBC = new POM();
	ODBC.ODBC_Query();
}

////////SingleSeriesPie2DChart//////////////////

@Then("^Choose SingleSeriesPieTwoDChart$")
public void choose_SingleSeriesPieTwoDChart() throws Throwable {
  POM ChartType = new POM();
  ChartType.SingleSeriesPie2DChart();
}

@Then("^Choose SingleSeriesPieThreeDChart$")
public void choose_SingleSeriesPieThreeDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.SingleSeriesPie3DChart();
}

@Then("^Choose SingleSeriesDoughnutTwoDChart$")
public void choose_SingleSeriesDoughnutTwoDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.SingleSeriesDoughnut2DChart();
}

@Then("^Choose SingleSeriesFunnelChart$")
public void choose_SingleSeriesFunnelChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.SingleSeriesFunnelChart();
}



@Then("^Choose SingleSeriesWaterfallTwoDChart$")
public void choose_SingleSeriesWaterfallTwoDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.SingleSeriesWaterfall2DChart();
}

@Then("^Choose SingleSeriesParetoThreeDChart$")
public void choose_SingleSeriesParetoThreeDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.SingleSeriesPareto3DChart();
}

@Then("^Choose SingleSeriesKagiChart$")
public void choose_SingleSeriesKagiChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.SingleSeriesKagiChart();
}

@Then("^Choose MultiSeriesLineTwoDChart$")
public void choose_MultiSeriesLineTwoDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesLine2DChart();
}

@Then("^Choose SingleSeriesMSAreaChart$")
public void choose_SingleSeriesMSAreaChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.SingleSeriesMSAreaChart();
}

@Then("^Choose MultiSeriesBarTwoDChart$")
public void choose_MultiSeriesBarTwoDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesBar2DChart();
}

@Then("^Choose MultiSeriesBarThreeDChart$")
public void choose_MultiSeriesBarThreeDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesBar3DChart();
}

@Then("^Choose MultiSeriesSplineChart$")
public void choose_MultiSeriesSplineChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesSplineChart();
}

@Then("^Choose MultiSeriesSplineAreaChart$")
public void choose_MultiSeriesSplineAreaChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesSplineAreaChart();
}

@Then("^Choose MultiSeriesLogarithmlineChart$")
public void choose_MultiSeriesLogarithmlineChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesLogarithmlineChart();
}

@Then("^Choose MultiSeriesLogarithmicColumnChart$")
public void choose_MultiSeriesLogarithmicColumnChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesLogarithmicColumnChart();
}

@Then("^Choose MultiSeriesRadarChart$")
public void choose_MultiSeriesRadarChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesRadarChart();
}

@Then("^Choose MultiSeriesInverseAreaChart$")
public void choose_MultiSeriesInverseAreaChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesInverseAreaChart();
}

@Then("^choose MultiSeriesInverseTwoDColumnChart$")
public void choose_MultiSeriesInverseTwoDColumnChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesInverse2DColumnChart();
}

@Then("^choose MultiSeriesInverseTwoDLineChart$")
public void choose_MultiSeriesInverseTwoDLineChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesInverse2DLineChart();
}

@Then("^choose MultiSeriesMarimekkoChart$")
public void choose_MultiSeriesMarimekkoChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesMarimekkoChart();
}

@Then("^choose MultiSeriesZoomLineChart$")
public void choose_MultiSeriesZoomLineChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesZoomLineChart();
}

@Then("^choose MultiSeriesStepLineChart$")
public void choose_MultiSeriesStepLineChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MultiSeriesStepLineChart();
}

@Then("^choose StackedColumnTwoDChart$")
public void choose_StackedColumnTwoDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.StackedColumn2DChart();
}

@Then("^choose StackedColumnThreeDChart$")
public void choose_StackedColumnThreeDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.StackedColumn3DChart();
}

@Then("^choose StackedAreaTwoDChart$")
public void choose_StackedAreaTwoDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.StackedArea2DChart();
}

@Then("^choose StackedBarTwoDChart$")
public void choose_StackedBarTwoDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.StackedBar2DChart();
	
}

@Then("^choose StackedBarThreeDChart$")
public void choose_StackedBarThreeDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.StackedBar3DChart();
}

@Then("^choose TwoDSingleYCombinationChart$")
public void choose_TwoDSingleYCombinationChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.TwoDSingleYCombinationChart();
}

@Then("^choose ThreeDSingleYCombinationChart$")
public void choose_ThreeDSingleYCombinationChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.ThreeDSingleYCombinationChart();
}
/*
@Then("^choose MSColumnLine(\\d+)DChart$")
public void choose_MSColumnLine_DChart(int arg1) throws Throwable {
	POM ChartType = new POM();
	ChartType.M
}*/

@Then("^choose ColumnThreeDLineDYChart$")
public void choose_ColumnThreeDLineDYChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.Column3DLineDYChart();
}

/*@Then("^choose StackedColumn(\\d+)DLineDualY$")
public void choose_StackedColumn_DLineDualY(int arg1) throws Throwable {
	POM ChartType = new POM();
	ChartType.
}*/

@Then("^choose TwoDDualYCombination$")
public void choose_TwoDDualYCombination() throws Throwable {
	POM ChartType = new POM();
	ChartType.TwoDDualYCombination();
}

@Then("^choose TwoDStackedColumnlineDualY$")
public void choose_TwoDStackedColumnlineDualY() throws Throwable {
	POM ChartType = new POM();
	ChartType.TwoDStackedColumnlineDualY();
}

@Then("^choose TwoDStackedColumnSingleY$")
public void choose_TwoDStackedColumnSingleY() throws Throwable {
	POM ChartType = new POM();
	ChartType.TwoDStackedColumnSingleY();
}

@Then("^choose ThreeDStackedColumnLineSingleY$")
public void choose_ThreeDStackedColumnLineSingleY() throws Throwable {
	POM ChartType = new POM();
	ChartType.ThreeDStackedColumnLineSingleY();
}

@Then("^choose ScatterXYPlot$")
public void choose_ScatterXYPlot() throws Throwable {
	POM ChartType = new POM();
	ChartType.ScatterXYPlot();
}

@Then("^choose Bubble$")
public void choose_Bubble() throws Throwable {
	POM ChartType = new POM();
	ChartType.Bubble();
}

@Then("^choose ScrollColumnTwoD$")
public void choose_ScrollColumnTwoD() throws Throwable {
	POM ChartType = new POM();
	ChartType.ScrollColumn2D();
}

@Then("^choose ScrollLineTwoD$")
public void choose_ScrollLineTwoD() throws Throwable {
	POM ChartType = new POM();
	ChartType.ScrollLine2D();
}

@Then("^choose ScrollAreaTwoD$")
public void choose_ScrollAreaTwoD() throws Throwable {
	POM ChartType = new POM();
	ChartType.ScrollArea2D();
}

@Then("^choose ScrollStackedColumnTwoD$")
public void choose_ScrollStackedColumnTwoD() throws Throwable {
	POM ChartType = new POM();
	ChartType.ScrollStackedColumn2D();
}

@Then("^choose ScrollCombiTwoD$")
public void choose_ScrollCombiTwoD() throws Throwable {
	POM ChartType = new POM();
	ChartType.ScrollCombi2D();
}

@Then("^choose ScrollCombiDYTwoD$")
public void choose_ScrollCombiDYTwoD() throws Throwable {
	POM ChartType = new POM();
	ChartType.ScrollCombiDY2D();
}
@Then("^choose MSColumnLineThreeDChart$")
public void choose_MSColumnLineThreeDChart() throws Throwable {
	POM ChartType = new POM();
	ChartType.MSColumnLine3DChart();
	
}

@Then("^choose StackedColumnThreeDLineDualY$")
public void choose_StackedColumnThreeDLineDualY() throws Throwable {
	POM ChartType = new POM();
	ChartType.StackedColumn3DLineDualY();
}
@Given("^Choose Multi Series from Chart Category Dropdown$")
public void choose_Multi_Series_from_Chart_Category_Dropdown() throws Throwable {
	POM ChartType = new POM();
	ChartType.ChartcategoryMultiSeriesChoose();

}

@Given("^Choose Stacked Chart from Chart Category Dropdown$")
public void choose_Stacked_Chart_from_Chart_Category_Dropdown() throws Throwable {
	POM ChartType = new POM();
	ChartType.ChartcategoryStackedChartChoose();

}

@Given("^Choose Combination Charts from Chart Category Dropdown$")
public void choose_Combination_Charts_from_Chart_Category_Dropdown() throws Throwable {
	POM ChartType = new POM();
	ChartType.ChartcategoryCombinationChartChoose();

}

@Given("^Choose X-Y charts from Chart Category Dropdown$")
public void choose_X_Y_charts_from_Chart_Category_Dropdown() throws Throwable {
	POM ChartType = new POM();
	ChartType.ChartcategoryXYChartChoose();

}

@Given("^Choose Scroll charts from Chart Category Dropdown$")
public void choose_Scroll_charts_from_Chart_Category_Dropdown() throws Throwable {
	POM ChartType = new POM();
	ChartType.ChartcategoryScrollChartChoose();
}

}